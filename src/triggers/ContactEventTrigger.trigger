trigger ContactEventTrigger on Contact (after insert, after update) {

    Integer runsize = Trigger.new.size();

    // Set the context for the runtime to triggered
    ddcLib1.Runtime.IS_TRIGGERED_CONTEXT = true;
    // This is inherited from system
    // Trigger.isExecuting() = true
    
    // Determine if trigger can run
    MatchScenario defms = MatchScenario.getDefault('Contact', new ApexPages.Message[0]);   // Should check for non-null messages!

    Boolean triggerable = false;
    Boolean automateable = false;
    if (defms != null) {
        
        defms = MatchScenario.getScenarioDefinition(defms.scenarioId, new ApexPages.Message[0]);    // Should check for non-null messages!
        
        if (defms.isActive) {
            
            triggerable = defms.thisSetting.contactTriggerEnabled;
            automateable = defms.thisSetting.contactAutomationEnabled;
        }
    }

    if (triggerable && (Test.isRunningTest() || runsize < 8)) {

        Boolean hasRunBefore = false;
        if (Trigger.isBefore && !Trigger.isInsert) {
            hasRunBefore = ddcLib1.Runtime.getBeforeRunStatus(Trigger.newMap.keySet());
        }
        
        Boolean hasRunAfter = false;
        if (Trigger.isAfter && Trigger.newMap.keySet().size() > 0) {
            hasRunAfter = ddcLib1.Runtime.getAfterRunStatus(Trigger.newMap.keySet());
        }
    
        if (Trigger.isBefore && !hasRunBefore && (!ddcLib1.Runtime.IS_FUTURE_CONTEXT && !ddcLib1.Runtime.IS_BATCH_CONTEXT && !System.isFuture() && !System.isBatch())) {
            ddcLib1.Runtime.HOST_IS_BEFORE = true;
            if (Trigger.isInsert) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_INSERT = true;
                // impl
            }
            else if (Trigger.isUpdate) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_UPDATE = true;
                // impl
            }
        }
        else if (Trigger.isAfter && !hasRunAfter && (!ddcLib1.Runtime.IS_FUTURE_CONTEXT && !ddcLib1.Runtime.IS_BATCH_CONTEXT && !System.isFuture() && !System.isBatch())) {
            if (Trigger.isInsert) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_INSERT = true;
                for (Contact c : Trigger.new) {
                    automateable = MatchScenario.getScenarioForWrapper(new SObjectWrapper(c, DDCType.CONTACT)).thisSetting.contactAutomationEnabled;
                    if (!c.ddcDet__Automation_Disabled__c && automateable) {
                        RuntimeMediator.asynchMatchTriggered(Trigger.newMap.keySet(), 'Contact');
                    }
                }
            }
            else if (Trigger.isUpdate) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_UPDATE = true;
                for (Contact c : Trigger.new) {
                    automateable = MatchScenario.getScenarioForWrapper(new SObjectWrapper(c, DDCType.CONTACT)).thisSetting.contactAutomationEnabled;
                    if (!c.ddcDet__Automation_Disabled__c && automateable) {
                        RuntimeMediator.asynchMatchTriggered(Trigger.newMap.keySet(), 'Contact');
                    }
                }
            }
        }
    }
    else if(triggerable) {

        Integer numJobs = 0;
        String qstring = 'SELECT Id FROM Contact';
        Set<Id> idSet = new Set<Id>();
        
        for (Contact c : Trigger.new) {
            automateable = MatchScenario.getScenarioForWrapper(new SObjectWrapper(c, DDCType.CONTACT)).thisSetting.contactAutomationEnabled;
            if (!c.ddcDet__Automation_Disabled__c && automateable) {
                idSet.add(c.Id);
            }
        }
    
        BatchableCleanJob job = new BatchableCleanJob(qstring, idSet);

        List<AsyncApexJob> qresult = [SELECT Id, JobType, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedDate, CompletedDate, ParentJobId, CreatedById, CreatedBy.Name
            FROM AsyncApexJob WHERE JobType IN('BatchApex','BatchApexWorker') AND Status NOT IN('Aborted','Completed','Failed') ORDER BY CreatedDate DESC LIMIT 5 ];
        
        numJobs = qresult.size();
        
        while (numJobs < 5) {
            // Fire new job
            Database.executeBatch(job, 3);
            numJobs++;
        }
    }
    else {
        System.debug(LoggingLevel.INFO,'ddcDet_ContactEventTrigger is currently disabled!');
    }
}