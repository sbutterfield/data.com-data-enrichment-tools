trigger AccountEventTrigger on Account (after insert, after update) {

    Integer runsize = Trigger.new.size();
    
    // Determine if trigger can run
    MatchScenario defms = MatchScenario.getDefault('Account', new ApexPages.Message[0]);   // Should check for non-null messages!
    
    Boolean triggerable = false;
    Boolean automateable = false;
    if(defms != null) {
        
        defms = MatchScenario.getScenarioDefinition(defms.scenarioId, new ApexPages.Message[0]);    // Should check for non-null messages!
        
        if (defms.isActive) {
            
            triggerable = defms.thisSetting.accountTriggerEnabled;
            automateable = defms.thisSetting.accountAutomationEnabled;
        }
    }

    if(triggerable && (Test.isRunningTest() || runsize < 10)) {

        Boolean hasRunBefore = false;
        if(Trigger.isBefore && !Trigger.isInsert) {
            hasRunBefore = ddcLib1.Runtime.getBeforeRunStatus(Trigger.newMap.keySet());
        }
        
        Boolean hasRunAfter = false;
        if(Trigger.isAfter && Trigger.newMap.keySet().size() > 0) {
            hasRunAfter = ddcLib1.Runtime.getAfterRunStatus(Trigger.newMap.keySet());
        }
    
        if(Trigger.isBefore && !hasRunBefore && (!ddcLib1.Runtime.IS_FUTURE_CONTEXT && !ddcLib1.Runtime.IS_BATCH_CONTEXT)) {
            ddcLib1.Runtime.HOST_IS_BEFORE = true;
            if(Trigger.isInsert) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_INSERT = true;
                // impl
            }
            else if(Trigger.isUpdate) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_UPDATE = true;
                // impl
            }
        }
        else if(Trigger.isAfter && !hasRunAfter && (!ddcLib1.Runtime.IS_FUTURE_CONTEXT && !ddcLib1.Runtime.IS_BATCH_CONTEXT)) {
            if(Trigger.isInsert) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_INSERT = true;
                for(Account a : Trigger.new) {
                    automateable = MatchScenario.getScenarioForWrapper(new SObjectWrapper(a, DDCType.COMPANY)).thisSetting.accountAutomationEnabled;
                    if(!a.ddcDet__Automation_Disabled__c && automateable) {
                        RuntimeMediator.asynchMatchTriggered(Trigger.newMap.keySet(), 'Account');
                    }
                }
            }
            else if(Trigger.isUpdate) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_UPDATE = true;
                for(Account a : Trigger.new) {
                    automateable = MatchScenario.getScenarioForWrapper(new SObjectWrapper(a, DDCType.COMPANY)).thisSetting.accountAutomationEnabled;
                    if(!a.ddcDet__Automation_Disabled__c && automateable) {
                        RuntimeMediator.asynchMatchTriggered(Trigger.newMap.keySet(), 'Account');
                    }
                }
            }
        }
    }
    else if(triggerable && automateable) {

        String qstring = 'SELECT Id FROM Account WHERE Id IN ' + Trigger.newMap.keySet();
        BatchableCleanJob job = new BatchableCleanJob(qstring);
        Database.executeBatch(job, 5);
    }
    else {
        System.debug(LoggingLevel.INFO,'ddcDet_AccountEventTrigger is currently disabled!');
    }
}