trigger LeadEventTrigger on Lead (after insert, after update) {

    Integer runsize = Trigger.new.size();

    // Set the context for the runtime to triggered
    ddcLib1.Runtime.IS_TRIGGERED_CONTEXT = true;
    // This is inherited from system
    // Trigger.isExecuting() = true
    
    // Determine if trigger can run
    MatchScenario defms = MatchScenario.getDefault('Lead', new ApexPages.Message[0]);   // Should check for non-null messages!

    Boolean triggerable = false;
    Boolean automateable = false;
    if (defms != null) {
        
        defms = MatchScenario.getScenarioDefinition(defms.scenarioId, new ApexPages.Message[0]);    // Should check for non-null messages!
        
        if (defms.isActive) {
            
            triggerable = defms.thisSetting.leadTriggerEnabled;
            automateable = defms.thisSetting.leadAutomationEnabled;
        }
    }

    if (triggerable && (Test.isRunningTest() || runsize < 8)) {

        Boolean hasRunBefore = false;
        if (Trigger.isBefore && !Trigger.isInsert) {
            hasRunBefore = ddcLib1.Runtime.getBeforeRunStatus(Trigger.newMap.keySet());
        }
        
        Boolean hasRunAfter = false;
        if (Trigger.isAfter && Trigger.newMap.keySet().size() > 0) {
            hasRunAfter = ddcLib1.Runtime.getAfterRunStatus(Trigger.newMap.keySet());
        }
    
        if (Trigger.isBefore && !hasRunBefore && (!ddcLib1.Runtime.IS_FUTURE_CONTEXT && !ddcLib1.Runtime.IS_BATCH_CONTEXT && !System.isFuture() && !System.isBatch())) {
            ddcLib1.Runtime.HOST_IS_BEFORE = true;
            if (Trigger.isInsert) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_INSERT = true;
                // impl
            }
            else if (Trigger.isUpdate) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_UPDATE = true;
                // impl
            }
        }
        else if (Trigger.isAfter && !hasRunAfter && (!ddcLib1.Runtime.IS_FUTURE_CONTEXT && !ddcLib1.Runtime.IS_BATCH_CONTEXT && !System.isFuture() && !System.isBatch())) {
            if (Trigger.isInsert) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_INSERT = true;
                for (Lead l : Trigger.new) {
                    ddcDet.MatchScenario thisScenario = MatchScenario.getScenarioForWrapper(new SObjectWrapper(l, DDCType.CONTACT));
                    automateable = thisScenario == null ? false : thisScenario.thisSetting.leadAutomationEnabled;
                    if (!l.ddcDet__Automation_Disabled__c && automateable) {
                        RuntimeMediator.asynchMatchTriggered(Trigger.newMap.keySet(), 'Lead');
                    }
                }
            }
            else if (Trigger.isUpdate) {
                ddcLib1.Runtime.ORIG_CONTEXT_WAS_UPDATE = true;
                for (Lead l : Trigger.new) {
                    ddcDet.MatchScenario thisScenario = MatchScenario.getScenarioForWrapper(new SObjectWrapper(l, DDCType.CONTACT));
                    automateable = thisScenario == null ? false : thisScenario.thisSetting.leadAutomationEnabled;
                    if (!l.ddcDet__Automation_Disabled__c && automateable) {
                        RuntimeMediator.asynchMatchTriggered(Trigger.newMap.keySet(), 'Lead');
                    }
                }
            }
        }
    }
    else if(triggerable) {

        Integer numJobs = 0;
        String qstring = 'SELECT Id FROM Lead';
        Set<Id> idSet = new Set<Id>();
        
        for (Lead l : Trigger.new) {
            ddcDet.MatchScenario thisScenario = MatchScenario.getScenarioForWrapper(new SObjectWrapper(l, DDCType.CONTACT));
            automateable = thisScenario == null ? false : thisScenario.thisSetting.leadAutomationEnabled;
            if (!l.ddcDet__Automation_Disabled__c && automateable) {
                idSet.add(l.Id);
            }
        }
    
        BatchableCleanJob job = new BatchableCleanJob(qstring, idSet);

        List<AsyncApexJob> qresult = [SELECT Id, JobType, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedDate, CompletedDate, ParentJobId, CreatedById, CreatedBy.Name
            FROM AsyncApexJob WHERE JobType IN('BatchApex','BatchApexWorker') AND Status NOT IN('Aborted','Completed','Failed') ORDER BY CreatedDate DESC LIMIT 5 ];
        
        numJobs = qresult.size();
        
        while (numJobs < 5) {
            // Fire new job
            Database.executeBatch(job, 3);
            numJobs++;
        }
    }
    else {
        System.debug(LoggingLevel.INFO,'ddcDet_LeadEventTrigger is currently disabled!');
    }
}