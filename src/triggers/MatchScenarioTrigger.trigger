trigger MatchScenarioTrigger on ddcDet__Match_Scenario__c (after insert, after update, before delete) {
	
	Set<SObject> toUpdate = new Set<SObject>();
	Set<Id> idSet = new Set<Id>();
	Boolean hasRunAfter = false;
	Boolean hasRunBefore = false;
	
	String singleQuote = '\'';
	
	if(Trigger.isafter) {
		
		RuntimeState.IS_TRIGGERED_CONTEXT = true;
		RuntimeState.HOST_IS_AFTER = true;
		hasRunAfter = RuntimeState.getAfterRunStatus(Trigger.newMap.keySet());
		
		if(Trigger.isInsert) {
			RuntimeState.ORIG_CONTEXT_WAS_INSERT = true;
		}
		
		if(Trigger.isUpdate) {
			RuntimeState.ORIG_CONTEXT_WAS_UPDATE = true;
		}
		
		if(Trigger.isInsert || Trigger.isUpdate && !hasRunAfter) {
		
			// New Match_Scenario__c being created
			for(Match_Scenario__c m : Trigger.new) {
				
				// Ensure that new scenario does not collide with existing
				String soql;
				// If this is an insert, and a default scenario
				if(Trigger.isInsert && m.Default__c == true) {
					
					soql = 'SELECT Id, Run_Order__c FROM Match_Scenario__c WHERE Default__c = true AND Object_Name__c = ' + singleQuote + m.Object_Name__c + singleQuote + ' AND Id != ' + singleQuote + m.Id + singleQuote;
					SObject[] qrs = Database.query(soql);
					System.debug(qrs);
					if(qrs != null && qrs.size() > 0) {
						// Do not allow more than one default scenario at a time
						m.addError('A default scenario is already defined, you cannot define another one. Modify the existing default scenario on the configuration panel.');
					}
				}
				
				if(m.Run_Order__c == null) {
		
					m.addError('A run order must be specified!');
				}
				else {
		
					// Re-shuffle existing scenarios Run_Order__c so that the new scenario can be saved.
					// Existing scenarios with the same or higher run order will be incremented +1
					soql = 'SELECT Id, Run_Order__c FROM Match_Scenario__c WHERE Object_Name__c = ' + singleQuote + m.Object_Name__c + singleQuote;
					SObject[] result = Database.query(soql);
					if(result != null) {
						Integer i = 0;
						for(SObject s : result) {
							// Run_Order of existing scenario
							Integer j = Integer.valueOf(s.get('ddcDet__Run_Order__c'));
							if( (j >= m.Run_Order__c && j < 99) && (m.Id != Id.valueOf((String)s.get('Id'))) ) {
								// Set temporary var to the same run order number as existing in iteration.
								i = j;
								// Add 1 to it
								i++;
								// Set it for update later.
								s.put('Run_Order__c',i);
								toUpdate.add(s);
								// Add the id to the Continuum
								idSet.add(Id.valueOf((String)s.get('Id')));
							}
						}
					}
				}
				
				if(RuntimeState.ORIG_CONTEXT_WAS_INSERT) {
					// Create new RuntimeSettings__c
					RuntimeSettings__c rs = new RuntimeSettings__c();
					rs.Name = m.Id;
					rs.Scenario_Id__c = m.Id;
					insert rs;
				}
			}
			
			// Set the new runtime state
			RuntimeState.getAfterRunStatus(idSet);
			RuntimeState.getBeforeRunStatus(idSet);
			
			SObject[] l = new SObject[]{};
			l.addAll(toUpdate);
			// Do updating of other scenarios
			Database.update(l);
		}
	}
	else if(Trigger.isBefore) {

		if(Trigger.isDelete) {

			// Existing Match_Scenario__c being deleted
			/**
			 * To-Do:
			 *	1)Ensure that the scenario being deleted is NOT the deafult scenario. If so, display an error.
			 *	2)Automatically increment the run order on other match scenarios for that sobject
			 */
		}
	}

}