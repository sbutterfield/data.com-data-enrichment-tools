trigger MockProbableAccount on Lead (before insert, before update) {

    for (Lead l : Trigger.new) {
        if (l.Data_com_Company_Id__c != null) {
            Account a = [SELECT Id FROM Account WHERE Jigsaw = :l.Data_com_Company_Id__c LIMIT 1];
            l.Probable_Account__c = a.Id;
        }
    }

}