/**
 * @author: Shawn Butterfield, Salesforce.com Inc
 * Location object returned by the Matcher API.
 */
public with sharing class MatcherLocation extends MatcherObject {
    
    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    public MatcherField locationId {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_LOCATION_ID); }
        set { mMap.put(MatcherAPI.TAG_NAME_LOCATION_ID, value); }
    }
    
    public MatcherField locationPhone {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_LOCATION_PHONE); }
        set { mMap.put(MatcherAPI.TAG_NAME_LOCATION_PHONE, value); }
    }
    
    public MatcherAddress address {
    	get { return (MatcherAddress) mMap.get(MatcherAPI.TAG_NAME_ADDRESS); }
    	set { mMap.put(MatcherAPI.TAG_NAME_ADDRESS, value); }
    }
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public MatcherLocation() {
    	super(MatcherAPI.TAG_NAME_LOCATION, MatcherAPI.SEQUENCE_LOCATION);
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * This will parse the given location node returned by the Matcher API.
     */
    public static MatcherLocation parse(Dom.XmlNode locationNode) {
        if (locationNode == null || !MatcherAPI.TAG_NAME_LOCATION.equals(locationNode.getName())) {
            return null;
        }
        
        // NOTE: A switch statement would be great here but Apex doesn't have one!
        MatcherLocation location = new MatcherLocation();
        Dom.XmlNode node;
        
        node = locationNode.getChildElement(MatcherAPI.TAG_NAME_LOCATION_ID, null);
        if (node != null) {
            try {
                location.locationId = new MatcherField(Long.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = locationNode.getChildElement(MatcherAPI.TAG_NAME_LOCATION_PHONE, null);
        if (node != null) {
            location.locationPhone = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = locationNode.getChildElement(MatcherAPI.TAG_NAME_ADDRESS, null);
        if (node != null) {
            location.address = MatcherAddress.parse(node);
        }
        
        return location;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testToXml() {
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.locationId = new MatcherField(1234L, true);
        location.locationPhone = new MatcherField('+1.650.235.8400', false);
        location.address = address;
        
        String expectedXml =
            '<' + MatcherAPI.TAG_NAME_LOCATION + '>' +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_LOCATION_ID, location.locationId) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_LOCATION_PHONE, location.locationPhone) +
                location.address.toXml() +
            '</' + MatcherAPI.TAG_NAME_LOCATION + '>';
        
        String actualXml = location.toXml();
        System.assertEquals(expectedXml, actualXml);
    }
    
    private static testmethod void testParse() {
        // Null
        System.assertEquals(null, MatcherLocation.parse(null));
        
        // Non-Location Node
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode root = doc.createRootElement('NotLocation', null, null);
        System.assertEquals(null, MatcherLocation.parse(root));
        
        // NOTE: The easiest way to test for equality is to compare XML strings because there are
        // no equals methods readily available for all objects other than String.
        
        // Location
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.locationId = new MatcherField(1234L, true);
        location.locationPhone = new MatcherField('+1.650.235.8400', false);
        location.address = address;
        String expectedXml = location.toXml();
        
        doc = new Dom.Document();
        doc.load(expectedXml);
        MatcherLocation actualLocation = MatcherLocation.parse(doc.getRootElement());
        System.assertNotEquals(null, actualLocation);
        System.assertEquals(expectedXml, actualLocation.toXml());
        
        // Invalid locationId
        location.locationId = null;
        expectedXml = location.toXml();
        
        Dom.XmlNode locationNode = doc.getRootElement();
        Dom.XmlNode locationIdNode = locationNode.getChildElement(MatcherAPI.TAG_NAME_LOCATION_ID, null);
        locationIdNode.addTextNode('NotNumber');
        actualLocation = MatcherLocation.parse(doc.getRootElement());
        System.assertNotEquals(null, actualLocation);
        System.assertEquals(expectedXml, actualLocation.toXml());
    }
    
}