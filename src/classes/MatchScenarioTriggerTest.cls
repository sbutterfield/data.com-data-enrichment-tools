/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class MatchScenarioTriggerTest {

	// Test generic new scenario
    static testMethod void testNewScenario() {
        Match_Scenario__c m = new Match_Scenario__c(
        	Name = 'New Generic Scenario',
        	Run_Order__c = 3,
        	Object_Name__c = 'Lead',
        	Default__c = false
        	
        );
        Test.startTest();
        insert m;
        Test.stopTest();
        
        // Assert it was created correctly.
        System.assert(m.Id != null);
    }
    
    static testMethod void testNewDefaultScenario() {
    	Boolean threwexception;
    	Match_Scenario__c m = new Match_Scenario__c(
        	Name = 'Default Scenario',
        	Run_Order__c = 3,
        	Object_Name__c = 'Lead',
        	Default__c = true
        	
        );
        Match_Scenario__c m2 = new Match_Scenario__c(
        	Name = 'Generic Scenario',
        	Run_Order__c = 3,
        	Object_Name__c = 'Lead',
        	Default__c = true
        	
        );
        Test.startTest();
        
        try {
        	// first insert successful
        	insert m;
        	// second insert throws exception
        	insert m2;
        }
        catch(Exception e) {
        	threwexception = true;
        }
        
        Test.stopTest();
        
        // Assert an exception was thrown. We shouldn't be able to create new Default Scenarios
        System.Assert(threwexception);
    }
    
    static testMethod void testScenarioUpdate() {
    	// TO DO: implement unit test
    }
    
    static testMethod void testReshuffling() {
    	// TO-DO: Implement a test to get the scenarios to re-shuffle
    	// Assert that they do it correctly
    }
}