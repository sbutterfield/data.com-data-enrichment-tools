public abstract class MatchedObject implements Matchable {
    
    public abstract SObject[] match();
    public abstract Descriptor getDescribe();
}