public class MatchLibraryFactory {

	private static Map<String, List<ddcDet__Referential_Match_Libraries__c>> CACHE_REF_LIBS_TO_TYPE;
	private static Map<String, List<ddcDet__Transformative_Match_Libraries__c>> CACHE_TRANS_LIBS_TO_TYPE;
	private static Map<Id, ddcDet__Match_Library__c> CACHE_MATCH_LIBRARIES;
	private static Map<Id, List<ddcDet__Library_Value__c>> CACHE_LIB_ID_TO_VALUE;

	//private static ddcDet__Referential_Match_Libraries__c[] CACHE_REF_LIBS;
	//private static ddcDet__Transformative_Match_Libraries__c[] CACHE_TRANS_LIBS;

	public static List<ddcDet.MatchLibrary> getMatchLibraries( Boolean isReferential, String sObjectType ) {
		List<ddcDet.MatchLibrary> libraries = new List<ddcDet.MatchLibrary>();
		if( isReferential ) {
			if( CACHE_REF_LIBS_TO_TYPE == null ) {
				initReferentialCache();
			}
			List<ddcDet__Referential_Match_Libraries__c> rmlSettings = CACHE_REF_LIBS_TO_TYPE.get( sObjectType.toUpperCase() );
			for( ddcDet__Referential_Match_Libraries__c rmlSetting : rmlSettings ) {
				System.debug( 'RML SETTING: ' + String.valueOf( rmlSetting ) );
				String matchLibraryId = rmlSetting.Match_Library_Id__c;
				ddcDet__Match_Library__c lib = getMatchLibrary( matchLibraryId );
				System.debug( 'Library: ' + String.valueOf( lib ) );

				List<ddcDet__Library_Value__c> libValues = getLibraryValues( lib.Id );
				System.debug( 'Library Values: ' + String.valueOf( libValues ) );
			}
		} else {
			if( CACHE_TRANS_LIBS_TO_TYPE == null ) {
				initTransformativeCache();
			}
			List<ddcDet__Transformative_Match_Libraries__c> tmlSettings = CACHE_TRANS_LIBS_TO_TYPE.get( sObjectType.toUpperCase() );
			for( ddcDet__Transformative_Match_Libraries__c tmlSetting : tmlSettings ) {
				System.debug( 'RML SETTING: ' + String.valueOf( tmlSetting ) );
				String matchLibraryId = tmlSetting.Match_Library_Id__c;
				ddcDet__Match_Library__c lib = getMatchLibrary( matchLibraryId );
				System.debug( 'Library: ' + String.valueOf( lib ) );

				List<ddcDet__Library_Value__c> libValues = getLibraryValues( lib.Id );
				System.debug( 'Library Values: ' + String.valueOf( libValues ) );
			}
		}
		return libraries;
	}

	private static void initReferentialCache() {
		List<ddcDet__Referential_Match_Libraries__c> allRefMatchLibs =  ddcDet__Referential_Match_Libraries__c.getAll().values();
		CACHE_REF_LIBS_TO_TYPE = new Map<String, List<ddcDet__Referential_Match_Libraries__c>>();
		for( ddcDet__Referential_Match_Libraries__c refMatchLib : allRefMatchLibs ) {
			String type = refMatchLib.ddcDet__SObject_Name__c.toUpperCase();
			List<ddcDet__Referential_Match_Libraries__c> typeMatchLibs = CACHE_REF_LIBS_TO_TYPE.get( type );
			if( typeMatchLibs == null ) {
				typeMatchLibs = new List<ddcDet__Referential_Match_Libraries__c>();
				CACHE_REF_LIBS_TO_TYPE.put( type, typeMatchLibs );
			}
			typeMatchLibs.add( refMatchLib );
		}
	}

	private static void initTransformativeCache() {
		List<ddcDet__Transformative_Match_Libraries__c> allTransMatchLibs =  ddcDet__Transformative_Match_Libraries__c.getAll().values();
		CACHE_TRANS_LIBS_TO_TYPE = new Map<String, List<ddcDet__Transformative_Match_Libraries__c>>();
		for( ddcDet__Transformative_Match_Libraries__c transMatchLib : allTransMatchLibs ) {
			String type = transMatchLib.ddcDet__SObject_Name__c.toUpperCase();
			List<ddcDet__Transformative_Match_Libraries__c> typeMatchLibs = CACHE_TRANS_LIBS_TO_TYPE.get( type );
			if( typeMatchLibs == null ) {
				typeMatchLibs = new List<ddcDet__Transformative_Match_Libraries__c>();
				CACHE_TRANS_LIBS_TO_TYPE.put( type, typeMatchLibs );
			}
			typeMatchLibs.add( transMatchLib );
		}
	}

	private static ddcDet__Match_Library__c getMatchLibrary( String matchLibraryId ) {
		ddcDet__Match_Library__c matchLibrary = null;
		try {
			Id mlID = Id.valueOf( matchLibraryId );
			if( mlID != null ) {
				if( CACHE_MATCH_LIBRARIES == null ) {
					CACHE_MATCH_LIBRARIES = new Map<Id, ddcDet__Match_Library__c>( [
						SELECT
							Id, Name, Type__c
						FROM
							ddcDet__Match_Library__c
					] );
				}
				matchLibrary = CACHE_MATCH_LIBRARIES.get( mlID );
			}
		} catch ( Exception e ) {
			// TODO Do Something...
		}
		return matchLibrary;
	}

	private static List<ddcDet__Library_Value__c> getLibraryValues( Id matchLibraryId ) {
		if( CACHE_LIB_ID_TO_VALUE == null ) {
			CACHE_LIB_ID_TO_VALUE = new Map<Id, List<ddcDet__Library_Value__c>>();
		}
		List<ddcDet__Library_Value__c> libValues = CACHE_LIB_ID_TO_VALUE.get( matchLibraryId );
		if( libValues == null ) {
			libValues = [
				SELECT
					Id, Name, Value__c, Equivalent_Value__c
				FROM
					ddcDet__Library_Value__c
				WHERE
					ddcDet__Match_Library__c = :matchLibraryId
			];
			CACHE_LIB_ID_TO_VALUE.put( matchLibraryId, libValues );
		}
		return libValues;
	}

	public static Boolean isTransCacheEmpty() {
		return CACHE_TRANS_LIBS_TO_TYPE.isEmpty();
	}

	public static Boolean isRefCacheEmpty() {
		return CACHE_REF_LIBS_TO_TYPE.isEmpty();
	}

}