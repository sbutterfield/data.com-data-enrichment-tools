global without sharing class BatchableCleanJob implements Database.Batchable<SObject>, 
    Database.AllowsCallouts, Database.Stateful {

    private final String query;
    private final Set<Id> idSet;

    global BatchableCleanJob(String q) {
    	this.query = q;
    }

    global BatchableCleanJob(String q, Set<Id> ids) {
        this.idSet = ids;

        if (this.idSet != null) {
            // Got the query from a trigger with a keyset to query against.
            this.query = q + ' WHERE Id IN :idSet';
        }
        else {
            this.query = q;
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<SObject> scope) {
    	
        // Inspect the type of sobjects we got
        Schema.SObjectType stype = scope[0].getSobjectType();

        // We need to re-query to make sure we got all of the fields we need
        Set<Id> ids = new Set<Id>();
        for (SObject s : scope) {
            ids.add(s.Id);
        }

        if (ids.size() < 1) {
            return;
        }

        String objectName = String.valueOf(stype);

        ddcDet.RuntimeMediator.executeDetRuntime(ids, objectName);
    }
    
    public void finish(Database.BatchableContext BC) {
    	// when the batch is done
    }

    /**
     *  Populates the look-up to D&B Company object for matched wrappers
     *  where relationship does not already exist and DunsNumber is not-null.
     *  This should be called after all matching has already been done.
     *  @param = Wrapper[] wrappers (a collection of any wrapper type)
     *  @return = void, operates directly on wrappers
     *  @throws = none
     */
    private static void setDandBCompany(Wrapper[] wrappers) {

        if (wrappers == null) {
            return;
        }

        // Mapping of dunsNumber --> Wrappers for that duns
        Map<String, Wrapper[]> dunsMapping = new Map<String, Wrapper[]>();

        // All matched wrappers, following logic will build dunsMapping
        for (Wrapper w : wrappers) {

            if (w.getDdcMatch() == null) {
                // No match was found for the wrapper. Skip it.
                continue;
            }

            DDCObject matchedObject = w.getDdcMatch();
            String dunsNumber = (String) matchedObject.objMap.get(ApiUtils.TAG_NAME_DUNS_NUMBER);

            if (dunsNumber == null) {
                // No duns to try and get D&B Company from database.
                continue;
            }
            
            Wrapper[] existing = dunsMapping.get(dunsNumber);
            
            if (existing == null) {
                
                dunsMapping.put(dunsNumber, new Wrapper[] {w});
            }
            else {
                
                existing.add(w);
                dunsMapping.put(dunsNumber, existing);
            }
        }

        if (dunsMapping == null) {
            // Nothing in the map means there are no D&B company assignments that are possible.
            return;
        }

        Set<String> keys = dunsMapping.keySet();
        String soql = 'SELECT Id,DunsNumber FROM DandBCompany WHERE DunsNumber IN :keys';
        List<SObject> dnbCompanies;
        try {
            dnbCompanies = Database.query(soql);
        }
        catch (Exception e) {
            // will happen if DandBCompany object is not available.
            System.debug('No DandBCompany object in this org!....');
        }

        if (dnbCompanies == null) {
            // No dnbcompanies came back from the query, nothing to try and do.
            return;
        }

        for (SObject s : dnbCompanies) {

            Wrapper[] relWrappers = dunsMapping.get((String) s.get('DunsNumber'));

            for (Wrapper w : relWrappers) {

                try{
                    w.getSObject().put('DandBCompanyId', s.Id);
                }
                catch (SObjectException se) {
                    System.debug('Error setting DandBCompanyId, field may not exist on this wrapper! :: ' +se.getMessage()+ '  ' +se.getStackTraceString());
                }
            }
        }
    }
}