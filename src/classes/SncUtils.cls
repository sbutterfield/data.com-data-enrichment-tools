public class SncUtils {

    private SncUtils() {
        // Private constructor does not allow instantiation.
    }

//  #########################################################################################
//  ~~~~~~~~~~~~~~~~~~~~~~~ OLD SNCUTILS - REMOVEME ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  #########################################################################################
    
    //---------------------------------------------------------------------------------------------
    // Constants
    //---------------------------------------------------------------------------------------------
    
    // Common Fields
    public static final String FIELD_ID = 'Id';
    public static final String FIELD_JIGSAW = 'Jigsaw';
    //public static final String FIELD_LAST_MODIFIED_DATE = 'LastModifiedDate';
    
    public static final Set<String> COMMON_FIELDS = new Set<String> {
        FIELD_ID,
        FIELD_JIGSAW

    };
    //        FIELD_LAST_MODIFIED_DATE
    
    public static final String COMMON_FIELD_LIST_STRING;
    static {
        String commonFields = '';
        for (String fieldName : COMMON_FIELDS) {
            commonFields +=
                ((commonFields.length() > 0) ? ',' : '') + fieldName;
        }
        COMMON_FIELD_LIST_STRING = commonFields;
    }
    
    // Shared Fields
    public static final String FIELD_FIRST_NAME = 'FirstName';
    public static final String FIELD_LAST_NAME = 'LastName';
    public static final String FIELD_TITLE = 'Title';
    public static final String FIELD_EMAIL = 'Email';
    public static final String FIELD_PHONE = 'Phone';
    public static final String FIELD_NUMBER_OF_EMPLOYEES = 'NumberOfEmployees';
    public static final String FIELD_ANNUAL_REVENUE = 'AnnualRevenue';
    public static final String FIELD_INDUSTRY = 'Industry';
    
    // Account Fields
    public static final String FIELD_NAME = 'Name';
    public static final String FIELD_TICKER_SYMBOL = 'TickerSymbol';
    public static final String FIELD_WEBSITE = 'Website';
    public static final String FIELD_OWNERSHIP = 'Ownership';
    public static final String FIELD_BILLING_STREET = 'BillingStreet';
    public static final String FIELD_BILLING_CITY = 'BillingCity';
    public static final String FIELD_BILLING_STATE = 'BillingState';
    public static final String FIELD_BILLING_COUNTRY = 'BillingCountry';
    public static final String FIELD_BILLING_POSTAL_CODE = 'BillingPostalCode';
    public static final String FIELD_FAX = 'Fax';
    public static final String FIELD_SITE = 'Site';
    public static final String FIELD_DUNS_NUMBER = 'DunsNumber';
    public static final String FIELD_TRADE_STYLE = 'Tradestyle';
    public static final String FIELD_YEAR_STARTED = 'YearStarted';
    public static final String FIELD_SIC = 'Sic';
    public static final String FIELD_SIC_DESC = 'SicDesc';
    public static final String FIELD_NAICS_CODE = 'NaicsCode';
    public static final String FIELD_NAICS_DESC = 'NaicsDesc';
    public static final String FIELD_DESCRIPTION = 'Description';
    
    // Contact Fields
    public static final String FIELD_ACCOUNT_ID = 'AccountId';
    public static final String FIELD_MAILING_STREET = 'MailingStreet';
    public static final String FIELD_MAILING_CITY = 'MailingCity';
    public static final String FIELD_MAILING_STATE = 'MailingState';
    public static final String FIELD_MAILING_COUNTRY = 'MailingCountry';
    public static final String FIELD_MAILING_POSTAL_CODE = 'MailingPostalCode';
    
    // Lead Fields
    public static final String FIELD_COMPANY = 'Company';
    public static final String FIELD_STREET = 'Street';
    public static final String FIELD_CITY = 'City';
    public static final String FIELD_STATE = 'State';
    public static final String FIELD_POSTAL_CODE = 'PostalCode';
    public static final String FIELD_COUNTRY = 'Country';
    public static final String FIELD_COMPANY_DUNS_NUMBER = 'CompanyDunsNumber';

// CITRIX FIELDS ===================================================
    public static final String FIELD_GLOBAL_ULTIMATE_NAME = 'Global_Ultimate_Business_Name__c';
    public static final String FIELD_GLOBAL_ULTIMATE_DUNS = 'GU_CompanyDunsNumber__c';
    public static final String FIELD_GLOBAL_ULTIMATE_EMPLOYEES = 'Global_Employees__c';
    public static final String FIELD_DOMESTIC_ULTIMATE_NAME = 'Domestic_Ultimate_Business_Name__c';
    public static final String FIELD_DOMESTIC_ULTIMATE_DUNS = 'Domestic_Ultimate_CompanyDunsNumber__c';
    public static final String FIELD_DOMESTIC_ULTIMATE_EMPLOYEES = 'Domestic_Ultimate_Employees__c';
    public static final String FIELD_PARENT_HQ_NAME = 'Parent_Company_Business_Name__c';
    public static final String FIELD_PARENT_HQ_DUNS = 'Parent_HQ_CompanyDunsNumber__c';
    public static final String FIELD_PARENT_HQ_EMPLOYEES = 'Parent_HQ_Employees__c';
// CITRIX ===================================================

// BAF
    public static final String FIELD_PRIMARY_NAICS = 'NAICS__c';

    
    public static final Set<String> NAME_FIELDS = new Set<String> {
        FIELD_FIRST_NAME,
        FIELD_LAST_NAME
    };

    public static final Map<String, String> ADDRESS_FIELDS_ACCOUNT = new Map<String, String> {
        FIELD_STREET => FIELD_BILLING_STREET,
        FIELD_CITY => FIELD_BILLING_CITY,
        FIELD_STATE => FIELD_BILLING_STATE,
        FIELD_POSTAL_CODE => FIELD_BILLING_POSTAL_CODE,
        FIELD_COUNTRY => FIELD_BILLING_COUNTRY
    };
    
    public static final Map<String, String> ADDRESS_FIELDS_CONTACT = new Map<String, String> {
        FIELD_STREET => FIELD_MAILING_STREET,
        FIELD_CITY => FIELD_MAILING_CITY,
        FIELD_STATE => FIELD_MAILING_STATE,
        FIELD_POSTAL_CODE => FIELD_MAILING_POSTAL_CODE,
        FIELD_COUNTRY => FIELD_MAILING_COUNTRY
    };
    
    public static final Map<String, String> ADDRESS_FIELDS_LEAD = new Map<String, String> {
        FIELD_STREET => FIELD_STREET,
        FIELD_CITY => FIELD_CITY,
        FIELD_STATE => FIELD_STATE,
        FIELD_POSTAL_CODE => FIELD_POSTAL_CODE,
        FIELD_COUNTRY => FIELD_COUNTRY
    };
    
    // NOTE: The field name should not clash with an existing field name, it is synthesized using
    // a combination of address fields. The strings shown below will not clash with any custom
    // fields because those have two underscores between the namespace and the field name as well
    // as a suffix of '__c'.
    public static final String MERGED_FIELD_NAME = 'ddcdet_MergedName';
    public static final String MERGED_FIELD_ADDRESS = 'ddcdet_MergedAddress';
    
    // See http://en.wikipedia.org/wiki/Data_Universal_Numbering_System for more information.
    public static final Integer DUNS_NUMBER_LENGTH = 9;
    public static final Integer DUNS_NUMBER_VISIBLE_OFFSET = 5;
    public static final String DUNS_NUMBER_OBFUSCATION = '*****';
    
    public static final String DEFAULT_INDUSTRY = 'Other';

    private Enum FieldMode { CRM, DDC }

    //---------------------------------------------------------------------------------------------
    // Public Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * Creates the container element for the Stare & Compare body.
     */    
    public static Component.Apex.PageBlockSection createContainer() {
        // VisualForce:
        // <apex:pageBlockSection columns="2">
        //     ... CRM Field ...
        //     ... DDC Field ...
        // </apex:pageBlockSection>
        
        Component.Apex.PageBlockSection container = new Component.Apex.PageBlockSection();
        container.columns = 2;
        
        return container;
    }

    /**
     * Creates the column displaying the CRM field value of the given fieldName.
     */
    public static Component.Apex.PageBlockSectionItem createCRMField(String recordType, String fieldName) {
        if (String.isEmpty(recordType) || String.isEmpty(fieldName)) {
            return null;
        }
        
        // We get better control of style using pageBlockSectionItem.
        // We need to wrap outputFields in a span to get more control of the style.
        
        // Standard Fields:
        // <apex:pageBlockSectionItem >
        //     <apex:outputLabel value="{!$ObjectType.<TYPE>.Fields.<FIELD>.Label}"/>
        //     <apex:outputPanel styleClass="dataEqual">
        //         <apex:outputField value="{!CRM.<FIELD>}"/>
        //     </apex:outputPanel>
        // </apex:pageBlockSectionItem>
        //
        // Address:
        // <apex:pageBlockSectionItem >
        //     <apex:outputLabel value="Billing Address"/>
        //     <apex:outputPanel styleClass="dataEqual">
        //         <apex:outputPanel layout="block"><apex:outputText value="{!CRMAddressLine1}"/></apex:outputPanel>
        //         <apex:outputPanel layout="block"><apex:outputText value="{!CRMAddressLine2}"/></apex:outputPanel>
        //         <apex:outputPanel layout="block"><apex:outputText value="{!CRMAddressLine3}"/></apex:outputPanel>
        //     </apex:outputPanel>
        // </apex:pageBlockSectionItem>
        
        Component.Apex.PageBlockSectionItem crmItem = new Component.Apex.PageBlockSectionItem();
        
        // Field Label
        Component.Apex.OutputLabel crmLabel = new Component.Apex.OutputLabel();
        if (fieldName.equals(MERGED_FIELD_NAME)) {
            crmLabel.expressions.value = '{!$ObjectType.' + recordType + '.Fields.Name.Label}';
        }
        else if (fieldName.equals(MERGED_FIELD_ADDRESS)) {
            crmLabel.expressions.value = '{!AddressLabel}';
        }
        else {
            crmLabel.expressions.value = '{!$ObjectType.' + recordType + '.Fields.' + fieldName + '.Label}';
        }
        crmItem.childComponents.add(crmLabel);
        
        // Field Value
        Component.Apex.OutputPanel crmFieldPanel;
        if (fieldName.equals(MERGED_FIELD_ADDRESS)) {
            crmFieldPanel = createAddressFieldPanel(FieldMode.CRM, false);
        }
        else {
            crmFieldPanel = new Component.Apex.OutputPanel();
            crmFieldPanel.styleClass = 'dataEqual';
            Component.Apex.OutputField crmField = new Component.Apex.OutputField();
            if (fieldName.equals(MERGED_FIELD_NAME)) {
               crmField.expressions.value = '{!CRM.Name}';
            }
            else {
                crmField.expressions.value = '{!CRM.' + fieldName + '}';
            }
            crmFieldPanel.childComponents.add(crmField);
        }
        crmItem.childComponents.add(crmFieldPanel);
        
        return crmItem;
    }
    
    /**
     * Creates the column displaying the Data.com field value of the given fieldName. Use the diff
     * parameter to specify the mode to render it in.
     */
    public static Component.Apex.PageBlockSectionItem createDDCField(String fieldName, Boolean diff, Boolean updateable) {
        if (String.isEmpty(fieldName)) {
            return null;
        }
        
        if (updateable == null) {
            updateable = true;
        }
        
        // We get better control of style using pageBlockSectionItem.
        // We need to wrap outputFields in a span to get more control of the style.
        
        // Standard Fields:
        // <apex:pageBlockSectionItem >
        //     <apex:inputCheckbox value="{!<FIELD>Checked}"/>
        //     <apex:outputPanel styleClass="{!IF(<FIELD>Diff, 'dataDiff', 'dataEqual')}">
        //         <apex:outputField value="{!DDC.<FIELD>}"/>
        //     </apex:outputPanel>
        // </apex:pageBlockSectionItem>
        //
        // Address:
        // <apex:pageBlockSectionItem >
        //     <apex:inputCheckbox value="{!AddressDiff}"/>
        //     <apex:outputPanel styleClass="{!IF(AddressDiff, 'dataEqual', 'dataDiff')}">
        //         <apex:outputPanel layout="block"><apex:outputText value="{!DDCAddressLine1}"/></apex:outputPanel>
        //         <apex:outputPanel layout="block"><apex:outputText value="{!DDCAddressLine2}"/></apex:outputPanel>
        //         <apex:outputPanel layout="block"><apex:outputText value="{!DDCAddressLine3}"/></apex:outputPanel>
        //     </apex:outputPanel>
        // </apex:pageBlockSectionItem>
        
        Component.Apex.PageBlockSectionItem ddcItem = new Component.Apex.PageBlockSectionItem();
        
        // Field Status
        if (diff == null) {
            Component.Apex.OutputText spacer = new Component.Apex.OutputText();
            ddcItem.childComponents.add(spacer);
        }
        else if (diff != null && !diff) {
            Component.Apex.Image image = new Component.Apex.Image();
            image.styleClass = 'checkImg';
            image.url = '/img/icon/greenCheckmark.png';
            ddcItem.childComponents.add(image);
        }
        else if (!updateable) {
            Component.Apex.OutputPanel info = createInfoPanel();
            ddcItem.childComponents.add(info);
        }
        else {
            Component.Apex.InputCheckbox checkBox = new Component.Apex.InputCheckbox();
            checkBox.expressions.value = '{!checkBoxes["' + fieldName + '"]}';
            ddcItem.childComponents.add(checkBox);
        }
        
        // Field Value
        Component.Apex.OutputPanel ddcFieldPanel;
        if (fieldName.equals(MERGED_FIELD_ADDRESS)) {
            ddcFieldPanel = createAddressFieldPanel(FieldMode.DDC, diff);
            ddcItem.childComponents.add(ddcFieldPanel);
        }
        else {
            ddcFieldPanel = new Component.Apex.OutputPanel();
            ddcFieldPanel.styleClass = (diff != null && diff) ? 'dataDiff' : 'dataEqual';
            if (fieldName.equals(MERGED_FIELD_NAME)) {
                // NOTE: We can't use the Name field here because the DDC object is not in the DB.
                Component.Apex.OutputText dunsText = new Component.Apex.OutputText();
                dunsText.expressions.value = '{!DDCFullNameForDisplay}';
                ddcFieldPanel.childComponents.add(dunsText);
            }
            else if (fieldName.equals(FIELD_DUNS_NUMBER) || fieldName.equals(FIELD_COMPANY_DUNS_NUMBER)) {
                Component.Apex.OutputText dunsText = new Component.Apex.OutputText();
                dunsText.expressions.value = '{!DDCDunsNumberForDisplay}';
                ddcFieldPanel.childComponents.add(dunsText);
            }
            else {
                Component.Apex.OutputField ddcField = new Component.Apex.OutputField();
                ddcField.expressions.value = '{!DDC.' + fieldName + '}';
                ddcFieldPanel.childComponents.add(ddcField);
            }
        }
        ddcItem.childComponents.add(ddcFieldPanel);
        
        return ddcItem;
    }
    
    /**
     * Constructs the first line of the merged address field which includes:
     * - Street
     */
    public static String getAddressLine1(sObject o, Map<String, sObjectField> sObjectFieldMap) {
        Map<String, String> fieldNameMap = getAddressFieldNameMap(o);
        if (fieldNameMap == null || sObjectFieldMap == null) {
            return '';
        }
        
        String fieldName = fieldNameMap.get(FIELD_STREET);
        if (!Utils.getCrud(fieldName, 'access', sObjectFieldMap)) {
            return '';
        }
        
        String fieldValue = (String) o.get(fieldName);
        if (fieldValue == null) {
            return '';
        }
        
        return fieldValue;
    }
    
    /**
     * Constructs the second line of the merged address field which includes:
     * - City
     * - State
     * - Postal Code
     */
    public static String getAddressLine2(sObject o, Map<String, sObjectField> sObjectFieldMap) {
        Map<String, String> fieldNameMap = getAddressFieldNameMap(o);
        if (fieldNameMap == null || sObjectFieldMap == null) {
            return '';
        }
        
        String result = '';
        String fieldName = fieldNameMap.get(FIELD_POSTAL_CODE);
        String fieldValue = (String) o.get(fieldName);
        if (String.isNotEmpty(fieldValue) && Utils.getCrud(fieldName, 'access', sObjectFieldMap)) {
            result = fieldValue;
        }
        
        fieldName = fieldNameMap.get(FIELD_STATE);
        fieldValue = (String) o.get(fieldName);
        if (String.isNotEmpty(fieldValue) && Utils.getCrud(fieldName, 'access', sObjectFieldMap)) {
            result = fieldValue +
                ((result.length() > 0) ? ' ' + result : '');
        }
        
        fieldName = fieldNameMap.get(FIELD_CITY);
        fieldValue = (String) o.get(fieldName);
        if (String.isNotEmpty(fieldValue) && Utils.getCrud(fieldName, 'access', sObjectFieldMap)) {
            result = fieldValue +
                ((result.length() > 0) ? ', ' + result : '');
        }
        
        return result;
    }
    
    /**
     * Constructs the third line of the merged address field which includes:
     * - Country
     */
    public static String getAddressLine3(sObject o, Map<String, sObjectField> sObjectFieldMap) {
        Map<String, String> fieldNameMap = getAddressFieldNameMap(o);
        if (fieldNameMap == null || sObjectFieldMap == null) {
            return '';
        }
        
        String fieldName = fieldNameMap.get(FIELD_COUNTRY);
        if (!Utils.getCrud(fieldName, 'access', sObjectFieldMap)) {
            return '';
        }
        
        String fieldValue = (String) o.get(fieldName);
        if (fieldValue == null) {
            return '';
        }
        
        return fieldValue;
    }
    
    /**
     * Retrieves a displayable full name for the given Contact or Lead. This basically concatenates
     * the FirstName and LastName fields when the data is accessible.
     * NOTE: Even though these object types have a Name field that supposedly does this for us, we
     * cannot always rely on it being non-null.
     */
    public static String getFullNameForDisplay(sObject o, Map<String, sObjectField> sObjectFieldMap) {
        if (o == null || sObjectFieldMap == null) {
            return null;
        }
        
        String fullName = null;
        
        if (Utils.getCrud(FIELD_LAST_NAME, 'access', sObjectFieldMap)) {
            String lastName = (String) o.get(FIELD_LAST_NAME);
            if (String.isNotEmpty(lastName)) {
                fullName = lastName;
            }
        }
        
        if (Utils.getCrud(FIELD_FIRST_NAME, 'access', sObjectFieldMap)) {
            String firstName = (String) o.get(FIELD_FIRST_NAME);
            if (String.isNotEmpty(firstName)) {
                fullName = (fullName == null) ? firstName : firstName + ' ' + fullName;
            }
        }
        
        return fullName;
    }
    
    /**
     * The DunsNumber field should be obfuscated if the CRM's value does not equal Data.com's value.
     */
    public static String getDunsNumberForDisplay(sObject crm, sObject ddc, Map<String, sObjectField> sObjectFieldMap) {
        // NOTE: D&B fields may not be present.
        if (crm == null || ddc == null || sObjectFieldMap == null) {
            return null;
        }
        
        String fieldName = (ddc instanceof Lead) ? FIELD_COMPANY_DUNS_NUMBER : FIELD_DUNS_NUMBER;
        if (!sObjectFieldMap.containsKey(fieldName) || !Utils.getCrud(fieldName, 'access', sObjectFieldMap)) {
            return null;
        }
        
        String dunsNumber = (String) ddc.get(fieldName);
        if (String.isEmpty(dunsNumber)) {
           return null;
        }
        
        if (dunsNumber.equals((String) crm.get(fieldName))) {
            // If a user's record already has the DunsNumber there's no harm in showing it.
            return dunsNumber;
        }
        else {
            return obfuscateDunsNumber(dunsNumber);
        }
    }
        
    //---------------------------------------------------------------------------------------------
    // Private Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * This is just a helper for the address methods which returns the set of address fields used
     * for the given object.
     */
    private static Map<String, String> getAddressFieldNameMap(sObject o) {
        if (o == null) {
            return null;
        }
        
        if (o instanceof Account) {
            return ADDRESS_FIELDS_ACCOUNT;
        }
        else if (o instanceof Contact) {
            return ADDRESS_FIELDS_CONTACT;
        }
        else if (o instanceof Lead) {
            return ADDRESS_FIELDS_LEAD;
        }
        else {
            return null;
        }
    }
    
    /**
     * Creates the merged address field.
     */
    private static Component.Apex.OutputPanel createAddressFieldPanel(FieldMode mode, Boolean diff) {
        // VisualForce:
        // <apex:outputPanel styleClass="{!IF(AddressDiff, 'dataEqual', 'dataDiff')}">
        //     <apex:outputPanel layout="block"><apex:outputText value="{!DDCAddressLine1}"/></apex:outputPanel>
        //     <apex:outputPanel layout="block"><apex:outputText value="{!DDCAddressLine2}"/></apex:outputPanel>
        //     <apex:outputPanel layout="block"><apex:outputText value="{!DDCAddressLine3}"/></apex:outputPanel>
        // </apex:outputPanel>
        
        if (mode == null) {
            return null;
        }
        
        Component.Apex.OutputPanel fieldPanel = new Component.Apex.OutputPanel();
        fieldPanel.styleClass = (diff != null && diff) ? 'dataDiff' : 'dataEqual';
        
        Component.Apex.OutputPanel textContainer1 = new Component.Apex.OutputPanel();
        textContainer1.layout = 'block';
        Component.Apex.OutputText line1 = new Component.Apex.OutputText();
        line1.expressions.value = '{!' + mode.name() + 'AddressLine1}';
        textContainer1.childComponents.add(line1);
        fieldPanel.childComponents.add(textContainer1);

        Component.Apex.OutputPanel textContainer2 = new Component.Apex.OutputPanel();
        textContainer2.layout = 'block';
        Component.Apex.OutputText line2 = new Component.Apex.OutputText();
        line2.expressions.value = '{!' + mode.name() + 'AddressLine2}';
        textContainer2.childComponents.add(line2);
        fieldPanel.childComponents.add(textContainer2);
        
        Component.Apex.OutputPanel textContainer3 = new Component.Apex.OutputPanel();
        textContainer3.layout = 'block';
        Component.Apex.OutputText line3 = new Component.Apex.OutputText();
        line3.expressions.value = '{!' + mode.name() + 'AddressLine3}';
        textContainer3.childComponents.add(line3);
        fieldPanel.childComponents.add(textContainer3);
        
        return fieldPanel;
    }
    
    /**
     * Creates the info icon with hover to tell the user that they don't have access to update the
     * field.
     */
    private static Component.Apex.OutputPanel createInfoPanel() {
        // VisualForce:
        // <apex:outputPanel onmouseover="addMouseOver(this)" styleClass="mouseOverInfoOuter">
        //     <apex:image styleClass="infoIcon" url="/s.gif" style="vertical-align:middle;" />
        //     <apex:outputPanel style="display:none;" styleClass="mouseOverInfo">
        //         <apex:outputText value="{!$Label.Jigsaw_Chatter_Enabled_Help}" />
        //     </apex:outputPanel>
        // </apex:outputPanel>
        
        Component.Apex.OutputPanel info = new Component.Apex.OutputPanel();
        info.onmouseover = 'addMouseOver(this)';
        info.styleClass = 'mouseOverInfoOuter';
        Component.Apex.Image icon = new Component.Apex.Image();
        icon.styleClass = 'infoIcon';
        icon.url = '/s.gif';
        icon.style = 'vertical-align:middle;';
        info.childComponents.add(icon);
        Component.Apex.OutputPanel hover = new Component.Apex.OutputPanel();
        hover.style = 'display:none;text-align:left;';
        hover.styleClass = 'mouseOverInfo';
        Component.Apex.OutputText text = new Component.Apex.OutputText();
        text.expressions.value = '{!FieldNotUpdateableLabel}';
        hover.childComponents.add(text);
        info.childComponents.add(hover);
        
        return info;
    }
    
    /**
     * Helper for DunsNumber obfuscation. Also makes it easier to write unit tests for orgs that
     * do not have D&B perms currently set.
     */
    private static String obfuscateDunsNumber(String dunsNumber) {
        if (String.isEmpty(dunsNumber)) {
            return null;
        }
        
        String result;
        try {
           result = DUNS_NUMBER_OBFUSCATION + dunsNumber.substring(DUNS_NUMBER_VISIBLE_OFFSET);
        }
        catch (Exception e) {
            result = null;
        }
        
        return result;
    }
    
}