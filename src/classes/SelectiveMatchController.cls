/**
 *	Generic sobject controller for selective match page container and apex page components.
 *	Retrieves, scores and returns a collection of results objects for the page to view and a user to select.
 */
public with sharing class SelectiveMatchController {
	
	private SObject RECORD;
	private final String recordId;
	
    //public List<MatcherObject> results { get; set; }
    public List<LeadWrapper> results { get; set; }
    public Boolean hasResults { get; set; }
    public String type { get; set; }
    public String selectedRecord { get; set; }
    public Boolean refreshPage { get; set; }
	
	//To-Do: if results == null, send a message back to the page (INFO) that there were no search results.
	private transient ApexPages.Message[] messages;
	
	public SelectiveMatchController(ApexPages.StandardController stdController) {
		this.recordId = ApexPages.currentPage().getParameters().get('id');
		this.type = stdController.getRecord().getSObjectType().getDescribe().getName();
		this.results = new List<LeadWrapper>();
        this.hasResults = false;
        this.refreshPage = false;
        this.messages = new List<ApexPages.Message>();
	}
	
	public static String buildJSON(Lead l) {
		
		MatcherContact result = MatcherAPI.matchLead(l, null);
		if(result != null) {
			return JSON.serialize(result);
		}
		
		return null;
	}
	
	public PageReference initPage() {
		String recordId = String.escapeSingleQuotes(this.recordId);
		String queryString;
		if(Utils.isValidId(recordId)) {
		// TO-DO: All queries with field selections should look-up dynamic field bindings from the binding agent and construct the query dynamically with those values.
			if(type == 'Lead') {
				queryString = 'SELECT Id,Jigsaw,FirstName,LastName,Company,CompanyDunsNumber,Website,Industry,AnnualRevenue,NumberOfEmployees,Email,Phone,Title,Street,City,State,PostalCode,Country FROM Lead l WHERE l.Id =\'' + ((recordId == null) ? '' : recordId) + '\' ' + 'LIMIT 1';
			}
			else if(type == 'Contact') {
				queryString = 'SELECT Id,Jigsaw,FirstName,LastName,Account.Name,Email,Phone,Title,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry FROM Contact c WHERE c.Id =\'' + ((recordId == null) ? '' : recordId) + '\' ' + 'LIMIT 1';
			}
        	else if(type == 'Account') {
        		queryString = 'SELECT Id,Jigsaw,Name,Website,TickerSymbol,Ownership,Sic,SicDesc,Description,Industry,Site,AnnualRevenue,NumberOfEmployees,DunsNumber,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry FROM Account a WHERE a.Id =\'' + ((recordId == null) ? '' : recordId) + '\' ' + 'LIMIT 1';
        	}
		}
		
		try {
			this.RECORD = Database.query(queryString);
		}
		catch(DMLException de) {
			System.debug('DMLException: ' +de.getMessage());
			messages.add(new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to fetch SObject from query: ' +de.getMessage()));
		}
        
        // Retrieve all possible matches from search api.
        if(RECORD != null) {
	        // TO-DO: Shouldn't use MatcherAPI here, should be using search api.
	        if(type == 'Lead') {
	        	MatcherContact mc = MatcherAPI.matchLead((Lead)RECORD, messages);
	        	if(mc != null) {
	        		/*LeadWrapper lw = new LeadWrapper();
	        		this.hasResults = true;
	        		lw.firstName = (String)mc.firstName.value;
	        		lw.lastName = (String)mc.lastName.value;
	        		lw.companyName = (String)mc.companyName.value;
	        		lw.title = (String)mc.title.value;
	        		lw.graveyard = 'Active'; 
	        		//(String)mc.graveyard.value == null ? 'Active' : 'Dead';
	        		lw.location = (String)buildLocation(mc.location);
	        		lw.phone = (String)mc.phone.value;
	        		lw.email = (String)mc.email.value;
	        		lw.companyDuns = (String)mc.dunsNumber.value;
	        		lw.contactId = String.valueOf(mc.contactId.value);
	        		if(RECORD.get('Jigsaw') != null && String.valueOf(mc.contactId.value) == RECORD.get('Jigsaw')) {
	        			lw.selected = true;
	        		}
	        		results.add(lw);*/
	        	}
	        	Account a = new Account(Name = (String)RECORD.get('Company'), Website = (String)RECORD.get('Website'), BillingStreet = (String)RECORD.get('Street'), BillingCity = (String)RECORD.get('City'), BillingState = (String)RECORD.get('State'), BillingPostalCode = (String)RECORD.get('PostalCode'), BillingCountry = (String)RECORD.get('Country'), Phone = (String)RECORD.get('Phone'));
	        	MatcherCompany mcomp = MatcherAPI.matchAccount(a, messages);
	        	if(mcomp != null) {
	        		/*LeadWrapper lw = new LeadWrapper();
	        		this.hasResults = true;
	        		lw.companyName = (String)mcomp.companyName.value;
	        		lw.website = (String)mcomp.website.value;
	        		lw.phone = (String)mcomp.location.locationPhone.value;
	        		lw.location = (String)buildLocation(mcomp.location);
	        		lw.firstName = '-';
	        		lw.lastName = '-';
	        		lw.title = '-';
	        		lw.companyDuns = (String)mcomp.dunsNumber.value;
	        		lw.contactId = String.valueOf(mcomp.companyId.value);
	        		
	        		if(RECORD.get('CompanyDunsNumber') != null && String.valueOf(mcomp.dunsNumber.value) == RECORD.get('CompanyDunsNumber')) {
	        			lw.selected = true;
	        		}
	        		results.add(lw);*/
	        	}
	        	else {
	        		this.hasResults = false;
	        	}
	        }
	        else if(type == 'Contact') {
	        	// TO-DO: Build this out for Contact object as well. You'll have to gather Account attributes as well in the query above.
	        	//MatcherAPI.matchContact((Contact)RECORD, messages);
	        }
	        else if(type == 'Account') {
	        	/*MatcherCompany mc = MatcherAPI.matchAccount((Account)RECORD, messages);
	        	if(mc != null) {
		        	if(RECORD.get('Jigsaw') != null && mc.companyId == RECORD.get('Jigsaw')) {
		        		mc.selected = true;
		        	}
		        	results.add(mc);
	        	}*/
	        }
        }
        else {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Unable to retrieve any potential match candidate results.', ''));
        }
        
        if(!hasResults) {
        	if(messages != null) {
        		messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'Testing', ''));
        	}
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Unable to retrieve any potential match candidate results.', ''));
        }
        
        
        return null;
	}
	
	public PageReference selectAndCompare() {
		String ddcId;
		Lead l;
		for(LeadWrapper lw : results) {
			/*if(lw.selected) {
				RECORD.put('FirstName', lw.firstName);
				RECORD.put('LastName', lw.lastName);
				RECORD.put('Email', lw.email);
				//RECORD.put('Website', lw.website);
				RECORD.put('Phone', lw.phone);
				//RECORD.put('Company', lw.companyName);
				RECORD.put('Jigsaw', lw.contactId);
				System.debug(RECORD);
			}*/
		}
		update RECORD;
		this.refreshPage = true;
		return null;
	}
	
	public PageReference selectAndClean() {
		// TODO: Do we need to do any processing at this point?
		String ddcId = null;
        for (LeadWrapper lw : results) {
            /*if (lw.selected) {
            	ddcId = lw.contactId;
            	break;
            }*/
        }
        
        if (recordId != null && ddcId != null) {
            PageReference p = Page.SncLead;
            Map<String, String> params = p.getParameters();
            params.put('id', EncodingUtil.urlEncode(recordId, 'UTF-8'));
            params.put('ddcid', EncodingUtil.urlEncode(ddcId, 'UTF-8'));
            return p;
        }
        else {
        	return null;
        }
	}
	
	private String buildLocation(MatcherLocation ml) {
		String location = null;
    	String separator = ', ';
    	String newLine = '\n';
    	if(ml != null) {
    		location = ml.address.addressLine.value + newLine;
    		location += ml.address.city.value;
    		location += separator + ml.address.state.value;
    		location += separator + ml.address.zip.value;
    		location += separator + ml.address.country.value;
    	}
    	return location;
	}

}