public class SearchFacet {
	
	//---------------------------------------------------------------------------------------------
	// Fields
	//---------------------------------------------------------------------------------------------
	
    public String fieldName;
    public Map<String, Integer> facetValues;
    
    // Type of search result (contact/company)
    public String type;
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public SearchFacet(String type, String fieldName, Map<String,Integer> facetValues) {
        this.fieldName = fieldName;
        this.type = type;
        this.facetValues = facetValues;
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    public static void parseFacets(Dom.XmlNode[] facetNodes) {
        Integer val;
        String facetName;
        // To-Do: Loop over all of the XML Nodes (should be <facetField> nodes), parse <facetFieldName> and 
        
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------

}