/********************************************//**
 *  Contains common API utilities methods and properties that are exposed globally for use by
 *  code outside of the ddcDet namespace.
 *
 *  @author     Shawn Butterfield, Salesforce.com
 *  @since      4/29/2014
 *  @version    1.3
 ***********************************************/
global class ApiUtils {

    //---------------------------------------------------------------------------------------------
    // FIELDS
    // Some fields remain only visible to the ddcdet namespace and are still 'public' below
    //---------------------------------------------------------------------------------------------
    global static final String TAG_NAME_HITS = 'totalHits';
    global static final String TAG_NAME_ID = 'id';
    
    /* Containers */
    // Facets
    public static final String TAG_NAME_FACETS = 'facetFields';
    public static final String TAG_NAME_FACET_FIELD = 'facetField';
    public static final String TAG_NAME_FACET_FIELD_NAME = 'facetFieldName';
    public static final String TAG_NAME_FACET_VALUE_COUNT = 'facetValueCount';
    public static final String TAG_NAME_FACET_VALUES = 'facetValues';
    public static final String TAG_NAME_FACET_VALUE = 'facetValue';

    // Matcher
    global static final String TAG_NAME_MATCHES = 'matches';
    global static final String TAG_NAME_MATCH = 'match';
    global static final String TAG_NAME_MATCHED_ITEMS = 'matchedItems';
    global static final String TAG_NAME_EXTERNAL_KEY = 'externalKey';
    global static final String TAG_NAME_LIST = 'list';
    // Matcher Location
    global static final String TAG_NAME_LOCATION = 'location';
    global static final String TAG_NAME_LOCATION_PHONE = 'locationPhone';
    global static final String TAG_NAME_ADDRESS_LINE = 'addressLine';
    
    // Company
    public static final String TAG_NAME_GET_COMPANY = 'getCompany';
    global static final String TAG_NAME_COMPANIES = 'companies';
    global static final String TAG_NAME_COMPANY = 'company';
    global static final String TAG_NAME_REQUESTED_COMPANY_IDS = 'requestedCompanyIds';
    
    // Contact
    public static final String TAG_NAME_GET_CONTACT = 'getContact';
    global static final String TAG_NAME_CONTACTS = 'contacts';
    global static final String TAG_NAME_CONTACT = 'contact';
    global static final String TAG_NAME_REQUESTED_CONTACT_IDS = 'requestedContactIds';
    
    /* Attributes */
    
    // Matcher Field Attributes
    global static final String TAG_ATTRIBUTE_DIFF = 'diff';

    // Address
    global static final String TAG_NAME_ADDRESS = 'address';
    global static final String TAG_NAME_ADDRESS_CITY = 'city';
    global static final String TAG_NAME_ADDRESS_STATE = 'state';
    global static final String TAG_NAME_ADDRESS_ZIP = 'zip';
    global static final String TAG_NAME_ADDRESS_COUNTRY = 'country';
    
    // Shared
    global static final String TAG_NAME_UPDATED_DATE = 'updatedDate';
    global static final String TAG_NAME_SOURCE = 'source';
    global static final String TAG_NAME_UNRECOGNIZED = 'unrecognized';
    
    // Company
    global static final String TAG_NAME_COMPANY_NAME = 'companyName';
    global static final String TAG_NAME_WEBSITE = 'website';
    global static final String TAG_NAME_COMPANY_ID = 'companyId';
    global static final String TAG_NAME_STOCK_TICKER = 'stockSymbol';
    global static final String TAG_NAME_STOCK_EXCHANGE = 'stockExchange';
    global static final String TAG_NAME_EMPLOYEE_COUNT = 'employeeCount';
    global static final String TAG_NAME_EMPLOYEE_RANGE = 'employeeRange';
    global static final String TAG_NAME_REVENUE = 'revenue';
    global static final String TAG_NAME_REVENUE_RANGE = 'revenueRange';
    global static final String TAG_NAME_GRAVEYARDED = 'graveyarded';
    global static final String TAG_NAME_OWNERSHIP = 'ownership';
    global static final String TAG_NAME_INDUSTRY1 = 'industry1';
    global static final String TAG_NAME_INDUSTRY2 = 'industry2';
    global static final String TAG_NAME_INDUSTRY3 = 'industry3';
    global static final String TAG_NAME_SUB_INDUSTRY1 = 'subIndustry1';
    global static final String TAG_NAME_SUB_INDUSTRY2 = 'subIndustry2';
    global static final String TAG_NAME_SUB_INDUSTRY3 = 'subIndustry3';
    global static final String TAG_NAME_SFDC_INDUSTRY = 'sfdcIndustry';
    global static final String TAG_NAME_CREATED_ON = 'createdOn';
    global static final String TAG_NAME_ACTIVE_CONTACTS = 'activeContacts';
    global static final String TAG_NAME_COMPANY_WIKI = 'companyWiki';
    global static final String TAG_NAME_YEAR_STARTED = 'yearStarted';
    global static final String TAG_NAME_FORTUNE_RANK = 'fortuneRank';
    global static final String TAG_NAME_DUNS_NUMBER = 'dunsNumber';
    global static final String TAG_NAME_TRADESTYLE = 'tradeStyle';
    global static final String TAG_NAME_PRIMARY_SIC = 'primarySic';
    global static final String TAG_NAME_PRIMARY_SIC_DESC = 'primarySicDesc';
    global static final String TAG_NAME_FAX_NUMBER = 'faxNumber';
    global static final String TAG_NAME_LOCATION_STATUS = 'locationStatus';
    global static final String TAG_NAME_LATITUDE = 'latitude';
    global static final String TAG_NAME_LONGITUDE = 'longitude';
    global static final String TAG_NAME_PRIMARY_NAICS = 'primaryNaics';
    global static final String TAG_NAME_PRIMARY_NAICS_DESC = 'primaryNaicsDesc';
    global static final String TAG_NAME_COMPANY_SYNOPSIS = 'companySynopsis';
    global static final String TAG_NAME_SHIPPING_CITY = 'shippingCity';
    global static final String TAG_NAME_SHIPPING_COUNTRY_CODE = 'shippingCountryCode';
    global static final String TAG_NAME_SHIPPING_STATE = 'shippingState';
    global static final String TAG_NAME_SHIPPING_ADDRESS = 'shippingAddress';
    global static final String TAG_NAME_SHIPPING_ZIP = 'shippingZip';
    global static final String TAG_NAME_INTERNATIONAL_CODE = 'internationalCode';
    global static final String TAG_NAME_METRO_AREA = 'metroArea';
    global static final String TAG_NAME_COUNTRY_CODE = 'countryCode';
    
    // Addt'l D&B Attributes
    global static final String TAG_NAME_COUNTRY_ACCESS_CODE = 'countryAccessCode';
    global static final String TAG_NAME_OWNERSHIP_TYPE = 'ownershipType';
    global static final String TAG_NAME_SALES_VOLUME = 'salesVolume';
    global static final String TAG_NAME_EMPLOYEES_TOTAL = 'employeesTotal';
    global static final String TAG_NAME_OUT_OF_BUSINESS = 'outOfBusiness';
    global static final String TAG_NAME_EMPLOYEES_HERE = 'employeesHere';
    global static final String TAG_NAME_GLOBAL_ULTIMATE_EMPLOYEES = 'globalUltimateTotalEmployees';
    global static final String TAG_NAME_TRADESTYLE2 = 'tradestyle2';
    global static final String TAG_NAME_TRADESTYLE3 = 'tradestyle3';
    global static final String TAG_NAME_TRADESTYLE4 = 'tradestyle4';
    global static final String TAG_NAME_TRADESTYLE5 = 'tradestyle5';
    global static final String TAG_NAME_NATIONAL_ID = 'nationalId';
    global static final String TAG_NAME_NATIONAL_ID_TYPE = 'nationalIdType';
    global static final String TAG_NAME_US_TAX_ID = 'usTaxId';
    global static final String TAG_NAME_FAMILY_MEMBERS = 'familyMembers';
    global static final String TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER = 'globalUltimateDunsNumber';
    global static final String TAG_NAME_GLOBAL_ULTIMATE_BUSINESS_NAME = 'globalUltimateBusinessName';
    global static final String TAG_NAME_PARENT_OR_HQ_DUNS_NUMBER = 'parentOrHqDunsNumber';
    global static final String TAG_NAME_PARENT_OR_HQ_BUSINESS_NAME = 'parentOrHqBusinessName';
    global static final String TAG_NAME_DOMESTIC_ULTIMATE_DUNS_NUMBER = 'domesticUltimateDunsNumber';
    global static final String TAG_NAME_DOMESTIC_ULTIMATE_BUSINESS_NAME = 'domesticUltimateBusinessName';
    global static final String TAG_NAME_FIPS_CODE = 'fipsMsaCode';
    global static final String TAG_NAME_LOCATION_STATUS_CODE = 'locationStatusCode';
    global static final String TAG_NAME_OWN_OR_RENT = 'ownOrRent';
    global static final String TAG_NAME_EMPLOYEES_HERE_RELIABILITY = 'employeesHereReliability';
    global static final String TAG_NAME_SALES_VOLUME_RELIABILITY = 'salesVolumeReliability';
    global static final String TAG_NAME_CURRENCY_CODE = 'currencyCode';
    global static final String TAG_NAME_LEGAL_STATUS = 'legalStatus';
    global static final String TAG_NAME_MINORITY_OWNED = 'minorityOwned';
    global static final String TAG_NAME_WOMEN_OWNED = 'womenOwned';
    global static final String TAG_NAME_SMALL_BUSINESS = 'smallBusiness';
    global static final String TAG_NAME_MARKETING_CLUSTER = 'marketingCluster';
    global static final String TAG_NAME_IMPORT_EXPORT_AGENT = 'importExportAgent';
    global static final String TAG_NAME_SUBSIDIARY = 'subsidiary';
    global static final String TAG_NAME_GEO_CODE_ACCURACY = 'geoCodeAccuracy';
    global static final String TAG_NAME_MARKETING_PRESCREEN = 'marketingPreScreen';
    global static final String TAG_NAME_EMPLOYEES_TOTAL_RELIABILITY = 'employeesTotalReliability';
    
    // SIC & NAICS
    global static final String TAG_NAME_SECOND_SIC = 'secondSic';
    global static final String TAG_NAME_SECOND_SIC_DESC = 'secondSicDesc';
    global static final String TAG_NAME_THIRD_SIC = 'thirdSic';
    global static final String TAG_NAME_THIRD_SIC_DESC = 'thirdSicDesc';
    global static final String TAG_NAME_FOURTH_SIC = 'fourthSic';
    global static final String TAG_NAME_FOURTH_SIC_DESC = 'fourthSicDesc';
    global static final String TAG_NAME_FIFTH_SIC = 'fifthSic';
    global static final String TAG_NAME_FIFTH_SIC_DESC = 'fifthSicDesc';
    global static final String TAG_NAME_SIXTH_SIC = 'sixthSic';
    global static final String TAG_NAME_SIXTH_SIC_DESC = 'sixthSicDesc';
    global static final String TAG_NAME_SECOND_NAICS = 'secondNaics';
    global static final String TAG_NAME_SECOND_NAICS_DESC = 'secondNaicsDesc';
    global static final String TAG_NAME_THIRD_NAICS = 'thirdNaics';
    global static final String TAG_NAME_THIRD_NAICS_DESC = 'thirdNaicsDesc';
    global static final String TAG_NAME_FOURTH_NAICS = 'fourthNaics';
    global static final String TAG_NAME_FOURTH_NAICS_DESC = 'fourthNaicsDesc';
    global static final String TAG_NAME_FIFTH_NAICS = 'fifthNaics';
    global static final String TAG_NAME_FIFTH_NAICS_DESC = 'fifthNaicsDesc';
    global static final String TAG_NAME_SIXTH_NAICS = 'sixthNaics';
    global static final String TAG_NAME_SIXTH_NAICS_DESC = 'sixthNaicsDesc';
    
    // Contact
    global static final String TAG_NAME_EMAIL = 'email';
    global static final String TAG_NAME_FIRST_NAME = 'firstname';
    global static final String TAG_NAME_LAST_NAME = 'lastname';
    global static final String TAG_NAME_PHONE = 'phone';
    global static final String TAG_NAME_TITLE = 'title';
    global static final String TAG_NAME_CONTACT_ID = 'contactId';
    global static final String TAG_NAME_GRAVEYARD_STATUS = 'graveyardStatus';
    global static final String TAG_NAME_DEPARTMENT = 'department';
    global static final String TAG_NAME_AREA_CODE = 'areaCode';

    // 'fake' properties used by DDCCompany to extend standard api data attributes
    global static final String TAG_NAME_DOMESTIC_ULTIMATE_EMPLOYEES = 'domesticUltimateEmployees';
    global static final String TAG_NAME_PARENT_HQ_EMPLOYEES = 'parentHqEmployees';

    //---------------------------------------------------------------------------------------------
    // PROPERTIES
    //---------------------------------------------------------------------------------------------
    /* PROPERTY SEQUENCES */
    // Contact
    global static final Set<String> SEQUENCE_CONTACT = new Set<String> {
        TAG_NAME_COMPANY_ID,
        TAG_NAME_CONTACT_ID,
        TAG_NAME_TITLE,
        TAG_NAME_COMPANY_NAME,
        TAG_NAME_UPDATED_DATE,
        TAG_NAME_GRAVEYARD_STATUS,
        TAG_NAME_FIRST_NAME,
        TAG_NAME_LAST_NAME,
        TAG_NAME_ADDRESS_CITY,
        TAG_NAME_ADDRESS_STATE,
        TAG_NAME_ADDRESS_COUNTRY,
        TAG_NAME_ADDRESS_ZIP,
        TAG_NAME_DEPARTMENT,
        TAG_NAME_AREA_CODE,
        TAG_NAME_PHONE,
        TAG_NAME_EMAIL,
        TAG_NAME_ADDRESS
        /* --NO LONGER IN USE --
            TAG_NAME_INDUSTRY1,
            TAG_NAME_INDUSTRY2,
            TAG_NAME_INDUSTRY3,
            TAG_NAME_SUB_INDUSTRY1,
            TAG_NAME_SUB_INDUSTRY2,
            TAG_NAME_SUB_INDUSTRY3,
            TAG_NAME_SFDC_INDUSTRY,
            TAG_NAME_EMPLOYEE_COUNT,
            TAG_NAME_REVENUE
        */
    };
    
    // Company
    global static final Set<String> SEQUENCE_COMPANY = new Set<String> {
        TAG_NAME_COMPANY_ID,
        TAG_NAME_COMPANY_NAME,
        TAG_NAME_PHONE,
        TAG_NAME_ADDRESS,
        TAG_NAME_ADDRESS_CITY,
        TAG_NAME_ADDRESS_STATE,
        TAG_NAME_ADDRESS_ZIP,
        TAG_NAME_ADDRESS_COUNTRY,
        TAG_NAME_WEBSITE,
        TAG_NAME_STOCK_TICKER,
        TAG_NAME_STOCK_EXCHANGE,
        TAG_NAME_REVENUE,
        TAG_NAME_REVENUE_RANGE,
        TAG_NAME_EMPLOYEE_COUNT,
        TAG_NAME_EMPLOYEE_RANGE,
        TAG_NAME_OWNERSHIP,
        TAG_NAME_FORTUNE_RANK,
        TAG_NAME_GRAVEYARDED,
        TAG_NAME_INDUSTRY1,
        TAG_NAME_SUB_INDUSTRY1,
        TAG_NAME_CREATED_ON,
        TAG_NAME_UPDATED_DATE,
        TAG_NAME_ACTIVE_CONTACTS,
        TAG_NAME_COMPANY_WIKI,
        TAG_NAME_YEAR_STARTED,
        TAG_NAME_SOURCE,
        TAG_NAME_DUNS_NUMBER,
        TAG_NAME_TRADESTYLE,
        TAG_NAME_PRIMARY_SIC,
        TAG_NAME_PRIMARY_SIC_DESC,
        TAG_NAME_FAX_NUMBER,
        TAG_NAME_LOCATION_STATUS,
        TAG_NAME_LATITUDE,
        TAG_NAME_LONGITUDE,
        TAG_NAME_SHIPPING_CITY,
        TAG_NAME_SHIPPING_COUNTRY_CODE,
        TAG_NAME_SHIPPING_STATE,
        TAG_NAME_SHIPPING_ADDRESS,
        TAG_NAME_SHIPPING_ZIP,
        TAG_NAME_INTERNATIONAL_CODE,
        TAG_NAME_METRO_AREA,
        TAG_NAME_COUNTRY_CODE,
        TAG_NAME_PRIMARY_NAICS,
        TAG_NAME_PRIMARY_NAICS_DESC,
        TAG_NAME_COMPANY_SYNOPSIS,
        TAG_NAME_COUNTRY_ACCESS_CODE,
        TAG_NAME_OWNERSHIP_TYPE,
        TAG_NAME_SALES_VOLUME,
        TAG_NAME_EMPLOYEES_TOTAL,
        TAG_NAME_OUT_OF_BUSINESS,
        TAG_NAME_SECOND_SIC,
        TAG_NAME_SECOND_SIC_DESC,
        TAG_NAME_THIRD_SIC,
        TAG_NAME_THIRD_SIC_DESC,
        TAG_NAME_FOURTH_SIC,
        TAG_NAME_FOURTH_SIC_DESC,
        TAG_NAME_SECOND_NAICS,
        TAG_NAME_SECOND_NAICS_DESC,
        TAG_NAME_THIRD_NAICS,
        TAG_NAME_THIRD_NAICS_DESC,
        TAG_NAME_FOURTH_NAICS,
        TAG_NAME_FOURTH_NAICS_DESC,
        TAG_NAME_EMPLOYEES_HERE,
        TAG_NAME_GLOBAL_ULTIMATE_EMPLOYEES,
        TAG_NAME_TRADESTYLE2,
        TAG_NAME_TRADESTYLE3,
        TAG_NAME_FAMILY_MEMBERS,
        TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER,
        TAG_NAME_GLOBAL_ULTIMATE_BUSINESS_NAME,
        TAG_NAME_PARENT_OR_HQ_DUNS_NUMBER,
        TAG_NAME_PARENT_OR_HQ_BUSINESS_NAME,
        TAG_NAME_DOMESTIC_ULTIMATE_DUNS_NUMBER,
        TAG_NAME_DOMESTIC_ULTIMATE_BUSINESS_NAME,
        TAG_NAME_FIPS_CODE,
        TAG_NAME_SUBSIDIARY

        /* No need to parse, these are not being used or implemented 9/15/2013:
         *  TAG_NAME_SFDC_INDUSTRY,
         *  TAG_NAME_INDUSTRY2,
         *  TAG_NAME_INDUSTRY3,
         *  TAG_NAME_SUB_INDUSTRY2,
         *  TAG_NAME_SUB_INDUSTRY3,
         *  TAG_NAME_FIFTH_SIC,
         *  TAG_NAME_FIFTH_SIC_DESC,
         *  TAG_NAME_SIXTH_SIC,
         *  TAG_NAME_SIXTH_SIC_DESC,
         *  TAG_NAME_FIFTH_NAICS,
         *  TAG_NAME_FIFTH_NAICS_DESC,
         *  TAG_NAME_SIXTH_NAICS,
         *  TAG_NAME_SIXTH_NAICS_DESC,
         *  TAG_NAME_TRADESTYLE4,
         *  TAG_NAME_TRADESTYLE5,
         *  TAG_NAME_NATIONAL_ID,
         *  TAG_NAME_NATIONAL_ID_TYPE,
         *  TAG_NAME_US_TAX_ID,
         *  TAG_NAME_OWN_OR_RENT,
         *  TAG_NAME_EMPLOYEES_HERE_RELIABILITY,
         *  TAG_NAME_SALES_VOLUME_RELIABILITY,
         *  TAG_NAME_CURRENCY_CODE,
         *  TAG_NAME_LEGAL_STATUS,
         *  TAG_NAME_MINORITY_OWNED,
         *  TAG_NAME_WOMEN_OWNED,
         *  TAG_NAME_SMALL_BUSINESS,
         *  TAG_NAME_MARKETING_CLUSTER,
         *  TAG_NAME_IMPORT_EXPORT_AGENT,
         *  TAG_NAME_GEO_CODE_ACCURACY,
         *  TAG_NAME_MARKETING_PRESCREEN,
         *  TAG_NAME_EMPLOYEES_TOTAL_RELIABILITY,
         */
    };

    /**
     *  Since some company properties must be mask'd for display purposes to avoid confusion during
     *  configuration, the API needs to know how to translate the DDCProperty Display Friendly Name
     *  in to the actual API TAG name for use with serializing and de-serializing Xml
     */
    // Overlapping Company type'd properties Map of encoded values from API NAME => PROPERTY NAME
    global static Map<String, String> COMPANY_PROPERTIES_ENCODING = new Map<String, String> { 
        ApiUtils.TAG_NAME_PHONE => 'companyPhone'
        ,ApiUtils.TAG_NAME_ADDRESS => 'billingAddress'
        ,ApiUtils.TAG_NAME_ADDRESS_CITY => 'billingCity'
        ,ApiUtils.TAG_NAME_ADDRESS_STATE => 'billingState'
        ,ApiUtils.TAG_NAME_ADDRESS_ZIP => 'billingZip'
        ,ApiUtils.TAG_NAME_ADDRESS_COUNTRY => 'billingCountry'
    };
	
    //---------------------------------------------------------------------------------------------
    // CONSTRUCTORS
    //---------------------------------------------------------------------------------------------
    private ApiUtils() {
		// No instance instantation allowed.
	}
}