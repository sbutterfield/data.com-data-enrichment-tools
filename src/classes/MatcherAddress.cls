/**
 * @author: Shawn Butterfield, Salesforce.com Inc
 * Address object returned by the Matcher API.
 */
public with sharing class MatcherAddress extends MatcherObject {

    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    public MatcherField addressLine {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_ADDRESS_LINE); }
        set { mMap.put(MatcherAPI.TAG_NAME_ADDRESS_LINE, value); }
    }
    
    public MatcherField addressLine2 {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_ADDRESS_LINE_2); }
        set { mMap.put(MatcherAPI.TAG_NAME_ADDRESS_LINE_2, value); }
    }
    
    public MatcherField city {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_ADDRESS_CITY); }
        set { mMap.put(MatcherAPI.TAG_NAME_ADDRESS_CITY, value); }
    }
    
    public MatcherField state {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_ADDRESS_STATE); }
        set { mMap.put(MatcherAPI.TAG_NAME_ADDRESS_STATE, value); }
    }
    
    public MatcherField zip {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_ADDRESS_ZIP); }
        set { mMap.put(MatcherAPI.TAG_NAME_ADDRESS_ZIP, value); }
    }
    
    public MatcherField country {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_ADDRESS_COUNTRY); }
        set { mMap.put(MatcherAPI.TAG_NAME_ADDRESS_COUNTRY, value); }
    }
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public MatcherAddress() {
    	super(MatcherAPI.TAG_NAME_ADDRESS, MatcherAPI.SEQUENCE_ADDRESS);
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * This will parse the given address node returned by the Matcher API.
     */
    public static MatcherAddress parse(Dom.XmlNode addressNode) {
        if (addressNode == null || !MatcherAPI.TAG_NAME_ADDRESS.equals(addressNode.getName())) {
            return null;
        }
        
        // NOTE: A switch statement would be great here but Apex doesn't have one!
        MatcherAddress address = new MatcherAddress();
        Dom.XmlNode node;
        
        node = addressNode.getChildElement(MatcherAPI.TAG_NAME_ADDRESS_LINE, null);
        if (node != null) {
            address.addressLine = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = addressNode.getChildElement(MatcherAPI.TAG_NAME_ADDRESS_LINE_2, null);
        if (node != null) {
            address.addressLine2 = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = addressNode.getChildElement(MatcherAPI.TAG_NAME_ADDRESS_CITY, null);
        if (node != null) {
            address.city = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = addressNode.getChildElement(MatcherAPI.TAG_NAME_ADDRESS_STATE, null);
        if (node != null) {
            address.state = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = addressNode.getChildElement(MatcherAPI.TAG_NAME_ADDRESS_ZIP, null);
        if (node != null) {
            address.zip = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = addressNode.getChildElement(MatcherAPI.TAG_NAME_ADDRESS_COUNTRY, null);
        if (node != null) {
            address.country = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        return address;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testToXml() {
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        String expectedXml =
            '<' + MatcherAPI.TAG_NAME_ADDRESS + '>' +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_ADDRESS_LINE, address.addressLine) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_ADDRESS_LINE_2, address.addressLine2) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_ADDRESS_CITY, address.city) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_ADDRESS_STATE, address.state) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_ADDRESS_ZIP, address.zip) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_ADDRESS_COUNTRY, address.country) +
            '</' + MatcherAPI.TAG_NAME_ADDRESS + '>';
        
        String actualXml = address.toXml();
        System.assertEquals(expectedXml, actualXml);
    }
    
    private static testmethod void testParse() {
        // Null
        System.assertEquals(null, MatcherAddress.parse(null));
        
        // Non-Address Node
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode root = doc.createRootElement('NotAddress', null, null);
        System.assertEquals(null, MatcherAddress.parse(root));
        
        // NOTE: The easiest way to test for equality is to compare XML strings because there are
        // no equals methods readily available for all objects other than String.
        
        // Address
        MatcherAddress expectedAddress = new MatcherAddress();
        expectedAddress.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        expectedAddress.addressLine2 = new MatcherField('Ste 400', false);
        expectedAddress.city = new MatcherField('San Mateo', true);
        expectedAddress.state = new MatcherField('CA', false);
        expectedAddress.zip = new MatcherField('94404-5059', true);
        expectedAddress.country = new MatcherField('USA', false);
        String expectedXml = expectedAddress.toXml();
        
        doc = new Dom.Document();
        doc.load(expectedXml);
        MatcherAddress actualAddress = MatcherAddress.parse(doc.getRootElement());
        System.assertNotEquals(null, actualAddress);
        System.assertEquals(expectedXml, actualAddress.toXml());
    }
    
}