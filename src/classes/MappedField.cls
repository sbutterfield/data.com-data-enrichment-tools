public class MappedField {
    
    
    //---------------------------------------------------------------------------------------------
    // Constants
    //---------------------------------------------------------------------------------------------
	public static final String SOBJECT_NAME = Utils.NAMESPACE_PREFIX + 'Mapped_Field__c';
	public static final String DDCPROPERTY_SOBJECT_NAME = Utils.NAMESPACE_PREFIX + 'DDC_Property__c';
	
	
    //---------------------------------------------------------------------------------------------
    // Object & Fields
    //---------------------------------------------------------------------------------------------
	public Mapped_Field__c cMappedField;
	
	/* Wasn't using this, comment out for now
	private DDC_Property__c cRelatedProperty;
	*/
	
	public static String DDC_PROPERTY_FIELD = 'ddcDet__Property__c'; //Returns DDC_Property__c SObject
	public static String FILL_EMPTY_FIELD = 'ddcDet__FillEmpty__c';
	public static String MAPPED_FIELD_FIELD = 'ddcDet__Mapped_Field__c'; // String of field name
	public static String MATCH_SCENARIO_FIELD = 'ddcDet__Match_Scenario__c'; //Returns the Match_Scenario__c SObject
	public static String OVERWRITE_FIELD = 'ddcDet__Overwrite__c';
	public static String SOBJECT_FIELD = 'ddcDet__SObject__c'; //Formula from Match_Scenario__c, Returns String
	// "Name" Field returns ScenarioId value as a String
	public static String PROPERTY_NAME_FIELD = 'ddcDet__Property_Name__c'; //Formula from DDCProperty to push the proper name of the property up to the mapping it belongs to
	public static String SOAP_TYPE_FIELD = 'ddcDet__SoapType__c';
	public static String ENTERPRISE_PROPERTY_FIELD = 'ddcDet__Enterprise_Property__c'; //Boolean formula field from DDCProperty designates whether or not the related property is a D&B Enterprise property
	
	//---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    public String id { get; set; }
	public String ddcproperty { get; set; }
	public String propertyName { get; set; }
	public String matchScenario { get; set; }
	
	/* Commented out: caused bugs b/c cannot instantiate a new MappedField wrapper when this is null. Null is required here when creating a new mapping.
	public Schema.SObjectField mappingField { get; set; }
	*/
	
	public String fieldType { get; set; } // Relates to Schema.SoapType Enum. See: http://goo.gl/QfkmPE
	public String mappedFieldName { get; set; }
	public String correspondingTypes { get; set; }
	public Boolean overwrite { get; set; }
	public Boolean fill { get; set; }
	public String sfobject { get; set; }
	
	
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
	// This constructor creates a new mapped field for you.
	public MappedField(String id, String ddcproperty, String sfobject, String propertyName, String matchScenario, String mappedFieldName, String fieldType, String correspondingTypes, Boolean overwrite, Boolean fill) {
		this.id = id;
		this.ddcproperty = ddcproperty;
		this.propertyName = propertyName;
		this.matchScenario = matchScenario;
		this.mappedFieldName = mappedFieldName;
		this.fieldType = fieldType;
		this.overwrite = overwrite;
		this.fill = fill;
		this.sfobject = sfobject;
	}
	
	// Creates a new wrapper instance from a concrete sobject.
	public MappedField(Mapped_Field__c input) {
		this.ddcproperty = (String)input.get(DDC_PROPERTY_FIELD);
		this.matchScenario = (String)input.get(MATCH_SCENARIO_FIELD);
		
		/* Commented out: caused bugs b/c cannot instantiate a new MappedField wrapper when this is null. Null is required here when creating a new mapping.
		// get the sobject describe from cache and then fetch the SObjectField type from the SObject map based on the value of the object on the Match_Scenario__c and the field the user has mapped on the Mapped_Field__c
		this.mappingField = Utils.getSObjectDescribe((String)input.get(SOBJECT_FIELD)).fields.getMap().get((String)input.get(MAPPED_FIELD_FIELD));
		*/
		
		this.mappedFieldName = (String)input.get(MAPPED_FIELD_FIELD);
		// Getting the field casting type so that we know how to cast the value coming back from the API
		if ((String)input.get(SOAP_TYPE_FIELD) != null) {
			// Try and get the SOAP Type from the existing mapping if possible. This is safe since we are not creating a whole new Mapped Field at this point.
			this.fieldType = (String)input.get(SOAP_TYPE_FIELD);
		}
		else {
			try {
			    this.fieldType = String.valueOf(Utils.getFieldDescribe((String)input.get(SOBJECT_FIELD), (String)input.get(MAPPED_FIELD_FIELD)));
			}
			catch(Exception e) {
				System.debug('Exception occurred getting the SOAP Type for the field: ' +input.get(MAPPED_FIELD_FIELD)+ ' \n ' + ' It is likely that the field no longer exists on the object it was mapped for.');
				// Bad field in the mapping. It will need to be re-mapped!
				this.fieldType = null;
			}
		}
		this.cMappedField = input;
		this.propertyName = (String)input.get(PROPERTY_NAME_FIELD);
		this.overwrite = Boolean.valueOf(input.get(OVERWRITE_FIELD));
		this.fill = Boolean.valueOf(input.get(FILL_EMPTY_FIELD));
		this.id = input.Id;
		this.sfobject = (String)input.get(SOBJECT_FIELD);
	}

	// Default constructor is public, can have an empty mapped field.
	public MappedField() {}
	
	
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
	
	/**
		To-Do: Remember to set the soap property fresh for the upsert in case the field mapping actually changed. You'll chew up a describe but it's not that bad.
	 *	@param objName = the sobject for the MappedField to bind to. It can be passed as a null if necessary but only if the concrete Mapped_Field__c is bound to the MappedField(input)
	 *	@param input = instace of the MappedField for upsert (to be pushed to an SObject)
	 *	@param messages = a collection of page messages. might be null.
	*/
	public static Boolean upsertMappedField(MappedField input, String objName, ApexPages.Message[] messages) {
		
		Boolean result;
	
		// Verify than input is not null.
		// Concrete instance may not exist since we may be creating one on id == null
		if (input != null) {
			Schema.SoapType st;
			Boolean validSoapBinding = false; // Assume we do NOT have a valid binding
			if (objName != null) {
				// If objName is null, it is a new mapping and we need to describe some stuff
				st = Utils.getFieldDescribe(objName, input.mappedFieldName).getSoapType();
				validSoapBinding = input.correspondingTypes.containsIgnoreCase(String.valueOf(st));
			}
			
			if (validSoapBinding) {
				// Validate that properties that should be Ids are actually Ids
				if (Utils.isValidId(input.ddcproperty) && Utils.isValidId(input.matchScenario)) {
					if (!Utils.isValidId(input.id)) {
						// New mapped field
						input.cMappedField = new Mapped_Field__c();
					}
					else if (input.cMappedField == null) {
						// Existing field mapping so we need to update.
						// cMappedField might be null if we're getting it back from AJAX, otherwise it is within a normal runtime request so save the soql.
						// SOQL to get the concrete record again.
						Map<String, Schema.SObjectField> fieldmap = Utils.getFieldListing(SOBJECT_NAME);
						if (fieldmap != null) {
							String soql = 'SELECT ';

							for (Schema.SObjectField fName : fieldmap.values()) {
							    soql += fName + ',';
							}
							
							// Trim off last comma so we don't throw query exceptions
							soql = soql.removeEnd(',');
							
							// Add the FROM clause
							soql += ' FROM ' + SOBJECT_NAME;
							
							// Add where clause for objName
							soql += ' WHERE Id = \'' + Id.valueOf(input.id) + '\'';

							input.cMappedField =  Database.query(soql);
						}
					}
					
					/*Transfer attributes from wrapper to concrete object*/
					input.cMappedField.put('Name', input.matchScenario);
					
					// Sets the ddcproperty (Lookup Field) to the correct ddcproperty id. This should always carry over.
					input.cMappedField.put(DDC_PROPERTY_FIELD, input.ddcproperty);
					
					// Set the mapped field api name. Prefer to use Schema.SObjectField, but we can utilize the mappedFieldName string as well.
					input.cMappedField.put(MAPPED_FIELD_FIELD, input.mappedFieldname);
					input.cMappedField.put(MATCH_SCENARIO_FIELD, input.matchScenario);
					input.cMappedField.put(SOAP_TYPE_FIELD, String.valueOf(st));
					
					// Fill & Overwrite
					input.cMappedField.put(FILL_EMPTY_FIELD, input.fill);
					input.cMappedField.put(OVERWRITE_FIELD, input.overwrite);
				}
				else {
					throw new ddcLib1.NullPointerException('Expected non-null ddcproperty & matchScenario for MappedField input. Received: ddcproperty='+input.ddcproperty +',matchScenario='+input.matchScenario);
				}
				
				// Do the DML funky chicken
				try {
					Database.UpsertResult ur = Database.upsert(input.cMappedField);
					result = ur.isSuccess();
				}
				catch(DMLException de) {
					System.debug('Unable to save the MappedField back to schema: ' +de.getMessage() +'/n'+de.getStackTraceString());
					messages.add(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.DDCUnrecoverableError, de.getMessage()));
				}
			}
			else {
				// Invalid Soap Binding, must throw an exception because DML never happened
				result = false;
				throw new ddcLib1.IllegalStateException('Incompatible Soap Type for binding mapped field to ddc property. I needed a field of type:'+input.correspondingTypes+' , received:'+st);
			}
		}
		return result;
	}

}