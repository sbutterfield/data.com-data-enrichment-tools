public class MatchedDandBCompany extends MatchedObject {

	DandBCompanyWrapper dbc;
	
	public DandBCompanyWrapper getDandBCompanyWrapper() {
		return this.dbc;
	}
	
	private void setDandBCompanyWrapper(DandBCompanyWrapper dbc) {
		this.dbc = dbc;
	}
	
	private MatchedDandBCompany() {
		// default constructor
	}
	
	public MatchedDandBCompany(Wrapper w) {
		super();
		if(w instanceof DandBCompanyWrapper) {
			setDandBCompanyWrapper((DandBCompanyWrapper)w);
		}
		else throw new ddcLib1.IllegalArgumentException('Unable to attempt a match without the correct wrapper. Expected DandBCompanyWrapper, got: ' +w);
	}
	
	public override SObject[] match() {
		System.debug('Calling match within MatchedDandBCompany with abstract final implementation.');
		return new SObject[0];
	}
	
	public void match(Wrapper w) {
		if(w instanceof DandBCompanyWrapper) {
			setDandBCompanyWrapper((DandBCompanyWrapper)w);
		}
		else throw new ddcLib1.IllegalArgumentException('Unable to attempt a match without the correct wrapper. Expected DandBCompanyWrapper, got: ' +w);
		
		System.debug('Calling match within MatchedDandBCompany with abstract final implementation.');
	}
	
	public override Descriptor getDescribe() {
		
		MatchedDandBCompanyDescriptor thisDescriptor; 
		
		if(thisDescriptor == null) {
			thisDescriptor = new MatchedDandBCompanyDescriptor();
		}
		
		return thisDescriptor.getDescribe();
	}
	
	/**
	 *	INNER CLASSES
	 *	Descritpor for MatchedDandBCompany. Instantiated when requested, otherwise save the statements.
	 */
	private class MatchedDandBCompanyDescriptor extends Descriptor {
	    
	    //---------------------------------------------------------------------------------------------
	    // Properties
	    //---------------------------------------------------------------------------------------------
	    public String matchCode;
	    
	    //---------------------------------------------------------------------------------------------
	    // Constructors
	    //---------------------------------------------------------------------------------------------
	    MatchedDandBCompanyDescriptor() {
			super();
	    	getDescribe();
	    }
	    
	    //---------------------------------------------------------------------------------------------
	    // Methods
	    //---------------------------------------------------------------------------------------------
		public override Descriptor getDescribe() {
			matchCode = 'test match code result';
			return this;
		}
		
		public override String getToken() {
			return null;
		}
		
		public override String getType() {
			return null;
		}
	}
}