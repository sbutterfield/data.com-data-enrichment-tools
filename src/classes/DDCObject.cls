/**
 *  Virtual object container that defines the basic characteristics of a DDC Object (Company/Contact)
 *  Provides a signature and the underlying control structure for accessing data from a parent (DDCContact/DDCCompany)
 *  All Api objects from Data.com extend this class
 */
global virtual class DDCObject {
    
    public Map<String, Object> objMap; // Value instead of Object should be MappedField in final implementation! <----- FIXME: type=Object for lexis nexis only
    public String container; // The api tag container if any
    public Set<String> sequence; // The sequence for the DDC Api object this instance represents

    // Universal property for all DDCObject
    public String externalKey {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_EXTERNAL_KEY); }
        set { objMap.put(ApiUtils.TAG_NAME_EXTERNAL_KEY, value); }
    }

    /**
        FIXME :: Match scoring and weighting should be handled by a separate de-coupled architecture!
     */
     public Double score { 
        get; set;
     }

    public DDCObject(String container, Set<String> sequence, Map<String, Object> objMap, String externalKey) {
        this.objMap = new Map<String, Object>();
        this.container = container;
        this.sequence = sequence;
        this.objMap = objMap;
        this.externalKey = externalKey;
    }
    
    public DDCObject(String container, Set<String> sequence) {
        this.objMap = new Map<String, Object>();
        this.container = container;
        this.sequence = sequence;
    }
    
    public DDCObject() {
        // default constructor
        this.objMap = new Map<String, Object>();
    }
    
    public Type getType() {
        return Type.forName(this.getName());
    }
    
    public virtual String getName() {
        return 'DDCObject';
    }
    
    // To-Do: Implement a lot more behavior in final version:
    //     > isEmpty() checks to see if map value is instanceof MappedField
    //     > toXml() writes the object to Xml document
    //     > parse() parses the object from an Xml document

}