public interface Describes {

    Descriptor getDescribe();
    String getToken();
    String getType();
}