/**
 *  Wrapper for the DDCProperty__c concrete SObject
 *  Mostly encapsulates additional behaviors for DDCProperty, not generally used as an Apex object reference
 *  throughout normal runtimes.
 */
 
public class DDCProperty {
	
	public String id { get; set; }
	public String name { get; set; }
	public String ptype { get; set; }
	public String dtype { get; set; }
	public Boolean isEnterprise { get; set; }
	
	/* Wasn't using this, commenting out for now
	public DDC_Property__c cProperty { get; set; }
	*/

	private DDCProperty(String id, String name, String ptype, String dtype, Boolean isEnterprise) {
		this.id = id;
		this.name = name;
		this.ptype = ptype;
		this.dtype = dtype;
		this.isEnterprise = isEnterprise;
	}

	private DDCProperty() {
		// Default constructor is constrained to use within DDCProperty
	}
	
	/**
	 *	Localized cache of properties, returned in the event ddc properties have already
	 *	been constructed for this runtime.
	 */
	private static Map<String, Map<String, DDCProperty>> PROPERTIES_CACHE { 
		get {
			if (PROPERTIES_CACHE == null) {
				System.debug( 'Init Properties Cache' );
				PROPERTIES_CACHE = new Map<String, Map<String,DDCProperty>>();
			} 
			return PROPERTIES_CACHE;
		}
	}
	
	/**
	 * @param input = a valid ddcobject name (Contact or Company)
	 * @returns: Map<String(DDCType "Contact" or "Company"), Map<String(property name), DDCProperty(Wrapper for the complete property to reference)>>
	 */
	public static Map<String, Map<String, DDCProperty>> getProperties(String input) {
		Map<String, Map<String, DDCProperty>> result = new Map<String, Map<String, DDCProperty>>();
		
		if(PROPERTIES_CACHE != null && PROPERTIES_CACHE.keySet().size() > 0) {
			// Cache already loaded, just return it.
			return PROPERTIES_CACHE;
		}
		
		String soql;
		Map<String, Schema.SObjectField> fmap = Utils.getFieldListing(MappedField.DDCPROPERTY_SOBJECT_NAME);
		try {
			soql = 'SELECT ';
			for(Schema.SObjectField s : fmap.values()) {
				soql += s + ',';
			}
			
			// Trim off last comma so we don't throw query exceptions
			soql = soql.removeEnd(',');
			
			// Add the FROM clause
			soql += ' FROM ' + MappedField.DDCPROPERTY_SOBJECT_NAME;
			
			// Add the WHERE clause if there is an input. If not, it will just fetch every property of all types.
			if(input != null) {
				soql += ' WHERE ddcDet__Property_Type__c = \'' + input + '\'';
			}
			
			List<ddcDet__DDC_Property__c> qResult = Database.query(soql);
			
			if(qResult != null) {
				// Build the inner map
				Map<String, DDCProperty> contactInner = new Map<String, DDCProperty>();
				Map<String, DDCProperty> companyInner = new Map<String, DDCProperty>();
				Map<String, DDCProperty> sharedInner = new Map<String, DDCProperty>();
				for(ddcDet__DDC_Property__c r : qResult) {
					DDCProperty p = new DDCProperty();
					if(r.ddcDet__Property_Type__c == 'Contact') {
						// Add to contactInner
						p.id = r.Id;
						p.name = r.Name;
						p.ptype = r.ddcDet__Property_Type__c;
						p.dtype = r.ddcDet__Corresponding_Types__c;
						p.isEnterprise = r.ddcDet__DnB_Enterprise__c;
						
						/* Wasn't using this, commenting out for now
						p.cProperty = r;
						*/
						
						contactInner.put(r.Name, p);
					}
					else if(r.ddcDet__Property_Type__c == 'Company') {
						// Add to companyInner
						p.id = r.Id;
						p.name = r.Name;
						p.ptype = r.ddcDet__Property_Type__c;
						p.dtype = r.ddcDet__Corresponding_Types__c;
						p.isEnterprise = r.ddcDet__DnB_Enterprise__c;
						
						/* Wasn't using this, commenting out for now
						p.cProperty = r;
						*/
						
						companyInner.put(r.Name, p);
					}
					/**
					 *	Added below to handle adding shared properties and stuff on-the-fly
					 *	10/28/2013 - sbutterfield
					 */
					else {
						// Shared property type

						p.id = r.Id;
						p.name = r.Name;
						p.ptype = r.ddcDet__Property_Type__c;
						p.dtype = r.ddcDet__Corresponding_Types__c;
						p.isEnterprise = r.ddcDet__DnB_Enterprise__c;

						sharedInner.put(r.Name, p);
					}
				}
				// Construct final mapping
				result.put('Contact', contactInner);
				result.put('Company', companyInner);
				result.put('Shared', sharedInner);

				// Set the local cache for future use
				PROPERTIES_CACHE = result;
			}
		}
		catch(System.QueryException qe) {
			// do exception handling here.
			System.debug('--!!! Exception ocurred while getting all properties from the database !!!--  ' +qe.getMessage()+qe.getStackTraceString());
		}
		return result;
	}

}