public class AsynchInstallNotifier {

	@future(callout=true) 
	public static void notifyLMA() {
		// Do callout to notify license manager REST Servlet
		// Should do initial license creation within subscriber org here after talking to LMA
		/* Realize that LMA may not have our license yet. The REST Servlet should tell us what is going on, if it's not ready yet we will have to schedule something for a future runtime. */
	}
}