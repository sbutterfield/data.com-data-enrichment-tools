/**
 *  The runtime mediator handles channels of runtimes based on configurable rules.
 */
public without sharing class RuntimeMediator {

    @future(Callout=true)
    public static void asynchMatchTriggered(Set<Id> ids, String objectName) {
        if (ids == null || objectName == null) {
            throw new ddcLib1.NullPointerException(
                'Unable to process asynchronous request. Expected non-null parameters for ids and object!'
            );
        }

        if (ids.size() > 8) {
            ddcLib1.Runtime.IS_TRIGGERED_CONTEXT = true;
        }
        else {
            ddcLib1.Runtime.IS_BATCH_CONTEXT = true;
        }

        RuntimeMediator.executeDETRuntime(ids, objectName);
    }

    /**
     *  Encapsulates all runtime logic for DET Batch and Asynch runtimes
     */
     public static void executeDETRuntime(Set<Id> ids, String objectName) {
        
        if (ids == null || objectName == null) {
            throw new ddcLib1.NullPointerException(
                'Unable to process asynchronous request. Expected non-null parameters for ids and object!'
            );
        }

        ApexPages.Message[] messages = new List<ApexPages.Message>();

        Map<String, Schema.SObjectField> fields = Utils.getFieldListing(objectName);

        Sobject[] sobjects;

        if (fields != null) {
            String soql = 'SELECT ';

            for (Schema.SObjectField fName : fields.values()) {
                soql += fName + ',';
            }
            
            // Trim off last comma so we don't throw query exceptions
            soql = soql.removeEnd(',');
            
            // Add the FROM clause
            soql += ' FROM ' + objectName;

            soql += ' WHERE Id IN :ids';
            
            sobjects =  Database.query(soql);
        }

        if (sobjects == null || sobjects.size() < 1) {

            throw new ddcLib1.FatalException('Query returned no results! Unable to continue.');
        }

        List<Wrapper> wrappers = new List<Wrapper>();

        // Inspect the type of sobjects we got
        Schema.SObjectType stype = sobjects[0].getSobjectType();

        // Bind to the correct DDCType
        DDCType thisType = Utils.getTypeBinding(sobjects[0].getSobjectType());

        // A flag so we know if expected fields exist
        Boolean isSupported = false;

        // Need to wrap based on type
        for (SObject s : sobjects) {

            if (stype == Lead.SobjectType) {
                isSupported = true;
                wrappers.add(new LeadWrapperDecorator(new SObjectWrapper(s, DDCType.CONTACT)));
            }
            else if(stype == Contact.SobjectType) {
                isSupported = true;
                wrappers.add(new ContactWrapperDecorator(new SObjectWrapper(s, DDCType.CONTACT)));
            }
            else if(stype == Account.SobjectType) {
                isSupported = true;
                wrappers.add(new AccountWrapperDecorator(new SObjectWrapper(s, DDCType.COMPANY)));
            }
        }

        // Unknown or unsupported sobject type. We can still process it.
        /**
         *  TO-DO :: Unknown sobjects will need to have their own instance of wrapper decorator.
         *  We should still be able to support this by fetching the desired decorator type from a
         *  custom setting. Need to implement some behavior here or figure out how to implement
         *  this abstract behavior outside of RuntimeMediator...
         */
        if (!isSupported) {
           throw new ddcLib1.UnsupportedObjectTypeException('Could not wrap Sobject, unsupported object type: ' +stype);
        }

        /**
         *  Do Api Matching first.
         *  1) Matcher-Api
         *  2) DUNSRight Api
         *
         *  After that, do look-up services
         *  3) Duns Number
         *  4) Company/Contact Id
         *
         *  Finally, throw the unmatched records at Fuzzy Matcher (invoke batch)
         *  5) Fuzzy Matcher
         */

        // (1)
        DDCApi.match(wrappers, messages);

        // (2)
        // TO-DO: Implement

        // (3)
        DDCApi.getDunsMatches(wrappers, messages);

        // (4)
        // TO-DO: Implement

System.debug('Wrapper[0] ddcMatch after DDCApi.match() call :: ' +wrappers[0].getDdcMatch());

        // (5)
        // Try and get matches based on generalization.
        // FuzzyMatcher will handle if generalization is enabled/disabled and will not override
        // a DDCApi match that may already exist on the wrapper.
        // Here we are not invoking a batchable context because Fuzzy Matcher is small for now (11/11/2013)
        FuzzyMatcher.getGeneralizedMatches(wrappers);
        FuzzyMatcher.getDomainMatches(wrappers);

        Wrapper[] reprocessMe = new List<Wrapper>();
        Map<String,Wrapper> finalWrappers = new Map<String,Wrapper>();

        if (wrappers[0].getScenario().thisMapping == null) return;
        
        for (Wrapper w : wrappers) {
            finalWrappers.put( (String) w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EXTERNAL_KEY).mappedFieldName), w);
            if (w.getDdcMatch() == null) {
                // re-wrap as a company instead.
                LeadWrapperDecorator lwd = new ddcDet.LeadWrapperDecorator(new SObjectWrapper(w.getSObject(), DDCType.COMPANY));
                reprocessMe.add(lwd);
            }
        }

        if (reprocessMe.size() > 0) {
            // Send Leads as Companies for match
            DDCApi.match(reprocessMe, messages);
            //wrappers.addall(reprocessMe);
            for (Wrapper w : reprocessMe) {
                if (w.getDdcMatch() != null) {
                    finalWrappers.put( (String) w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EXTERNAL_KEY).mappedFieldName), w);
                }
            }
        }


        // Iterate over the wrappers and set the concrete fields based on mapping, if a match was assigned
        for (Wrapper w : finalWrappers.values()) {
            if (w.getDdcMatch() == null || w.getDdcMatch().objMap == null) {
                continue;
            }
            // Fetch the scenario
            MatchScenario wScenario = w.getScenario();
            
            if (wScenario == null && wScenario.thisMapping == null) {
                // Skip and keep going if the scenario is null or the mappings are null.
                continue;
            }

            // Exclude non-D&B Matches if asked to
            if (w.getScenario().thisSetting.dnbMatchesOnly) {

                if (w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_DUNS_NUMBER) == null ||
                    String.valueOf(w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_DUNS_NUMBER)).length() == 0) {
                    // If there is no DUNS Number, skip this match.
                    System.debug('Skipping wrapper because it did not have a duns number: ' +w);
                    continue;
                }
            }

            if (w.getScenario().thisSetting.useGlobalUltimate) {
                // Only run this if the global ultimate is not already assigned.
                if (w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_DUNS_NUMBER) != w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER)) {
                    // Global ultimate is on for this wrapper.
                    // Fido, go get the data boy!
                    DDCCompany globalUltimate = DDCApi.getDetailsForDuns( (String) w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER), messages);

                    if (globalUltimate != null) {
                        // Good boy, Fido!
                        // Smash global ultimate stuff back in to the matched ddc objMap
                        w.getDdcMatch().objMap.putAll(globalUltimate.objMap);
                    }
                }
            }

            // Retrieve the sobject
            SObject s = w.getSObject();
            for (MappedField mf : wScenario.thisMapping.values()) {
                if (mf.mappedFieldName != null) {
                    // If overwrite, push the property
                    if (mf.overwrite) {
                        Utils.setSobjectField(s, mf, w.getDdcMatch());
                    }
                    // Else if it is fill
                    else if (mf.fill) {
                        Object val = s.get(mf.mappedFieldName); // Check the existing value
                        if (val == null) {
                            // Only push if for the mapped field it is currently null
                            Utils.setSobjectField(s, mf, w.getDdcMatch());
                        }
                    }
                }
            }
            s.put(SystemField.MATCH_SCENARIO_FIELD, wScenario.scenarioName);
            s.put(SystemField.MATCH_STATUS_FIELD, 'Matched!');
            s.put(SystemField.MATCH_CODE_FIELD, 'AD0' + wScenario.matchConfidenceThreshold);
            s.put(SystemField.LAST_MATCHED_FIELD, System.now());
            System.debug('Completed updates. SObject from wrapper :: ' +w.getSObject().get('ddcDet__MatchScenario__c'));
        }

        // Set the standard CleanStatus field for each wrapper being processed
        setCleanStatus(finalWrappers.values());

        // Find and associate D&B Company object to wrappers if exists
        setDandBCompany(finalWrappers.values());

        // Set global runtime variable so we don't call triggers again
        ddcLib1.Runtime.IS_FUTURE_CONTEXT = true;

        // Send the finalized wrappers for an Update.
        Boolean saveSuccess = DMLMediator.updateWrappers(finalWrappers.values());

        if (!saveSuccess) {
            
            throw new ddcLib1.FatalException('Unable to complete commit to database on wrapper(s) :: ' +wrappers);
        }
     }

    /**
     *  Populates the look-up to D&B Company object for matched wrappers
     *  where relationship does not already exist and DunsNumber is not-null.
     *  This should be called after all matching has already been done.
     *  @param = Wrapper[] wrappers (a collection of any wrapper type)
     *  @return = void, operates directly on wrappers
     *  @throws = none
     */
    private static void setDandBCompany(Wrapper[] wrappers) {

        if (wrappers == null) {
            return;
        }

        // Mapping of dunsNumber --> Wrappers for that duns
        Map<String, Wrapper[]> dunsMapping = new Map<String, Wrapper[]>();

        // All matched wrappers, following logic will build dunsMapping
        for (Wrapper w : wrappers) {

            if (w.getDdcMatch() == null) {
                // No match was found for the wrapper. Skip it.
                continue;
            }

            DDCObject matchedObject = w.getDdcMatch();
            String dunsNumber = (String) matchedObject.objMap.get(ApiUtils.TAG_NAME_DUNS_NUMBER);

            if (dunsNumber == null) {
                // No duns to try and get D&B Company from database.
                continue;
            }
            
            Wrapper[] existing = dunsMapping.get(dunsNumber);
            
            if (existing == null) {
                
                dunsMapping.put(dunsNumber, new Wrapper[] {w});
            }
            else {
                
                existing.add(w);
                dunsMapping.put(dunsNumber, existing);
            }
        }

        if (dunsMapping == null) {
            // Nothing in the map means there are no D&B company assignments that are possible.
            return;
        }

        Set<String> keys = dunsMapping.keySet();
        String soql = 'SELECT Id,DunsNumber FROM DandBCompany WHERE DunsNumber IN :keys';
        List<SObject> dnbCompanies;
        try {
            dnbCompanies = Database.query(soql);
        }
        catch (Exception e) {
            // will happen if DandBCompany object is not available.
            System.debug('No DandBCompany object in this org!....');
        }

        if (dnbCompanies == null) {
            // No dnbcompanies came back from the query, nothing to try and do.
            return;
        }

        for (SObject s : dnbCompanies) {

            Wrapper[] relWrappers = dunsMapping.get((String) s.get('DunsNumber'));

            for (Wrapper w : relWrappers) {

                try{
                    w.getSObject().put('DandBCompanyId', s.Id);
                }
                catch (SObjectException se) {
                    System.debug('Error setting DandBCompanyId, field may not exist on this wrapper! :: ' +se.getMessage()+ '  ' +se.getStackTraceString());
                }
            }
        }
    }

    /**
     *  Method attempts to set the Clean Status field when a DET match is found
     */
     private static void setCleanStatus(Wrapper[] wrappers) {
        if (wrappers == null) {
            return;
        }

        for (Wrapper w : wrappers) {
            if (w.getSObject() == null) {
                // No concrete underlying object, skip it.
                continue;
            }
            // Pull out the concrete object
            SObject thisObj = w.getSObject();
            // Wrappers with a match
            if (w.getDdcMatch() != null) {
                System.debug('Existing CleanStatus value :: ' +thisObj.get(SystemField.CLEAN_STATUS_FIELD));
                // Check to see if the match is "inactive"
                Boolean isDead;
                if (w.getType() == DDCType.CONTACT) {
                    if (w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_GRAVEYARD_STATUS) != null) { // NULL CHECK!
                        isDead = Boolean.valueOf(w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_GRAVEYARD_STATUS));
                        // Set clean status accordingly
                        thisObj.put(SystemField.CLEAN_STATUS_FIELD, isDead ? 'Inactive' : 'Acknowledged');
                    }
                }
                else if (w.getType() == DDCType.COMPANY) { // Company uses a different graveyard tag
                    if (w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_GRAVEYARD_STATUS) != null) { // NULL CHECK!
                        isDead = Boolean.valueOf(w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_GRAVEYARDED));
                        // Set clean status accordingly
                        thisObj.put(SystemField.CLEAN_STATUS_FIELD, isDead ? 'Inactive' : 'Acknowledged');
                    }
                }
            }
            // Wrappers without a match
            else {
                // Mark as "Not Found" instead of "Not Compared"
                thisObj.put(SystemField.CLEAN_STATUS_FIELD, 'NotFound');
            }
        }
     }
}