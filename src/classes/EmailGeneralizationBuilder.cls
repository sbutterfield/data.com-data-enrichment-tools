public class EmailGeneralizationBuilder {

    private String[] emailGeneralizations = new List<String>();
    private String[] generalizations = new List<String>();
    
    private final String LOCALE = 'en_US';

    private String s1 { get; set; }
    private String s2 { get; set; }
    private String domain { get; set; }
    
    private EmailGeneralizationBuilder() {}
    
    public EmailGeneralizationBuilder(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }
    
    public EmailGeneralizationBuilder(String s1, String s2, String domain) {
        this.s1 = s1;
        this.s2 = s2;
        this.domain = domain;
    }
    
    public String[] getEmailGeneralizations() {
        
        if (this.s1 == null || this.s2 == null || this.domain == null) {
        
            throw new ddcLib1.NullPointerException('Unable to perform emailGeneralizations with null arguments: s1= ' +s1+ ' s2= ' +s2+ ' domain= ' +domain);
        }

        this.emailGeneralizations = getEmailGeneralizations(this.s1, this.s2, this.domain);
        return emailGeneralizations;
    }

    public String[] getGeneralizations() {

        if (this.s1 == null || this.s2 == null) {
        
            throw new ddcLib1.NullPointerException('Unable to perform emailGeneralizations with null arguments: s1= ' +s1+ ' s2= ' +s2+ ' domain= ' +domain);
        }

        this.generalizations = getGeneralizations(this.s1, this.s2);
        return generalizations;
    }

    private String[] getEmailGeneralizations(String s1, String s2, String domain) {
        // both strings are not null, do processing... Example: s1=Bob s=Memmer
        emailGeneralizations.add(s1.toLowerCase(LOCALE) + s2.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "bobmemmer"
        emailGeneralizations.add(s1.toLowerCase(LOCALE) + '.' + s2.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "bob.memmer"
        emailGeneralizations.add(s1.toLowerCase(LOCALE) + '_' + s2.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // bob_memmer
        emailGeneralizations.add(s1.substring(0,1).toLowerCase(LOCALE) + s2.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "bmemmer"
        emailGeneralizations.add(s1.substring(0,1).toLowerCase(LOCALE) + '_' + s2.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "b_memmer"
        //emailGeneralizations.add(s1.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "bob"
        //emailGeneralizations.add(s1.toLowerCase(LOCALE) + s2.substring(0,1).toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "bobm"
        //emailGeneralizations.add(s2.toLowerCase(LOCALE) + s1.substring(0,1).toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "memmerb"
        //emailGeneralizations.add(s2.toLowerCase(LOCALE) + s1.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "memmerbob"
        //emailGeneralizations.add(s2.toLowerCase(LOCALE) + '.' + s1.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "memmer.bob"
        //emailGeneralizations.add(s2.toLowerCase(LOCALE) + '@' + domain.toLowerCase(LOCALE)); // "memmer"

        return emailGeneralizations;
    }

    private String[] getGeneralizations(String s1, String s2) {
        // both strings are not null, do processing... Example: s1=Bob s=Memmer
        Integer s1len = s1.length();
        Integer s2len = s2.length();
        generalizations.add(s1.toLowerCase(LOCALE) + s2.toLowerCase(LOCALE)); // "bobmemmer"
        generalizations.add(s1.toLowerCase(LOCALE) + '.' + s2.toLowerCase(LOCALE)); // "bob.memmer"
        generalizations.add(s1.toLowerCase(LOCALE) + '_' + s2.toLowerCase(LOCALE)); // bob_memmer
        generalizations.add(s1.substring(0,1).toLowerCase(LOCALE) + '_' + s2.toLowerCase(LOCALE)); // "b_memmer
        generalizations.add(s1.substring(0,1).toLowerCase(LOCALE) + s2.toLowerCase(LOCALE)); // "bmemmer"

        //generalizations.add(s1.toLowerCase(LOCALE)); // "bob"
        //generalizations.add(s1.toLowerCase(LOCALE) + s2.substring(0,1).toLowerCase(LOCALE)); // "bobm"
        //generalizations.add(s2.toLowerCase(LOCALE) + s1.toLowerCase(LOCALE)); // "memmerbob"
        //generalizations.add(s2.toLowerCase(LOCALE) + '.' + s1.toLowerCase(LOCALE)); // "memmer.bob"
        generalizations.add(s2.toLowerCase(LOCALE)); // "memmer"
        //generalizations.add(s2.toLowerCase(LOCALE) + s1.substring(0,1).toLowerCase(LOCALE)); // "memmerb"

        return generalizations;
    }
}