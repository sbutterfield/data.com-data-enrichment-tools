public class MatcherField {
	
	//---------------------------------------------------------------------------------------------
	// Fields
	//---------------------------------------------------------------------------------------------
	
    public Object value;
    public Boolean diff;
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public MatcherField(Object value, Boolean diff) {
        this.value = value;
        this.diff = diff;
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    public static Boolean parseDiff(Dom.XmlNode fieldNode) {
        // Check for diff attribute, assume false if not present.
        Boolean diff = false;
        if (fieldNode != null) {
            String attribute = fieldNode.getAttribute(MatcherAPI.TAG_ATTRIBUTE_DIFF, null);
            if (attribute != null) {
                diff = Boolean.valueOf(attribute);
            }
        }
        
        return diff;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testMatcherField() {
    	String expectedValue = 'Expected';
    	Boolean expectedDiff = true;
        MatcherField field = new MatcherField(expectedValue, expectedDiff);
        System.assertEquals(expectedValue, field.value);
        System.assertEquals(expectedDiff, field.diff);
    }
    
    private static testmethod void testParseDiff() {
    	Boolean expectedDiff = true;
    	Dom.Document doc = new Dom.Document();
    	Dom.XmlNode node = doc.createRootElement('field', null, null);
    	node.setAttribute('diff', String.valueOf(expectedDiff));
    	node.addTextNode('value');
    	System.assertEquals(expectedDiff, parseDiff(node));
    }
    
}