public class DDCCompany extends DDCObject implements Comparable {

    
    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------

    /* Properties of DDCCompany */
    public String companyId {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COMPANY_ID); }
        set { objMap.put(ApiUtils.TAG_NAME_COMPANY_ID, value); }
    }
    public String companyName {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COMPANY_NAME); }
        set { objMap.put(ApiUtils.TAG_NAME_COMPANY_NAME, value); }
    }
    public String phone {
        get { return (String) objMap.get(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_PHONE)); }
        set { objMap.put(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_PHONE), value); }
    }
    public String address {
        get { return (String) objMap.get(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS)); }
        set { objMap.put(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS), value); }
    }
    public String city {
        get { return (String) objMap.get(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_CITY)); }
        set { objMap.put(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_CITY), value); }
    }
    public String state {
        get { return (String) objMap.get(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_STATE)); }
        set { objMap.put(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_STATE), value); }
    }
    public String zip {
        get { return (String) objMap.get(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_ZIP)); }
        set { objMap.put(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_ZIP), value); }
    }
    public String country {
        get { return (String) objMap.get(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_COUNTRY)); }
        set { objMap.put(ApiUtils.COMPANY_PROPERTIES_ENCODING.get(ApiUtils.TAG_NAME_ADDRESS_COUNTRY), value); }
    }
    public String website {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_WEBSITE); }
        set { objMap.put(ApiUtils.TAG_NAME_WEBSITE, value); }
    }
    public String stockSymbol {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_STOCK_TICKER); }
        set { objMap.put(ApiUtils.TAG_NAME_STOCK_TICKER, value); }
    }
    public String stockExchange {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_STOCK_EXCHANGE); }
        set { objMap.put(ApiUtils.TAG_NAME_STOCK_EXCHANGE, value); }
    }
    public Double revenue {
        get {
            if (objMap.get(ApiUtils.TAG_NAME_REVENUE) == null) {
                return Double.valueOf('0');
            }
            return Double.valueOf(objMap.get(ApiUtils.TAG_NAME_REVENUE));
        }
        set { objMap.put(ApiUtils.TAG_NAME_REVENUE, value); }
    }
    public String revenueRange {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_REVENUE_RANGE); }
        set { objMap.put(ApiUtils.TAG_NAME_REVENUE_RANGE, value); }
    }
    public Double employeeCount {
        get {
            if (objMap.get(ApiUtils.TAG_NAME_EMPLOYEE_COUNT) == null) {
                return Double.valueOf('0');
            }
            return Double.valueOf(objMap.get(ApiUtils.TAG_NAME_EMPLOYEE_COUNT));
        }
        set { objMap.put(ApiUtils.TAG_NAME_EMPLOYEE_COUNT, value); }
    }
    public String employeeRange {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_EMPLOYEE_RANGE); }
        set { objMap.put(ApiUtils.TAG_NAME_EMPLOYEE_RANGE, value); }
    }
    public String ownership {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_OWNERSHIP); }
        set { objMap.put(ApiUtils.TAG_NAME_OWNERSHIP, value); }
    }
    public String fortuneRank {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FORTUNE_RANK); }
        set { objMap.put(ApiUtils.TAG_NAME_FORTUNE_RANK, value); }
    }
    public Boolean graveyarded {
        get {
            System.debug('-Getter method for TAG_NAME_GRAVEYARDED-');
            if (objMap.get(ApiUtils.TAG_NAME_GRAVEYARDED) == null) {
                // Always return something, assume false if null
                return false;
            }
            return Boolean.valueOf(objMap.get(ApiUtils.TAG_NAME_GRAVEYARDED));
        }
        set { objMap.put(ApiUtils.TAG_NAME_GRAVEYARDED, value); }
    }
    public String industry1 {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_INDUSTRY1); }
        set { objMap.put(ApiUtils.TAG_NAME_INDUSTRY1, value); }
    }
    public String subIndustry1 {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SUB_INDUSTRY1); }
        set { objMap.put(ApiUtils.TAG_NAME_SUB_INDUSTRY1, value); }
    }
    public DateTime createdOn {
        get {
            return Utils.parseISODate(String.valueOf(objMap.get(ApiUtils.TAG_NAME_CREATED_ON)));
        }
        set { objMap.put(ApiUtils.TAG_NAME_CREATED_ON, value); }
    }
    public DateTime updatedDate { 
        get {
            return Utils.parseISODate(String.valueOf(objMap.get(ApiUtils.TAG_NAME_CREATED_ON)));
        }
        set { objMap.put(ApiUtils.TAG_NAME_UPDATED_DATE, value); }
    }
    public Integer activeContacts { 
        get {
            if (objMap.get(ApiUtils.TAG_NAME_ACTIVE_CONTACTS) == null) {
                return Integer.valueOf('0');
            }
            return Integer.valueOf(objMap.get(ApiUtils.TAG_NAME_ACTIVE_CONTACTS));
        }
        set { objMap.put(ApiUtils.TAG_NAME_ACTIVE_CONTACTS, value); }
    }
    public String companyWiki {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COMPANY_WIKI); }
        set { objMap.put(ApiUtils.TAG_NAME_COMPANY_WIKI, value); }
    }
    public String yearStarted {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_YEAR_STARTED); }
        set { objMap.put(ApiUtils.TAG_NAME_YEAR_STARTED, value); }
    }
    public String source {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SOURCE); }
        set { objMap.put(ApiUtils.TAG_NAME_SOURCE, value); }
    }
    public String dunsNumber {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_DUNS_NUMBER); }
        set { objMap.put(ApiUtils.TAG_NAME_DUNS_NUMBER, value); }
    }
    public String tradestyle {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_TRADESTYLE); }
        set { objMap.put(ApiUtils.TAG_NAME_TRADESTYLE, value); }
    }
    public String primarySic { 
        get { return (String) objMap.get(ApiUtils.TAG_NAME_PRIMARY_SIC); }
        set { objMap.put(ApiUtils.TAG_NAME_PRIMARY_SIC, value); }
    }
    public String primarySicDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_PRIMARY_SIC_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_PRIMARY_SIC_DESC, value); }
    }
    public String faxNumber {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FAX_NUMBER); }
        set { objMap.put(ApiUtils.TAG_NAME_FAX_NUMBER, value); }
    }
    public String locationStatus {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_LOCATION_STATUS); }
        set { objMap.put(ApiUtils.TAG_NAME_LOCATION_STATUS, value); }
    }
    public String latitude {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_LATITUDE); }
        set { objMap.put(ApiUtils.TAG_NAME_LATITUDE, value); }
    }
    public String longitude {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_LONGITUDE); }
        set { objMap.put(ApiUtils.TAG_NAME_LONGITUDE, value); }
    }
    public String shippingCity {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SHIPPING_CITY); }
        set { objMap.put(ApiUtils.TAG_NAME_SHIPPING_CITY, value); }
    }
    public String shippingCountryCode {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SHIPPING_COUNTRY_CODE); }
        set { objMap.put(ApiUtils.TAG_NAME_SHIPPING_COUNTRY_CODE, value); }
    }
    public String shippingState {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SHIPPING_STATE); }
        set { objMap.put(ApiUtils.TAG_NAME_SHIPPING_STATE, value); }
    }
    public String shippingAddress {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SHIPPING_ADDRESS); }
        set { objMap.put(ApiUtils.TAG_NAME_SHIPPING_ADDRESS, value); }
    }
    public String shippingZip {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SHIPPING_ZIP); }
        set { objMap.put(ApiUtils.TAG_NAME_SHIPPING_ZIP, value); }
    }
    public String internationalCode {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_INTERNATIONAL_CODE); }
        set { objMap.put(ApiUtils.TAG_NAME_INTERNATIONAL_CODE, value); }
    }
    public String metroArea {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_METRO_AREA); }
        set { objMap.put(ApiUtils.TAG_NAME_METRO_AREA, value); }
    }
    public String countryCode {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COUNTRY_CODE); }
        set { objMap.put(ApiUtils.TAG_NAME_COUNTRY_CODE, value); }
    }
    public String primaryNaics {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_PRIMARY_NAICS); }
        set { objMap.put(ApiUtils.TAG_NAME_PRIMARY_NAICS, value); }
    }
    public String primaryNaicsDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_PRIMARY_NAICS_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_PRIMARY_NAICS_DESC, value); }
    }
    public String companySynopsis {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COMPANY_SYNOPSIS); }
        set { objMap.put(ApiUtils.TAG_NAME_COMPANY_SYNOPSIS, value); }
    }
    public String countryAccessCode {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COUNTRY_ACCESS_CODE); }
        set { objMap.put(ApiUtils.TAG_NAME_COUNTRY_ACCESS_CODE, value); }
    }
    public String ownershipType {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_OWNERSHIP_TYPE); }
        set { objMap.put(ApiUtils.TAG_NAME_OWNERSHIP_TYPE, value); }
    }
    public String salesVolume {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SALES_VOLUME); }
        set { objMap.put(ApiUtils.TAG_NAME_SALES_VOLUME, value); }
    }
    public Integer employeesTotal {
        get {
            if (objMap.get(ApiUtils.TAG_NAME_EMPLOYEES_TOTAL) == null) {
                return Integer.valueOf('0');
            }
            return Integer.valueOf(objMap.get(ApiUtils.TAG_NAME_EMPLOYEES_TOTAL));
        }
        set { objMap.put(ApiUtils.TAG_NAME_EMPLOYEES_TOTAL, value); }
    }
    public Boolean outOfBusiness {
        get {
            System.debug('-Getter method for TAG_NAME_OUT_OF_BUSINESS-');
            if (objMap.get(ApiUtils.TAG_NAME_OUT_OF_BUSINESS) == null) {
                // Always return something, assume false if null
                return false;
            }
            return Boolean.valueOf(objMap.get(ApiUtils.TAG_NAME_OUT_OF_BUSINESS));
        }
        set { objMap.put(ApiUtils.TAG_NAME_OUT_OF_BUSINESS, value); }
    }
    public String secondSic {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SECOND_SIC); }
        set { objMap.put(ApiUtils.TAG_NAME_SECOND_SIC, value); }
    }
    public String secondSicDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SECOND_SIC_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_SECOND_SIC_DESC, value); }
    }
    public String thirdSic {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_THIRD_SIC); }
        set { objMap.put(ApiUtils.TAG_NAME_THIRD_SIC, value); }
    }
    public String thirdSicDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_THIRD_SIC_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_THIRD_SIC_DESC, value); }
    }
    public String fourthSic {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FOURTH_SIC); }
        set { objMap.put(ApiUtils.TAG_NAME_FOURTH_SIC, value); }
    }
    public String fourthSicDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FOURTH_SIC_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_FOURTH_SIC_DESC, value); }
    }
    public String secondNaics {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SECOND_NAICS); }
        set { objMap.put(ApiUtils.TAG_NAME_SECOND_NAICS, value); }
    }
    public String secondNaicsDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_SECOND_NAICS_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_SECOND_NAICS_DESC, value); }
    }
    public String thirdNaics {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_THIRD_NAICS); }
        set { objMap.put(ApiUtils.TAG_NAME_THIRD_NAICS, value); }
    }
    public String thirdNaicsDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_THIRD_NAICS_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_THIRD_NAICS_DESC, value); }
    }
    public String fourthNaics {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FOURTH_NAICS); }
        set { objMap.put(ApiUtils.TAG_NAME_FOURTH_NAICS, value); }
    }
    public String fourthNaicsDesc {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FOURTH_NAICS_DESC); }
        set { objMap.put(ApiUtils.TAG_NAME_FOURTH_NAICS_DESC, value); }
    }
    public Double employeesHere {
        get {
            if (objMap.get(ApiUtils.TAG_NAME_EMPLOYEES_HERE) == null) {
                return Double.valueOf('0');
            }
            return Double.valueOf(objMap.get(ApiUtils.TAG_NAME_EMPLOYEES_HERE));
        }
        set { objMap.put(ApiUtils.TAG_NAME_EMPLOYEES_HERE, value); }
    }
    public Double globalUltimateEmployees {
        get { return Double.valueOf(objMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_EMPLOYEES)); }
        set { objMap.put(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_EMPLOYEES, value); }
    }
    public String tradestyle2 {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_TRADESTYLE2); }
        set { objMap.put(ApiUtils.TAG_NAME_TRADESTYLE2, value); }
    }
    public String tradestyle3 {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_TRADESTYLE3); }
        set { objMap.put(ApiUtils.TAG_NAME_TRADESTYLE3, value); }
    }
    public String familyMembers {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FAMILY_MEMBERS); }
        set { objMap.put(ApiUtils.TAG_NAME_FAMILY_MEMBERS, value); }
    }
    public String globalUltimateDunsNumber {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER); }
        set { objMap.put(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER, value); }
    }
    public String globalUltimateBusinessName {
        get { 
            if (objMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_BUSINESS_NAME) == null) {
                return String.valueOf('-not specified-');
            }
            return String.valueOf(objMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_BUSINESS_NAME));
        }
        set { objMap.put(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_BUSINESS_NAME, value); }
    }
    public String domesticUltimateDunsNumber {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_DUNS_NUMBER); }
        set { objMap.put(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_DUNS_NUMBER, value); }
    }
    public String domesticUltimateBusinessName {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_BUSINESS_NAME); }
        set { objMap.put(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_BUSINESS_NAME, value); }
    }
    public String parentOrHqDunsNumber {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_PARENT_OR_HQ_DUNS_NUMBER); }
        set { objMap.put(ApiUtils.TAG_NAME_PARENT_OR_HQ_DUNS_NUMBER, value); }
    }
    public String parentOrHqBusinessName {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_PARENT_OR_HQ_BUSINESS_NAME); }
        set { objMap.put(ApiUtils.TAG_NAME_PARENT_OR_HQ_BUSINESS_NAME, value); }
    }
    public String fipsMsaCode {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FIPS_CODE); }
        set { objMap.put(ApiUtils.TAG_NAME_FIPS_CODE, value); }
    }
    public Boolean subsidiary {
        get {
            System.debug('-Getter method for TAG_NAME_SUBSIDIARY-');
            if (objMap.get(ApiUtils.TAG_NAME_SUBSIDIARY) == null) {
                // Always return something, assume false if null
                return false;
            }
            return Boolean.valueOf(objMap.get(ApiUtils.TAG_NAME_SUBSIDIARY));
        }
        set { objMap.put(ApiUtils.TAG_NAME_SUBSIDIARY, value); }
    }

    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------

    // Construct new DDCCompany from Xml node
    public DDCCompany(Dom.XmlNode xml) {
        super(ApiUtils.TAG_NAME_COMPANY, ApiUtils.SEQUENCE_COMPANY);
        parse(xml);
    }
    
    public DDCCompany() {
        // default constructor
        super(ApiUtils.TAG_NAME_COMPANY, ApiUtils.SEQUENCE_COMPANY);
    }
    

    //---------------------------------------------------------------------------------------------
    // API
    //---------------------------------------------------------------------------------------------
    
    /* Signature required by Comparable interface */
    public Integer compareTo(Object input) {
        // Default sort implementation
        return compareTo(input, 'companyId', false);
    }
    
    // TO-DO: Provide a better implementation: http://goo.gl/MBQ3kc
    // Actually, this should be done by the API for shits sakes.... Dumbass.
    /* Override Comparable behavior using an overloaded signature */
    public Integer compareTo(Object input, String a, Boolean descending) {
        if (input == null || a == null || descending == null) {
            return null;
        }
        
        // Cast the input to a local variable
        DDCCompany c = (DDCCompany) input;
        
        Boolean defaultSort = false;
        if (a == 'activeContacts') {
            defaultSort = true;
        }
        
        if (defaultSort) {
            // using activeContacts
            Integer i = Integer.valueOf(this.activeContacts);
            Integer j = Integer.valueOf(c.activeContacts);
            if (i == j) {
                return 0;
            }
            // Default sort order is desc so j > i
            else if (j > i) {
                return 1;
            }
            else {
                return -1;
            }
        }
        else {
            // Implement using "a" = attribute to sort by.
            // Only allows certain sorting
            if (a == 'companyId') {
                Integer i = Integer.valueOf(this.companyId);
                Integer j = Integer.valueOf(c.companyId);
                if (i == j) {
                    return 0;
                }
                else if (i > j) {
                    return 1;
                }
                else {
                    return -1;
                }
            }
            else if (a == 'companyName') {
                if (this.companyName == c.companyName) {
                    return 0;
                }
                else if (this.companyName > c.companyName) {
                    return 1;
                }
                else {
                    return -1;
                }
            }
            else if (a == 'employeeCount') {
                if (c.employeeCount == null) {
                    return -1;
                }
                // descend in to DDCContact.contactCompany.activeContacts
                if (this.employeeCount == c.employeeCount) {
                    return 0;
                }
                else if (this.employeeCount > c.employeeCount) {
                    return 1;
                }
                else { 
                    return -1;
                }
            }
        }
        return null;
    }

    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------

    // Overridden method from super to return this instances type (DDCCompany)
    public override String getName() {
        return 'DDCCompany';
    }
    
    /* Parses a DDC Company object from an XML document */
    private void parse(Dom.XmlNode cnode) {
        
        /**
         *  For each tag in the sequence, pull out the content for that tag from the xml.
         *  Put extracted content in to a map: Map<String(TAG_NAME_VALUE{ddcproperty}),Object(Value of parsed result for the tag)>
         *  Then, iterate over all the keys and construct the DDCContact with the known attribute name that we would get from the tag.
         */
         
        Dom.XmlNode node;
        for (String tname : sequence) {
            
            node = cnode.getChildElement(tname, null);
            String equiv = ApiUtils.COMPANY_PROPERTIES_ENCODING.get(tname);
            if (node != null) {

                objMap.put(equiv != null ? equiv : tname, node.getText());
            }
            else {
                // Still add the property to the map otherwise the getter for
                // the property on this object will not be able to return a value at all from objMap
                objMap.put(equiv != null ? equiv : tname, null);
            }
        }
    }

}