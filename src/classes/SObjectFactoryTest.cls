@isTest(SeeAllData=true)
private class SObjectFactoryTest {

	static {
		SObjectFactory.FillAllFields = true; // increase test coverage without creating custom objects
	}  
	
	static testMethod void createsSingleObjectWithSingleRequiredStringField() {
		Account account = (Account)SObjectFactory.createSObject('Account');
		insert account;
		System.assert(account.Id != null);
		System.assert(account.Name != null);
	}
	
	static testMethod void createsObjectWithCascadeSetsLookupField() {
		Contact contact = (Contact)SObjectFactory.createSObject('Contact', true);
		insert contact;
		System.assert(contact.Id != null);
		System.assert(contact.AccountId != null);
	}
	
	static testMethod void createsObjectWithoutCascadeDoesNotSetLookupField() {
		Contact contact = (Contact)SObjectFactory.createSObject('Contact', false);
		insert contact;
		System.assert(contact.AccountId == null);
	}
	
	static testMethod void createObjectWithUnsupportedTypeThrowsException() {
		try {
			SObjectFactory.createSObject('Unsupported');
			System.assert(false);
		} catch (ddcLib1.UnsupportedObjectTypeException ex) {
			System.assert(true);
		}
	}
	
    /**
	 * To-Do: Re-enable when bulk match mediator is in place.
	 *
	static testMethod void creating200UniqueRecords() {
		
		List<SObject> contactsAsSObjects = SObjectFactory.createSObjectList('Contact', false, 200);
		insert contactsAsSObjects;
		for (SObject s : contactsAsSObjects)
		{
			Contact c = (Contact) s;
			System.assert(c.Id != null);
		}
	}
	*/
}