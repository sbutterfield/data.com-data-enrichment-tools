/**
 *  Descriptor - abstract class that provides the signature and implementation requirements for any given class to describe itself.
 *  Originally designed for Matchable objects to tell their requestor about how they matched, scored, calculated and which passes it was allowed
 *  to go through etc.
 *  Also used for a requestor to run a describe prior to casting the class and determine what it should be, or assert that it is what it says it is.
 *  @author: Shawn Butterfield
 */
public abstract class Descriptor implements Describes {

    String TOKEN;
    String TYPE;
    
    // Required signature. All classes capable of description should return an instance of themselves as a Descriptor.
    public abstract Descriptor getDescribe();
    
    // All descriptors should have accessors specifically for their token and type
    public abstract String getToken();
    public abstract String getType();
}