public virtual class MatchedAccount extends MatchedObject {
	
	//---------------------------------------------------------------------------------------------
    // Fields
    //---------------------------------------------------------------------------------------------
	
	public String
	name,
	website,
	street,
	city,
	state,
	zip,
	country,
	phone,
	tickerSymbol;
	
	
	//---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
	
	Account[] matches;
	
	
	//---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
	
	private MatchedAccount() {
		
	}

	public MatchedAccount(
	String name,
	String website,
	String street,
	String city,
	String state,
	String zip,
	String country,
	String phone,
	String tickerSymbol ) {
		
	}


    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
	public override SObject[] match() {
		return matches;
	}
	
	public override Descriptor getDescribe() {
		
		MatchedAccountDescriptor thisDescriptor; 
		
		if(thisDescriptor == null) {
			thisDescriptor = new MatchedAccountDescriptor();
		}
		
		return thisDescriptor.getDescribe();
	}
	
	/**
	 *	INNER CLASSES
	 *	Descritpor for MatchedAccount. Instantiated when requested, otherwise save the statements.
	 */
	private class MatchedAccountDescriptor extends Descriptor {
	    
	    //---------------------------------------------------------------------------------------------
	    // Properties
	    //---------------------------------------------------------------------------------------------
	    public String matchCode;
	    
	    //---------------------------------------------------------------------------------------------
	    // Constructors
	    //---------------------------------------------------------------------------------------------
	    MatchedAccountDescriptor() {
			super();
	    	getDescribe();
	    }
	    
	    //---------------------------------------------------------------------------------------------
	    // Methods
	    //---------------------------------------------------------------------------------------------
		public override Descriptor getDescribe() {
			matchCode = 'test match code result';
			return this;
		}
		
		public override String getToken() {
			return null;
		}
		
		public override String getType() {
			return null;
		}
	}
}