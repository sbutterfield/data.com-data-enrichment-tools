public class RuntimeSetting {
    
    // The current API name for the object this Apex class wraps
    public static final String SOBJECT_NAME = Utils.NAMESPACE_PREFIX + 'RuntimeSettings__c';

    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    // Booleans
    public Boolean accountTriggerEnabled { get; set; }
    public Boolean contactTriggerEnabled { get; set; }
    public Boolean leadTriggerEnabled { get; set; }
    public Boolean leadAutomationEnabled { get; set; }
    public Boolean contactAutomationEnabled { get; set; }
    public Boolean accountAutomationEnabled { get; set; }
    public Boolean isDefault { get; set; }
    public Boolean domainMatchEnabled { get; set; }
    public Boolean dunsLookupEnabled { get; set; }
    public Boolean duplicateIdentificationEnabled { get; set; }
    public Boolean duplicatePreventionEnabled { get; set; }
    public Boolean emailGeneralizationEnabled { get; set; }
    public Boolean fuzzyMatchEnabled { get; set; }
    public Boolean matchedAccountAssociativityEnabled { get; set; }
    public Boolean matchedContactAssociativityEnabled { get; set; }
    public Boolean matchedLeadAssociativityEnabled { get; set; }
    public Boolean useGlobalUltimate { get; set; }
    public Boolean useDomesticUltimate { get; set; }
    public Boolean useNearestHQ { get; set; }
    public Boolean doProximityMatch { get; set;}
    public Boolean doCompanyNameOnlyMatch { get; set; }
    public Boolean normalizeAddress { get; set; }
    public Boolean dnbMatchesOnly { get; set; }
    
    // Strings
    public String scenarioId { get; set; }
    public String scenarioName { get; set; }
    
    // Int
    public Integer dupeThreshold { get; set; }
    
    // Id
    public Id settingId { get; set; }
    
    // Concrete Sobject
    public RuntimeSettings__c cSetting { get; set; }
    
    //---------------------------------------------------------------------------------------------
    // Fields
    //---------------------------------------------------------------------------------------------
    public String ACCOUNT_EVENT_TRIGGER_FIELD = 'ddcDet__Account_Event_Trigger_Enabled__c';
    public String CONTACT_EVENT_TRIGGER_FIELD = 'ddcDet__Contact_Event_Trigger_Enabled__c';
    public String LEAD_EVENT_TRIGGER_FIELD = 'ddcDet__Lead_Event_Trigger_Enabled__c';
    public String LEAD_AUTOMATION_FIELD = 'ddcDet__Lead_Automation_Enabled__c';
    public String CONTACT_AUTOMATION_FIELD = 'ddcDet__Contact_Automation_Enabled__c';
    public String ACCOUNT_AUTOMATION_FIELD = 'ddcDet__Account_Automation_Enabled__c';
    public String DEFAULT_SETTINGS_FIELD = 'ddcDet__Default_Settings__c';
    public String DOMAIN_MATCHING_ENABLED_FIELD = 'ddcDet__Domain_Matching_Enabled__c';
    public String DUNS_LOOKUP_ENABLED_FIELD = 'ddcDet__DUNS_Lookup_Enabled__c';
    public String DUPLICATE_IDENTIFICATION_ENABLED_FIELD = 'ddcDet__Duplicate_Identification_Enabled__c';
    public String DUPLICATE_PREVENTION_ENABLED_FIELD = 'ddcDet__Duplicate_Prevention_Enabled__c';
    public String DUPLICATE_THRESHOLD_FIELD = 'ddcDet__Duplicate_Threshold__c';
    public String EMAIL_GENERALIZATION_ENABLED_FIELD = 'ddcDet__Email_Generalization_Enabled__c';
    public String FUZZY_MATCHING_ENABLED_FIELD = 'ddcDet__Fuzzy_Matching_Enabled__c';
    public String MATCHED_ACCOUNT_ASSOCIATIVITY_ENABLED_FIELD = 'ddcDet__Matched_Account_Associativity_Enabled__c';
    public String MATCHED_CONTACT_ASSOCIATIVITY_ENABLED_FIELD = 'ddcDet__Matched_Contact_Associativity_Enabled__c';
    public String MATCHED_LEAD_ASSOCIATIVITY_ENABLED_FIELD = 'ddcDet__Matched_Lead_Associativity_Enabled__c';
    public String SCENARIO_ID_FIELD = 'ddcDet__Scenario_Id__c';
    public String USE_DOMESTIC_ULTIMATE_MATCH_FIELD = 'ddcDet__Use_Domestic_Ultimate_Match__c';
    public String USE_GLOBAL_ULTIMATE_FIELD = 'ddcDet__Use_Global_Ultimate__c';
    public String USE_NEAREST_HQ_MATCH_FIELD = 'ddcDet__Use_Nearest_HQ_Match__c';
    public String DO_PROXIMITY_MATCH_FIELD = 'ddcDet__DoLocationProximityMatch__c';
    public String DO_COMPANY_NAME_ONLY_MATCH_FIELD = 'ddcDet__AttemptCompanyNameOnlyMatch__c';
    public String NORMALIZE_ADDRESS_FIELD = 'ddcDet__NormAddress__c';
    public String DNB_ONLY_MATCHES_FIELD = 'ddcDet__UseDnBMatchesOnly__c';
    
    
    //---------------------------------------------------------------------------------------------
    // Statics
    //---------------------------------------------------------------------------------------------

    private static Map<String, RuntimeSetting> settingsCache { 
        get {
            if (settingsCache == null) {
                settingsCache = new Map<String, RuntimeSetting>();
            }
            return settingsCache;
        }
    }
    
    /*public static final List<Schema.SObjectField> FIELD_SEQUENCE = new List<Schema.SObjectField> (getObjectFields());
    
    private static List<Schema.SObjectField> getObjectFields() {
        String objName = Utils.NAMESPACE_PREFIX + SOBJECT_NAME;
        Schema.SObjectType stype = Utils.getConcreteObject(objName);
        Map<String, Schema.SObjectField> fieldDesc = Utils.getFieldListing(objName);
        return fieldDesc.values();
    }*/


    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public RuntimeSetting() {
        // default constructor provides a blank apex object
    }
    
    // Construct a new instance from a concrete object
    // FIXME: Use static FIELD class attributes instead of hard-coded fields below...
    public RuntimeSetting(RuntimeSettings__c input) {
    	this.settingId = input.Id;
        this.scenarioName = input.Name;
        this.scenarioId = input.Scenario_Id__c;
        this.accountTriggerEnabled = input.Account_Event_Trigger_Enabled__c;
        this.contactTriggerEnabled = input.Contact_Event_Trigger_Enabled__c;
        this.leadTriggerEnabled = input.Lead_Event_Trigger_Enabled__c;
        this.leadAutomationEnabled = Boolean.valueOf(input.get(LEAD_AUTOMATION_FIELD));
        this.contactAutomationEnabled = Boolean.valueOf(input.get(CONTACT_AUTOMATION_FIELD));
        this.accountAutomationEnabled = Boolean.valueOf(input.get(ACCOUNT_AUTOMATION_FIELD));
        this.isDefault = input.Default_Settings__c;
        this.domainMatchEnabled = input.Domain_Matching_Enabled__c;
        this.duplicateIdentificationEnabled = input.Duplicate_Identification_Enabled__c;
        this.duplicatePreventionEnabled = input.Duplicate_Prevention_Enabled__c;
        this.dupeThreshold = Integer.valueOf(input.Duplicate_Threshold__c);
        this.dunsLookupEnabled = Boolean.valueOf(input.get(DUNS_LOOKUP_ENABLED_FIELD));
        this.emailGeneralizationEnabled = input.Email_Generalization_Enabled__c;
        this.fuzzyMatchEnabled = input.Fuzzy_Matching_Enabled__c;
        this.matchedAccountAssociativityEnabled = input.Matched_Account_Associativity_Enabled__c;
        this.matchedContactAssociativityEnabled = input.Matched_Contact_Associativity_Enabled__c;
        this.matchedLeadAssociativityEnabled = input.Matched_Lead_Associativity_Enabled__c;
        this.useDomesticUltimate = input.Use_Domestic_Ultimate_Match__c;
        this.useGlobalUltimate = input.Use_Global_Ultimate__c;
        this.useNearestHQ = input.Use_Nearest_HQ_Match__c;
        this.doProximityMatch = Boolean.valueOf(input.get(DO_PROXIMITY_MATCH_FIELD));
        this.doCompanyNameOnlyMatch = Boolean.valueOf(input.get(DO_COMPANY_NAME_ONLY_MATCH_FIELD));
        this.normalizeAddress = Boolean.valueOf(input.get(NORMALIZE_ADDRESS_FIELD));
        this.dnbMatchesOnly = Boolean.valueOf(input.get(DNB_ONLY_MATCHES_FIELD));
        this.cSetting = input;
    }
    
    // Construct a new instance from a scenario Id
    public RuntimeSetting(String scenarioId, ApexPages.Message[] messages) {
        getSettings(scenarioId, messages);
    }
    
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    // Looks up settings for a given scenario
    // A NULL scenarioId assumes you want the default settings returned.
    public static RuntimeSetting getSettings(String scenarioId, ApexPages.Message[] messages) {
        
        RuntimeSetting result;

        // Attempt to retrieve from cache first
        if (settingsCache != null && settingsCache.containsKey(scenarioId)) {
            System.debug('Retrieving RuntimeSetting from cache. :: ' +settingsCache.get(scenarioId));
            // Return if found in cache
            return result = settingsCache.get(scenarioId);
        }
        
        try {
            
            Map<String, Schema.SObjectField> fieldmap = Utils.getFieldListing(SOBJECT_NAME);
            String soql = 'SELECT ';
            if(fieldmap != null) {
	            for(Schema.SObjectField fName : fieldmap.values()) {
	                soql += fName + ',';
	            }
	            
	            // Trim off last comma so we don't throw query exceptions
	            soql = soql.removeEnd(',');
	            
	            // Add the FROM clause
	            soql += ' FROM ' + SOBJECT_NAME;
	            
	            // Add the WHERE clause if there is a scenario id
	            if(scenarioId != null) {
	                soql += ' WHERE Name = \'' + Id.valueOf(scenarioId) + '\'';
                    System.debug('Using scenarioId in SOQL :: ' +soql);
	            }
	            // Otherwise, select the defaults
	            else {
	                soql += ' WHERE ddcDet__DefaultSettings__c = true';
                    System.debug('NOT using scenario Id in soql!! :: ');
	            }
	            // Our query will ALWAYS return one record in this case.
	            RuntimeSettings__c rs = Database.query(soql);
	            if(rs != null) {
	               result = new RuntimeSetting(rs);
                   // Load the cache with the newly created result
                   System.debug('pushing setting in to cache; setting = :: ' +result+ ' for scenarioId = :: ' +scenarioId);
                   settingsCache.put(scenarioId, result);
	            }
            }
        }
        catch(Exception e) {
            System.debug('Unable to fetch scenario specific settings for the Id specified: ' + scenarioId +'\n' +'Error stream: ' +e.getStackTraceString() +' '+e);
            messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDCTransientError, e.getMessage()));
        }
        
        return result;
    }
    
    /**
     * Converts the RuntimeSetting Apex Wrapper object in to RuntimeSettings__c concrete object and upserts it in to the database.
     */
    public void upsertSettings(RuntimeSetting input, ApexPages.Message[] messages) {
        if(input == null) {
            throw new ddcLib1.IllegalArgumentException('Invalid input. Expected RuntimeSetting, received: ' +input);
        }
        
        RuntimeSettings__c rs = this.toSobject(input);
        
        try {
            upsert rs;
        }
        catch(DMLException d) {
            messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to create new settings: ', d.getMessage()));
        }
        
    }
    
    /**
     * Does the work to convert an instance of the SObject in to a wrapper.
     */
    public RuntimeSetting toWrapper(RuntimeSettings__c input) {
        if(input == null) return null;
        System.debug('running RuntimeSetting to wrapper on input = :: ' +input);
        RuntimeSetting result = new RuntimeSetting();
        result.settingId = input.Id;
        result.scenarioName = input.Name;
        result.scenarioId = input.Scenario_Id__c;
        result.accountTriggerEnabled = input.Account_Event_Trigger_Enabled__c;
        result.contactTriggerEnabled = input.Contact_Event_Trigger_Enabled__c;
        result.leadTriggerEnabled = input.Lead_Event_Trigger_Enabled__c;
        result.leadAutomationEnabled = Boolean.valueOf(input.get(LEAD_AUTOMATION_FIELD));
        result.contactAutomationEnabled = Boolean.valueOf(input.get(CONTACT_AUTOMATION_FIELD));
        result.accountAutomationEnabled = Boolean.valueOf(input.get(ACCOUNT_AUTOMATION_FIELD));
        result.isDefault = input.Default_Settings__c;
        result.domainMatchEnabled = input.Domain_Matching_Enabled__c;
        result.duplicateIdentificationEnabled = input.Duplicate_Identification_Enabled__c;
        result.duplicatePreventionEnabled = input.Duplicate_Prevention_Enabled__c;
        result.dupeThreshold = Integer.valueOf(input.Duplicate_Threshold__c);
        result.emailGeneralizationEnabled = input.Email_Generalization_Enabled__c;
        result.fuzzyMatchEnabled = input.Fuzzy_Matching_Enabled__c;
        result.matchedAccountAssociativityEnabled = input.Matched_Account_Associativity_Enabled__c;
        result.matchedContactAssociativityEnabled = input.Matched_Contact_Associativity_Enabled__c;
        result.matchedLeadAssociativityEnabled = input.Matched_Lead_Associativity_Enabled__c;
        result.useDomesticUltimate = input.Use_Domestic_Ultimate_Match__c;
        result.useGlobalUltimate = input.Use_Global_Ultimate__c;
        result.useNearestHQ = input.Use_Nearest_HQ_Match__c;
        result.doProximityMatch = Boolean.valueOf(input.get(DO_PROXIMITY_MATCH_FIELD));
        result.doCompanyNameOnlyMatch = Boolean.valueOf(input.get(DO_COMPANY_NAME_ONLY_MATCH_FIELD));
        result.normalizeAddress = Boolean.valueOf(input.get(NORMALIZE_ADDRESS_FIELD));
        result.dnbMatchesOnly = Boolean.valueOf(input.get(DNB_ONLY_MATCHES_FIELD));
        
        return result;
    }
    
    /**
     * Does the work to convert an instance of a Wrapper in to an instance of the SObject.
     */
    public RuntimeSettings__c toSobject(RuntimeSetting input) {
        if(input == null) return null;
        System.debug('running RuntimeSetting to Sobject on input = :: ' +input);
        RuntimeSettings__c rs = new RuntimeSettings__c();
        rs.Id = input.settingId;
        rs.Name = input.scenarioName;
        rs.Account_Event_Trigger_Enabled__c = input.accountTriggerEnabled;
        rs.Contact_Event_Trigger_Enabled__c = input.contactTriggerEnabled;
        rs.Lead_Event_Trigger_Enabled__c = input.leadTriggerEnabled;
        rs.put(LEAD_AUTOMATION_FIELD, input.leadAutomationEnabled);
        rs.put(CONTACT_AUTOMATION_FIELD, input.contactAutomationEnabled);
        rs.put(ACCOUNT_AUTOMATION_FIELD, input.accountAutomationEnabled);
        rs.Default_Settings__c = input.isDefault;
        rs.Domain_Matching_Enabled__c = input.domainMatchEnabled;
        rs.Duplicate_Identification_Enabled__c = input.duplicateIdentificationEnabled;
        rs.Duplicate_Prevention_Enabled__c = input.duplicatePreventionEnabled;
        rs.Duplicate_Threshold__c = Double.valueOf(input.dupeThreshold);
        rs.Email_Generalization_Enabled__c = input.emailGeneralizationEnabled;
        rs.Fuzzy_Matching_Enabled__c = input.fuzzyMatchEnabled;
        rs.Matched_Account_Associativity_Enabled__c = input.matchedAccountAssociativityEnabled;
        rs.Matched_Contact_Associativity_Enabled__c = input.matchedContactAssociativityEnabled;
        rs.Matched_Lead_Associativity_Enabled__c = input.matchedLeadAssociativityEnabled;
        rs.Scenario_Id__c = input.scenarioId;
        rs.Use_Domestic_Ultimate_Match__c = input.useDomesticUltimate;
        rs.Use_Global_Ultimate__c = input.useGlobalUltimate;
        rs.Use_Nearest_HQ_Match__c = input.useNearestHQ;
        rs.put(DO_PROXIMITY_MATCH_FIELD, input.doProximityMatch);
        rs.put(DO_COMPANY_NAME_ONLY_MATCH_FIELD, input.doCompanyNameOnlyMatch);
        rs.put(NORMALIZE_ADDRESS_FIELD, input.normalizeAddress);
        rs.put(DNB_ONLY_MATCHES_FIELD, input.dnbMatchesOnly);
        
        return rs;
    }
    
    // TRIED TO DO EVERYTHING DYNAMICALLY. EFF THAT:
    /*
     * The idea here is to create a mapping from the concrete field to the attribute.
     * RuntimeSetting wrapper would need to duplicate the ability to "get(String s)" like SObject.get() in order to fetch a property by string name.
    public static Map<String, String> concreteMapping;
    
    
    static {
        RuntimeSetting rs = new RuntimeSetting();
        concreteMapping = new Map<String,String> {
            rs.ACCOUNT_EVENT_TRIGGER_FIELD => 'accountTriggerEnabled',
            rs.CONTACT_EVENT_TRIGGER_FIELD => 'contactTriggerEnabled',
            rs.LEAD_EVENT_TRIGGER_FIELD => 'leadTriggerEnabled'
        };
    }*/
    /*
    String objName = Utils.NAMESPACE_PREFIX + OBJECT_NAME;
    Schema.SObjectType stype = Utils.getConcreteObject(objName);
    SObject ns = stype.newSObject();
    Map<String, Schema.SObjectField> stypeFields = Utils.getFieldListing(objName);
    if(input.settingId == null) {
        rs = (RuntimeSettings__c)stype.newSObject();
    }
    else {
        rs = (RuntimeSettings__c)stype.newSObject(input.settingId);
    }
    */
    /*
    for(Schema.SObjectField field : stypeFields.values()) {
        String relatedProperty = concreteMapping.get(String.valueOf(field));
    }
    */
}