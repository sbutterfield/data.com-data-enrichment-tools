/**
 *  Generic wrapper implementation for any Sobject type.
 *  Only specific Salesforce Objects should extend an instance of SObjectWrapper.cls
 *  All non-specific datatypes that must be treated as a Wrapper should extend WrapperDecorator.cls
 */

global virtual class SObjectWrapper implements Wrapper {

    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    // instance variable
    private SObject cSobject;
    
    // GETTER - implementation from Wrapper
    global SObject getSObject() {
    	return this.cSobject;
    }
    
    // instance variable
    private DDCType wType;
    
    // GETTER - implementation from Wrapper
    global DDCType getType() {
    	return this.wType;
    }

    // GETTER - implementation from Wrapper
    global MatchScenario getScenario() {
        return MatchScenario.getScenarioForWrapper(this);
    }
    
    // Instance variable, only set by DDCApi.
    private DDCObject ddcMatch;
    
    // GETTER - implementation from Wrapper
    global DDCObject getDdcMatch() {
        return this.ddcMatch;
    }

    global void setDdcMatch(DDCObject value) {
        this.ddcMatch = value;
    }
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    // globally available constructor - only method of instantiation
    global SObjectWrapper(SObject s, DDCType t) {
        this.cSobject = s;
        this.wType = t;
    }
    
    // Private default constructor
    private SObjectWrapper() {
    	
    }
}