/**
 * Company object returned by the Matcher API.
 */
public with sharing class MatcherCompany extends MatcherObject {
    
    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    public Boolean selected {
    	get; set;
    }
    
    public MatcherField website {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_WEBSITE); }
        set { mMap.put(MatcherAPI.TAG_NAME_WEBSITE, value); }
    }
    
    public MatcherField companyName {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_COMPANY_NAME); }
        set { mMap.put(MatcherAPI.TAG_NAME_COMPANY_NAME, value); }
    }
    
    public MatcherField stockTicker {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_STOCK_TICKER); }
        set { mMap.put(MatcherAPI.TAG_NAME_STOCK_TICKER, value); }
    }
    
    public MatcherField companyId {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_COMPANY_ID); }
        set { mMap.put(MatcherAPI.TAG_NAME_COMPANY_ID, value); }
    }
    
    public MatcherField employeeCount {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT); }
        set { mMap.put(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, value); }
    }
    
    public MatcherField revenue {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_REVENUE); }
        set { mMap.put(MatcherAPI.TAG_NAME_REVENUE, value); }
    }
    
    public MatcherField ownership {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_OWNERSHIP); }
        set { mMap.put(MatcherAPI.TAG_NAME_OWNERSHIP, value); }
    }
    
    public MatcherField graveyard {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_GRAVEYARD); }
        set { mMap.put(MatcherAPI.TAG_NAME_GRAVEYARD, value); }
    }
    
    public MatcherLocation location {
    	get { return (MatcherLocation) mMap.get(MatcherAPI.TAG_NAME_LOCATION); }
    	set { mMap.put(MatcherAPI.TAG_NAME_LOCATION, value); }
    }
    
    public MatcherField industry {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_INDUSTRY); }
        set { mMap.put(MatcherAPI.TAG_NAME_INDUSTRY, value); }
    }
    
    public MatcherField subIndustry {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SUB_INDUSTRY); }
        set { mMap.put(MatcherAPI.TAG_NAME_SUB_INDUSTRY, value); }
    }
    
    public MatcherField sfdcIndustry {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SFDC_INDUSTRY); }
        set { mMap.put(MatcherAPI.TAG_NAME_SFDC_INDUSTRY, value); }
    }
    
    public MatcherField yearStarted {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_YEAR_STARTED); }
        set { mMap.put(MatcherAPI.TAG_NAME_YEAR_STARTED, value); }
    }
    
    public MatcherField dunsNumber {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_DUNS_NUMBER); }
        set { mMap.put(MatcherAPI.TAG_NAME_DUNS_NUMBER, value); }
    }
    
    public MatcherField tradeStyle {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_TRADE_STYLE); }
        set { mMap.put(MatcherAPI.TAG_NAME_TRADE_STYLE, value); }
    }
    
    public MatcherField primarySic {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_PRIMARY_SIC); }
        set { mMap.put(MatcherAPI.TAG_NAME_PRIMARY_SIC, value); }
    }
    
    public MatcherField primarySicDesc {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_PRIMARY_SIC_DESC); }
        set { mMap.put(MatcherAPI.TAG_NAME_PRIMARY_SIC_DESC, value); }
    }
    
    public MatcherField faxNumber {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_FAX_NUMBER); }
        set { mMap.put(MatcherAPI.TAG_NAME_FAX_NUMBER, value); }
    }
    
    public MatcherField locationStatus {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_LOCATION_STATUS); }
        set { mMap.put(MatcherAPI.TAG_NAME_LOCATION_STATUS, value); }
    }
    
    public MatcherField primaryNaics {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_PRIMARY_NAICS); }
        set { mMap.put(MatcherAPI.TAG_NAME_PRIMARY_NAICS, value); }
    }
    
    public MatcherField primaryNaicsDesc {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_PRIMARY_NAICS_DESC); }
        set { mMap.put(MatcherAPI.TAG_NAME_PRIMARY_NAICS_DESC, value); }
    }
    
    public MatcherField companySynopsis {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_COMPANY_SYNOPSIS); }
        set { mMap.put(MatcherAPI.TAG_NAME_COMPANY_SYNOPSIS, value); }
    }
    
    public MatcherField shippingCity {
    	get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SHIPPING_CITY); }
    	set { mMap.put(MatcherAPI.TAG_NAME_SHIPPING_CITY, value); }
    }
    
    public MatcherField shippingCountryCode {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SHIPPING_COUNTRY_CODE); }
        set { mMap.put(MatcherAPI.TAG_NAME_SHIPPING_COUNTRY_CODE, value); }
    }
    
    public MatcherField shippingState {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SHIPPING_STATE); }
        set { mMap.put(MatcherAPI.TAG_NAME_SHIPPING_STATE, value); }
    }
    
    public MatcherField shippingAddress {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SHIPPING_ADDRESS); }
        set { mMap.put(MatcherAPI.TAG_NAME_SHIPPING_ADDRESS, value); }
    }
    
    public MatcherField shippingZip {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SHIPPING_ZIP); }
        set { mMap.put(MatcherAPI.TAG_NAME_SHIPPING_ZIP, value); }
    }
    
    public MatcherField internationalCode {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_INTERNATIONAL_CODE); }
        set { mMap.put(MatcherAPI.TAG_NAME_INTERNATIONAL_CODE, value); }
    }
    
    public MatcherField metroArea {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_METRO_AREA); }
        set { mMap.put(MatcherAPI.TAG_NAME_METRO_AREA, value); }
    }
    
    public MatcherField countryCode {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_COUNTRY_CODE); }
        set { mMap.put(MatcherAPI.TAG_NAME_COUNTRY_CODE, value); }
    }
    
    public MatcherField latitude {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_LATITUDE); }
        set { mMap.put(MatcherAPI.TAG_NAME_LATITUDE, value); }
    }
    
    public MatcherField longitude {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_LONGITUDE); }
        set { mMap.put(MatcherAPI.TAG_NAME_LONGITUDE, value); }
    }
    
    public MatcherField source {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SOURCE); }
        set { mMap.put(MatcherAPI.TAG_NAME_SOURCE, value); }
    }
    
    public MatcherField updatedTs {
    	get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_UPDATED_TS); }
    	set { mMap.put(MatcherAPI.TAG_NAME_UPDATED_TS, value); }
    }
    
    public MatcherField externalKey {
    	get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_EXTERNAL_KEY); }
    	set { mMap.put(MatcherAPI.TAG_NAME_EXTERNAL_KEY, value); }
    }
		

    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public MatcherCompany() {
    	super(MatcherAPI.TAG_NAME_COMPANY, MatcherAPI.SEQUENCE_COMPANY);
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * This will parse the given company node returned by the Matcher API.
     */
    public static MatcherCompany parse(Dom.XmlNode companyNode) {
    	if (companyNode == null || !MatcherAPI.TAG_NAME_COMPANY.equals(companyNode.getName())) {
    		return null;
    	}
    	
    	// NOTE: A switch statement would be great here but Apex doesn't have one!
        MatcherCompany company = new MatcherCompany();
        Dom.XmlNode node;
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_WEBSITE, null);
        if (node != null) {
        	company.website = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_COMPANY_NAME, null);
        if (node != null) {
            company.companyName = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_COMPANY_ID, null);
        if (node != null) {
            try {
                company.companyId = new MatcherField(Long.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_STOCK_TICKER, null);
        if (node != null) {
            company.stockTicker = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, null);
        if (node != null) {
            try {
                company.employeeCount = new MatcherField(Integer.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_REVENUE, null);
        if (node != null) {
            // NOTE: Larger Double values are displayed in scientific notation. Decimal has a
            // toPlainString method that returns a String value without scientific notation
            // which may be useful for conversion.
            try {
                company.revenue = new MatcherField(Double.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_OWNERSHIP, null);
        if (node != null) {
            company.ownership = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_GRAVEYARD, null);
        if (node != null) {
            company.graveyard = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_LOCATION, null);
        if (node != null) {
            company.location = MatcherLocation.parse(node);
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_INDUSTRY, null);
        if (node != null) {
            company.industry = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SUB_INDUSTRY, null);
        if (node != null) {
            company.subIndustry = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SFDC_INDUSTRY, null);
        if (node != null) {
            company.sfdcIndustry = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_YEAR_STARTED, null);
        if (node != null) {
            try {
                company.yearStarted = new MatcherField(Integer.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_DUNS_NUMBER, null);
        if (node != null) {
            company.dunsNumber = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_TRADE_STYLE, null);
        if (node != null) {
            company.tradeStyle = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_PRIMARY_SIC, null);
        if (node != null) {
            try {
                company.primarySic = new MatcherField(Integer.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_PRIMARY_SIC_DESC, null);
        if (node != null) {
            company.primarySicDesc = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }

        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_FAX_NUMBER, null);
        if (node != null) {
            company.faxNumber = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }

        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_LOCATION_STATUS, null);
        if (node != null) {
            company.locationStatus = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_PRIMARY_NAICS, null);
        if (node != null) {
            try {
                company.primaryNaics = new MatcherField(Integer.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_PRIMARY_NAICS_DESC, null);
        if (node != null) {
            company.primaryNaicsDesc = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_COMPANY_SYNOPSIS, null);
        if (node != null) {
            company.companySynopsis = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SHIPPING_CITY, null);
        if (node != null) {
            company.shippingCity = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SHIPPING_COUNTRY_CODE, null);
        if (node != null) {
            company.shippingCountryCode = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SHIPPING_STATE, null);
        if (node != null) {
            company.shippingState = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SHIPPING_ADDRESS, null);
        if (node != null) {
            company.shippingAddress = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SHIPPING_ZIP, null);
        if (node != null) {
            company.shippingZip = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_INTERNATIONAL_CODE, null);
        if (node != null) {
            company.internationalCode = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_METRO_AREA, null);
        if (node != null) {
            try {
                company.metroArea = new MatcherField(Integer.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_COUNTRY_CODE, null);
        if (node != null) {
            company.countryCode = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_LATITUDE, null);
        if (node != null) {
            company.latitude = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_LONGITUDE, null);
        if (node != null) {
            company.longitude = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_SOURCE, null);
        if (node != null) {
            company.source = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_UPDATED_TS, null);
        if (node != null) {
        	company.updatedTs = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_EXTERNAL_KEY, null);
        if(node != null) {
        	company.externalKey = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        return company;
    }
    
    /**
     * Converts this object to an Account object for use with the JStareAndCompare page.
     * The sObjectFieldMap is used to determine whether or not an org has access to D&B fields.
     */
    public Account toAccount(Map<String, Schema.sObjectField> sObjectFieldMap) {
    	Account a = new Account();
        
        if (companyName != null && companyName.value != null) {
        	a.Name = (String) companyName.value;
        }
        if (employeeCount != null && employeeCount.value != null) {
        	a.NumberOfEmployees = (Integer) employeeCount.value;
        }
        if (revenue != null && revenue.value != null) {
        	a.AnnualRevenue = (Double) revenue.value;
        }
        if (stockTicker != null && stockTicker.value != null) {
        	a.TickerSymbol = (String) stockTicker.value;
        }
        if (website != null && website.value != null) {
            a.Website = (String) website.value;
        }
        if (sfdcIndustry != null && sfdcIndustry.value != null) {
            a.Industry = (String) sfdcIndustry.value;
        }
        if (ownership != null && ownership.value != null) {
            a.Ownership = (String) ownership.value;
        }

        if (location != null) {
            if (location.address != null) {
                String addressLine1 = null;
                if (location.address.addressLine != null && location.address.addressLine.value != null) {
                    addressLine1 = (String) location.address.addressLine.value;
                }
                String addressLine2 = null;
                if (location.address.addressLine2 != null && location.address.addressLine2.value != null) {
                    addressLine2 = (String) location.address.addressLine2.value;
                }
                String address = null;
                if (addressLine2 != null && addressLine2.length() > 0) {
                    address = addressLine2;
                }
                if (addressLine1 != null && addressLine1.length() > 0) {
                    address = addressLine1 + ((address != null) ? ' ' + address : '');
                }
                
                if (address != null) {
                	a.BillingStreet = address;
                }
                if (location.address.city != null && location.address.city.value != null) {
                    a.BillingCity = (String) location.address.city.value;
                }
                if (location.address.state != null && location.address.state.value != null) {
                    a.BillingState = (String) location.address.state.value;
                }
                if (location.address.zip != null && location.address.zip.value != null) {
                    a.BillingPostalCode = (String) location.address.zip.value;
                }
                if (location.address.country != null && location.address.country.value != null) {
                    a.BillingCountry = (String) location.address.country.value;
                }
            }
            
            if (location.locationPhone != null && location.locationPhone.value != null) {
                a.Phone = (String) location.locationPhone.value;
            }
        }
        
        if (faxNumber != null && faxNumber.value != null) {
            a.Fax = (String) faxNumber.value;
        }
        if (locationStatus != null && locationStatus.value != null) {
            a.Site = (String) locationStatus.value;
        }
        
        if (Utils.getCrud('Account','DunsNumber', 'access')) {
            if (dunsNumber != null && dunsNumber.value != null) {
            	a.put('DunsNumber', (String) dunsNumber.value);
            }
        }
        
        if (Utils.getCrud('Account','Tradestyle', 'access')) {
            if (tradeStyle != null && tradeStyle.value != null) {
            	a.put('Tradestyle', (String) tradeStyle.value);
            }
        }
        
        if (Utils.getCrud('Account','YearStarted', 'access')) {
            if (yearStarted != null && yearStarted.value != null) {
            	a.put('YearStarted', String.valueOf(yearStarted.value));
            }
        }
        
        if (primarySic != null && primarySic.value != null) {
        	a.Sic = String.valueOf(primarySic.value);
        }
        if (primarySicDesc != null && primarySicDesc.value != null) {
        	a.SicDesc = (String) primarySicDesc.value;
        }
        
        if (Utils.getCrud('Account','NaicsCode', 'access')) {
            if (primaryNaics != null && primaryNaics.value != null) {
            	a.put('NaicsCode', String.valueOf(primaryNaics.value));
            }
        }
        
        if (Utils.getCrud('Account','NaicsDesc', 'access')) {
            if (primaryNaicsDesc != null && primaryNaicsDesc.value != null) {
            	a.put('NaicsDesc', (String) primaryNaicsDesc.value);
            }
        }
        
        if (companySynopsis != null && companySynopsis.value != null) {
        	a.Description = (String) companySynopsis.value;
        }
        
        // Last Update
        if (updatedTs != null && updatedTs.value != null) {
            //a.put('INSERT_CONCRETE_FIELD_HERE') = DateTime.valueOfGmt(((String) updatedTs.value).replace('T', ' '));
        }
        
        // Non-UI Fields
        if (companyId != null && companyId.value != null) {
            //a.put('INSERT_CONCRETE_FIELD_HERE') = String.valueOf(companyId.value);
        }
        
        return a;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testToXml() {
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.locationId = new MatcherField(1234L, true);
        location.locationPhone = new MatcherField('+1.650.235.8400', false);
        location.address = address;
        
        MatcherCompany company = new MatcherCompany();
        company.website = new MatcherField('www.cisco.com', true);
        company.companyName = new MatcherField('Cisco Systems, Incorporated', false);
        company.companyId = new MatcherField(3333L, true);
        company.stockTicker = new MatcherField('CSCO', false);
        company.employeeCount = new MatcherField(5212, true);
        company.revenue = new MatcherField(Double.valueOf('4.004E10'), false);
        company.ownership = new MatcherField('Public', true);
        company.graveyard = new MatcherField('GRAVEYARDED', false);
        company.location = location;
        company.industry = new MatcherField('Education', true);
        company.subIndustry = new MatcherField('Elementary and Secondary Schools', false);
        company.sfdcIndustry = new MatcherField('Education', true);
        company.yearStarted = new MatcherField(2001, false);
        company.dunsNumber = new MatcherField('123456789', true);
        company.tradeStyle = new MatcherField('Test Trade Style', false);
        company.primarySic = new MatcherField(1111, true);
        company.primarySicDesc = new MatcherField('Test Primary Sic Desc', false);
        company.faxNumber = new MatcherField('650-225-1122', true);
        company.locationStatus = new MatcherField('Headquarters', false);
        company.primaryNaics = new MatcherField(1111, true);
        company.primaryNaicsDesc = new MatcherField('Test Primary Sic Desc', false);
        company.companySynopsis = new MatcherField('Test Company Synopsis', true);
        company.shippingCity = new MatcherField('Shipping City', false);
        company.shippingCountryCode = new MatcherField('Shipping Country Code', true);
        company.shippingState = new MatcherField('Shipping State', false);
        company.shippingAddress = new MatcherField('Shipping Address', true);
        company.shippingZip = new MatcherField('Shipping Zip', false);
        company.internationalCode = new MatcherField('International Code', true);
        company.metroArea = new MatcherField(1234, false);
        company.countryCode = new MatcherField('12', true);
        company.latitude = new MatcherField('1234', false);
        company.longitude = new MatcherField('1234', true);
        company.source = new MatcherField('Jigsaw', false);
        company.updatedTs = new MatcherField('2012-02-29T19:38:47', true);
        company.externalKey = new MatcherField('001d000000Vdr3fAAB', false);
        
        String expectedXml =
            '<' + MatcherAPI.TAG_NAME_COMPANY + '>' +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_WEBSITE, company.website) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_COMPANY_NAME, company.companyName) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_COMPANY_ID, company.companyId) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_STOCK_TICKER, company.stockTicker) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, company.employeeCount) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_REVENUE, company.revenue) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_OWNERSHIP, company.ownership) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_GRAVEYARD, company.graveyard) +
                company.location.toXml() +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_INDUSTRY, company.industry) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SUB_INDUSTRY, company.subIndustry) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SFDC_INDUSTRY, company.sfdcIndustry) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_YEAR_STARTED, company.yearStarted) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_DUNS_NUMBER, company.dunsNumber) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_TRADE_STYLE, company.tradeStyle) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_PRIMARY_SIC, company.primarySic) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_PRIMARY_SIC_DESC, company.primarySicDesc) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_FAX_NUMBER, company.faxNumber) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_LOCATION_STATUS, company.locationStatus) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_PRIMARY_NAICS, company.primaryNaics) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_PRIMARY_NAICS_DESC, company.primaryNaicsDesc) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_COMPANY_SYNOPSIS, company.companySynopsis) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SHIPPING_CITY, company.shippingCity) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SHIPPING_COUNTRY_CODE, company.shippingCountryCode) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SHIPPING_STATE, company.shippingState) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SHIPPING_ADDRESS, company.shippingAddress) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SHIPPING_ZIP, company.shippingZip) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_INTERNATIONAL_CODE, company.internationalCode) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_METRO_AREA, company.metroArea) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_COUNTRY_CODE, company.countryCode) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_LATITUDE, company.latitude) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_LONGITUDE, company.longitude) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SOURCE, company.source) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_UPDATED_TS, company.updatedTs) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_EXTERNAL_KEY, company.externalKey) +
            '</' + MatcherAPI.TAG_NAME_COMPANY + '>';
        
        String actualXml = company.toXml();
        System.assertEquals(expectedXml, actualXml);
    }
    
    private static testmethod void testParse() {
        // Null
        System.assertEquals(null, MatcherCompany.parse(null));
        
        // Non-Company Node
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode root = doc.createRootElement('NotCompany', null, null);
        System.assertEquals(null, MatcherCompany.parse(root));
        
        // NOTE: The easiest way to test for equality is to compare XML strings because there are
        // no equals methods readily available for all objects other than String.
        
        // Company
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.locationId = new MatcherField(1234L, true);
        location.locationPhone = new MatcherField('+1.650.235.8400', false);
        location.address = address;
        
        MatcherCompany company = new MatcherCompany();
        company.website = new MatcherField('www.cisco.com', true);
        company.companyName = new MatcherField('Cisco Systems, Incorporated', false);
        company.companyId = new MatcherField(3333L, true);
        company.stockTicker = new MatcherField('CSCO', false);
        company.employeeCount = new MatcherField(5212, true);
        company.revenue = new MatcherField(Double.valueOf('4.004E10'), false);
        company.ownership = new MatcherField('Public', true);
        company.graveyard = new MatcherField('GRAVEYARDED', false);
        company.location = location;
        company.industry = new MatcherField('Education', true);
        company.subIndustry = new MatcherField('Elementary and Secondary Schools', false);
        company.sfdcIndustry = new MatcherField('Education', true);
        company.yearStarted = new MatcherField(2001, false);
        company.dunsNumber = new MatcherField('123456789', true);
        company.tradeStyle = new MatcherField('Test Trade Style', false);
        company.primarySic = new MatcherField(1111, true);
        company.primarySicDesc = new MatcherField('Test Primary Sic Desc', false);
        company.faxNumber = new MatcherField('650-225-1122', true);
        company.locationStatus = new MatcherField('Headquarters', false);
        company.primaryNaics = new MatcherField(1111, true);
        company.primaryNaicsDesc = new MatcherField('Test Primary Sic Desc', false);
        company.companySynopsis = new MatcherField('Test Company Synopsis', true);
        company.shippingCity = new MatcherField('Shipping City', false);
        company.shippingCity = new MatcherField('Shipping City', false);
        company.shippingCountryCode = new MatcherField('Shipping Country Code', true);
        company.shippingState = new MatcherField('Shipping State', false);
        company.shippingAddress = new MatcherField('Shipping Address', true);
        company.shippingZip = new MatcherField('Shipping Zip', false);
        company.internationalCode = new MatcherField('International Code', true);
        company.metroArea = new MatcherField(1234, false);
        company.countryCode = new MatcherField('12', true);
        company.latitude = new MatcherField('1234', false);
        company.longitude = new MatcherField('1234', true);
        company.source = new MatcherField('Jigsaw', false);
        company.updatedTs = new MatcherField('2012-02-29T19:38:47', true);
        company.externalKey = new MatcherField('001d000000Vdr3fAAB', false);
        String expectedXml = company.toXml();
        
        doc = new Dom.Document();
        doc.load(expectedXml);
        MatcherCompany actualCompany = MatcherCompany.parse(doc.getRootElement());
        System.assertNotEquals(null, actualCompany);
        System.assertEquals(expectedXml, actualCompany.toXml());
        
        // Invalid companyId, employeeCount, revenue, yearStarted, primarySic, primaryNaics, metroArea
        company.companyId = null;
        company.employeeCount = null;
        company.revenue = null;
        company.yearStarted = null;
        company.primarySic = null;
        company.primaryNaics = null;
        company.metroArea = null;
        expectedXml = company.toXml();
        
        Dom.XmlNode companyNode = doc.getRootElement();
        Dom.XmlNode node = companyNode.getChildElement(MatcherAPI.TAG_NAME_COMPANY_ID, null);
        node.addTextNode('NotNumber');
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, null);
        node.addTextNode('NotNumber');
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_REVENUE, null);
        node.addTextNode('NotNumber');
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_YEAR_STARTED, null);
        node.addTextNode('NotNumber');
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_PRIMARY_SIC, null);
        node.addTextNode('NotNumber');
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_PRIMARY_NAICS, null);
        node.addTextNode('NotNumber');
        node = companyNode.getChildElement(MatcherAPI.TAG_NAME_METRO_AREA, null);
        node.addTextNode('NotNumber');
        actualCompany = MatcherCompany.parse(doc.getRootElement());
        System.assertNotEquals(null, actualCompany);
        System.assertEquals(expectedXml, actualCompany.toXml());
    }
    
    private static testmethod void testToAccount() {
        // Missing Fields
        MatcherCompany company = new MatcherCompany();
        System.assertNotEquals(null, company.toAccount(null));
        
        // All Fields
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.locationPhone = new MatcherField('+1.650.235.8400', false);
        location.address = address;
        
        company = new MatcherCompany();
        company.website = new MatcherField('www.cisco.com', true);
        company.companyName = new MatcherField('Cisco Systems, Incorporated', false);
        company.companyId = new MatcherField(3333L, true);
        company.stockTicker = new MatcherField('CSCO', false);
        company.employeeCount = new MatcherField(5212, true);
        company.revenue = new MatcherField(Double.valueOf('4.004E10'), false);
        company.sfdcIndustry = new MatcherField('Education', true);
        company.ownership = new MatcherField('Public', true);
        company.location = location;
        company.faxNumber = new MatcherField('+1.650.235.8401', false);
        company.locationStatus = new MatcherField('Headquarters', true);
        company.dunsNumber = new MatcherField('123456789', false);
        company.tradeStyle = new MatcherField('Test Trade Style', true);
        company.yearStarted = new MatcherField('1990', false);
        company.primarySic = new MatcherField(1111, true);
        company.primarySicDesc = new MatcherField('Test Primary Sic Desc', false);
        company.primaryNaics = new MatcherField(1111, true);
        company.primaryNaicsDesc = new MatcherField('Test Primary Sic Desc', false);
        company.companySynopsis = new MatcherField('Test Company Synopsis', true);
        company.updatedTs = new MatcherField('2012-02-29T19:38:47', true);
        
        Account a = company.toAccount(null);
        System.assertNotEquals(null, a);
        System.assertEquals(company.website.value, a.Website);
        System.assertEquals(company.companyName.value, a.Name);
        //System.assertEquals(String.valueOf(company.companyId.value), a.jigsaw_clean__Jigsaw_Id__c);
        System.assertEquals(company.stockTicker.value, a.TickerSymbol);
        System.assertEquals(company.employeeCount.value, a.NumberOfEmployees);
        System.assertEquals(company.revenue.value, a.AnnualRevenue);
        System.assertEquals(company.sfdcIndustry.value, a.Industry);
        System.assertEquals(company.ownership.value, a.Ownership);
        System.assertEquals(location.locationPhone.value, a.Phone);
        System.assertEquals(address.addressLine.value + ' ' + address.addressLine2.value, a.BillingStreet);
        System.assertEquals(address.city.value, a.BillingCity);
        System.assertEquals(address.state.value, a.BillingState);
        System.assertEquals(address.zip.value, a.BillingPostalCode);
        System.assertEquals(address.country.value, a.BillingCountry);
        System.assertEquals(company.faxNumber.value, a.Fax);
        System.assertEquals(company.locationStatus.value, a.Site);
        //System.assertEquals(company.dunsNumber.value, a.DunsNumber);
        //System.assertEquals(company.tradeStyle.value, a.TradeStyle);
        //System.assertEquals(company.yearStarted.value, a.YearStarted);
        System.assertEquals(String.valueOf(company.primarySic.value), a.Sic);
        System.assertEquals(company.primarySicDesc.value, a.SicDesc);
        //System.assertEquals(company.primaryNaics.value, a.Naics);
        //System.assertEquals(company.primaryNaicsDesc.value, a.NaicsDesc);
        System.assertEquals(company.companySynopsis.value, a.Description);
        //System.assertEquals(DateTime.valueOfGmt(((String) company.updatedTs.value).replace('T', ' ')), a.jigsaw_clean__Jigsaw_Last_Modified_Date__c);
    }
    
}