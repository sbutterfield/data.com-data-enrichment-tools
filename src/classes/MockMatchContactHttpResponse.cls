global class MockMatchContactHttpResponse implements HttpCalloutMock {

	global HttpResponse respond(HttpRequest req) {
		String XML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><matches><match><matchedItems><contact><firstName diff="false">Shawn</firstName><lastName diff="false">Butterfield</lastName><companyName diff="false">Salesforce.com, Inc.</companyName><title diff="false">Solutions Architect</title><email diff="false">sbutterfield@salesforce.com</email><phone diff="false">+1.509.481.9932</phone><location><locationPhone diff="false"></locationPhone><address><addressLine diff="false">12825 E Mirabeau Pkwy Ste 101</addressLine><city diff="false">Spokane</city><country diff="false">United States</country><state diff="false">WA</state><zip diff="false">99216-1464</zip></address></location><metadata/><contactId diff="true">46833664</contactId><companyId diff="true">7140724</companyId><employeeCount diff="false">0</employeeCount><revenue diff="false">0.0</revenue><industry diff="true">Other</industry><industryCode diff="true">1240000</industryCode><subIndustry diff="true">Other</subIndustry><subIndustryCode diff="true">1249900</subIndustryCode><sfdcIndustry diff="false">Other</sfdcIndustry><dunsNumber diff="true">043317919</dunsNumber><source diff="true">Data.com - D&amp;B</source><updatedTs diff="true">2012-02-06T10:35:34</updatedTs></contact></matchedItems><externalKey>00Qd000000Lo0r1EAB</externalKey></match></matches>';
		HttpResponse res = new HttpResponse();
		res.setBody(XML);
		res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
		res.setStatusCode(201);
		return res;
	}
}