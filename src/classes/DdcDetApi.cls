/********************************************//**
 *  DET Api class that encapsulates complex DET behaviors in to a globally accessible
 *  codespace. This class allows application code outside of DET to perform DET specific operations
 *  without exposing the back-end nature of the application routines.
 *
 *  @author     Shawn Butterfield, Salesforce.com
 *  @since      4/17/2014
 *  @version    1.2
 ***********************************************/
global class DdcDetApi {
	
    //---------------------------------------------------------------------------------------------
    // CONSTRUCTORS
    //---------------------------------------------------------------------------------------------
    public DdcDetApi() {
		
	}

    //---------------------------------------------------------------------------------------------
    // PUBLIC API
    //---------------------------------------------------------------------------------------------

    /**
     *  API to retreive stare-and-compare body data for a Lead record.
     *  @param  l           Lead record to use for JSON building
     *  @param  messages    Page messages collection that bubbles up through the runtime
     *  @return    A properly formatted JSON string. Can return null.
     */
     global static String getSncLead(Lead l, ApexPages.Message[] messages) {
        String result;

        // Wrap the lead
        Wrapper lw = new LeadWrapperDecorator(new SObjectWrapper(l, DDCType.CONTACT));
        // Get it's scenario
        MatchScenario lwScenario = lw.getScenario();
        // Fetch the contact and company Id off the record if they exist. They may be in the same field and we
        // do not know which is actually a contact or company Id in DDC.
        String contactId_field = lwScenario.thisMapping.get(ApiUtils.TAG_NAME_CONTACT_ID).mappedFieldName;
        String companyId_field = lwScenario.thisMapping.get(ApiUtils.TAG_NAME_COMPANY_ID).mappedFieldName;
        String contactId = contactId_field == null ? null : (String) lw.getSObject().get(contactId_field);
        String companyId = companyId_field == null ? null : (String) lw.getSObject().get(companyId_field);
        Boolean isCompany = Boolean.valueOf(lw.getSObject().get(SystemField.IS_COMPANY_MATCH_FIELD));

        Set<String> idset = new Set<String>();

        if (isCompany && companyId != null) {
            idset.add(companyId);
        }
        else if (contactId != null) {
            idset.add(contactId);
        }
        
        Map<String, DDCContact> sContacts;
        Map<String, DDCCompany> sCompanies;
        if (idset.size() > 0) {
            if (isCompany) {
                sCompanies = DDCApi.retrieveCompanies(idset, messages);
            }
            else {
                sContacts = DDCApi.retrieveContacts(idset, messages);
            }
        }

        JSONGenerator gen = JSON.createGenerator(false);

        if (sContacts != null) {
            
        }

        return result;
     }
    //---------------------------------------------------------------------------------------------
    // PRIVATE FUNCTIONS
    //---------------------------------------------------------------------------------------------
}