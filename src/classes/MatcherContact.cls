public class MatcherContact extends MatcherObject {
    
    //---------------------------------------------------------------------------------------------
    // Constants
    //---------------------------------------------------------------------------------------------
    
    private static final Integer MAX_TITLE_LENGTH = 80;
    
    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    public Boolean selected { get; set; }

    public MatcherField naicsCode {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_PRIMARY_NAICS); }
        set { mMap.put(ApiUtils.TAG_NAME_PRIMARY_NAICS, value); }
    }
    
// CITRIX======================================================
/*
    public MatcherField globalUltimateName {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_BUSINESS_NAME); }
        set { mMap.put(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_BUSINESS_NAME, value); }
    }
    public MatcherField globalUltimateDuns {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER); }
        set { mMap.put(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_DUNS_NUMBER, value); }
    }
    public MatcherField globalUltimateEmployees {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_EMPLOYEES); }
        set { mMap.put(ApiUtils.TAG_NAME_GLOBAL_ULTIMATE_EMPLOYEES, value); }
    }
    public MatcherField domesticUltimateName {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_BUSINESS_NAME); }
        set { mMap.put(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_BUSINESS_NAME, value); }
    }
    public MatcherField domesticUltimateDuns {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_DUNS_NUMBER); }
        set { mMap.put(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_DUNS_NUMBER, value); }
    }
    public MatcherField domesticUltimateEmployees {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_EMPLOYEES); }
        set { mMap.put(ApiUtils.TAG_NAME_DOMESTIC_ULTIMATE_EMPLOYEES, value); }
    }
    public MatcherField parentHqName {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_PARENT_OR_HQ_BUSINESS_NAME); }
        set { mMap.put(ApiUtils.TAG_NAME_PARENT_OR_HQ_BUSINESS_NAME, value); }
    }
    public MatcherField parentHqDuns {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_PARENT_OR_HQ_DUNS_NUMBER); }
        set { mMap.put(ApiUtils.TAG_NAME_PARENT_OR_HQ_DUNS_NUMBER, value); }
    }
    public MatcherField parentHqEmployees {
        get { return (MatcherField) mMap.get(ApiUtils.TAG_NAME_PARENT_HQ_EMPLOYEES); }
        set { mMap.put(ApiUtils.TAG_NAME_PARENT_HQ_EMPLOYEES, value); }
    }
*/
// CITRIX======================================================

    public MatcherField email {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_EMAIL); }
        set { mMap.put(MatcherAPI.TAG_NAME_EMAIL, value); }
    }

    public MatcherField website {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_WEBSITE); }
        set { mMap.put(MatcherAPI.TAG_NAME_WEBSITE, value); }
    }
    
    public MatcherField firstName {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_FIRST_NAME); }
        set { mMap.put(MatcherAPI.TAG_NAME_FIRST_NAME, value); }
    }
    
    public MatcherField lastName {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_LAST_NAME); }
        set { mMap.put(MatcherAPI.TAG_NAME_LAST_NAME, value); }
    }
    
    public MatcherField companyName {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_COMPANY_NAME); }
        set { mMap.put(MatcherAPI.TAG_NAME_COMPANY_NAME, value); }
    }
    
    public MatcherField phone {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_PHONE); }
        set { mMap.put(MatcherAPI.TAG_NAME_PHONE, value); }
    }
    
    public MatcherField title {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_TITLE); }
        set { mMap.put(MatcherAPI.TAG_NAME_TITLE, value); }
    }
    
    public MatcherField contactId {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_CONTACT_ID); }
        set { mMap.put(MatcherAPI.TAG_NAME_CONTACT_ID, value); }
    }
    
    public MatcherField companyId {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_COMPANY_ID); }
        set { mMap.put(MatcherAPI.TAG_NAME_COMPANY_ID, value); }
    }
    
    public MatcherField employeeCount {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT); }
        set { mMap.put(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, value); }
    }
    
    public MatcherField revenue {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_REVENUE); }
        set { mMap.put(MatcherAPI.TAG_NAME_REVENUE, value); }
    }
    
    public MatcherField graveyard {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_GRAVEYARD); }
        set { mMap.put(MatcherAPI.TAG_NAME_GRAVEYARD, value); }
    }
    
    public MatcherLocation location {
        get { return (MatcherLocation) mMap.get(MatcherAPI.TAG_NAME_LOCATION); }
        set { mMap.put(MatcherAPI.TAG_NAME_LOCATION, value); }
    }
    
    public MatcherField industry {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_INDUSTRY); }
        set { mMap.put(MatcherAPI.TAG_NAME_INDUSTRY, value); }
    }
    
    public MatcherField subIndustry {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SUB_INDUSTRY); }
        set { mMap.put(MatcherAPI.TAG_NAME_SUB_INDUSTRY, value); }
    }
    
    public MatcherField sfdcIndustry {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_SFDC_INDUSTRY); }
        set { mMap.put(MatcherAPI.TAG_NAME_SFDC_INDUSTRY, value); }
    }
    
    public MatcherField dunsNumber {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_DUNS_NUMBER); }
        set { mMap.put(MatcherAPI.TAG_NAME_DUNS_NUMBER, value); }
    }
    
    public MatcherField updatedTs {
        get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_UPDATED_TS); }
        set { mMap.put(MatcherAPI.TAG_NAME_UPDATED_TS, value); }
    }
    
    public MatcherField externalKey {
    	get { return (MatcherField) mMap.get(MatcherAPI.TAG_NAME_EXTERNAL_KEY); }
    	set { mMap.put(MatcherAPI.TAG_NAME_EXTERNAL_KEY, value); }
    }
    
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public MatcherContact() {
        super(MatcherAPI.TAG_NAME_CONTACT, MatcherAPI.SEQUENCE_CONTACT);
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * This will parse the given contact node returned by the Matcher API.
     */
    public static MatcherContact parse(Dom.XmlNode contactNode) {
        if (contactNode == null || !MatcherAPI.TAG_NAME_CONTACT.equals(contactNode.getName())) {
            return null;
        }
        
        // NOTE: A switch statement would be great here but Apex doesn't have one!
        MatcherContact contact = new MatcherContact();
        Dom.XmlNode node;
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_EMAIL, null);
        if (node != null) {
            contact.email = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_FIRST_NAME, null);
        if (node != null) {
            contact.firstName = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_LAST_NAME, null);
        if (node != null) {
            contact.lastName = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_COMPANY_NAME, null);
        if (node != null) {
            contact.companyName = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_PHONE, null);
        if (node != null) {
            contact.phone = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_TITLE, null);
        if (node != null) {
            contact.title = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_CONTACT_ID, null);
        if (node != null) {
            try {
                contact.contactId = new MatcherField(String.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_COMPANY_ID, null);
        if (node != null) {
            try {
                contact.companyId = new MatcherField(String.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, null);
        if (node != null) {
            try {
                contact.employeeCount = new MatcherField(Integer.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_REVENUE, null);
        if (node != null) {
            // NOTE: Larger Double values are displayed in scientific notation. Decimal has a
            // toPlainString method that returns a String value without scientific notation
            // which may be useful for conversion.
            try {
                contact.revenue = new MatcherField(Double.valueOf(node.getText()), MatcherField.parseDiff(node));
            }
            catch (Exception e) {
                // Do nothing.
            }
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_GRAVEYARD, null);
        if (node != null) {
            contact.graveyard = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_LOCATION, null);
        if (node != null) {
            contact.location = MatcherLocation.parse(node);
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_INDUSTRY, null);
        if (node != null) {
            contact.industry = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_SUB_INDUSTRY, null);
        if (node != null) {
            contact.subIndustry = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_SFDC_INDUSTRY, null);
        if (node != null) {
            contact.sfdcIndustry = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_DUNS_NUMBER, null);
        if (node != null) {
            contact.dunsNumber = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_UPDATED_TS, null);
        if (node != null) {
            contact.updatedTs = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_EXTERNAL_KEY, null);
        if(node != null) {
        	contact.externalKey = new MatcherField(node.getText(), MatcherField.parseDiff(node));
        }
        
        return contact;
    }
    
    /**
     * Converts this object to a Lead object for use with the JStareAndCompareLead page.
     * The sObjectFieldMap is used to determine whether or not an org has access to D&B fields.
     */
    public Lead toLead() {
        Lead l = new Lead();
        
        if (firstName != null && firstName.value != null) {
            l.FirstName = (String) firstName.value;
        }
        if (lastName != null && lastName.value != null) {
            l.LastName = (String) lastName.value;
        }
        if (companyName != null && companyName.value != null) {
            l.Company = (String) companyName.value;
        }
        if (title != null && title.value != null) {
            l.Title = (String) title.value;
            if (l.Title.length() > MAX_TITLE_LENGTH) {
            	l.Title = l.Title.substring(0, MAX_TITLE_LENGTH);
            }
        }
        if (email != null && email.value != null) {
            l.Email = (String) email.value;
        }
        if (phone != null && phone.value != null) {
            l.Phone = (String) phone.value;
        }
        
        if (location != null && location.address != null) {
            String addressLine1 = null;
            if (location.address.addressLine != null && location.address.addressLine.value != null) {
                addressLine1 = (String) location.address.addressLine.value;
            }
            String addressLine2 = null;
            if (location.address.addressLine2 != null && location.address.addressLine2.value != null) {
                addressLine2 = (String) location.address.addressLine2.value;
            }
            String address = null;
            if (addressLine2 != null && addressLine2.length() > 0) {
                address = addressLine2;
            }
            if (addressLine1 != null && addressLine1.length() > 0) {
                address = addressLine1 + ((address != null) ? ' ' + address : '');
            }
            
            if (address != null) {
                l.Street = address;
            }
            if (location.address.city != null && location.address.city.value != null) {
                l.City = (String) location.address.city.value;
            }
            if (location.address.state != null && location.address.state.value != null) {
                l.State = (String) location.address.state.value;
            }
            if (location.address.zip != null && location.address.zip.value != null) {
                l.PostalCode = (String) location.address.zip.value;
            }
            if (location.address.country != null && location.address.country.value != null) {
                l.Country = (String) location.address.country.value;
            }
        }
        
        if (employeeCount != null && employeeCount.value != null) {
            l.NumberOfEmployees = Integer.valueOf(employeeCount.value);
        }
        if (revenue != null && revenue.value != null) {
            l.AnnualRevenue = Double.valueOf(revenue.value);
        }
        if (sfdcIndustry != null && sfdcIndustry.value != null) {
            l.Industry = (String) sfdcIndustry.value;
        }
        
        if (Utils.getCrud('Lead','CompanyDunsNumber', 'access')) {
            if (dunsNumber != null && dunsNumber.value != null) {
                l.put('CompanyDunsNumber', (String) dunsNumber.value);
            }
        }
        
        // Last Update
        if (updatedTs != null && updatedTs.value != null) {
            //l.LastModifiedDate = (Datetime)updatedTs.value;
            //l.LastModifiedDate = Datetime.valueOfGmt(String.valueOf(updatedTs.value).replace('T', ' '));
        }
        
        // Non-UI Fields
        if (contactId != null && contactId.value != null) {
             l.Jigsaw = String.valueOf(contactId.value);
        }

        if (website != null && website.value != null) {
            l.Website = String.valueOf(website.value);
        }
// BAF

        if (naicsCode != null && naicsCode.value != null) {
            try {
                l.put(ddcDet.SncUtils.FIELD_PRIMARY_NAICS, Integer.valueOf(naicsCode.value));
            }
            catch (Exception e) {
                // invalid integer. don't put anything there.
            }
        }

// CITRIX ===================================================
/*
        if (globalUltimateEmployees != null && globalUltimateEmployees.value != null) {
            l.put(SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES, Integer.valueOf(globalUltimateEmployees.value));
        }

        if (globalUltimateDuns != null && globalUltimateDuns.value != null) {
            l.put(SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS, String.valueOf(globalUltimateDuns.value));
        }

        if (globalUltimateName != null && globalUltimateDuns.value != null) {
            l.put(SncUtils.FIELD_GLOBAL_ULTIMATE_NAME, String.valueOf(globalUltimateName.value));
        }

        if (domesticUltimateDuns != null && domesticUltimateDuns.value != null) {
            l.put(SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS, String.valueOf(domesticUltimateDuns.value));
        }

        if (domesticUltimateName != null && domesticUltimateName.value != null) {
            l.put(SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME, String.valueOf(domesticUltimateName.value));
        }

        if (domesticUltimateEmployees != null && domesticUltimateEmployees.value != null) {
            l.put(SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES, Integer.valueOf(domesticUltimateEmployees.value));
        }

        if (parentHqDuns != null && parentHqDuns.value != null) {
            l.put(SncUtils.FIELD_PARENT_HQ_DUNS, String.valueOf(parentHqDuns.value));
        }

        if (parentHqName != null && parentHqName.value != null) {
            l.put(SncUtils.FIELD_PARENT_HQ_NAME, String.valueOf(parentHqName.value));
        }

        if (parentHqEmployees != null && parentHqEmployees.value != null) {
            l.put(SncUtils.FIELD_PARENT_HQ_EMPLOYEES, Integer.valueOf(parentHqEmployees.value));
        }
*/
// CITRIX ===================================================


        
        return l;
    }
    
    /**
     * Converts this object to a Contact object for use with the JStareAndCompareContact page.
     * The sObjectFieldMap is used to determine whether or not an org has access to D&B fields.
     */
    public Contact toContact() {
        Contact c = new Contact();
        
        if (firstName != null && firstName.value != null) {
            c.FirstName = (String) firstName.value;
        }
        if (lastName != null && lastName.value != null) {
            c.LastName = (String) lastName.value;
        }
        if (title != null && title.value != null) {
            c.Title = (String) title.value;
            if (c.Title.length() > MAX_TITLE_LENGTH) {
                c.Title = c.Title.substring(0, MAX_TITLE_LENGTH);
            }
        }
        if (email != null && email.value != null) {
            c.Email = (String) email.value;
        }
        if (phone != null && phone.value != null) {
            c.Phone = (String) phone.value;
        }
        
        if (location != null && location.address != null) {
            String addressLine1 = null;
            if (location.address.addressLine != null && location.address.addressLine.value != null) {
                addressLine1 = (String) location.address.addressLine.value;
            }
            String addressLine2 = null;
            if (location.address.addressLine2 != null && location.address.addressLine2.value != null) {
                addressLine2 = (String) location.address.addressLine2.value;
            }
            String address = null;
            if (addressLine2 != null && addressLine2.length() > 0) {
                address = addressLine2;
            }
            if (addressLine1 != null && addressLine1.length() > 0) {
                address = addressLine1 + ((address != null) ? ' ' + address : '');
            }
            
            if (address != null) {
                c.MailingStreet = address;
            }
            if (location.address.city != null && location.address.city.value != null) {
                c.MailingCity = (String) location.address.city.value;
            }
            if (location.address.state != null && location.address.state.value != null) {
                c.MailingState = (String) location.address.state.value;
            }
            if (location.address.zip != null && location.address.zip.value != null) {
                c.MailingPostalCode = (String) location.address.zip.value;
            }
            if (location.address.country != null && location.address.country.value != null) {
                c.MailingCountry = (String) location.address.country.value;
            }
        }
        
        // Last Update
        if (updatedTs != null && updatedTs.value != null) {
            //c.jigsaw_clean__Jigsaw_Last_Modified_Date__c = DateTime.valueOfGmt(((String) updatedTs.value).replace('T', ' '));
        }
        
        // Non-UI Fields
        if (contactId != null && contactId.value != null) {
             //c.jigsaw_clean__Jigsaw_Id__c = String.valueOf(contactId.value);
        }
        
        return c;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testToXml() {
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.locationId = new MatcherField(1234L, true);
        location.locationPhone = new MatcherField('+1.650.235.8400', false);
        location.address = address;
        
        MatcherContact contact = new MatcherContact();
        contact.email = new MatcherField('bqueener@salesforce.com', true);
        contact.firstName = new MatcherField('Brett', false);
        contact.lastName = new MatcherField('Queener', true);
        contact.companyName = new MatcherField('Salesforce.com, Inc.', false);
        contact.phone = new MatcherField('+1.415.901.8473', true);
        contact.title = new MatcherField('Executive Vice President and General Manager, Data.com', false);
        contact.contactId = new MatcherField('8931912', true);
        contact.companyId = new MatcherField('159110', false);
        contact.employeeCount = new MatcherField(3607, true);
        contact.revenue = new MatcherField(Double.valueOf('1.08E9'), false);
        contact.graveyard = new MatcherField('GRAVEYARDED', true);
        contact.location = location;
        contact.industry = new MatcherField('Software & Internet', false);
        contact.subIndustry = new MatcherField('Software , E-commerce and Internet Businesses', true);
        contact.sfdcIndustry = new MatcherField('Technology', false);
        contact.dunsNumber = new MatcherField('123456789', true);
        //contact.updatedTs = new MatcherField('2012-02-29T19:38:47', true);
        contact.externalKey = new MatcherField('003d000000ikxJDAAY', false);
        
        String expectedXml =
            '<' + MatcherAPI.TAG_NAME_CONTACT + '>' +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_EMAIL, contact.email) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_FIRST_NAME, contact.firstName) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_LAST_NAME, contact.lastName) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_COMPANY_NAME, contact.companyName) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_PHONE, contact.phone) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_TITLE, contact.title) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_CONTACT_ID, contact.contactId) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_COMPANY_ID, contact.companyId) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, contact.employeeCount) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_REVENUE, contact.revenue) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_GRAVEYARD, contact.graveyard) +
                contact.location.toXml() +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_INDUSTRY, contact.industry) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SUB_INDUSTRY, contact.subIndustry) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_SFDC_INDUSTRY, contact.sfdcIndustry) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_DUNS_NUMBER, contact.dunsNumber) +
                //MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_UPDATED_TS, contact.updatedTs) +
                MatcherUtils.fieldToXml(MatcherAPI.TAG_NAME_EXTERNAL_KEY, contact.externalKey) +
            '</' + MatcherAPI.TAG_NAME_CONTACT + '>';
        
        String actualXml = contact.toXml();
        System.assertEquals(expectedXml, actualXml);
    }
    
    private static testmethod void testParse() {
        // Null
        System.assertEquals(null, MatcherContact.parse(null));
        
        // Non-Contact Node
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode root = doc.createRootElement('NotContact', null, null);
        System.assertEquals(null, MatcherContact.parse(root));
        
        // NOTE: The easiest way to test for equality is to compare XML strings because there are
        // no equals methods readily available for all objects other than String.
        
        // Contact
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.locationId = new MatcherField(1234L, true);
        location.locationPhone = new MatcherField('+1.650.235.8400', false);
        location.address = address;
        
        MatcherContact contact = new MatcherContact();
        contact.email = new MatcherField('bqueener@salesforce.com', true);
        contact.firstName = new MatcherField('Brett', false);
        contact.lastName = new MatcherField('Queener', true);
        contact.companyName = new MatcherField('Salesforce.com, Inc.', false);
        contact.phone = new MatcherField('+1.415.901.8473', true);
        contact.title = new MatcherField('Executive Vice President and General Manager, Data.com', false);
        contact.contactId = null;
        contact.companyId = null;
        contact.employeeCount = new MatcherField(3607, true);
        contact.revenue = new MatcherField(Double.valueOf('1.08E9'), false);
        contact.graveyard = new MatcherField('GRAVEYARDED', true);
        contact.location = location;
        contact.industry = new MatcherField('Software & Internet', false);
        contact.subIndustry = new MatcherField('Software , E-commerce and Internet Businesses', true);
        contact.sfdcIndustry = new MatcherField('Technology', false);
        contact.dunsNumber = new MatcherField('123456789', true);
        //contact.updatedTs = new MatcherField('2012-02-29T19:38:47', true);
        contact.externalKey = new MatcherField('003d000000ikxJDAAY', false);
        String expectedXml = contact.toXml();
        
        doc = new Dom.Document();
        doc.load(expectedXml);
        MatcherContact actualContact = MatcherContact.parse(doc.getRootElement());
        System.assertNotEquals(null, actualContact);
        System.assertEquals(expectedXml, actualContact.toXml());
        
        // Invalid contactId, companyId, employeeCount, revenue
        contact.contactId = null;
        contact.companyId = null;
        contact.employeeCount = null;
        contact.revenue = null;
        expectedXml = contact.toXml();
        
        Dom.XmlNode contactNode = doc.getRootElement();
        Dom.XmlNode node;
        // = contactNode.getChildElement(MatcherAPI.TAG_NAME_CONTACT_ID, null);
        // Node is now treated as text!
        //node.addTextNode('NotNumber');
        //node = contactNode.getChildElement(MatcherAPI.TAG_NAME_COMPANY_ID, null);
        // Node is now treated as text!
        //node.addTextNode('NotNumber');
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_EMPLOYEE_COUNT, null);
        node.addTextNode('NotNumber');
        node = contactNode.getChildElement(MatcherAPI.TAG_NAME_REVENUE, null);
        node.addTextNode('NotNumber');
        actualContact = MatcherContact.parse(doc.getRootElement());
        System.assertNotEquals(null, actualContact);
        System.assertEquals(expectedXml, actualContact.toXml());
    }
    
    private static testmethod void testToLead() {
        // Missing Fields
        MatcherContact contact = new MatcherContact();
        System.assertNotEquals(null, contact.toLead());

        // All Fields
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.address = address;
        
        contact = new MatcherContact();
        contact.email = new MatcherField('bqueener@salesforce.com', true);
        contact.firstName = new MatcherField('Brett', false);
        contact.lastName = new MatcherField('Queener', true);
        contact.companyName = new MatcherField('Salesforce.com, Inc.', false);
        contact.phone = new MatcherField('+1.415.901.8473', true);
        contact.title = new MatcherField('12345678901234567890123456789012345678901234567890123456789012345678901234567890 Executive Vice President and General Manager, Data.com', false);
        contact.contactId = new MatcherField('8931912', true);
        contact.companyId = new MatcherField('159110', false);
        contact.employeeCount = new MatcherField(3607, true);
        contact.revenue = new MatcherField(Double.valueOf('1.08E9'), false);
        contact.location = location;
        contact.industry = new MatcherField('Software & Internet', false);
        contact.subIndustry = new MatcherField('Software , E-commerce and Internet Businesses', true);
        contact.sfdcIndustry = new MatcherField('Technology', false);
        contact.dunsNumber = new MatcherField('123456789', true);
        //contact.updatedTs = new MatcherField('2012-02-29T19:38:47', true);
        
        Lead l = contact.toLead();
        System.assertNotEquals(null, l);
        System.assertEquals(contact.email.value, l.Email);
        System.assertEquals(contact.firstName.value, l.FirstName);
        System.assertEquals(contact.lastName.value, l.LastName);
        System.assertEquals(contact.companyName.value, l.Company);
        System.assertEquals(contact.phone.value, l.Phone);
        System.assertEquals(((String) contact.title.value).substring(0, MAX_TITLE_LENGTH), l.Title);
        //System.assertEquals(String.valueOf(contact.contactId.value), l.jigsaw_clean__Jigsaw_Id__c);
        System.assertEquals(contact.employeeCount.value, l.NumberOfEmployees);
        System.assertEquals(contact.revenue.value, l.AnnualRevenue);
        System.assertEquals(address.addressLine.value + ' ' + address.addressLine2.value, l.Street);
        System.assertEquals(address.city.value, l.City);
        System.assertEquals(address.state.value, l.State);
        System.assertEquals(address.zip.value, l.PostalCode);
        System.assertEquals(address.country.value, l.Country);
        System.assertEquals(contact.sfdcIndustry.value, l.Industry);
        //System.assertEquals(contact.dunsNumber.value, l.CompanyDunsNumber);
        //System.assertEquals(DateTime.valueOfGmt(((String) contact.updatedTs.value).replace('T', ' ')), l.jigsaw_clean__Jigsaw_Last_Modified_Date__c);
    }
    
    private static testmethod void testToContact() {
        // Missing Fields
        MatcherContact contact = new MatcherContact();
        System.assertNotEquals(null, contact.toContact());

        // All Fields
        MatcherAddress address = new MatcherAddress();
        address.addressLine = new MatcherField('777 Mariners Island Blvd', true);
        address.addressLine2 = new MatcherField('Ste 400', false);
        address.city = new MatcherField('San Mateo', true);
        address.state = new MatcherField('CA', false);
        address.zip = new MatcherField('94404-5059', true);
        address.country = new MatcherField('USA', false);
        
        MatcherLocation location = new MatcherLocation();
        location.address = address;
        
        contact = new MatcherContact();
        contact.email = new MatcherField('bqueener@salesforce.com', true);
        contact.firstName = new MatcherField('Brett', false);
        contact.lastName = new MatcherField('Queener', true);
        contact.phone = new MatcherField('+1.415.901.8473', true);
        contact.title = new MatcherField('12345678901234567890123456789012345678901234567890123456789012345678901234567890 Executive Vice President and General Manager, Data.com', false);
        contact.contactId = new MatcherField('8931912', true);
        contact.location = location;
        //contact.updatedTs = new MatcherField('2012-02-29T19:38:47', true);
        
        Contact c = contact.toContact();
        System.assertNotEquals(null, c);
        System.assertEquals(contact.email.value, c.Email);
        System.assertEquals(contact.firstName.value, c.FirstName);
        System.assertEquals(contact.lastName.value, c.LastName);
        System.assertEquals(contact.phone.value, c.Phone);
        System.assertEquals(((String) contact.title.value).substring(0, MAX_TITLE_LENGTH), c.Title);
        //System.assertEquals(String.valueOf(contact.contactId.value), c.jigsaw_clean__Jigsaw_Id__c);
        System.assertEquals(address.addressLine.value + ' ' + address.addressLine2.value, c.MailingStreet);
        System.assertEquals(address.city.value, c.MailingCity);
        System.assertEquals(address.state.value, c.MailingState);
        System.assertEquals(address.zip.value, c.MailingPostalCode);
        System.assertEquals(address.country.value, c.MailingCountry);
        //System.assertEquals(DateTime.valueOfGmt(((String) contact.updatedTs.value).replace('T', ' ')), c.jigsaw_clean__Jigsaw_Last_Modified_Date__c);
    }
}