public with sharing class DETConfigController {

    // Page selections
    public static String selectedScenario { get; set; } // Setter will get an Id of the matched scenario
    public String selectedObject { get; set; } // Simply the SObject Label
    public static MatchScenario scenarioWrapper { get; set; } // A wrapper for the selected scenario containing everything needed to describe/inspect the scenario in detail.
    public String batchSoqlString { get; set; } // the soql statement for new batchable submit from page form
    
    // Accessors that control different page behaviors
    public Boolean hasAccess { get; set; }
    public Boolean hasScenarios { get; set; }
    public Boolean hasSettings { get; set; }
    public Boolean hasMappings { get; set; }
    public Boolean hasMatcherConfig { get; set; }
    public Boolean showNewButton { get; set; }
    
    // Page messages
    private transient ApexPages.Message[] messages;
    
    // Private class properties for transacting with data throughtout the page lifecycle.
    private transient RuntimeSettings__c runtimeSetting;
    
    public DETConfigController() {
        // To-Do: Check user for has access in Utils class. Perm should be "View Setup & Configuration"
        hasAccess = true;
        this.messages = new List<ApexPages.Message>();
    }
    
    public PageReference reRender() {
        // Initialize page data for each re-render. Rerendered components will not display correctly without this.
        this.messages = new List<ApexPages.Message>();
        hasScenarios = false;
        hasSettings = false;
        hasMappings = false;
        hasMatcherConfig = false;
        scenarioWrapper = null;
        getScenarios();
        getSettings();
        getMappings();
        getWeightings();
        Boolean hasMessage = false;
        for (ApexPages.Message message : messages) {
            ApexPages.addMessage(message);
            if (message.getSeverity().ordinal() >= ApexPages.Severity.INFO.ordinal()) {
                hasMessage = true;
            }
        }
        return null;
    }
    
    public List<SelectOption> getSobjects() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));

        List<AggregateResult> allScenarios = [SELECT ddcDet__Object_Name__c FROM Match_Scenario__c WHERE IsDeleted = false GROUP BY ddcDet__Object_Name__c];

        if (allScenarios == null) {
            return options;
        }

        // If the scenarios array is not null, add all the names to the list
        for (AggregateResult ar : allScenarios) {
            options.add(new SelectOption((String) ar.get('ddcDet__Object_Name__c'), (String) ar.get('ddcDet__Object_Name__c')));
        }

        return options;
    }
    
    public List<SelectOption> getScenarios() {
        List<SelectOption> result = new List<SelectOption>();
        System.debug('calling getScenarios -- selectedObject: ' + selectedObject + ' selectedScenario: ' + selectedScenario + ' hasSettings = ' + hasSettings);
        if(!String.isBlank(selectedObject)) {
        showNewButton = true;
            
            /* NEW BEHAVIOR - Retrieve all instaces of Match_Scenario__c via master scenario behavior wrapper "MatchScenario" */
            
            Map<String, Match_Scenario__c> scenarios = MatchScenario.getAllScenariosForObject(selectedObject, messages);
            if(scenarios != null) {
                // Initialize picklist with blank entry so that onchange ajax works.
                result.add(new SelectOption('',''));
                for(String s : scenarios.keySet()) {
                    // Add the scenario name from the keyset of the mapping returned. This can be used to reference any scenario and its registered wrapper settings.
                    result.add(new SelectOption(scenarios.get(s).Id , s));
                }
                if(result.size() > 1) {
                    hasScenarios = true;
                }
                // The collection will already have the blank val. We dont want to show that to the user.
                // The page should display the new match scenario button.
                else if(result.size() <= 1) {
                    hasScenarios = false;
                    selectedScenario = null;
                }
            }
        }
        else {
            // no object selected so hide the new button.
            showNewButton = false;
        }
        System.debug('hasScenarios: ' +hasScenarios);
        System.debug('the scenarios: ' +result);
        return result;
    }

    public RuntimeSettings__c getSettings() {
        
        hasSettings = false;
        RuntimeSettings__c result;
        if(hasScenarios && !String.isBlank(selectedScenario)) {
            
            System.debug(selectedScenario);
            System.debug('##########apex page messages: ' +messages);
            scenarioWrapper = MatchScenario.getScenarioDefinition(selectedScenario, messages, false);
            if(scenarioWrapper != null) {
                
                result = scenarioWrapper.thisSetting.cSetting;
                this.hasSettings = true;
            }
            else {
                this.hasMappings = false;
                this.hasSettings = false;
                this.hasMatcherConfig = false;
            }
            
            System.debug(result);
        }
        this.runtimeSetting = result;
        return result;
    }
    
    public Map<String, List<SelectOption>> getWeightings() {
        Map<String, List<SelectOption>> result;
        if(scenarioWrapper != null) {
        
            result = new Map<String, List<SelectOption>>();
        
            List<SelectOption> intList = new List<SelectOption>();
            for(Integer i = 0; i<=9; i++) {
                intList.add(new SelectOption(String.valueOf(i) , String.valueOf(i)));   
            }
            MatchScenario.MatchWeighting weightingDefinition = new MatchScenario.MatchWeighting();
            Map<String, String> labelsMapping = weightingDefinition.fieldLabelsMapping;
            for(String label : labelsMapping.keySet()) {
	            result.put(label, intList);
            }
        }
        
        if(result != null) {
        
                hasMatcherConfig = true;
        }
        return result;
    }
    
    public Map<String, MappedField> getMappings() {
    	System.debug('mappings before new map instantiation: ' +this);
    	
        Map<String, MappedField> result = new Map<String, MappedField>();
        
        if(scenarioWrapper != null) {
            hasMappings = true;
            result = scenarioWrapper.thisMapping;
            System.debug('result from getMappings: ' +result);
        }
        
        return result;
    }
    
    public Map<String, String> getAutocompleteFields() {
    	Map<String, String> result = new Map<String, String>();
        
    	if(selectedObject != null && hasMappings) {
    		
    		Map<String, SObjectField> fmap = Utils.getFieldListing(selectedObject);
    		
    		for(SObjectField field : fmap.values()) {
    			// Removing SOAP type describe from the results array. It's too taxing -
                // if the org has a large number of fields and it's harder to parse the value from the autocomplete.
    			// Schema.DescribeFieldResult dfr = Utils.getFieldDescribe(selectedObject, String.valueOf(fname));
    			//' ('+dfr.getSoapType()+')'+
    			
                try {
                    //Add it to the correct map of its type
                    String fieldName = String.valueOf( field );
                    String fieldType = '"' + String.valueOf( field.getDescribe().getSOAPType() ).toLowerCase().capitalize() + '"';
                    String fieldsForType = result.get( fieldType );
                    fieldsForType = (fieldsForType == null ? '' : fieldsForType.replaceAll( '"', '' ) + ',') + fieldName;
                    result.put( fieldType, '"' + fieldsForType + '"' );
                }
                catch (System.SObjectException se) {
                    System.debug('Catching type inaccessible exception in autocomplete field loader');
                    continue;
                }
    		}
    	}

        System.debug( result );
    	return result;
    }
    

    /*  Initial version by SB, replaced by above by DR
    
    public String[] getAutocompleteFields() {
        String[] result = new List<String>();
        if(selectedObject != null && hasMappings) {
            
            Map<String, SObjectField> fmap = Utils.getFieldListing(selectedObject);
            
            for(SObjectField fname : fmap.values()) {
                // Removing SOAP type describe from the results array. It's too taxing -
                // if the org has a large number of fields and it's harder to parse the value from the autocomplete.
                // Schema.DescribeFieldResult dfr = Utils.getFieldDescribe(selectedObject, String.valueOf(fname));
                //' ('+dfr.getSoapType()+')'+
                result.add('"'+String.valueOf(fname)+'"');
            }
        }
        return result;
    }
    /*
    */
    
    public String getScenarioJavascript() {
        return MatchScenario.getJavascriptBody(scenarioWrapper);
    }

    
    //---------------------------------------------------------------------------------------------
    // Javascript Remotes
    //---------------------------------------------------------------------------------------------
    
    // Generic remote save
    @RemoteAction
    public static PageReference remoteSave(String sobj, String field, Object val) {
        
        // Do logic to update the sobject input field with the specified value
        
        if(sobj == null || field == null || val == null) {
            
            ApexPages.Message pMessage = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to save the setting you changed, the required arguments were not supplied to the asynchronous update queue. \n Please refresh the page and try again.');
            ApexPages.addMessage(pMessage);
        }
        System.debug(sobj);
        //sobj.put(field, val);
        //update sobj;
        
        return null;
    }
    
    // Saves a match scenario from the config panel
    @RemoteAction
    public static Boolean remoteSaveScenario(String field, Object fval, Object scenarioId) {
    	Boolean result;
    	if(field == null || fval == null || scenarioId == null) {
    		throw new ddcLib1.IllegalStateException('Invalid input recieved for remoteSaveScenario:  ' +field+ '  '+fval+' '+scenarioId);
    	}
    	
    	MatchScenario ms = MatchScenario.getScenarioDefinition(String.valueOf(scenarioId), new ApexPages.Message[0]);
    	
    	try {
    	   ms.thisConcreteScenario.put(field, fval);	
    	}
    	catch(System.SObjectException se) {
    		// Field type exception. Cast fval as a decimal instead to handle an Integer input.
    		System.debug('Escaping gracefully from System.SObjectException! ' + '\n'+se.getMessage() +'\n'+se.getStackTraceString());
    		ms.thisConcreteScenario.put(field, Integer.valueOf(fval));
    	}
    	result = Database.update(ms.thisConcreteScenario).isSuccess();
    	
    	return result;
    }
    
    // Saves a field mapping from the mapping table on the config panel
    @RemoteAction
    public static Boolean remoteSaveToMapping(MappedField mf, String objName) {
    	Boolean result;
    	if(mf == null) {
    		throw new ddcLib1.IllegalStateException('Invalid input recieved for remoteSaveToMappings:  ' +mf+ '  '+objName);
    	}
    	System.debug('remoteSaveToMappings was called successfully!   ' +mf +'      ' +objName);

		result = MappedField.upsertMappedField(mf, objName, new ApexPages.Message[0]);

    	return result;
    }
    
    // Saves a runtime setting configuration element from the config panel
    @RemoteAction
    public static Boolean remoteSaveSetting(String field, Object checkval, Object matchScenarioId) {
        Boolean result;
        if(field == null || checkval == null || matchScenarioId == null) {
        	throw new ddcLib1.NullPointerException('Null arguments are not valid when trying to save a runtime setting. Expected a field value, got: ' + field + ' . Expected a checkval, got: ' + checkval);
        }
        System.debug('remote save setting called successfully: ' +field+ ', ' +checkval+ ', ' +matchScenarioId);
        
        //RuntimeSetting thisSetting = new RuntimeSetting((String)matchScenarioId, new ApexPages.Message[0]);
        
        RuntimeSetting thisSetting = RuntimeSetting.getSettings((String)matchScenarioId, new ApexPages.Message[0]);
        System.debug('Debugging DETConfigController.remoteSaveSetting.thisSetting.cSetting == :: ' +thisSetting.cSetting);
        thisSetting.cSetting.put(field, Boolean.valueOf(checkval));
        update thisSetting.cSetting;

        return result;
    }
    
    public PageReference newScenario() {
        Schema.DescribeSObjectResult R = Match_Scenario__c.SObjectType.getDescribe();
        // Add /o to end of URL to view recent
        return new PageReference('/' + R.getKeyPrefix() + '/e');
    }
    
}