public interface MatchLibraryInterface {

	/**
	 * @return
	 * 		true if a match was found
	 */
	Boolean performMatch( Wrapper w, SObject o );

}