public class MatchedLead extends MatchedObject {

	LeadWrapper lw;
	
	public LeadWrapper getLeadWrapper() {
		return this.lw;
	}
	
	private void setLeadWrapper(LeadWrapper lw) {
		this.lw = lw;
	}
	
	private MatchedLead() {
		// default constructor
	}
	
	public MatchedLead(Wrapper w) {
		super();
		if(w instanceof LeadWrapper) {
			setLeadWrapper((LeadWrapper)w);
		}
		else throw new ddcLib1.IllegalArgumentException('Unable to attempt a match without the correct wrapper. Expected LeadWrapper, got: ' +w);
	}
	
	public override SObject[] match() {
		System.debug('Calling match within MatchedLead with abstract final implementation.');
		return new SObject[0];
	}
	
	// Matches a wrapper to another wrapper and is encapsulated by an instance of MatchedLead
	public void match(Wrapper w) {
		if(w instanceof LeadWrapper) {
			setLeadWrapper((LeadWrapper)w);
		}
		else throw new ddcLib1.IllegalArgumentException('Unable to attempt a match without the correct wrapper. Expected LeadWrapper, got: ' +w);
		
		System.debug('Calling match within MatchedLead with abstract final implementation.');
	}
	
	public override Descriptor getDescribe() {
		
		MatchedLeadDescriptor thisDescriptor; 
		
		if(thisDescriptor == null) {
			thisDescriptor = new MatchedLeadDescriptor();
		}
		
		return thisDescriptor.getDescribe();
	}
	
	/**
	 *	INNER CLASSES
	 *	Descritpor for MatchedLead. Instantiated when requested, otherwise save the statements.
	 */
	private class MatchedLeadDescriptor extends Descriptor {
	    
	    //---------------------------------------------------------------------------------------------
	    // Properties
	    //---------------------------------------------------------------------------------------------
	    public String matchCode;
	    
	    //---------------------------------------------------------------------------------------------
	    // Constructors
	    //---------------------------------------------------------------------------------------------
	    MatchedLeadDescriptor() {
			super();
	    	getDescribe();
	    }
	    
	    //---------------------------------------------------------------------------------------------
	    // Methods
	    //---------------------------------------------------------------------------------------------
		public override Descriptor getDescribe() {
			matchCode = 'test match code result';
			return this;
		}
		
		public override String getToken() {
			return null;
		}
		
		public override String getType() {
			return null;
		}
	}
}