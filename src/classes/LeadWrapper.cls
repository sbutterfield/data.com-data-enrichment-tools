/**
 *	FIXME: Getters and setters need to be rethought
 *	FIXME: 
 */
 
 
public virtual class LeadWrapper extends SObjectWrapper {

	// Wrapped concrete
	public Lead cLead { get; set; }
	
	// Set the enumerated type for the wrapper so the implementation for a wrapper knows how to get attributes from it.
	public final DDCType typeOf = DDCType.CONTACT;
	
	/* This is an example of what the getters and setters should look like
	public String firstName {
		get { return this.cLead.*GET MAPPED FIELD FOR PROPERTY*.*GET VALUE FROM FIELD FROM cLead*; } 
		set { this.cLead.*GET MAPPED FIELD FOR PROPERTY*.*PUT THE VALUE IN TO THE FIELD* = value; } 
	}
	*/
	
	// Contact attrib
	public String firstName { get; set; }
	public String lastName { get; set;}
	public String email { get; set; }
	public String title { get; set; }
	public String phone { get; set; }
	public String address { get; set; }
	public String city { get; set; }
	public String state { get; set; }
	public String zip { get; set; }
	public String country { get; set; }
	public String contactId { get; set; }
	
	// Company attrib
	public String website { get; set; }
	public String dunsNumber { get; set; }
	public String companyId { get; set; }
	public String companyName { get; set; }

	
	public void setSObject(SObject l) {
		if(l instanceof Lead) {
			this.cLead = (Lead)l;
			this.firstName = this.cLead.FirstName;
			this.lastName = this.cLead.LastName;
			this.email = this.cLead.Email;
			this.title = this.cLead.Title;
			this.phone = this.cLead.Phone;
			this.address = this.cLead.Street;
			this.city = this.cLead.City;
			this.state = this.cLead.State;
			this.zip = this.cLead.PostalCode;
            this.country = this.cLead.Country;
			this.contactId = this.cLead.Jigsaw;
			this.companyName = this.cLead.Company;
			this.website = this.cLead.Website;
			this.dunsNumber = this.cLead.CompanyDunsNumber;   // Only available if mapped
		}
		else throw new ddcLib1.IllegalArgumentException('Cannot construct a Lead Wrapper without a valid instance of Lead.');
	}
	
	// Reserved for matching a wrapper to another wrapper/SObject
	/*public virtual SObject[] match() {
		return new SObject[0];
	}
	
	public virtual override Map<String, MatchedObject[]> getMatches() {
		return new Map<String, MatchedObject[]>();
	}*/
	
	public LeadWrapper(Lead l) {
		super(l, DDCType.CONTACT);
		setSObject(l);
	}
	
	
}