public class FieldMapping {
	
	/**
	 * Retrieves all of the mapped fields and their corresponding metadata
	 * @return = Map<String (ddc property name), MappedField (wrapper for a Mapped_Field__c)>
	 */
	public static Map<String, MappedField> getFieldMappingForScenario(MatchScenario input) {
		
		Map<String, MappedField> result;
		
		if(input.objectName == null) return result;
		
		Map<String, Schema.SOBjectField> objField = Utils.GLOBAL_DESCRIBE_CACHE.get(input.objectName).getDescribe().fields.getMap();
		
		Map<String, Schema.SObjectField> mfieldMap = Utils.GLOBAL_DESCRIBE_CACHE.get(MappedField.SOBJECT_NAME).getDescribe().fields.getMap();
		
		String soql;
		List<Mapped_Field__c> qResult;
		
		// Select all of the fields and all of the mappings for the sobject on the MatchScenario input
		try {
			
			soql = 'SELECT ';
			
			for(Schema.SObjectField fName : mfieldMap.values()) {
				soql += fName + ',';
			}
			
			// Trim off last comma so we don't throw query exceptions
			soql = soql.removeEnd(',');
			
			// Add the FROM clause
			soql += ' FROM ' + MappedField.SOBJECT_NAME;
			
			// Add the WHERE clause if there is a scenario id
			if(input.scenarioId != null) {
				soql += ' WHERE Name = \'' + input.scenarioId + '\'';
			}
			
			qResult = Database.query(soql);
		}
		catch(Exception e) {
			// Usually reserved for Query Exceptions, but other shit can go wrong here...
			System.debug('Exception occurred trying to fetch the custom field mapping settings. ' +e);
			result = null;
		}
		
		// Build the result map for the mapped fields returned by the query
		result = new Map<String, MappedField>();
		MappedField mfwrapper;
		for(Mapped_Field__c cmf: qResult) {
			mfwrapper = new MappedField(cmf);
			result.put(mfwrapper.propertyName, mfwrapper);
		}
		
		Map<String, Map<String, DDCProperty>> allProperties = DDCProperty.getProperties(null);
		
		/**  
		 *	Whether we have mapped fields or not, need to wrap and 
		 *	show all possible attributes for mapping so that new mappings may be created if not exist
		 */
		// If the sobject is "Contact", remove the Company attributes
		// Otherwise, if the sobject is "Account" remove the Contact attributes
		if(input.objectName == 'Contact') {
			allProperties.remove('Company');
		}
		else if(input.objectName == 'Account') {
			allProperties.remove('Contact');
		}
		
		// Will only iterate max of twice for each mapping for the type (Contact and/or Company)
		for(Map<String, DDCProperty> typeMapping : allProperties.values()) {
			for(String propertyName : typeMapping.keySet()) {
				// If the result map already built a MappedField wrapper for display:
				if(result.containsKey(propertyName)) {
					// Added to fetch the correspondingTypes from property, otherwise upsert cast doesn't work correctly later. (MappedField.upsertMappedField())
					mfwrapper = result.get(propertyName);
					mfwrapper.correspondingTypes = typeMapping.get(propertyName).dtype;
					result.put(propertyName,mfwrapper);
				}
				else {
					// Whole new MappedField, instantiate and copy in as much as we can. Upsert takes care of the rest.
					mfwrapper = new MappedField();
					mfwrapper.cMappedField = null;
					mfwrapper.correspondingTypes = typeMapping.get(propertyName).dtype;
					mfwrapper.ddcproperty = typeMapping.get(propertyName).id;
					mfwrapper.matchScenario = input.scenarioId;
					mfwrapper.fieldType = null;
					mfwrapper.fill = false;
					mfwrapper.overwrite = false;
					mfwrapper.id = null;
					mfwrapper.mappedFieldName = null;
					mfwrapper.propertyName = propertyName;
					result.put(propertyName, mfwrapper);
				}
			}
		}
		
		return result;
	}

}