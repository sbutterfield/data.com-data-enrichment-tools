/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest(SeeAllData=false)
private class UtilsTest {

	// Test for field permissions
    static testMethod void testGetCrud() {
        String field = 'Name';
        String obj = 'Account';
        String action = 'access';
        System.assert(Utils.getCrud(obj, field, action));
        action = 'create';
        System.assert(Utils.getCrud(obj, field, action));
        action = 'update';
        System.assert(Utils.getCrud(obj, field, action));
        action = 'delete';
        System.assertEquals(Utils.getCrud(obj, field, action), false);
        action = 'cascade';
        System.assertEquals(Utils.getCrud(obj, field, action), false);
        action = 'write';
        System.assert(Utils.getCrud(obj, field, action));
    }
    
    // Test to check if running user has license
    static testMethod void testHasLicense() {
    	System.assertEquals(true, Utils.hasLicense());
    }
    
    // Test to check if the current object is actually a salesforce Id
    static testMethod void testIsSalesforceId() {
    	Lead l = new Lead(FirstName='test',LastName='lead',Company='Salesforce',Email='sbutterfield@salesforce.com');
    	insert l;
    	Id validId = l.Id;
    	System.assert(Utils.isValidId(validId)); 
    	String invalidId = '6738ifuyga798fh897fq9fh';
    	System.assertEquals(Utils.isValidId(invalidId), false);
    }

    private static testmethod void testParseDate() {
        System.assertEquals(null, Utils.parseDate(null));
        System.assertEquals(null, Utils.parseDate(''));
        System.assertEquals(datetime.newInstance(2004, 4, 26, 23, 24, 40), Utils.parseDate('2004-04-26 23:24:40 PDT'));
    }
    
    private static testmethod void testParseISODate() {
        System.assertEquals(null, Utils.parseISODate(null));
        System.assertEquals(null, Utils.parseISODate(''));
        System.assertEquals(datetime.newInstanceGMT(2004, 4, 26, 23, 24, 40), Utils.parseISODate('2004-04-26T23:24:40Z'));
        System.assertEquals(datetime.newInstanceGMT(2010, 6, 28, 17, 25, 58), Utils.parseISODate('2010-06-28T17:25:58Z'));
    }

    private static testmethod void testCreateRequest() {
        // Null URL
        HttpRequest req = Utils.createRequest(null, null, null, null, null);
        System.assertEquals(null, req);
        
        // NOTE: There is no method to retrieve the timeout.
        
        // Null Parameters
        String expectedEndpoint = 'http://www.salesforce.com';
        String expectedMethod = 'GET';
        Boolean expectedCompressed = false;
        req = Utils.createRequest(expectedEndpoint, null, null, expectedCompressed, null);
        System.assertNotEquals(null, req);
        System.assertEquals(expectedMethod, req.getMethod());
        System.assertEquals(expectedCompressed, req.getCompressed());
        
        // Valid
        expectedEndpoint = 'http://www.salesforce.com';
        expectedMethod = 'POST';
        expectedCompressed = true;
        req = Utils.createRequest(expectedEndpoint, '<matches/>', expectedMethod, expectedCompressed, 20000);
        System.assertNotEquals(null, req);
        System.assertEquals(expectedMethod, req.getMethod());
        System.assertEquals(expectedCompressed, req.getCompressed());
    }
    
    private static testmethod void testSendRequest() {
        ApexPages.Message[] messages = new List<ApexPages.Message>();
        Integer expectedMessages = 0;
        
        // Null
        System.assertEquals(null, Utils.sendRequest(null, messages));
        System.assertEquals(expectedMessages, messages.size());
        
        // Valid
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('http://www.jigsaw.com/');
        System.assertNotEquals(null, Utils.sendRequest(req, messages));
        System.assertEquals(expectedMessages, messages.size());
    }

    private static testmethod void testGetProfilePerms() {
        // Read Only profile should not have access to customize application.
        Profile p = [select id from profile where name='Read Only']; 
        System.assertEquals(false, Utils.getProfilePerms('PermissionsCustomizeApplication', p.Id));
        
        // System Administrator profile should have access to customize application.
        p = [select id from profile where name='System Administrator']; 
        System.assertEquals(true, Utils.getProfilePerms('PermissionsCustomizeApplication', p.Id));
        
        // Invalid profile should not have access to anything.
        p = new Profile();
        System.assertEquals(false, Utils.getProfilePerms('PermissionsCustomizeApplication', p.Id));
    }

    private static testmethod void testMessageListHasSeverity() {
        List<ApexPages.Message> messages = null;
        System.assertEquals(false, Utils.messageListHasSeverity(messages, null));
        
        messages = new List<ApexPages.Message>();
        System.assertEquals(false, Utils.messageListHasSeverity(messages, null));
        
        messages = new List<ApexPages.Message>{new ApexPages.Message(ApexPages.Severity.INFO, 'Not an error.')};
        System.assertEquals(false, Utils.messageListHasSeverity(messages, ApexPages.Severity.ERROR));
        
        messages = new List<ApexPages.Message>{new ApexPages.Message(ApexPages.Severity.FATAL, 'Fatal error!')};
        System.assertEquals(true, Utils.messageListHasSeverity(messages, ApexPages.Severity.ERROR));
        
        messages = new List<ApexPages.Message>{new ApexPages.Message(ApexPages.Severity.ERROR, 'Regular error!')};
        System.assertEquals(true, Utils.messageListHasSeverity(messages, ApexPages.Severity.ERROR));
    }

    private static testMethod void testPhoneEquals() {
        System.assertEquals(true, Utils.phoneEquals(null, null));
        System.assertEquals(true, Utils.phoneEquals('', ''));
        System.assertEquals(false, Utils.phoneEquals(null, ''));
        System.assertEquals(false, Utils.phoneEquals('', null));
        System.assertEquals(false, Utils.phoneEquals(null, '+1.415.901.7000'));
        System.assertEquals(false, Utils.phoneEquals('', '+1.415.901.7000'));
        
        System.assertEquals(true, Utils.phoneEquals('+1.415.901.7000', '+1.415.901.7000'));
        System.assertEquals(true, Utils.phoneEquals('415.901.7000', '(415) 901-7000'));
        System.assertEquals(true, Utils.phoneEquals('+1.415.901.7000', '(415) 901-7000'));
        System.assertEquals(false, Utils.phoneEquals('+1.415.901.7000', '+44.415.901.7000'));
    }
}