/**
 *  @author: Shawn Butterfield, Salesforce.com Inc
 *  This class is used as a process controller for individual contexts of trigger and/or class related runtimes
 *  The static variables assigned here can be set and shared acrossed a single running contexts or thrown into a future one
 */
@Deprecated
global class RuntimeState {
	@Deprecated
	global static Boolean IS_BATCH_CONTEXT = false;
	@Deprecated
	global static Boolean IS_TRIGGERED_CONTEXT = false;
	@Deprecated
	global static Boolean IS_SINGLE_RECORD = false;
	@Deprecated
	global static Boolean IS_FUTURE_CONTEXT = false;
	@Deprecated
	global static Boolean HOST_IS_BEFORE = false;
	@Deprecated
	global static Boolean HOST_IS_AFTER = false;
	@Deprecated
	global static Boolean ORIG_CONTEXT_WAS_INSERT = false;
	@Deprecated
	global static Boolean ORIG_CONTEXT_WAS_UPDATE = false;
	@Deprecated
	global static Boolean beforeContinuumHasBeenSet = false;
	@Deprecated
	global static Boolean afterContinuumHasBeenSet = false;

	static Set<Id> setBeforeContinuum { get; private set; }

	static Set<Id> setAfterContinuum { get; private set; }
	
	/**
	 *	@method: getRunStatus
	 *	@param: Set<> of Ids
	 *	The method is passed a set of record Ids and is compared to another continuum set of record Ids within the current running context.
	 *	If the set has already been through the current context, the method returns true. If not, the method add's the new Ids to the continuum and returns false.
	 */
	@Deprecated
	global static Boolean getBeforeRunStatus(Set<Id> idSet) {
		
		if(idSet != null) {
			
			if(!beforeContinuumHasBeenSet) { setBeforeContinuum = new Set<Id>(); }
			
			if(!setBeforeContinuum.containsAll(idSet)) {
				
				for(Id thisId : idSet) {
				
					setBeforeContinuum.add(thisId);
				}
				
				beforeContinuumHasBeenSet = true;
				return false;
			}
			else {
				
				return true;
			}
		}
		else return false;
	}
	@Deprecated
	global static Boolean getAfterRunStatus(Set<Id> idSet) {
		
		if(idSet != null) {
			
			if(!afterContinuumHasBeenSet) { setAfterContinuum = new Set<Id>(); }
			
			if(!setAfterContinuum.containsAll(idSet)) {
				
				for(Id thisId : idSet) {
				
					setAfterContinuum.add(thisId);
				}
				
				afterContinuumHasBeenSet = true;
				return false;
			}
			else {
				
				return true;
			}
		}
		else return false;
	}
	@Deprecated
	global static String dump() {
		return 'future context=' +IS_FUTURE_CONTEXT+ 
		' is batch=' +IS_BATCH_CONTEXT+
		' is triggered=' +IS_TRIGGERED_CONTEXT+
		' is single record=' +IS_SINGLE_RECORD+
		' is before=' +HOST_IS_BEFORE+ 
		' is after=' +HOST_IS_AFTER+ 
		' original context was insert=' +ORIG_CONTEXT_WAS_INSERT+ 
		' original context was update=' +ORIG_CONTEXT_WAS_UPDATE+ 
		' before continuum has been set=' +beforeContinuumHasBeenSet+ 
		' after continuum has been set=' +afterContinuumHasBeenSet+
		'\n Contents of before continuum: ' +setBeforeContinuum+
		'\n Contents of after continuum: ' +setAfterContinuum;
	}
}