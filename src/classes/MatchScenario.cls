global with sharing class MatchScenario implements Comparable {

    //---------------------------------------------------------------------------------------------
    // Properties of a concrete Match_Scenario__c
    //---------------------------------------------------------------------------------------------
    public String scenarioId { get; set; }
    public String scenarioName { get; set; }
    public String objectName { get; set; }
    public String rule1 { get; set; }
    public String rule2 { get; set; }
    public String rule3 { get; set; }
    public String rule4 { get; set; }
    public String rule5 { get; set; }
    
    public Integer runOrder { get; set; }

    // Match weightings:
    public Integer matchConfidenceThreshold { get; set; }
    public Integer companyAntipatternWeight { get; set; }
    public Integer companyDirectPhoneWeight { get; set; }
    public Integer companyNameLocationWeight { get; set; }
    public Integer companyNameDomainWeight { get; set; }
    public Integer companyNameOnlyWeight { get; set; }
    public Integer emailWeight { get; set; }
    public Integer firstLastPhoneWeight { get; set; }
    public Integer firstLastTitleWeight { get; set; }
    public Integer firstLastGeneralizedEmailWeight { get; set; }
    public Integer locationAgreementWeight { get; set; }
    public Integer tradestyleWeight { get; set; }
    public Integer websiteDomainWeight { get; set; }

    // Boolean properties
    public Boolean isDefault { get; set; }
    public Boolean isActive { get; set; }
    
    // Static property's for concrete names used throughout implementation
    public static final String SOBJECT_NAME = Utils.NAMESPACE_PREFIX + 'Match_Scenario__c';
    public static final String OBJECT_NAME_FIELD = 'Object_Name__c';
    public static final String RULE_1_FIELD = 'Rule_Criteria_1__c';
    public static final String RULE_2_FIELD = 'Rule_Criteria_2__c';
    public static final String RULE_3_FIELD = 'Rule_Criteria_3__c';
    public static final String RULE_4_FIELD = 'Rule_Criteria_4__c';
    public static final String RULE_5_FIELD = 'Rule_Criteria_5__c';
    public static final String RUN_ORDER_FIELD = 'Run_Order__c';
    public static final String FILTER_LOGIC_FIELD = 'Filter_Logic__c';
    public static final String IS_DEFAULT_FIELD = 'Default__c';
    public static final String ACTIVE_FIELD = 'Active__c';
    public static final String MATCH_THRESHOLD_WEIGHT_FIELD = 'Match_Confidence_Threshold__c';
    public static final String COMPANY_ANTIPATTERN_WEIGHT_FIELD = 'Company_Antipattern_Weight__c';
    public static final String COMPANY_PHONE_WEIGHT_FIELD = 'Company_Direct_Phone_Weight__c';
    public static final String COMPANY_NAME_LOCATION_WEIGHT_FIELD = 'Company_Name_Location_Weight__c';
    public static final String COMPANY_NAME_ONLY_WEIGHT_FIELD = 'Company_Name_Only_Weight__c';
    public static final String COMPANY_NAME_WEBSITE_DOMAIN_WEIGHT_FIELD = 'Company_Name_Website_Domain_Weight__c';
    public static final String EMAIL_WEIGHT_FIELD = 'Email_Weight__c';
    public static final String FN_LN_GENERALIZED_EMAIL_WEIGHT_FIELD = 'FN_LN_Generalized_Email_Weight__c';
    public static final String FN_LN_PHONE_WEIGHT_FIELD = 'FirstName_LastName_Phone_Weight__c';
    public static final String FN_LN_TITLE_WEIGHT_FIELD = 'FirstName_LastName_Title_Weight__c';
    public static final String LOCATION_AGREEMENT_WEIGHT_FIELD = 'Location_Agreement_Weight__c';
    public static final String TRADESTYLE_WEIGHT_FIELD = 'Tradestyle_Weight__c';
    public static final String WEBSITE_DOMAIN_WEIGHT_FIELD = 'Website_Domain_Weight__c';

    //---------------------------------------------------------------------------------------------
    // Cache (optimizes runtime)
    //---------------------------------------------------------------------------------------------

    /**
     *  Cached copy of the default scenario for this runtime
     */
    private static MatchScenario defaultCache { get; set; }

    /**
     *  Local cache of all match scenarios already defined for this runtime instance.
     *  @definition = < (String) Id of Match_Scenario__c in MatchScenario, (MatchScenario) Abstract MatchScenario >
     */
    private static Map<String, MatchScenario> scenarioCache { 
        get {
            if (scenarioCache == null) {
                scenarioCache = new Map<String, MatchScenario>();
            }
            return scenarioCache;
        }
    }

    private static Map<String, MatchScenario> wrapperCache {
        get {
            if(wrapperCache == null) {
                wrapperCache = new Map<String, MatchScenario>();
            }
            return wrapperCache;
        }
    }

    /**
     *  Cache used for retrieving all scenarios for an object.
     *  see getAllScenariosForObject() method.
     */
    private static Map<String, Match_Scenario__c> objectScenariosCache { 
        get {
            if (objectScenariosCache == null) {
                objectScenariosCache = new Map<String, Match_Scenario__c>();
            }
            return objectScenariosCache;
        }
    }
    

    //---------------------------------------------------------------------------------------------
    // Wrappers encapsulated within this implementation
    //---------------------------------------------------------------------------------------------
    // Each wrapper this MatchScenario encapsulates based on their connection to "thisConcreteScenario" (Match_Scenario__c)
    public Map<String, MappedField> thisMapping { get; set; }
    public RuntimeSetting thisSetting { get; set; }
    
    // The concrete Match_Scenario__c this wrapper encapsulates.
    public Match_Scenario__c thisConcreteScenario { get; set; }
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    /*
     *  @throws ddcLib1.NullPointerException
     */
    public MatchScenario(Match_Scenario__c cObj) {
    	// Construct a new Match scenario from it's concrete SObject
    	if (cObj == null) {
    		throw new ddcLib1.NullPointerException('Unable to construct a new instance of MatchScenario from a null concrete argument in constructor.');
    	}
    	this.isActive = Boolean.valueOf(cObj.get(ACTIVE_FIELD));
    	this.isDefault = Boolean.valueOf(cObj.get(IS_DEFAULT_FIELD));
    	this.objectName = (String)cObj.get(OBJECT_NAME_FIELD);
    	this.rule1 = (String)cObj.get(RULE_1_FIELD);
    	this.rule2 = (String)cObj.get(RULE_2_FIELD);
    	this.rule3 = (String)cObj.get(RULE_3_FIELD);
    	this.rule4 = (String)cObj.get(RULE_4_FIELD);
    	this.rule5 = (String)cObj.get(RULE_5_FIELD);
        // If there is no run order defined, give it the lowest possible value.
        // Causes NPE later if not properly constructed, shouldn't every get a null RO.
    	this.runOrder = cObj.get(RUN_ORDER_FIELD) != null ? Integer.valueOf(cObj.get(RUN_ORDER_FIELD)) : 100;
    	this.scenarioId = cObj.Id;
    	this.scenarioName = cObj.Name;
        this.matchConfidenceThreshold = Integer.valueOf(cObj.get(MATCH_THRESHOLD_WEIGHT_FIELD));
        this.companyAntipatternWeight = Integer.valueOf(cObj.get(COMPANY_ANTIPATTERN_WEIGHT_FIELD));
        this.companyDirectPhoneWeight = Integer.valueOf(cObj.get(COMPANY_PHONE_WEIGHT_FIELD));
        this.companyNameLocationWeight = Integer.valueOf(cObj.get(COMPANY_NAME_LOCATION_WEIGHT_FIELD));
        this.companyNameDomainWeight = Integer.valueOf(cObj.get(COMPANY_NAME_WEBSITE_DOMAIN_WEIGHT_FIELD));
        this.companyNameOnlyWeight = Integer.valueOf(cObj.get(COMPANY_NAME_ONLY_WEIGHT_FIELD));
        this.emailWeight = Integer.valueOf(cObj.get(EMAIL_WEIGHT_FIELD));
        this.firstLastPhoneWeight = Integer.valueOf(cObj.get(FN_LN_PHONE_WEIGHT_FIELD));
        this.firstLastTitleWeight = Integer.valueOf(cObj.get(FN_LN_TITLE_WEIGHT_FIELD));
        this.firstLastGeneralizedEmailWeight = Integer.valueOf(cObj.get(FN_LN_GENERALIZED_EMAIL_WEIGHT_FIELD));
        this.locationAgreementWeight = Integer.valueOf(cObj.get(LOCATION_AGREEMENT_WEIGHT_FIELD));
        this.tradestyleWeight = Integer.valueOf(cObj.get(TRADESTYLE_WEIGHT_FIELD));
        this.websiteDomainWeight = Integer.valueOf(cObj.get(WEBSITE_DOMAIN_WEIGHT_FIELD));
    	this.thisConcreteScenario = cObj;
    }
    
    public MatchScenario() {
    	// Default constructor
    }
    
    
    /**
     * Inner class apex object to wrap the details about a match weighting.
     * This class describes:
     *  1) available match weight indices and their allowed values
     *  2) cache current values for MatchScenario
     *  3) provides as a container for describing how a wrapper scored for each of the valid MatchWeighting "indexes" 
     */
    public class MatchWeighting {
        
         public Map<String, String> fieldLabelsMapping = new Map<String, String> {
        	'Match Confidence Threshold' => MATCH_THRESHOLD_WEIGHT_FIELD,
            'Company Antipattern Weight' => COMPANY_ANTIPATTERN_WEIGHT_FIELD,
            'Company Direct Phone Weight' => COMPANY_PHONE_WEIGHT_FIELD,
            'Company Name + Location Weight' => COMPANY_NAME_LOCATION_WEIGHT_FIELD,
            'Company Name Only Weight' => COMPANY_NAME_ONLY_WEIGHT_FIELD,
            'Company Name + Website/Domain Weight' => COMPANY_NAME_WEBSITE_DOMAIN_WEIGHT_FIELD,
            'Email Weight' => EMAIL_WEIGHT_FIELD,
            'FN + LN + Generalized Email Weight' => FN_LN_GENERALIZED_EMAIL_WEIGHT_FIELD,
            'FirstName + LastName + Phone Weight' => FN_LN_PHONE_WEIGHT_FIELD,
            'FirstName + LastName + Title Weight' => FN_LN_TITLE_WEIGHT_FIELD,
            'Location Agreement Weight' => LOCATION_AGREEMENT_WEIGHT_FIELD,
            'Tradestyle Weight' => TRADESTYLE_WEIGHT_FIELD,
            'Website/Domain Weight' => WEBSITE_DOMAIN_WEIGHT_FIELD
        };

        public MatchWeighting(MatchScenario input) {
            if (input == null || input.thisConcreteScenario == null) {
                // Not prepared to build a new match weighting instance
                return;
            }
        }

        public MatchWeighting() {
            // default constructor
        }
    }
    
    //---------------------------------------------------------------------------------------------
    // Comparable Impelementation
    //---------------------------------------------------------------------------------------------
    public Integer compareTo(Object compareTo) {
        // Default implementation, sort by runOrder -- ASCENDING
        MatchScenario msIn = (MatchScenario)compareTo;
        Integer i = msIn.runOrder;
        Integer j = this.runOrder;
        if (msIn.runOrder != null && this.runOrder != null) {
        	if (j == i) {
        		return 0;
        	}
        	if (j > i) {
        		return 1;
        	}
            else {
            	return -1;
            }
        }
        // If run order is null, it is not valid to try and sort. Return only -1
        return -1;
    }

    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    public static MatchScenario getDefault(String objName, ApexPages.Message[] messages) {
    	MatchScenario result;

        if (objName == null || messages == null) {
            return null;
        }

        // If there is a default for the runtime already, return it.
        if (defaultCache != null) {
            return defaultCache;
        }
    	
    	String soql;
        // Try block handles anything that can go wrong. Caller will have to check for null responses.
        try {
            
            Map<String, Schema.SObjectField> fieldmap = Utils.getFieldListing(SOBJECT_NAME);
            if (fieldmap != null) {
                soql = 'SELECT ';
            
                for(Schema.SObjectField fName : fieldmap.values()) {
                    soql += fName + ',';
                }
                
                // Trim off last comma so we don't throw query exceptions
                soql = soql.removeEnd(',');
                
                // Add the FROM clause
                soql += ' FROM ' + SOBJECT_NAME;
                
                // Add where clause for objName
                soql += ' WHERE ' + OBJECT_NAME_FIELD + ' = ' + '\'' + objName + '\'';
                
                soql += ' AND ' + IS_DEFAULT_FIELD + ' = true';
                
                Match_Scenario__c queryResult = Database.query(soql);
                
                if (queryResult != null) {
                    // Build the mapping of all scenario results
                    result = new MatchScenario(queryResult);
                }
            }
        }
        catch(Exception e) {
            System.debug('Unable to retrieve all scenarios for the object requested: ' + objName + e.getStackTraceString());
            messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDCTransientError, e.getMessage()));
        }
        
        return result;
    }
    
    /**
     * Returns a mapping of all Match_Scenario__c sobjects for a given object name (Lead, Contact, Account)
     * @return = Map<String(scneario name), Match_Scenario__c(concrete sobject)>
     */
    public static Map<String, Match_Scenario__c> getAllScenariosForObject(String objName, ApexPages.Message[] messages) {
        // Escape the objName since it's coming from UI and we cannot trust it all the time.
        objName = String.escapeSingleQuotes(objName);
        
        Map<String, Match_Scenario__c> result;
        
        String soql;
        // Try block handles anything that can go wrong. Caller will have to check for null responses.
        try {
            
            Map<String, Schema.SObjectField> fieldmap = Utils.getFieldListing(SOBJECT_NAME);
            if (fieldmap != null) {
                soql = 'SELECT ';
            
                for(Schema.SObjectField fName : fieldmap.values()) {
                    soql += fName + ',';
                }
                
                // Trim off last comma so we don't throw query exceptions
                soql = soql.removeEnd(',');
                
                // Add the FROM clause
                soql += ' FROM ' + SOBJECT_NAME;
                
                // Add where clause for objName
                soql += ' WHERE ' + OBJECT_NAME_FIELD + ' = ' + '\'' + objName + '\'';
                
                List<Match_Scenario__c> queryResult = Database.query(soql);
                
                if (queryResult != null && queryResult.size() > 0) {
                    // Build the mapping of all scenario results
                    result = new Map<String, Match_Scenario__c>();
                    for(Match_Scenario__c ms : queryResult) {
                        result.put(ms.Name, ms);
                        objectScenariosCache.put(ms.Name, ms);
                    }
                }
            }
        }
        catch(Exception e) {
            System.debug('Unable to retrieve all scenarios for the object requested: ' + objName + e.getStackTraceString());
            messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDCTransientError, e.getMessage()));
        }
        
        return result;
    }
    

    /**
     *  Will always use the local cache for scenarios
     */
    public static MatchScenario getScenarioDefinition(String scenarioId, ApexPages.Message[] messages) {

        return getScenarioDefinition(scenarioId, messages, true);
    }

    /**
     *  Fetches all of the inner details (RuntimeSetting, FieldMapping) for a MatchScenario
     *  @return = MatchScenario (with all inner wrappers defined)
     *  This method may return a cached copy of the MatchScenario by using a Scenario Id l/u on local cache
     */
    public static MatchScenario getScenarioDefinition(String scenarioId, ApexPages.Message[] messages, Boolean useCache) {
        MatchScenario result = new MatchScenario();
        
        if (Utils.isValidId(scenarioId)) {

            if (scenarioCache != null && useCache) {
                if(scenarioCache.get(scenarioId) != null) {
                    return scenarioCache.get(scenarioId);
                }
            }

            String soql;
            try {
                
                Map<String, Schema.SObjectField> fieldmap = Utils.getFieldListing(SOBJECT_NAME);
                
                if (fieldMap != null) {
                
                    soql = 'SELECT ';
                    
                    for(Schema.SObjectField fName : fieldmap.values()) {
                        soql += fName + ',';
                    }
                    
                    // Trim off last comma so we don't throw query exceptions
                    soql = soql.removeEnd(',');
                    
                    // Add the FROM clause
                    soql += ' FROM ' + SOBJECT_NAME;
                    
                    // Add where clause for objName
                    soql += ' WHERE Id = \'' + Id.valueOf(scenarioId) + '\'';

                    Match_Scenario__c queryResult = Database.query(soql);
                
                    if (queryResult != null) {
                        // Build the mapping of all scenario results
                        result = new MatchScenario(queryResult);
                    }
                    
                    if (result.scenarioId != null) {

                        result.thisSetting = RuntimeSetting.getSettings(scenarioId, messages);
                        result.thisMapping = FieldMapping.getFieldMappingForScenario(result);

                        // Add result to local static cache
                        scenarioCache.put(result.scenarioId, result);
                    }
                }
            }
            catch(Exception e) {
                System.debug('Unable to retrieve or construct the scenario definition for scenario id: ' + scenarioId + '\n' +e.getStackTraceString() +e);
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDCTransientError, e.getMessage()));
            }
        }

        return result;
    }


    /**
     *  This method will attempt to ascertain which match scenario belongs to the wrapper object supplied to it
     *  !Note: This method is allowed to return null in cases where the default scenario has been deactivated...
     *  @return = MatchScenario (may be from runtime cache)
     */
    public static MatchScenario getScenarioForWrapper(Wrapper w) {
        
        MatchScenario result;

        if (w == null) {
            return null;
        }
        
        SObject wrappedObj = w.getSObject();

        // Try and pull from the cache first
        if (wrapperCache != null) {
            if (wrapperCache.get( (String) wrappedObj.get('Id')) != null) {
                return wrapperCache.get((String) (wrappedObj.get('Id')));
            }
        }

        // Get the sobject type in question
        Schema.SobjectType stype = wrappedObj.getSObjectType();
        // Get all of the match scenarios for this object
        Map<String, Match_Scenario__c> scenarios;
        if (objectScenariosCache != null && objectScenariosCache.keySet().size() > 0) {
            // if the cache was loaded, then it is already known to be valid for this sobject
            // we know this because this can only be called in a static way from one sobject thread
            scenarios = objectScenariosCache;
        }
        else {
            // Get a fresh working set of scenarios
            scenarios = getAllScenariosForObject(String.valueOf(stype), new ApexPages.Message[0]);
        }
        
        /* Happens sometimes during tests that don't have a scenario */
        if (scenarios == null) {
        	return result;
        }
        
        // Put the scenarios in the correct run order.
        List<Match_Scenario__c> msList = scenarios.values();
        Utils.sortByField(msList, 'Run_Order__c');

        Map<String, String> rulesMap;

        for (Match_Scenario__c ms : msList) {

        	if (!Boolean.valueOf(ms.get(IS_DEFAULT_FIELD)) && Boolean.valueOf(ms.get(ACTIVE_FIELD))) {
        	
	        	rulesMap = new Map<String, String>();
	        	
	        	try {
	        		
	        		if (ms.get(RULE_1_FIELD) != null) {
		        	
		        		List<String> parts = String.valueOf(ms.get(RULE_1_FIELD)).split('=', 2);
		        		// Put the rule in to a mapping
		        		/* 
		        		 * parts[0] = field
		        		 * parts[parts.size()-1] = value
		        		 */ 
		        		rulesMap.put(parts[0].trim(), parts[parts.size()-1].trim());
	        		}
	        		
	        		if (ms.get(RULE_2_FIELD) != null) {
	        		
	        		    List<String> parts = String.valueOf(ms.get(RULE_2_FIELD)).split('=', 2);
	                    // Put the rule in to a mapping
	                    /* 
	                     * parts[0] = field
	                     * parts[parts.size()-1] = value
	                     */ 
	                    rulesMap.put(parts[0].trim(), parts[parts.size()-1].trim());
	        		}
	        		
	        		if (ms.get(RULE_3_FIELD) != null) {
	        		
	        			List<String> parts = String.valueOf(ms.get(RULE_3_FIELD)).split('=', 2);
	                    // Put the rule in to a mapping
	                    /* 
	                     * parts[0] = field
	                     * parts[parts.size()-1] = value
	                     */ 
	                    rulesMap.put(parts[0].trim(), parts[parts.size()-1].trim());
	        		}
	        		
	        		if (ms.get(RULE_4_FIELD) != null) {
	        		
	        			List<String> parts = String.valueOf(ms.get(RULE_4_FIELD)).split('=', 2);
	                    // Put the rule in to a mapping
	                    /* 
	                     * parts[0] = field
	                     * parts[parts.size()-1] = value
	                     */ 
	                    rulesMap.put(parts[0].trim(), parts[parts.size()-1].trim());
	        		}
	        		
	        		if (ms.get(RULE_5_FIELD) != null) {
	        		
	        			List<String> parts = String.valueOf(ms.get(RULE_5_FIELD)).split('=', 2);
	                    // Put the rule in to a mapping
	                    /* 
	                     * parts[0] = field
	                     * parts[parts.size()-1] = value
	                     */ 
	                    rulesMap.put(parts[0].trim(), parts[parts.size()-1].trim());
	        		}
	        	}
	        	catch(Exception e) {
	        		// Gracefully catch and debug the exception
	        		System.debug(
	        		  'Silently catching exception while trying to evaluate rule criteria for Match_Scenario: ' + ms 
	        		  + '\n' 
	        		  + 'Exception encoutered was: ' + e.getMessage() + ' ' + e.getStackTraceString());
	        		// Keep processing until the rules are exhausted and the default is encountered.
	        		continue;
	        	}
        	}
        	else {
System.debug('Applying default scenario to wrapper :: ' +ms);
        		// Default scenario, apply to wrapper and terminate - if active
                if (!Boolean.valueOf(ms.get(ACTIVE_FIELD))) {
                    continue;
                }
        		result = new MatchScenario(ms);
                
                result.thisSetting = RuntimeSetting.getSettings(result.scenarioId, new ApexPages.Message[] { });

                // Only if we are not within a trigger, otherwise too expensive!
                if (!ddcLib1.Runtime.IS_TRIGGERED_CONTEXT) {
                    result.thisMapping = FieldMapping.getFieldMappingForScenario(result);
                }
                
                // Add scenario to cache
                scenarioCache.put(result.scenarioId, result);
                wrapperCache.put((String) w.getSObject().get('Id'), result);

        		return result;
        	}
        	
        	Boolean isThisRule = false;
        	// If there are rules to evaluate
        	if (rulesMap != null) {
        		// Do evaluation on wrapper
        		for (String f : rulesMap.keySet()) {
	        		try {
	        			Object val = wrappedObj.get(f);
	        			if (val != null && String.valueOf(val).equalsIgnoreCase(String.valueOf(rulesMap.get(f)))) {
	        				// If there is a value in that field for this obj, and that value is equivalent to the rule in the scenario:
	        				// then let the rule method temporarily assume that the wrapper belongs to this rule
	        				isThisRule = true;
	        			}
	        			else {
	        				// As soon as this scenario is ruled out by evaluation, break out of this loop and back up to the scenario iterable.
	        				isThisRule = false;
	        				break;
	        			}
	        		}
	        		catch(Exception e) {
	        			// something went wrong while trying to evaluate the rules, try and continue processing
                        System.debug('ERROR: Issue while evaluating rules, the error was ::  ' +e);
                        System.debug('MatchScenario will continue processing at the next field or next rule if no more fields!');
	        			continue;
	        		}
        		}
        	}
        	
        	if (isThisRule) {
System.debug('Applying non-default rule, named--->: ' +ms.Name + '   isActive? ::  ' +ms.get(ACTIVE_FIELD));
                if (!Boolean.valueOf(ms.get(ACTIVE_FIELD))) {
                    continue;
                }
        		result = new MatchScenario(ms);

                result.thisSetting = RuntimeSetting.getSettings(result.scenarioId, new ApexPages.Message[] { });

                // Only if we are not within a trigger, otherwise too expensive!
                if (!ddcLib1.Runtime.IS_TRIGGERED_CONTEXT) {
                    result.thisMapping = FieldMapping.getFieldMappingForScenario(result);
                }
                
                scenarioCache.put(result.scenarioId, result);
                wrapperCache.put((String) w.getSObject().get('Id'), result);

        		return result;
        	}
        }
        // Always have a result, sometimes it may be null - should be expected.
        return result;
    }
    
    /**
     * Builds a javascript ready array for parsing and displaying the contents of this.MatchScenario on a VF page.
     */
    public static String getJavascriptBody(MatchScenario input) {
        String result;
        
        if (input != null) {
            
            result = Json.serialize(input);
            System.debug('serialized MatchScenario in JSON format: ' +input);
        }
        
        return result;
    }

    /**
     *  Attempts to logically split up rule field evaluation values based on all allowed
     *  operators in to a hashmap
     *  @param  fieldData       All data from rules fields
     *  @return                 A mapping of the field, evaluator and value
     */

}