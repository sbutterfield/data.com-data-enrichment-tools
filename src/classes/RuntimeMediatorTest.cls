/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class RuntimeMediatorTest {

    /*static testMethod void testMatchLead() {

    	Test.setMock(HttpCalloutMock.class, new MockMatchContactHttpResponse());
        Lead l = new Lead(FirstName='Shawn',LastName='Butterfield',Email='sbutterfield@salesforce.com',Title='',Company='Salesforce.com',Street='1 Market',Website='www.salesforce.com',City='San Francisco',State='CA',Country='USA');
		insert l;
		Test.startTest();
		RuntimeMediator.matchLead(l.Id);
		setupCompanyMock();
		update l;
		RuntimeMediator.matchLead(l.Id);
		Test.stopTest();
    }
    
    
    private static void setupContactMock() {
    	StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    	mock.setStaticResource('mockContactMatcherResponse');
    	mock.setStatusCode(201);
    	mock.setStatus('Created');
    	mock.setHeader('Content-Type', 'application/xml');
    	Test.setMock(HttpCalloutMock.class, mock);
    }
    
    private static void setupCompanyMock() {
    	StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    	mock.setStaticResource('mockCompanyMatcher');
    	mock.setStatusCode(201);
    	mock.setStatus('Created');
    	mock.setHeader('Content-Type', 'application/xml');
    	Test.setMock(HttpCalloutMock.class, mock);
    }
    */
}