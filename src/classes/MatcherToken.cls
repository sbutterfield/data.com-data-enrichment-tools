/**
 * @author: Shawn Butterfield, Salesforce.com Inc
 * MatcherToken object returned by the Matcher Token API.
 */
public class MatcherToken extends MatcherObject {
	
    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    public String token {
        get { return (String) mMap.get(MatcherTokenAPI.TAG_NAME_TOKEN); }
        set { mMap.put(MatcherTokenAPI.TAG_NAME_TOKEN, value); }
    }
    
    public Boolean errorGettingToken {
    	get { return (Boolean) mMap.get(MatcherTokenAPI.TAG_NAME_ERROR_GETTING_TOKEN); }
    	set { mMap.put(MatcherTokenAPI.TAG_NAME_ERROR_GETTING_TOKEN, value); }
    }
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public MatcherToken() {
        super(MatcherTokenAPI.TAG_NAME_GET_API_TOKEN, MatcherTokenAPI.SEQUENCE);
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * This will parse the given matcherToken node returned by the Matcher Token API.
     */
    public static MatcherToken parse(Dom.XmlNode matcherTokenNode) {
				if(matcherTokenNode == null || matcherTokenNode.getName() != MatcherTokenAPI.TAG_NAME_GET_API_TOKEN) {
					return null;
				}
				
        MatcherToken matcherToken = new MatcherToken();
        Dom.XmlNode node;
        
        node = matcherTokenNode.getChildElement(MatcherTokenAPI.TAG_NAME_TOKEN, null);
        if (node != null) {
            matcherToken.token = node.getText();
        }
        
        node = matcherTokenNode.getChildElement(MatcherTokenAPI.TAG_NAME_ERROR_GETTING_TOKEN, null);
        if (node != null) {
        	matcherToken.errorGettingToken = Boolean.valueOf(node.getText());
        }
        
        return matcherToken;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testToXml() {
        MatcherToken matcherToken = new MatcherToken();
        matcherToken.token = 'abc123';
        matcherToken.errorGettingToken = true;
        
        String expectedXml =
            '<' + MatcherTokenAPI.TAG_NAME_GET_API_TOKEN + '>' +
                MatcherUtils.objectToXml(MatcherTokenAPI.TAG_NAME_TOKEN, matcherToken.token) +
                MatcherUtils.objectToXml(MatcherTokenAPI.TAG_NAME_ERROR_GETTING_TOKEN, matcherToken.errorGettingToken) +
            '</' + MatcherTokenAPI.TAG_NAME_GET_API_TOKEN + '>';
        
        String actualXml = matcherToken.toXml();
        System.assertEquals(expectedXml, actualXml);
    }
    
    private static testmethod void testParse() {
        // Null
        System.assertEquals(null, MatcherToken.parse(null));
        
        // Non-Matcher Token Node
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode root = doc.createRootElement('NotMatcherToken', null, null);
        System.assertEquals(null, MatcherToken.parse(root));
        
        // NOTE: The easiest way to test for equality is to compare XML strings because there are
        // no equals methods readily available for all objects other than String.
        
        // MatcherToken
        MatcherToken expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = 'abc123';
        expectedMatcherToken.errorGettingToken = true;
        String expectedXml = expectedMatcherToken.toXml();
        
        doc = new Dom.Document();
        doc.load(expectedXml);
        MatcherToken actualMatcherToken = MatcherToken.parse(doc.getRootElement());
        System.assertNotEquals(null, actualMatcherToken);
        System.assertEquals(expectedXml, actualMatcherToken.toXml());
    }
    
}