public interface Matchable {

	SObject[] match();
}