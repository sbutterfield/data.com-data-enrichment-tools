global class SystemField {
	
    global static String PREFIX = Utils.NAMESPACE_PREFIX;

    global static String MATCH_STATUS_FIELD = PREFIX + 'System_MatchStatus__c';
    global static String MATCH_SCENARIO_FIELD = PREFIX + 'MatchScenario__c';
    global static String MATCH_CODE_FIELD = PREFIX + 'MatchCode__c';
    global static String IS_COMPANY_MATCH_FIELD = PREFIX + 'IsCompanyOnlyMatch__c';
    global static String IS_SYSTEM_UPDATE_FIELD = PREFIX + 'IsSystemUpdate__c';
    global static String LAST_MATCHED_FIELD = PREFIX + 'LastMatched__c';
    global static String CLEAN_STATUS_FIELD = 'CleanStatus';

    private SystemField() {
		// Instances of SystemField cannot be instantiated.
	}
}