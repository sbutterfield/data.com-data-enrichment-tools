/**
 *  Defines the contract for a Wrapper (any sobject that DET can act upon)
 *  Wrappers must have an instance of SObject and DDCType to work properly.
 */
 
global interface Wrapper {

    /* Wrappers must contain an instance of a concrete SObject */
    SObject getSObject();
    
    /* Wrappers must have an assigned type */
    DDCType getType();

    /* Wrappers must have an assigned Scenario */
    MatchScenario getScenario();

    /* Wrappers must be able to call getDdcMatch */
    DDCObject getDdcMatch();

    /* Wrappers must be able to set the ddc match */
    void setDdcMatch(DDCObject value);
}