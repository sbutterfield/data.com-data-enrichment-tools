/**
 * @author: Shawn Butterfield, Salesforce.com Inc
 * Base object for use with the Matcher API.
 */
public with sharing virtual class MatcherObject {

    //---------------------------------------------------------------------------------------------
    // Fields
    //---------------------------------------------------------------------------------------------
    
    public Map<String, Object> mMap;
    public String mContainer;
    public List<String> mSequence;
    
    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public MatcherObject(String container, List<String> sequence) {
        mMap = new Map<String, Object>();
        mContainer = container;
        mSequence = sequence;
    }
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * This checks if this object contains any non-empty field values.
     */
    public Boolean isEmpty() {
        Boolean result = true;
        for (String tagName : mMap.keySet()) {
            Object o = mMap.get(tagName);
            if (o == null) {
                continue;
            }
            
            if (o instanceof MatcherField) {
                MatcherField field = (MatcherField) o;
                if (field.value != null) {
                   result = false;
                   break;
                }
            }
            else if (o instanceof MatcherObject) {
                MatcherObject obj = (MatcherObject) o;
                if (!obj.isEmpty()) {
                    result = false;
                    break;
                }
            }
            else {
                result = false;
                break;
            }
        }
        
        return result;
    }
    
    /**
     * This converts this object to an XML string for use with the Matcher API.
     */
    public String toXml() {
        XmlStreamWriter w = new XmlStreamWriter();
        appendXml(w);
        String xml = w.getXmlString();
        w.close();
        return xml;
    }
    
    /**
     * This appends the XML representation of this object to the given XmlStreamWriter if the
     * writer is non-null and this object contains any non-empty field values.
     */
    public void appendXml(XmlStreamWriter w) {
        if (mContainer == null) {
            throw new ddcLib1.IllegalArgumentException(
              'Missing required parameter; container tag name should be non-null.');
        }
        
        if (w == null || isEmpty()) {
            return;
        }
        
        w.writeStartElement(null, mContainer, null);
        
        List<String> tagNames;
        if (mSequence == null || mSequence.size() == 0) {
            System.debug('Sequence not specified; falling back on keySet.');
            tagNames = new List<String>();
            tagNames.addAll(mMap.keySet());
        }
        else {
            tagNames = mSequence;
        }
        
        for (String tagName : tagNames) {
            Object o = mMap.get(tagName);
            if (o == null) {
                continue;
            }
            
            if (o instanceof MatcherField) {
                MatcherField field = (MatcherField) o;
                if (field.value != null) {
                    Boolean diff = (field.diff == null) ? false : field.diff;
                    w.writeStartElement(null, tagName, null);
                    w.writeAttribute(null, null, MatcherAPI.TAG_ATTRIBUTE_DIFF, String.valueOf(diff));
                    w.writeCharacters(String.valueOf(field.value));
                    w.writeEndElement();
                }
            }
            else if (o instanceof MatcherObject) {
                MatcherObject obj = (MatcherObject) o;
                obj.appendXml(w);
            }
            else {
                w.writeStartElement(null, tagName, null);
                w.writeCharacters(String.valueOf(o));
                w.writeEndElement();
            }
        }
        
        w.writeEndElement();
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testIsEmpty() {
        // Empty
        List<String> children = new List<String> { 'nullField', 'emptyField', 'emptyObject', 'valid' };
        MatcherObject parentObject = new MatcherObject('parentContainer', children);
        System.assertEquals(true, parentObject.isEmpty());
        
        // Null Field
        parentObject.mMap.put('nullField', null);
        System.assertEquals(true, parentObject.isEmpty());
        
        // Empty Field
        MatcherField childField = new MatcherField(null, null);
        parentObject.mMap.put('emptyField', childField);
        System.assertEquals(true, parentObject.isEmpty());
        
        // Empty Object
        MatcherObject childObject = new MatcherObject('childContainer', new List<String> { 'childField' });
        childObject.mMap.put('childField', childField);
        parentObject.mMap.put('emptyObject', childObject);
        System.assertEquals(true, parentObject.isEmpty());
        
        // Non-Empty Field
        childField = new MatcherField('value', false);
        parentObject.mMap.put('valid', childField);
        System.assertEquals(false, parentObject.isEmpty());
        
        // Non-Empty Object
        childObject.mMap.put('childField', childField);
        parentObject.mMap.put('valid', childObject);
        System.assertEquals(false, parentObject.isEmpty());
        
        // Non-Empty String
        MatcherObject simpleObject = new MatcherObject('container', new List<String> { 'child' });
        simpleObject.mMap.put('child', 'value');
        System.assertEquals(false, simpleObject.isEmpty());
    }
    
    private static testmethod void testToXml() {
        // Exception
        Boolean hadException = false;
        MatcherObject o = new MatcherObject(null, null);
        try {
            o.toXml();
        }
        catch (Exception e) {
            hadException = true;
        }
        System.assert(true, hadException);
        
        // Empty
        o = new MatcherObject('parentContainer', null);
        System.assertEquals('', o.toXml());
        
        // No Sequence
        MatcherField field = new MatcherField('value', true);
        MatcherField childField = new MatcherField('childValue', false);
        MatcherObject obj = new MatcherObject('childContainer', null);
        obj.mMap.put('childField', childField);
        
        String expectedDatetime = '2011-07-22 08:04:25';
        
        o.mMap.put('nullField', null);
        o.mMap.put('emptyField', new MatcherField(null, null));
        o.mMap.put('emptyObject', new MatcherObject('emptyContainer', null));
        o.mMap.put('validField', field);
        o.mMap.put('validObject', obj);
        o.mMap.put('validDatetime', Datetime.valueOfGmt(expectedDatetime));
        System.assertNotEquals('', o.toXml());
        
        // Sequence
        o.mSequence = new List<String> { 'nullField', 'emptyField', 'emptyObject', 'validField', 'validObject', 'validDatetime' };
        String expectedXml =
           '<parentContainer>' +
               MatcherUtils.fieldToXml('validField', field) +
               '<childContainer>' +
                   MatcherUtils.fieldToXml('childField', childField) +
               '</childContainer>' +
               '<validDatetime>' + expectedDatetime + '</validDatetime>' +
           '</parentContainer>';
        System.assertEquals(expectedXml, o.toXml());
    }
    
}