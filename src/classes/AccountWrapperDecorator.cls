public class AccountWrapperDecorator extends WrapperDecorator {
    
    private Wrapper w;

    public Boolean isQualified { 
        get { 
            return getIsQualified();
        }
        private set;
    }
    
    public AccountWrapperDecorator(Wrapper w) {
        super(w);
        this.w = w;
    }

    public override MatchScenario getScenario() {
        return MatchScenario.getScenarioForWrapper(this.w);
    }

    private Boolean getIsQualified() {
        if (this.getSObject() != null) {

        }

        return false;
    }
}