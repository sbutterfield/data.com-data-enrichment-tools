public class ScheduledCleanJob implements Schedulable {
    public void execute(SchedulableContext sc) {
        //String leadQuery = 'Select Id, Name from lead  where  ownerid = \'00G000000071vFy\' and Marketo_id_assigned_on__c > 2013-10-09T00:00:00Z and Marketo_id_assigned_on__c < 2013-10-31T00:00:00Z and ddcDet__System_MatchStatus__c != \'Not Matched!\' LIMIT 5000';
        String leadQuery = 'select id from Lead where UAT_TAG__c LIKE \'UAT_Batch_D0106143D_SRK%\'';
        String contactQuery = 'Select Id From Contact';
        String accountQuery = 'Select Id From Account';
       ddcDet.BatchableCleanJob leadbatch = new ddcDet.BatchableCleanJob(leadQuery);
         /*ddcDet.BatchableCleanJob contactbatch = new ddcDet.BatchableCleanJob(contactQuery);
        ddcDet.BatchableCleanJob accountbatch = new ddcDet.BatchableCleanJob(accountQuery); */
        database.executebatch(leadbatch, 3);
        /*database.executebatch(contactbatch, 3);
        database.executebatch(accountbatch, 3);*/
    }
}