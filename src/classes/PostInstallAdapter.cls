global without sharing class PostInstallAdapter implements InstallHandler {
    
    global Boolean success = false;
    
    global void onInstall(InstallContext context) {
        // Fresh installation
        if(context.previousVersion() == null) {
            
            // Set up custom settings
            //createSettings();
            
            // Notify external LMA
            //AsynchInstallNotifier.notifyLMA();
            
            User u = [Select Id, Email from User where Id =:context.installerID()];
            String toAddress= u.Email;
            String[] toAddresses = new String[]{toAddress};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('support@salesforce.com');
            mail.setSenderDisplayName('Package Support');
            
            //if(!success) {
                //mail.setSubject('Data.com Data Enrichment Tools Failed to install as expected');
                //mail.setPlainTextBody('There was one or more errors when trying to take the initial configuration steps during installation.');
            //}
            //else {
                mail.setSubject('Data.com Data Enrichment Tools Installed Successfully!');
                mail.setPlainTextBody('Thanks for installing Data Enrichment Tools! You still have a few steps remaining. Launch the configuration engine to map fields, as set up your personalized configuration.');
                success = true;
            //}
            Messaging.sendEmail(new Messaging.Email[] { mail });
        }
        // Upgrading packages
        else if(context.isUpgrade()) {
            //impl
        }
        // Push upgrade
        else if(context.isPush()) {
            //impl
        }
    }

    // Transactional methods:
    @future(Callout=false)
    private static void createSettings() {
        RuntimeSettings__c config;
        try {
            config = RuntimeSettings__c.getInstance('Settings');
            if(config == null) {
                config = new RuntimeSettings__c();
                config.Name = 'Settings';
                config.Default_Settings__c = true;
                config.Account_Event_Trigger_Enabled__c = true;
                config.Contact_Event_Trigger_Enabled__c = true;
                config.Lead_Event_Trigger_Enabled__c = true;
                insert config;
            }
        }
        catch(Exception e) {
            //return false;
        }
    }
    
}