/**
 * @author: Shawn Butterfield, Salesforce.com Inc
 * API Manager for matcher tokenizer
 */
public class MatcherTokenAPI {
    
    //---------------------------------------------------------------------------------------------
    // Constants
    //---------------------------------------------------------------------------------------------
    
    // Containers   
    public static final String TAG_NAME_GET_API_TOKEN = 'getApiToken';
    
    // Token
    public static final String TAG_NAME_TOKEN = 'apiToken';
		
	public static final String TAG_NAME_ERROR_GETTING_TOKEN = 'errorInGettingToken';    
    
    public static final List<String> SEQUENCE = new List<String> {
        TAG_NAME_TOKEN,
        TAG_NAME_ERROR_GETTING_TOKEN
    };
    
    public static final String SETTING_NAME_PROD = 'APIToken';
    public static final String SETTING_NAME_TEST = 'TestAPIToken';
    
    private static final String PROGRAMID = 'traction_prod_02012013';
    private static final String HEADER_PROGRAMID = 'partnerapp.programid';
    private static final String HEADER_ORGID = 'orgid';
    private static final String ORGID = UserInfo.getOrganizationId() + '_ASSESS';
    
    private static final String TOKEN_API_ENDPOINT = 'https://partnerapp.data.com/api/getApiToken';
    
    //---------------------------------------------------------------------------------------------
    // API
    //---------------------------------------------------------------------------------------------
    
    /**
     * Fetches the Matcher API token for the current org from the getToken API.
     */
    public static MatcherToken getToken(ApexPages.Message[] messages) {
        return getToken(messages, null);
    }
    
    /**
     * Same as above but with the additional simulatedResponse parameter which allows test code to
     * test code with simulated responses.
     */
    public static MatcherToken getToken(ApexPages.Message[] messages, HttpResponse simulatedResponse) {
        Dom.Document doc;
        if (simulatedResponse != null) {
            doc = getMatcherTokenResponse(messages, simulatedResponse);
        }
        else {
            doc = getMatcherTokenResponse(messages, null);
        }
        
        if (doc == null) {
            return null;
        }
        
        MatcherToken mt = MatcherToken.parse(doc.getRootElement());
        if (mt == null || mt.token == null || mt.token.length() == 0) {
        	if (messages != null) {
        		messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, MatcherUtils.MESSAGE_TRANSIENT_ERROR, ''));
        	}
        	
            return null;
        }
        return mt;
    }
    
    /**
     * Saves the given MatcherToken to the appropriate custom setting.
     */
    public static Boolean setToken(MatcherToken mt, ApexPages.Message[] messages) {
    	return setToken(mt, messages, false);
    }
    
    /**
     * Same as above with the additional simulatedError parameter which allows test code to test
     * code with a simulated error.
     */
    // NOTE: This needs to be public so that JSetApiToken can get sufficient code coverage.
    public static Boolean setToken(MatcherToken mt, ApexPages.Message[] messages, Boolean simulatedError) {
        if (mt == null || mt.token == null || mt.token.length() == 0) {
        	if (messages != null) {
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, MatcherUtils.MESSAGE_TRANSIENT_ERROR, ''));
        	}
        	
        	return false;
        }
        
        if (mt.errorGettingToken != null && mt.errorGettingToken) {
        	if (messages != null) {
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, 'Token not useable error.', ''));
        	}
        	
        	return false;
        }
            
        Boolean success;
        try {
            if (simulatedError) {
                throw new ddcLib1.TestException('SIMULATED_ERROR');
            }
            
            String settingName = Test.isRunningTest() ? SETTING_NAME_TEST : SETTING_NAME_PROD;
            MatcherToken__c matcherTokenSetting = MatcherToken__c.getInstance(settingName);
            if (matcherTokenSetting == null) {
                matcherTokenSetting = new MatcherToken__c(Name=settingName, Token__c=mt.token);
                insert matcherTokenSetting;
            }
            else {
                matcherTokenSetting.Token__c = mt.token;
                update matcherTokenSetting;
            }
            
            success = true;
        }
        catch (Exception e) {
            if (messages != null) {
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, MatcherUtils.MESSAGE_TOKEN_FAIL, ''));
            }
            
            success = false;
        }
        
        return success;
    }
    
    /**
     * Deletes the MatcherToken if it is set.
     */
    public static Boolean deleteToken() {
        return deleteToken(false);
    }
    
    /**
     * Same as above with the additional simulatedError parameter which allows test code to test
     * code with a simulated error.
     */
    private static Boolean deleteToken(Boolean simulatedError) {
        Boolean deleted;
        try {
            if (Test.isRunningTest() && simulatedError) {
                throw new ddcLib1.TestException('SIMULATED_ERROR');
            }
            
            String settingName = Test.isRunningTest() ? SETTING_NAME_TEST : SETTING_NAME_PROD;
            MatcherToken__c matcherTokenSetting = MatcherToken__c.getInstance(settingName);
            if (matcherTokenSetting != null) {
            	delete matcherTokenSetting;
            	deleted = true;
            }
            else {
                deleted = false;
            }
        }
        catch (Exception e) {
            deleted = false;
        }
        
        return deleted;
    }
    
    /**
     * Gets the current Matcher API token if it is present.
     */
    public static String getTokenSetting() {
        String settingName = Test.isRunningTest() ? SETTING_NAME_TEST : SETTING_NAME_PROD;
        MatcherToken__c matcherTokenSetting = MatcherToken__c.getInstance(settingName);
        if (matcherTokenSetting == null) {
        	return null;
        }
        else {
            return matcherTokenSetting.Token__c;
        }
    }
    
    /**
     * Checks if the token is already set to a non-null/non-empty value.
     */
    public static Boolean isTokenSet() {
    	String token = getTokenSetting();
    	if (token != null && token.length() > 0) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    public static HttpResponse getMockTokenResponse() {
        MatcherToken expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = 'abc123';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody(expectedMatcherToken.toXml());
        res.setStatusCode(200);
        return res;
        //MatcherToken actualMatcherToken = getToken(messages, res);
    }
    
    //---------------------------------------------------------------------------------------------
    // Private
    //---------------------------------------------------------------------------------------------
    
    /**
     * Constructs the request URL for the Matcher Token API.
     */
    private static String getTokenApiEndpoint() {
        return TOKEN_API_ENDPOINT;
    }
    
    /**
     * Attempts to make a request with the given URL. An XML response is returned if we get what is
     * expected from the backend. If an error occurs during the request, an ApexPage.Message will
     * be added to the messages list and null will be returned.
     *
     * Pass the desired response in the simulatedResponse parameter to simulate a response.
     */
    private static Dom.Document getMatcherTokenResponse(ApexPages.Message[] messages, HttpResponse simulatedResponse) {
        HttpResponse res;
        if (Test.isRunningTest()) {
       		res = simulatedResponse;
        }
        else {
            HttpRequest req = MatcherUtils.createRequest(getTokenApiEndpoint(), null, 'GET', false, 20000);
            req.setHeader(HEADER_PROGRAMID, PROGRAMID);
            req.setHeader(HEADER_ORGID, ORGID);
            res = MatcherUtils.sendRequest(req, messages);
System.debug('HttpResponse from tokenizer  = ' +res);
System.debug('HttpResponse body from tokenizer: ' +res.getBody());
System.debug('HttpResponse headers: ' +res.getHeaderKeys());
System.debug('HttpResponse error header: ' +res.getHeader('errorCode') + ' && ' +res.getHeader('errorMsg'));
            if (res == null) {
                return null;
            }
        }
        
        if (res == null || res.getHeaderKeys() == null || res.getHeaderKeys().size() == 0) {
            if (messages != null) {
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, MatcherUtils.MESSAGE_TRANSIENT_ERROR, ''));
            }
            
            return null;
        }
        
        // Check if an error was returned. All error information is in the header.
        // NOTE: Currently all errors are sent with a Status Code of 200 (OK). We'll need to check
        // for errorCode, errorMsg, and httpStatusCode in the response header for information about
        // errors.
        String errorCode = res.getHeader('errorCode');
        if (errorCode != null) {
            if (messages != null) {
                String errorDetail = res.getHeader('errorMsg');
                if (errorDetail == null) {
                    errorDetail = '';
                }
                
                // Errors will come back with an httpStatusCode >= 400.
                String statusCodeString = res.getHeader('httpStatusCode');
                Integer statusCode = 0;
                try {
                    if (statusCodeString != null) {
                        statusCode = Integer.valueOf(statusCodeString);
                    }
                }
                catch (Exception e) {
                    // Do nothing.
                }
                
                String errorSummary = MatcherUtils.MESSAGE_GENERIC_ERROR;
                if (statusCode >= 400) {
                	// Cannot validate identity, show unrecoverable error.
                    if ('WRONG_SESSIONID'.equals(errorCode)) {
                        errorSummary = MatcherUtils.MESSAGE_UNRECOVERABLE_ERROR;
                    }
                    else if ('INVALID_ORG_ID'.equals(errorCode)) {
                    	errorSummary = MatcherUtils.MESSAGE_UNRECOVERABLE_ERROR;
                    	// NOTE: errorDetail from server needs rebranding, don't use it
                    	errorDetail = '';
                    }
                }
                
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, errorSummary, errorDetail));
            }
        
            return null;
        }
        
        // W-1230440: HttpResponse.getBodyDocument may throw an exception for badly formed documents.
        Dom.Document doc;
        try {
            doc = res.getBodyDocument();
        }
        catch (Exception e) {
            if (messages != null) {
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, MatcherUtils.MESSAGE_TRANSIENT_ERROR, ''));
            }
            doc = null;
        }
		System.debug(doc);
        return doc;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testGetTokenApiEndpoint() {
    	System.assertEquals(TOKEN_API_ENDPOINT, getTokenApiEndpoint());
    }
    
    private static testmethod void testGetMatcherTokenResponse() {
        ApexPages.Message[] messages = new List<ApexPages.Message>();
        Integer expectedMessages = 0;
        
        // Null/Empty response
        expectedMessages++;
        System.assertEquals(null, getMatcherTokenResponse(messages, null));
        System.assertEquals(expectedMessages, messages.size());
        Integer lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_TRANSIENT_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // WRONG_SESSIONID
        expectedMessages++;
        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);
        res.setHeader('errorCode', 'WRONG_SESSIONID');
        res.setHeader('errorMessage', 'Session is expired or wrong.');
        res.setHeader('httpStatusCode', '403');
        System.assertEquals(null, getMatcherTokenResponse(messages, res));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_UNRECOVERABLE_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // INVALID_ORG_ID
        expectedMessages++;
        res = new HttpResponse();
        res.setStatusCode(200);
        res.setHeader('errorCode', 'INVALID_ORG_ID');
        res.setHeader('errorMessage', 'Your account isn\'t recognized in Jigsaw. Contact support@jigsaw.com');
        res.setHeader('httpStatusCode', '403');
        System.assertEquals(null, getMatcherTokenResponse(messages, res));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_UNRECOVERABLE_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // TOKEN_CREATION_FAILED
        expectedMessages++;
        res = new HttpResponse();
        res.setStatusCode(200);
        res.setHeader('errorCode', 'TOKEN_CREATION_FAILED');
        res.setHeader('errorMessage', 'Token Creation failed');
        res.setHeader('httpStatusCode', '403');
        System.assertEquals(null, getMatcherTokenResponse(messages, res));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_GENERIC_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Other errorCode
        expectedMessages++;
        res = new HttpResponse();
        res.setStatusCode(200);
        res.setHeader('errorCode', 'OTHER_ERROR');
        res.setHeader('errorMessage', 'Something else happened.');
        res.setHeader('httpStatusCode', 'NotNumber');
        System.assertEquals(null, getMatcherTokenResponse(messages, res));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_GENERIC_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Bad Response
        expectedMessages++;
        res = new HttpResponse();
        res.setHeader('Content-Type', 'text/html');
        res.setBody(
            '<page:applyDecorator name="anonymous">' +
            '<html>' +
                '<head>' +
                    '<title>Page not found</title>' +
                '</head>' +
            '<body>' +
                '<p>Page not found. Please check URL one more time.</p>' +
            '</body>' +
            '</html>' +
            '</page:applyDecorator>'
        );
        System.assertEquals(null, getMatcherTokenResponse(messages, res));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_TRANSIENT_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Valid
        res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody('<someTag/>');
        System.assertNotEquals(null, getMatcherTokenResponse(messages, res));
        System.assertEquals(expectedMessages, messages.size());
    }
    
    private static testmethod void testGetToken() {
        ApexPages.Message[] messages = new List<ApexPages.Message>();
        Integer expectedMessages = 0;
        
        // Null/Empty Response
        expectedMessages++;
        System.assertEquals(null, getToken(messages));
        System.assertEquals(expectedMessages, messages.size());
        Integer lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_TRANSIENT_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Invalid
        expectedMessages++;
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody('<someTag/>');
        System.assertEquals(null, getToken(messages, res));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_TRANSIENT_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Empty Token
        expectedMessages++;
        MatcherToken expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = '';
        res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody(expectedMatcherToken.toXml());
        System.assertEquals(null, getToken(messages, res));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_TRANSIENT_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Valid, New
        expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = 'abc123';
        res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody(expectedMatcherToken.toXml());
        MatcherToken actualMatcherToken = getToken(messages, res);
        System.assertNotEquals(null, actualMatcherToken);
        System.assertEquals(expectedMessages, messages.size());
        System.assertEquals(expectedMatcherToken.token, actualMatcherToken.token);
        
        // Valid, Existing
        expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = 'def456';
        res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody(expectedMatcherToken.toXml());
        actualMatcherToken = getToken(messages, res);
        System.assertNotEquals(null, actualMatcherToken);
        System.assertEquals(expectedMessages, messages.size());
        System.assertEquals(expectedMatcherToken.token, actualMatcherToken.token);
    }
    
    private static testmethod void testSetToken() {
        ApexPages.Message[] messages = new List<ApexPages.Message>();
        Integer expectedMessages = 0;
        
        // Null/Empty MatcherToken
        expectedMessages++;
        System.assertEquals(false, setToken(null, messages));
        System.assertEquals(expectedMessages, messages.size());
        Integer lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_TRANSIENT_ERROR.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Token Not Useable
        expectedMessages++;
        MatcherToken expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = 'abc123';
        expectedMatcherToken.errorGettingToken = true;
        System.assertEquals(false, setToken(expectedMatcherToken, messages, false));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? 'Token not useable error.'.equals(messages.get(lastIndex).getSummary()) : false);
        
        // Valid, New
        expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = 'abc123';
        System.assertEquals(true, setToken(expectedMatcherToken, messages, false));
        MatcherToken__c matcherTokenSetting = MatcherToken__c.getInstance(SETTING_NAME_TEST);
        System.assertEquals(expectedMessages, messages.size());
        System.assertEquals(expectedMatcherToken.token, matcherTokenSetting.Token__c);
        
        // Valid, Existing
        expectedMatcherToken = new MatcherToken();
        expectedMatcherToken.token = 'def456';
        System.assertEquals(true, setToken(expectedMatcherToken, messages, false));
        matcherTokenSetting = MatcherToken__c.getInstance(SETTING_NAME_TEST);
        System.assertEquals(expectedMessages, messages.size());
        System.assertEquals(expectedMatcherToken.token, matcherTokenSetting.Token__c);
        
        // Save Error
        expectedMessages++;
        System.assertEquals(false, setToken(expectedMatcherToken, messages, true));
        System.assertEquals(expectedMessages, messages.size());
        lastIndex = messages.size() - 1;
        System.assertEquals(true, (lastIndex >= 0) ? MatcherUtils.MESSAGE_TOKEN_FAIL.equals(messages.get(lastIndex).getSummary()) : false);
    }
    
    private static testmethod void testDeleteToken() {
        // Not Set
        System.assertEquals(false, deleteToken());
        
        // Set
        MatcherToken__c matcherTokenSetting = new MatcherToken__c(Name=SETTING_NAME_TEST, Token__c='abc123');
        insert matcherTokenSetting;
        
        // Delete Error
        System.assertEquals(false, deleteToken(true));
        
        // Success
        System.assertEquals(true, deleteToken(false));
        matcherTokenSetting = MatcherToken__c.getInstance(SETTING_NAME_TEST);
        System.assertEquals(null, matcherTokenSetting);
    }
    
    private static testmethod void testGetTokenSetting() {
        // Not Set
        System.assertEquals(null, getTokenSetting());
        
        // Set
        String expectedToken = 'abc123';
        MatcherToken__c matcherTokenSetting = new MatcherToken__c(Name=SETTING_NAME_TEST, Token__c=expectedToken);
        insert matcherTokenSetting;
        
        System.assertEquals(expectedToken, getTokenSetting());
    }

    private static testmethod void testIsTokenSet() {
    	// Not Set
    	System.assertEquals(false, isTokenSet());
    	
    	// Set
        MatcherToken__c matcherTokenSetting = new MatcherToken__c(Name=SETTING_NAME_TEST, Token__c='abc123');
        insert matcherTokenSetting;
        
        System.assertEquals(true, isTokenSet());
    }
     
}