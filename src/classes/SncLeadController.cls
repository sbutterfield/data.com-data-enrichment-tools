public class SncLeadController {

    //---------------------------------------------------------------------------------------------
    // Constants
    //---------------------------------------------------------------------------------------------
    
    /**
     * Set of fields used on the page. This will be used to construct the field list for the SOQL
     * query used to retrieve the CRM record.
     */
    private static final Set<String> FIELD_NAMES = new Set<String> {
        SncUtils.FIELD_NAME,
        SncUtils.FIELD_FIRST_NAME,
        SncUtils.FIELD_LAST_NAME,
        SncUtils.FIELD_COMPANY,
        SncUtils.FIELD_TITLE,
        SncUtils.FIELD_EMAIL,
        SncUtils.FIELD_PHONE,
        SncUtils.FIELD_STREET,
        SncUtils.FIELD_CITY,
        SncUtils.FIELD_STATE,
        SncUtils.FIELD_POSTAL_CODE,
        SncUtils.FIELD_COUNTRY,
        SncUtils.FIELD_NUMBER_OF_EMPLOYEES,
        SncUtils.FIELD_ANNUAL_REVENUE,
        SncUtils.FIELD_INDUSTRY,
        SncUtils.FIELD_COMPANY_DUNS_NUMBER,
        SncUtils.FIELD_WEBSITE,
// CITRIX
/*
        SncUtils.FIELD_GLOBAL_ULTIMATE_NAME,
        SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES,
        SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS,
        SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME,
        SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS,
        SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES,
        SncUtils.FIELD_PARENT_HQ_NAME,
        SncUtils.FIELD_PARENT_HQ_DUNS,
        SncUtils.FIELD_PARENT_HQ_EMPLOYEES
*/
// CITRIX
// BAF
        ddcDet.SncUtils.FIELD_PRIMARY_NAICS
    };
    
    /**
     * List of fields that will actually be rendered on the page. They will be rendered in the
     * order specified here.
     */
    private static final List<String> SEQUENCE = new List<String> {
        SncUtils.MERGED_FIELD_NAME,
        SncUtils.FIELD_COMPANY,
        SncUtils.FIELD_TITLE,
        SncUtils.FIELD_EMAIL,
        SncUtils.FIELD_WEBSITE,
        SncUtils.FIELD_PHONE,
        SncUtils.MERGED_FIELD_ADDRESS,
        SncUtils.FIELD_NUMBER_OF_EMPLOYEES,
        SncUtils.FIELD_ANNUAL_REVENUE,
        SncUtils.FIELD_INDUSTRY,
        SncUtils.FIELD_COMPANY_DUNS_NUMBER,
// CITRIX
/*
        SncUtils.FIELD_GLOBAL_ULTIMATE_NAME,
        SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES,
        SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS,
        SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME,
        SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS,
        SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES,
        SncUtils.FIELD_PARENT_HQ_NAME,
        SncUtils.FIELD_PARENT_HQ_DUNS,
        SncUtils.FIELD_PARENT_HQ_EMPLOYEES
*/
// CITRIX
// BAF
        ddcDet.SncUtils.FIELD_PRIMARY_NAICS
    };
    
    //---------------------------------------------------------------------------------------------
    // Fields
    //---------------------------------------------------------------------------------------------
    
    private final Lead l;
    private Lead ddcLead;
    private Wrapper lw;

    private Map<String, Schema.sObjectField> sObjectFieldMap;
    private transient ApexPages.Message[] messages;
    private Boolean notFound;
    private Boolean inSync;
    private Boolean hiddenFields;
    private Boolean hasError;
    private Map<String, Boolean> diffs;
    private Map<String, Boolean> checkBoxes;
    private Boolean isCompany; //DRAdd
    
    public String CRMAddressLine1 { get; set; }
    public String CRMAddressLine2 { get; set; }
    public String CRMAddressLine3 { get; set; }
    
    public String DDCAddressLine1 { get; set; }
    public String DDCAddressLine2 { get; set; }
    public String DDCAddressLine3 { get; set; }
    
    public Boolean refreshParent { get; set; }
    
    public String ddcid { get; set; }
    public String selectType { get; set; }

// CITRIX S_N_C PILOT - REMOVE ME
    private String sicCode { get; set; }

    public String leadName { get { return l != null ? l.Name : 'No Lead'; } }

    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    public SncLeadController() {
    	try {
        this.sObjectFieldMap = Schema.SObjectType.Lead.Fields.getMap();
        this.messages = new List<ApexPages.Message>();
        this.notFound = false;
        this.inSync = false;
        this.hiddenFields = false;
        this.hasError = false;
        this.diffs = new Map<String, Boolean>();
        this.checkBoxes = new Map<String, Boolean>();
        
        // Fetch CRM Record
        Map<String, Schema.SObjectField> fieldmap = Utils.getFieldListing('Lead');
        String fieldListString = '';
        for (String fieldName : fieldmap.keySet()) {
            fieldListString += fieldName + ',';
        }
        fieldListString = fieldListString.removeEnd(',');
        // NOTE: Remember to escape the id parameter since it comes from the URL!
        String id = ApexPages.currentPage().getParameters().get('id');
        String queryString =
            'SELECT ' + fieldListString + ' FROM Lead ' +
            'WHERE Id=\'' + ((id == null) ? '' : String.escapeSingleQuotes(id)) + '\' ' +
            'LIMIT 1';
        System.debug('query string = ' +queryString);
        try {
            this.l = (Lead) Database.query(queryString);
        }
        catch (Exception e) {
            System.debug('QUERY EXCEPTION: ' +e);
            this.l = new Lead();
        }
        
        CRMAddressLine1 = SncUtils.getAddressLine1(l, sObjectFieldMap);
        CRMAddressLine2 = SncUtils.getAddressLine2(l, sObjectFieldMap);
        CRMAddressLine3 = SncUtils.getAddressLine3(l, sObjectFieldMap);

        lw = new LeadWrapperDecorator(new SObjectWrapper(l, DDCType.CONTACT));
        MatchScenario lwScenario = lw.getScenario();
        String contactId_field = lwScenario.thisMapping.get(ApiUtils.TAG_NAME_CONTACT_ID).mappedFieldName;
        String companyId_field = lwScenario.thisMapping.get(ApiUtils.TAG_NAME_COMPANY_ID).mappedFieldName;
        String contactId = contactId_field == null ? null : (String) lw.getSObject().get(contactId_field);
        String companyId = companyId_field == null ? null : (String) lw.getSObject().get(companyId_field);
        
        // Fetch Data.com Record
        if(contactId != null && ApexPages.currentPage().getParameters().get('ddcid') == null) {
        	ddcid = contactId;
        }
        else {
        	ddcid =  ApexPages.currentPage().getParameters().get('ddcid');
        }
        String leadSelectType = l.IsCompanyOnlyMatch__c != null && l.IsCompanyOnlyMatch__c ? String.valueOf( DDCType.COMPANY ) : String.valueOf( DDCType.CONTACT );
        

        selectType = ApexPages.currentPage().getParameters().get('type') == null ? leadSelectType : ApexPages.currentPage().getParameters().get('type');

        // Initialize wrappers
        MatcherContact mc;
        
        if (String.isNotEmpty(ddcid)) {
        	
        	// Assume it might already be matched or a select match is coming through so let's get the matches
        	
        	// we need to use getContact retrieve by Id
        	Set<String> ids = new Set<String>{ddcid};
        	Map<String, DDCContact> sContacts;
        	Map<String, DDCCompany> sCompanies;
        	if(selectType.equalsIgnoreCase(String.valueOf(DDCType.CONTACT))) {
               isCompany = false;
               sContacts = DDCApi.retrieveContacts(ids, messages);
            }
            else if(selectType.equalsIgnoreCase(String.valueOf(DDCType.COMPANY))) {
                isCompany = true;
                sCompanies = DDCApi.retrieveCompanies(ids, messages);
            }


        	
        	// Build new MatcherContact for right now. We need to change this in the near future.
        	mc = new MatcherContact();
        	
        	if(selectType.equalsIgnoreCase(String.valueOf(DDCType.CONTACT))) {
        	
	        	// Only going to get one contact response
	        	DDCContact thisContact = sContacts.get(ddcid);
                if (thisContact != null) {
	        	  thisContact = thisContact.getContactCompany();
                }
	            // FIXME: Need exception handling for when we get a bad DDC Id and returns nothing back.
	        	mc.companyName = new MatcherField(thisContact.companyName, false);
	        	mc.companyId = new MatcherField(thisContact.companyId, false);
	        	mc.contactId = new MatcherField(thisContact.contactId, false);
	        	mc.email = new MatcherField(thisContact.email, false);
                mc.website = new MatcherField(thisContact.contactCompany.website, false);
	        	mc.title = new MatcherField(thisContact.title, false);
	        	mc.phone = new MatcherField(thisContact.phone, false);
	        	
	        	/* Matcher Location */
	        	MatcherLocation ml = new MatcherLocation();
	        	ml.locationPhone = new MatcherField(thisContact.phone, false);
	        	ml.locationId = new MatcherField(thisContact.contactId, false);
	        	MatcherAddress ma = new MatcherAddress();
	        	ma.addressLine = new MatcherField(thisContact.address, false);
	        	ma.country = new MatcherField(thisContact.country, false);
	        	ma.city = new MatcherField(thisContact.city, false);
	        	ma.state = new MatcherField(thisContact.state, false);
                ma.zip = new MatcherField(thisContact.zip, false);
	        	ml.address = ma;
	        	mc.location = ml;
	        	
	        	mc.firstName = new MatcherField(thisContact.firstName, false);
	        	mc.lastName = new MatcherField(thisContact.lastName, false);
                if( thisContact.contactCompany != null ) {

                    // Need to gather domestic ultimate and parent/hq details
                    // Construct new DDCCompanies for each duns number needed
                    DDCCompany[] skeletons = new List<DDCCompany>();
                    if (thisContact.contactCompany.domesticUltimateDunsNumber != null) {
                        DDCCompany newCompany = new DDCCompany();
                        newCompany.dunsNumber = thisContact.contactCompany.domesticUltimateDunsNumber;
                        newCompany.externalKey = thisContact.contactCompany.domesticUltimateDunsNumber;
                        skeletons.add(newCompany);
                    }
                    if (thisContact.contactCompany.parentOrHqDunsNumber != null) {
                        DDCCompany newCompany = new DDCCompany();
                        newCompany.dunsNumber = thisContact.contactCompany.parentOrHqDunsNumber;
                        newCompany.externalKey = thisContact.contactCompany.parentOrHqDunsNumber;
                        skeletons.add(newCompany);
                    }

                    // Do callout if there are skeletons:
                    Map<String, DDCCompany> additionalCompanies = new Map<String, DDCCompany>(); 
                    if (skeletons != null && skeletons.size() > 0) {
                        additionalCompanies = DDCApi.retrieveCompaniesByDuns(skeletons, messages);
                    }

    	        	mc.employeeCount = new MatcherField(Integer.valueOf(thisContact.contactCompany.employeeCount), false);
    	        	mc.revenue = new MatcherField(thisContact.contactCompany.revenue, false);
    	        	mc.industry = new MatcherField(thisContact.contactCompany.primarySicDesc, false);
    	        	mc.subIndustry = new MatcherField(thisContact.contactCompany.primarySicDesc, false);
    	        	mc.sfdcIndustry = new MatcherField(thisContact.contactCompany.primarySicDesc, false);
    	        	mc.updatedTs = new MatcherField(thisContact.updatedDate, false);
    	        	mc.dunsNumber = new MatcherField(thisContact.contactCompany.dunsNumber, false);
// BAF
                    mc.naicsCode = new MatcherField(thisContact.contactCompany.primaryNaics, false);

// CITRIX PILOT
/*
                    sicCode = thisContact.contactCompany.primarySic;
                    mc.globalUltimateName = new MatcherField(thisContact.contactCompany.globalUltimateBusinessName, false);
                    mc.globalUltimateEmployees = new MatcherField(thisContact.contactCompany.globalUltimateEmployees, false);
                    mc.globalUltimateDuns = new MatcherField(thisContact.contactCompany.globalUltimateDunsNumber, false);
                    mc.domesticUltimateName = new MatcherField(thisContact.contactCompany.domesticUltimateBusinessName, false);
                    mc.domesticUltimateDuns = new MatcherField(thisContact.contactCompany.domesticUltimateDunsNumber, false);
System.debug('additionalCompanies ::  ' +additionalCompanies);
System.debug('thisContact.contactCompany.domesticUltimateDunsNumber :: ' +thisContact.contactCompany.domesticUltimateDunsNumber);
System.debug('thisContact.contactCompany.parentOrHqDunsNumber :: ' +thisContact.contactCompany.parentOrHqDunsNumber);
                    if (additionalCompanies != null && additionalCompanies.keySet().size() > 0) {
                        mc.domesticUltimateEmployees = new MatcherField(additionalCompanies.get(thisContact.contactCompany.domesticUltimateDunsNumber).employeeCount, false);
                        mc.parentHqEmployees = new MatcherField(additionalCompanies.get(thisContact.contactCompany.parentOrHqDunsNumber).employeeCount, false);
                    }
                    else if (mc.globalUltimateDuns == mc.domesticUltimateDuns || mc.globalUltimateDuns == mc.parentHqDuns) {
                        mc.domesticUltimateEmployees = mc.employeeCount;
                        mc.parentHqEmployees = mc.employeeCount;
                    }
                    mc.parentHqName = new MatcherField(thisContact.contactCompany.parentOrHqBusinessName, false);
                    mc.parentHqDuns = new MatcherField(thisContact.contactCompany.parentOrHqDunsNumber, false);
*/
// CITRIX PILOT

                } else {
                    mc.updatedTs = new MatcherField(thisContact.updatedDate, false);
                } 
        	}
        	else if(selectType.equalsIgnoreCase(String.valueOf(DDCType.COMPANY))) {
        		
        		DDCCompany thisCompany = sCompanies.get(ddcid);
        		
        		mc.companyId = new MatcherField(thisCompany.companyId, false);
        		mc.companyName = new MatcherField(thisCompany.companyName, false);
        		mc.website = new MatcherField(thisCompany.website, false);
        		mc.phone = new MatcherField(thisCompany.phone, false);
        		
        		/* Matcher Location */
        		MatcherLocation ml = new MatcherLocation();
                ml.locationPhone = new MatcherField(thisCompany.phone, false);
                ml.locationId = new MatcherField(thisCompany.companyId, false);
                MatcherAddress ma = new MatcherAddress();
                ma.addressLine = new MatcherField(thisCompany.address, false);
                ma.country = new MatcherField(thisCompany.country, false);
                ma.city = new MatcherField(thisCompany.city, false);
                ma.state = new MatcherField(thisCompany.state, false);
                ma.zip = new MatcherField(thisCompany.zip, false);
                ml.address = ma;
                mc.location = ml;
                
                mc.dunsNumber = new MatcherField(thisCompany.dunsNumber, false);
                mc.employeeCount = new MatcherField(Integer.valueOf(thisCompany.employeeCount), false);
                mc.revenue = new MatcherField(thisCompany.revenue, false);
                mc.sfdcIndustry = new MatcherField(thisCompany.primarySicDesc, false);
                mc.updatedTs = new MatcherField(thisCompany.updatedDate, false);
// BAF
                mc.naicsCode = new MatcherField(thisCompany.primaryNaics, false);

// CITRIX
/*
                sicCode = thisCompany.primarySic;

                mc.globalUltimateName = new MatcherField(thisCompany.globalUltimateBusinessName, false);
                mc.globalUltimateEmployees = new MatcherField(thisCompany.globalUltimateEmployees, false);
                mc.globalUltimateDuns = new MatcherField(thisCompany.globalUltimateDunsNumber, false);
                mc.domesticUltimateName = new MatcherField(thisCompany.domesticUltimateBusinessName, false);
                mc.domesticUltimateDuns = new MatcherField(thisCompany.domesticUltimateDunsNumber, false);
                mc.parentHqName = new MatcherField(thisCompany.parentOrHqBusinessName, false);
                mc.parentHqDuns = new MatcherField(thisCompany.parentOrHqDunsNumber, false);

                // Need to gather domestic ultimate and parent/hq details
                // Construct new DDCCompanies for each duns number needed
                DDCCompany[] skeletons = new List<DDCCompany>();
                if (thisCompany.domesticUltimateDunsNumber != null) {
                    DDCCompany newCompany = new DDCCompany();
                    newCompany.dunsNumber = thisCompany.domesticUltimateDunsNumber;
                    newCompany.externalKey = thisCompany.domesticUltimateDunsNumber;
                    skeletons.add(newCompany);
                }
                if (thisCompany.parentOrHqDunsNumber != null) {
                    DDCCompany newCompany = new DDCCompany();
                    newCompany.dunsNumber = thisCompany.parentOrHqDunsNumber;
                    newCompany.externalKey = thisCompany.parentOrHqDunsNumber;
                    skeletons.add(newCompany);
                }

                // Do callout if there are skeletons:
                Map<String, DDCCompany> additionalCompanies = new Map<String, DDCCompany>(); 
                if (skeletons != null && skeletons.size() > 0) {
                    additionalCompanies = DDCApi.retrieveCompaniesByDuns(skeletons, messages);
                }
System.debug('additionalCompanies ::  ' +additionalCompanies);
System.debug('thisContact.contactCompany.domesticUltimateDunsNumber :: ' +thisCompany.domesticUltimateDunsNumber);
System.debug('thisContact.contactCompany.parentOrHqDunsNumber :: ' +thisCompany.parentOrHqDunsNumber);
                if (additionalCompanies != null && additionalCompanies.keySet().size() > 0) {
                        mc.domesticUltimateEmployees = new MatcherField(additionalCompanies.get(thisCompany.domesticUltimateDunsNumber).employeeCount, false);
                        mc.parentHqEmployees = new MatcherField(additionalCompanies.get(thisCompany.parentOrHqDunsNumber).employeeCount, false);
                }
                else if (mc.globalUltimateDuns == mc.domesticUltimateDuns || mc.globalUltimateDuns == mc.parentHqDuns) {
                    mc.domesticUltimateEmployees = mc.employeeCount;
                    mc.parentHqEmployees = mc.employeeCount;
                }
*/
// CITRIX
        	}
        }
        else {
        	//mc = MatcherAPI.matchLead(l, messages);
        }
        
        if (mc != null) {
            this.ddcLead = mc.toLead();
            initState();
        }
        else {
            this.ddcLead = new Lead();
            
            Boolean hasMessage = false;
            for (ApexPages.Message message : messages) {
				ApexPages.addMessage(message);
                if (message.getSeverity().ordinal() >= ApexPages.Severity.INFO.ordinal()) {
                   hasMessage = true;
                }
            }
            
            if (!hasMessage) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.DDC_SNC_Error_Record_Not_Found, ''));
                notFound = true;
            }
            else {
                hasError = true;
            }
        }
        
        DDCAddressLine1 = SncUtils.getAddressLine1(ddcLead, sObjectFieldMap);
        DDCAddressLine2 = SncUtils.getAddressLine2(ddcLead, sObjectFieldMap);
        DDCAddressLine3 = SncUtils.getAddressLine3(ddcLead, sObjectFieldMap);

    } catch ( Exception e ) {
        System.debug( System.LoggingLevel.ERROR, 'Exception in Constructor: ' + String.valueOf( e ) );
        System.debug( System.LoggingLevel.ERROR, 'Exception in Constructor: ' + e.getLineNumber() );
        System.debug( System.LoggingLevel.ERROR, 'Exception in Constructor: ' + e.getStackTraceString() );
        throw e;
    }

    }
    
    //---------------------------------------------------------------------------------------------
    // Public
    //---------------------------------------------------------------------------------------------
    
    public String getddccontactJson() {
    	
    	List<DDCContact> sContacts = DDCApi.findSimilarContacts(lw, messages);
    	
    	// Open the javascript array
    	String javascript_string = '[';
    	
    	if(sContacts != null) {
	    	// Add the contacts to selective results array for parsing and display on the fingers
	    	for(Integer i = 0; (i < sContacts.size() && i < 4); i++) {
	    		DDCContact c = sContacts[i].getContactCompany();
                if (c == null) {

                    break;
                }
System.debug('ddccontact pre-serialize ::  ' + c);
	    		javascript_string += Json.serialize(c);
	    		if(i < sContacts.size()-1 && i < 10) {
	    			javascript_string += ',';
	    		}
	    	}
    	}
    	// Close the javascript object array
    	javascript_string += ']';
    	System.debug('javascript_string:  ' +javascript_string);
    	return javascript_string;
    }
    
    public String getddccompanyJson() {

    	List<DDCCompany> sCompanies = DDCApi.findSimilarCompanies(lw, messages);

        try {
            sCompanies.sort();
        }
        catch (Exception e) {
            // Sort wasn't able to finish, just use the results un-sorted if necessary.
        }
    	
        // Open the javascript array
        String javascript_string = '[';

        //JsonGenerator jsonGenerator = new JSON.createGenerator(false);

        //jsonGenerator.writeStartObject(); // Writes {
        
        if(sCompanies != null) {
	        // Add the contacts to selective results array for parsing and display on the fingers
	        for(Integer i = 0; (i < sCompanies.size() && i < 10); i++) {
                
                if (sCompanies[i] == null) {

                    break;
                }
System.debug('ddccompany pre-serialize ::  ' + sCompanies[i]);
                javascript_string += Json.serialize(sCompanies[i]);

                //jsonGenerator.writeStringField(ApiUtils.TAG_NAME_COMPANY_NAME, sCompanies[i].objMap.get(TAG_NAME_COMPANY_NAME));
	            
                if(i < sCompanies.size()-1 && i < 10) {
	            
                    javascript_string += ',';
	            }
	        }
        }
        
        // Close the javascript object array
        javascript_string += ']';
        System.debug('javascript_string:  ' +javascript_string);
        return javascript_string;
    }
    
    /**
     * Retrieves the Salesforce record.
     */
    public Lead getCRM() {
        return l;
    }
    
    /**
     * Retrieves the Data.com record.
     */
    public Lead getDDC() {
        return ddcLead;
    }
    
    /**
     * Retrieves checkbox states.
     */
    public Map<String, Boolean> getCheckBoxes() {
    	return checkBoxes;
    }
    
    /**
     * Returns true if the request record was not found in Data.com.
     */
    public Boolean getNotFound() {
        return notFound;
    }
    
    /**
     * Returns true if all accessible fields are equal.
     */
    public Boolean getInSync() {
        return inSync;
    }
    
    /**
     * Returns true if there was an error getting record data from Data.com.
     */
    public Boolean getHasError() {
        return hasError;
    }
    
    /**
     * Returns the label used for the merged address field.
     * NOTE: This is required because the label is only referenced in Dynamic VisualForce so the
     * packaging system cannot see the dependency and does not include it in the bundle.
     */
    public String getAddressLabel() {
        return System.Label.DDC_SNC_Lead_Address_Label;
    }
    
    /**
     * Returns the custom label to be used when a user does not have write access to a field.
     * NOTE: This is required because the label is only referenced in Dynamic VisualForce so the
     * packaging system cannot see the dependency and does not include it in the bundle.
     */
    public String getFieldNotUpdateableLabel() {
        return System.Label.DDC_SNC_Info_Field_Not_Updateable;
    }
    
    /**
     * Retrieves the Data.com DunsNumber value which is obfuscated if necessary.
     */ 
    public String getDDCDunsNumberForDisplay() {
        return SncUtils.getDunsNumberForDisplay(l, ddcLead, sObjectFieldMap);
    }
    
    /**
     * Retrieves the Data.com Name value.
     */
    public String getDDCFullNameForDisplay() {
        return SncUtils.getFullNameForDisplay(ddcLead, sObjectFieldMap);
    }
    
    /**
     * Builds the actual Stare & Compare content body.
     */
    public Component.Apex.PageBlockSection getSNCBody() {
        Component.Apex.PageBlockSection body = SncUtils.createContainer();
        
        Boolean allEqual = true;
        for (String fieldName : SEQUENCE) {
            String mappedFieldName;
            if (fieldName.equals(SncUtils.MERGED_FIELD_NAME)) {
                // NOTE: FLS is tied to the merged name field and looks like it's always accessible
                // by any profile.
                mappedFieldName = SncUtils.FIELD_LAST_NAME;
            }
            else if (fieldName.equals(SncUtils.MERGED_FIELD_ADDRESS)) {
                // NOTE: The merged address field is a special case. When visibility of the merged
                // field is hidden in the UI, all related fields are hidden, so we only need to check
                // one of them.
                mappedFieldName = SncUtils.FIELD_STREET;
            }
            else {
                mappedFieldName = fieldName;
            }
            
            // Create the field if it is visible to the current user.
            if (Utils.getCrud(mappedFieldName, 'access', sObjectFieldMap)) {
                Boolean diff = diffs.get(fieldName);
                body.childComponents.add(SncUtils.createCRMField('Lead', fieldName));
                body.childComponents.add(SncUtils.createDDCField(
                    fieldName, diff, true));
                
                // We know the records are not in sync if any field is accessible and different.
                if (diff != null && diff) {
                    allEqual = false;
                }
            }
            else if (sObjectFieldMap.containsKey(mappedFieldName)) {
                // We have hidden fields if a field is not accessible but is present in the schema.
                hiddenFields = true;
            }
        }
        
        inSync = allEqual;
        
        if (hiddenFields && !notFound && !hasError) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.DDC_SNC_Info_Hidden_Fields, ''));
        }
        
        return body;
    }

    public PageReference doUpdateAndRedirect() {
        doUpdate();
        PageReference p = new PageReference( '/' + l.Id );
        p.setRedirect( true );
        return p;
    }
    
    public PageReference doUpdate() {
        // Check if user can make modifications to this object.
        Schema.DescribesObjectResult leadDescribed = Schema.sObjectType.Lead;
        if (!leadDescribed.isUpdateable()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDC_SNC_Error_Object_Not_Updateable, ''));
            return null;
        }
        
        // If a checkbox is checked and the user has update access to the field, update the CRM
        // record value with the the one given by data.com.
        Set<String> fieldNames = checkBoxes.keySet();
        for (String fieldName : fieldNames) {
        	if (checkBoxes.get(fieldName) != true) {
        		continue;
        	}
        	
        	if (SncUtils.MERGED_FIELD_NAME.equals(fieldName)) {
                for (String nameField : SncUtils.NAME_FIELDS) {
                    if (Utils.getCrud(nameField, 'update', sObjectFieldMap)) {
                        l.put(nameField, ddcLead.get(nameField));
                    }
                }
        	}
        	else if (SncUtils.MERGED_FIELD_ADDRESS.equals(fieldName)) {
                for (String addressField : SncUtils.ADDRESS_FIELDS_LEAD.values()) {
                    if (Utils.getCrud(addressField, 'update', sObjectFieldMap)) {
                        l.put(addressField, ddcLead.get(addressField));
                    }
                }
        	}
        	else {
                if (Utils.getCrud(fieldName, 'update', sObjectFieldMap)) {
                    l.put(fieldName, ddcLead.get(fieldName));
                }
        	}
        }
// CITRIX PILOT
        //l.put('Primary_SIC_Code__c', sicCode);
// CITRIX PILOT
        l.put('Jigsaw',ddcid);
        l.put('ddcDet__Automation_Disabled__c',true);
        l.put('ddcDet__System_MatchStatus__c','Matched!');
        l.put('ddcDet__MatchCode__c', 'MF07');

        l.put('ddcDet__IsCompanyOnlyMatch__c', isCompany);
        l.put('CleanStatus', 'Acknowledged');
        
        boolean success;
        try {
            update l;
            success = true;
        }
        catch (Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDC_SNC_Error_Saving, e + ''));
            success = false;
        }
        
        if (success) {
            refreshParent = true;
        }
        
        return null;
    }
    
    public PageReference cancel() {
        return null;
    }
    
    //---------------------------------------------------------------------------------------------
    // Private Methods
    //---------------------------------------------------------------------------------------------
    
    private void initState() {
        // Name is a merged field.
        for (String fieldName : SncUtils.NAME_FIELDS) {
            String ddcValue = (String) ddcLead.get(fieldName);
            if (String.isEmpty(ddcValue)) {
                continue;
            }
            Boolean nameDiff = !ddcValue.equalsIgnoreCase((String) l.get(fieldName));
            diffs.put(SncUtils.MERGED_FIELD_NAME, nameDiff);
            if (nameDiff) {
                break;
            }
        }
        
        diffs.put(SncUtils.FIELD_COMPANY, String.isEmpty(ddcLead.Company) ? null : !ddcLead.Company.equalsIgnoreCase(l.Company));
        // NOTE: Titles need to be truncated to 80 characeters to save the record so we can't
        // use the diff flag returned by the Matcher API.
        diffs.put(SncUtils.FIELD_TITLE, String.isEmpty(ddcLead.Title) ? null : !ddcLead.Title.equalsIgnoreCase(l.Title));
        diffs.put(SncUtils.FIELD_EMAIL, String.isEmpty(ddcLead.Email) ? null : !ddcLead.Email.equalsIgnoreCase(l.Email));
        diffs.put(SncUtils.FIELD_PHONE, String.isEmpty(ddcLead.Phone) ? null : !Utils.phoneEquals(l.Phone, ddcLead.Phone));
        
        // Address is a merged field.
        for (String fieldName : SncUtils.ADDRESS_FIELDS_LEAD.values()) {
            String ddcValue = (String) ddcLead.get(fieldName);
            if (String.isEmpty(ddcValue)) {
                continue;
            }
            Boolean addressDiff = !ddcValue.equalsIgnoreCase((String) l.get(fieldName));
            diffs.put(SncUtils.MERGED_FIELD_ADDRESS, addressDiff);
            if (addressDiff) {
                break;
            }
        }
        
        diffs.put(SncUtils.FIELD_NUMBER_OF_EMPLOYEES, (ddcLead.NumberOfEmployees == null || ddcLead.NumberOfEmployees == 0) ? null : l.NumberOfEmployees != ddcLead.NumberOfEmployees);
        diffs.put(SncUtils.FIELD_ANNUAL_REVENUE, (ddcLead.AnnualRevenue == null || ddcLead.AnnualRevenue == 0) ? null : l.AnnualRevenue != ddcLead.AnnualRevenue);
        diffs.put(SncUtils.FIELD_INDUSTRY, String.isEmpty(ddcLead.Industry) || ddcLead.Industry.equalsIgnoreCase(SncUtils.DEFAULT_INDUSTRY) ? null : !ddcLead.Industry.equalsIgnoreCase(l.Industry));
        
        // NOTE: D&B fields may not be present.
        if (sObjectFieldMap.containsKey(SncUtils.FIELD_COMPANY_DUNS_NUMBER)) {
            diffs.put(SncUtils.FIELD_COMPANY_DUNS_NUMBER, String.isEmpty((String) ddcLead.get(SncUtils.FIELD_COMPANY_DUNS_NUMBER)) ? null :
               l.get(SncUtils.FIELD_COMPANY_DUNS_NUMBER) != ddcLead.get(SncUtils.FIELD_COMPANY_DUNS_NUMBER));
        }

        diffs.put(SncUtils.FIELD_WEBSITE, (ddcLead.Website == null ? null : l.Website == null ? true : !URIUtils.parseDomain(ddcLead.Website).equalsIgnoreCase(URIUtils.parseDomain(l.Website))));

// BAF
        diffs.put(SNCUtils.FIELD_PRIMARY_NAICS, (ddcLead.get(ddcDet.SncUtils.FIELD_PRIMARY_NAICS) == null ? null : l.get(ddcDet.SncUtils.FIELD_PRIMARY_NAICS) == null ? true : l.get(ddcDet.SncUtils.FIELD_PRIMARY_NAICS) != ddcLead.get(ddcDet.SncUtils.FIELD_PRIMARY_NAICS)));

// CITRIX ===================================================
/*
        diffs.put(SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS, (ddcLead.get(SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS) == null ? null : !(String.valueOf(l.get(SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS)) == null ? false : String.valueOf(l.get(SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS)).equalsIgnoreCase( (String) ddcLead.get(SncUtils.FIELD_GLOBAL_ULTIMATE_DUNS)))));
        diffs.put(SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES, (ddcLead.get(SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES) == null ? null : !(Integer.valueOf(l.get(SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES)) == null ? false : Integer.valueOf(l.get(SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES)) == Integer.valueOf(ddcLead.get(SncUtils.FIELD_GLOBAL_ULTIMATE_EMPLOYEES)))));
        diffs.put(SncUtils.FIELD_GLOBAL_ULTIMATE_NAME, (ddcLead.get(SncUtils.FIELD_GLOBAL_ULTIMATE_NAME) == null ? null : !(String.valueOf(l.get(SncUtils.FIELD_GLOBAL_ULTIMATE_NAME)) == null ? false : String.valueOf(l.get(SncUtils.FIELD_GLOBAL_ULTIMATE_NAME)).equalsIgnoreCase( (String) ddcLead.get(SncUtils.FIELD_GLOBAL_ULTIMATE_NAME)))));
        diffs.put(SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS, (ddcLead.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS) == null ? null : !(String.valueOf(l.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS)) == null ? false : String.valueOf(l.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS)).equalsIgnoreCase( (String) ddcLead.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_DUNS)))));
        diffs.put(SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME, (ddcLead.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME) == null ? null : !(String.valueOf(l.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME)) == null ? false : String.valueOf(l.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME)).equalsIgnoreCase( (String) ddcLead.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_NAME)))));
        diffs.put(SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES, (ddcLead.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES) == null ? null : !(Integer.valueOf(l.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES)) == null ? false : Integer.valueOf(l.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES)) == Integer.valueOf(ddcLead.get(SncUtils.FIELD_DOMESTIC_ULTIMATE_EMPLOYEES)))));
        diffs.put(SncUtils.FIELD_PARENT_HQ_DUNS, (ddcLead.get(SncUtils.FIELD_PARENT_HQ_DUNS) == null ? null : !(String.valueOf(l.get(SncUtils.FIELD_PARENT_HQ_DUNS)) == null ? false : String.valueOf(l.get(SncUtils.FIELD_PARENT_HQ_DUNS)).equalsIgnoreCase( (String) ddcLead.get(SncUtils.FIELD_PARENT_HQ_DUNS)))));
        diffs.put(SncUtils.FIELD_PARENT_HQ_NAME, (ddcLead.get(SncUtils.FIELD_PARENT_HQ_NAME) == null ? null : !(String.valueOf(l.get(SncUtils.FIELD_PARENT_HQ_NAME)) == null ? false : String.valueOf(l.get(SncUtils.FIELD_PARENT_HQ_NAME)).equalsIgnoreCase( (String) ddcLead.get(SncUtils.FIELD_PARENT_HQ_NAME)))));
        diffs.put(SncUtils.FIELD_PARENT_HQ_EMPLOYEES, (ddcLead.get(SncUtils.FIELD_PARENT_HQ_EMPLOYEES) == null ? null : !(Integer.valueOf(l.get(SncUtils.FIELD_PARENT_HQ_EMPLOYEES)) == null ? false : Integer.valueOf(l.get(SncUtils.FIELD_PARENT_HQ_EMPLOYEES)) == Integer.valueOf(ddcLead.get(SncUtils.FIELD_PARENT_HQ_EMPLOYEES)))));
*/
// CITRIX ===================================================
        
        // Initialize checkboxes for dynamic VisualForce bindings.
        for (String fieldName : diffs.keySet()) {
            if (diffs.get(fieldName) == true) {
                checkBoxes.put(fieldName, false);
            }
        }
    }
    
}