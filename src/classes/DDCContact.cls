/**
 *  Data.com Contact apex object to wrap a Data.com Contact returned by the DDC Api webservice
 *  To-Do: Implement additional behaviors to retrieve the related DDCCompany object for this contact and wrap it.
 */

public class DDCContact extends DDCObject implements Comparable {


    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------

    /* Properties of DDCContact */
    // THESE SHOULD *ALWAYS* BE THE SAME NAME AND ORDER AS THEIR CORRESPONDING TAG NAMES (DDCApi.cls)
    public String companyId {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COMPANY_ID); }
        set { objMap.put(ApiUtils.TAG_NAME_COMPANY_ID, value); }
    }
    public String contactId {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_CONTACT_ID); }
        set { objMap.put(ApiUtils.TAG_NAME_CONTACT_ID, value); }
    }
    public String title {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_TITLE); }
        set { objMap.put(ApiUtils.TAG_NAME_TITLE, value); }
    }
    public String companyName {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_COMPANY_NAME); }
        set { objMap.put(ApiUtils.TAG_NAME_COMPANY_NAME, value); }
    }
    public DateTime updatedDate {
        get {
            return Utils.parseISODate(String.valueOf(objMap.get(ApiUtils.TAG_NAME_CREATED_ON)));
        }
        set { objMap.put(ApiUtils.TAG_NAME_UPDATED_DATE, value); }
    }
    public String graveyardStatus {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_GRAVEYARD_STATUS); }
        set { objMap.put(ApiUtils.TAG_NAME_GRAVEYARD_STATUS, value); }
    }
    public String firstName {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_FIRST_NAME); }
        set { objMap.put(ApiUtils.TAG_NAME_FIRST_NAME, value); }
    }
    public String lastName {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_LAST_NAME); }
        set { objMap.put(ApiUtils.TAG_NAME_LAST_NAME, value); }
    }
    public String city {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_ADDRESS_CITY); }
        set { objMap.put(ApiUtils.TAG_NAME_ADDRESS_CITY, value); }
    }
    public String state {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_ADDRESS_STATE); }
        set { objMap.put(ApiUtils.TAG_NAME_ADDRESS_STATE, value); }
    }
    public String country {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_ADDRESS_COUNTRY); }
        set { objMap.put(ApiUtils.TAG_NAME_ADDRESS_COUNTRY, value); }
    }
    public String zip {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_ADDRESS_ZIP); }
        set { objMap.put(ApiUtils.TAG_NAME_ADDRESS_ZIP, value); }
    }
    public String department {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_DEPARTMENT); }
        set { objMap.put(ApiUtils.TAG_NAME_DEPARTMENT, value); }
    }
    public String areaCode {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_AREA_CODE); }
        set { objMap.put(ApiUtils.TAG_NAME_AREA_CODE, value); }
    }
    public String phone {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_PHONE); }
        set { objMap.put(ApiUtils.TAG_NAME_PHONE, value); }
    }
    public String email {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_EMAIL); }
        set { objMap.put(ApiUtils.TAG_NAME_EMAIL, value); }
    }
    public String address {
        get { return (String) objMap.get(ApiUtils.TAG_NAME_ADDRESS); }
        set { objMap.put(ApiUtils.TAG_NAME_ADDRESS, value); }
    }
    
    // Added to support select match & stare-and-compare properly
    public String fullName { 
        get {
            return String.valueOf(objMap.get(ApiUtils.TAG_NAME_FIRST_NAME) + '  ' + objMap.get(ApiUtils.TAG_NAME_LAST_NAME));
        } 
        private set; 
    }

    // This is a wrapped instance of DDCCompany. Represents this contact's corresponding Data.com company
    public DDCCompany contactCompany { get; set; }


    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------

    /**
     *  Constructs a new DDCContact from an Xml Document
     */
    public DDCContact(Dom.XmlNode xml) {
        super(ApiUtils.TAG_NAME_CONTACT, ApiUtils.SEQUENCE_CONTACT);
        parse(xml);
    }

    public DDCContact(DDCObject input) {
        super(ApiUtils.TAG_NAME_CONTACT, ApiUtils.SEQUENCE_CONTACT, input.objMap, input.externalKey);
    }
    
    public DDCContact() {
        // default constructor
        super(ApiUtils.TAG_NAME_CONTACT, ApiUtils.SEQUENCE_CONTACT);
    }
    

    //---------------------------------------------------------------------------------------------
    // API
    //---------------------------------------------------------------------------------------------

    /**
     *  Retrieves the related DDCCompany for this DDCContact and wraps it within the object instance
     */
    public DDCContact getContactCompany() {
        if (this.contactCompany != null) {
            // If there is no valid companyId for the current instance of DDCContact,
            // return the current object to the client.
            // If there is already a contactCompany bound to the DDCContact, just return this instance
            return this;
        }
        // Retrieve the first DDCCompany in the resulting collection since we will only be sending a single companyId through.
        //this.contactCompany = DDCApi.retrieveCompanies(new Set<String>{this.companyId}, new ApexPages.Message[]{}).values()[0];

        //DRADD for Issue #6
        Map<String, DDCCompany> companyMatches = DDCApi.retrieveCompanies(new Set<String>{this.companyId}, new ApexPages.Message[]{});
        if( companyMatches.values().size() > 0 ) {
            this.contactCompany = companyMatches.values()[0];
        } else {
            this.contactCompany = new DDCCompany();
        }

        // Merge data maps before returning, otherwise automation has weird behavior.
        this.objMap.putAll(this.contactCompany.objMap);
        return this;
    }

    /**
     *  DR TODO Possible fix
     *   Map<String, DDCCompany> companyMatches = DDCApi.retrieveCompanies(new Set<String>{this.companyId}, new ApexPages.Message[]{});
     *   if( companyMatches.values().size() > 0 ) {
     *       this.contactCompany = companyMatches.values()[0];
     *       // Merge data maps before returning, otherwise automation has weird behavior.
     *   } else {
     *       this.contactCompany = new DDCCompany();
     *   }
     *       this.objMap.putAll(this.contactCompany.objMap);
     */
    
    /* Signature required by Comparable interface */
    public Integer compareTo(Object input) {
        // Default sort implementation
        return compareTo(input, 'updatedDate', false);
    }
    
    /* Override Comparable behavior using an overloaded signature */
    public Integer compareTo(Object input, String a, Boolean descending) {
        if (input == null || a == null || descending == null) {
            return null;
        }
        
        // Cast the input to a local variable
        DDCContact c = (DDCContact)input;
        
        Boolean defaultSort;
        if (a == 'updatedDate') {
            defaultSort = true;
        }
        
        if (defaultSort) {
            // using updatedDate
            if (this.updatedDate == c.updatedDate) {
                return 0;
            }
            else if (this.updatedDate > c.updatedDate) {
                return 1;
            }
            else {
                return -1;
            }
        }
        else {
            // Implement using "a" = attribute to sort by.
            // Only allows certain sorting
            if (a == 'contactId') {
                if (this.contactId == c.contactId) {
                    return 0;
                }
                else if (this.contactId > c.contactId) {
                    return 1;
                }
                else {
                    return -1;
                }
            }
            else if (a == 'lastName') {
                if (this.lastName == c.lastName) {
                    return 0;
                }
                else if (this.lastName > c.lastName) {
                    return 1;
                }
                else {
                    return -1;
                }
            }
            else if (a == 'activeContacts') {
                if (c.contactCompany == null) {
                    return null;
                }
                // descend in to DDCContact.contactCompany.activeContacts
                if (this.contactCompany.activeContacts == c.contactCompany.activeContacts) {
                    return 0;
                }
                else if (this.contactCompany.activeContacts > c.contactCompany.activeContacts) {
                    return 1;
                }
                else { 
                    return -1;
                }
            }
        }
        return null;
    }


    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------

    // Overridden method from super to return this instances type (DDCContact)
    public override String getName() {
        return 'DDCContact';
    }

    /* Parses a DDC Contact object from an XML document */
    // Only called from the constructor
    private void parse(Dom.XmlNode cnode) {
        
        /**
         *  For each tag in the sequence, pull out the content for that tag from the xml.
         *  Put extracted content in to a map: Map<String(TAG_NAME_VALUE{ddcproperty}),Object(Value of parsed result for the tag)>
         *  Then, iterate over all the keys and construct the DDCContact with the known attribute name that we would get from the tag.
         */

        Dom.XmlNode node;
        for (String tname : sequence) {
            
            node = cnode.getChildElement(tname, null);
            if (node != null) {
            
                objMap.put(tname, node.getText());
            }
            else {
                // Always put the tag in the map, otherwise we cannot serialize this object properly
                objMap.put(tname, null);
            }
        }
    }

}