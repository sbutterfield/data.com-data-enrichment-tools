/**
 *  Controller extension for the DETConfig page that handles all batch transaction-ability for the user's inputs
 *  Parent class: DETConfigController.cls
 */

public with sharing class BatchSubmitController {

    // Accessors - controls page behaviors
    public Boolean hasBatchJobs { get; set; }

    private static final Integer BATCH_SCOPE = 2;

    public BatchSubmitController(DETConfigController controller) {

    }
    
    /*
        Used on the page to populate the batch jobs table
    */
    public String getBatches() {
        ApexClass bcjClass = [SELECT Id, Name, NamespacePrefix, IsValid FROM ApexClass WHERE Name = 'BatchableCleanJob'];
        if (bcjClass == null) return null;
        List<AsyncApexJob> qresult;
        String result;
        hasBatchJobs = false;
        qresult = [SELECT Id, JobType, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedDate, CompletedDate, ParentJobId, CreatedById, CreatedBy.Name
            FROM AsyncApexJob WHERE JobType IN('BatchApex','BatchApexWorker') AND ApexClassId = :bcjClass.Id ORDER BY CreatedDate DESC LIMIT 15 ];
        if(qresult != null) {
            AsyncApexJob[] jobs = new List<AsyncApexJob>();
            for (AsyncApexJob job : qresult) {
                if (job.CreatedBy != null) {
                    jobs.add(job);
                }
            }
            hasBatchJobs = true;
            result = JSON.serialize(jobs);
        }
        return result;
    }
    
    //---------------------------------------------------------------------------------------------
    // Javascript Remotes
    //---------------------------------------------------------------------------------------------
    
    /**
     *  VF Remote: Accepts soql query string from batch handler page and invokes the underlying batch job
     *  @returns: String = JSON serialized array of the asynch jobs table after the new one has been queued
     *  Since vf remote actions know how to serialize themselves for proper javascript use, 
     *      we don't need to return a serialized result here, just the array.
     */
    @RemoteAction
    public static List<AsyncApexJob> remotePushNewJob(String soql) {
    	
    	if(soql == null) {
    		throw new ddcLib1.NullPointerException('Unexpected null argument not allowed. Method requires a query statement.');
    	}
    	
    	BatchableCleanJob job = new BatchableCleanJob(soql);
    	Id batchprocessid = Database.executeBatch(job, BATCH_SCOPE);
    	
    	List<AsyncApexJob> qresult;
    	qresult = [SELECT Id, JobType, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedDate, CompletedDate, ParentJobId, CreatedById, CreatedBy.Name
            FROM AsyncApexJob WHERE JobType IN('BatchApex','BatchApexWorker') ORDER BY CreatedDate DESC LIMIT 15 ];
        
        return qresult;
    }

}