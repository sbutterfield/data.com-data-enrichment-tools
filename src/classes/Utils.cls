/**
 *  Utilities class has all utility methods and static runtime caches that the
 *  application would universally need.
 *
******************/


public class Utils {

    //---------------------------------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------------------------------
    
    public static String NAMESPACE_PREFIX = 'ddcdet__';
    public static String REGEX_ID = '[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}';
    public static Map<String, Schema.DescribeFieldResult> FIELD_DESCRIBE_CACHE = new Map<String, Schema.DescribeFieldResult>();
    public static Map<String, Schema.DescribeSObjectResult> SOBJECT_DESCRIBE_CACHE = new Map<String, Schema.DescribeSObjectResult>(); 
    public static Map<String, Schema.SObjectType> GLOBAL_DESCRIBE_CACHE = Schema.getGlobalDescribe();
    
    private static final String REGEX_PHONE_NUMBER = '\\W*';
    private static final Integer PHONE_NUMBER_FORMAT_LENGTH = 10;

    //---------------------------------------------------------------------------------------------
    // Constructors
    //---------------------------------------------------------------------------------------------
    
    /**
     * Hidden constructor, only static utilities are allowed.
     */
    private Utils() {}
    
    //---------------------------------------------------------------------------------------------
    // Methods
    //---------------------------------------------------------------------------------------------
    
    /**
     * Given a list of ApexPages.Messages, this determines if at least the min severity level is
     * present. Note that there is a similar native implementation present:
     * ApexPages.hasMessages(ApexPages.Severity) 
     * We could use that but since it only checks for the presence of specific severity types, we
     * would need to make several calls to it to achieve the same effect as this method.
     * ApexPages.Severity Enum: { FATAL, ERROR, WARNING, INFO, CONFIRM }
     */
    public static boolean messageListHasSeverity(List<ApexPages.Message> messages, ApexPages.Severity min) {
        if (messages == null || min == null) {
            return false;
        }
        
        boolean hasMessage = false;
        for (ApexPages.Message message : messages) {
            ApexPages.Severity severity = message.getSeverity();
            if (severity.ordinal() <= min.ordinal()) {
                hasMessage = true;
                break;
            }
        }
        return hasMessage;
    }

    public static String escapeJavascript(String input) {
        String result = input;
        // Clean up whitespace
        input.trim();
        // Enclose the input in double quotes
        if(!input.startsWith('"') && !input.endsWith('"')) {
            result = '"' + input + '"';
        }
        return result;
    }

    /**
     *  Safely writes a value from a DDCObject to an SObject field based on a mapping that is known to exist
     *  @return = void, acts directly on the SObject Field
     *  @throws = none
     */
    public static void setSobjectField(Sobject s, MappedField mf, DDCObject ddc) {

        if (s == null || mf == null || ddc == null) {
            
            // Do nothing.
            return;
        }

        Object ddc_val = ddc.objMap.get(mf.propertyName);

        // Never potentially overwrite or fill with null.
        if (ddc_val == null) {
            return;
        }
        
        try {
System.debug('putting value: ' +ddc_val+ '  into field: ' +mf.mappedFieldName);
            s.put(mf.mappedFieldName, ddc_val);
        }
        catch(SObjectException e) {
System.debug('!&&&&& CAUGHT TYPE EXCEPTION! ' +e.getMessage() +'    ----> ' +mf.mappedFieldName + '  ' +ddc_val);
            // Need to do some custom casting and bindings
            if (mf.fieldType.equalsIgnoreCase(String.valueOf(Schema.SOAPType.DOUBLE))) {

                // Cast as double
                if (ddc_val == null) {
                    s.put(mf.mappedFieldName, Double.valueOf('0'));
                }
                s.put(mf.mappedFieldName, Double.valueOf(ddc_val));
            }
            else if (mf.fieldType.equalsIgnoreCase(String.valueOf(Schema.SOAPType.DATETIME))) {

                // Cast as datetime
                s.put(mf.mappedFieldName, Utils.parseISODate((String) ddc_val));
            }
            else if (mf.fieldType.equalsIgnoreCase(String.valueOf(Schema.SOAPType.INTEGER))) {

                // Cast as integer
                if (ddc_val == null) {
                    s.put(mf.mappedFieldName, Integer.valueOf('0'));
                }
                s.put(mf.mappedFieldName, Integer.valueOf(ddc_val));
            }
            else if (mf.fieldType.equalsIgnoreCase(String.valueOf(Schema.SOAPType.BOOLEAN))) {

                // Cast as a boolean (returns false by default)
                s.put(mf.mappedFieldName, Boolean.valueOf(ddc_val));
            }
        }


    }
    
    public static Boolean getCrud(String obj, String fieldName, String action) {
        if (obj == null) {
            return false;
        }
        
        Map<String, Schema.SObjectField> sObjectFieldMap;
        try {
            Schema.DescribeSObjectResult sObjectDescribe = Schema.getGlobalDescribe().get(obj).getDescribe();
            sObjectFieldMap = sObjectDescribe.fields.getMap(); 
        }
        catch (Exception e) {
            sObjectFieldMap = null;
        }
        
        return getCrud(fieldName, action, sObjectFieldMap);
    }
    
    public static Boolean getCrud(String fieldName, String action, Map<String, Schema.SObjectField> sObjectFieldMap) {
        if (fieldName == null || action == null || sObjectFieldMap == null) {
            return false;
        }
        
        Schema.SObjectField field = sObjectFieldMap.get(fieldName);
        if (field == null) {
            return false;
        }
        
        Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
        if (fieldDescribe == null) {
            return false;
        }
        
        Boolean result;
        if ('create'.equals(action) || 'insert'.equalsIgnoreCase(action)) {
            result = fieldDescribe.isCreateable();
        }
        else if ('update'.equalsIgnoreCase(action) || 'write'.equalsIgnoreCase(action)) {
            result = fieldDescribe.isUpdateable();
        }
        else if ('view'.equalsIgnoreCase(action) || 'access'.equalsIgnoreCase(action) || 'read'.equalsIgnoreCase(action)) {
            result = fieldDescribe.isAccessible();
        }
        else if ('delete'.equalsIgnoreCase(action) || 'nill'.equalsIgnoreCase(action)) {
            result = fieldDescribe.isNillable();
        }
        else if ('cascade'.equalsIgnoreCase(action)) {
            result = fieldDescribe.isCascadeDelete();
        }
        else if ('writeRequireMasterRead'.equalsIgnoreCase(action) || 'master'.equalsIgnoreCase(action)){
            result = fieldDescribe.isWriteRequiresMasterRead();
        }
        else {
            result = false;
        }
        
        return result;
    }

    /**
     * Checks to make sure that the runtime has a license to continue.
     */
    // To-Do: Overload this method to check a particular User
    public static Boolean hasLicense() {
        Boolean result;
        try {
            result = UserInfo.isCurrentUserLicensed('ddcDet');
        }
        catch(Exception e) {
            result = false;
        }
        
        return result;
    }
    
    /**
     * Validates that this is a Salesforce.com Id
     */
     public static Boolean isValidId(Object o) {
        if (o == null) {
            return false;
        }
        
        Boolean result;
        Pattern p = Pattern.compile(REGEX_ID);
        try {
            Matcher m = p.matcher(String.valueOf(o));
            result = m.matches();
        }
        catch(Exception e) {
            System.debug(LoggingLevel.ERROR, 'Exception while validating Id: ' + e);
            result = false;
        }
        
        return result;
     }
     
    /**
     * Utility for testing the equality of phone numbers.
     */
    public static Boolean phoneEquals(String phone1, String phone2) {
        String normalizedPhone1 = (phone1 == null) ? null : phone1.replaceAll(REGEX_PHONE_NUMBER, '');
        String normalizedPhone2 = (phone2 == null) ? null : phone2.replaceAll(REGEX_PHONE_NUMBER, '');
        if (normalizedPhone1 == normalizedPhone2) {
            return true;
        }
        
        // For certain locales, Salesforce will format 10-digit phone numbers using the (XXX) XXX-XXXX
        // format. This is triggered when there is no leading '+' character and the country code is
        // '1'. Extension characters are appeneded to the end and are not counted.
        // See the formatPhone function at: https://na1.salesforce.com/static/022509/js/functions.js
        
        Integer length1 = (normalizedPhone1 == null) ? 0 : normalizedPhone1.length();
        Integer length2 = (normalizedPhone2 == null) ? 0 : normalizedPhone2.length();
        Boolean isccna1 = (length1 > PHONE_NUMBER_FORMAT_LENGTH && normalizedPhone1.startsWith('1'));
        Boolean isccna2 = (length2 > PHONE_NUMBER_FORMAT_LENGTH && normalizedPhone2.startsWith('1'));
        if (length2 > length1 && isccna2) {
            return normalizedPhone2.substring(1).equals(normalizedPhone1);
        }
        else if (length1 > length2 && isccna1) {
            return normalizedPhone1.substring(1).equals(normalizedPhone2);
        }
        else {
            return false;
        }
    }

    /*Parse Datatime from string*/
    public static Datetime parseDate(String str) {
        if (str != null && str.length() > 0) {
            String[] tokens = str.split(' ');
            if (tokens.size()>1) {
                String dateS = tokens[0] + ' ' + tokens[1];
                return DateTime.valueOf(dateS);
            }
        }
        return null;
    }
    
    /*Parse Datatime in ISO format from string*/
    public static Datetime parseISODate(String str) {
        if (str != null && str.length() > 0) {
            str = str.replaceAll('T',' ').replaceAll('Z','');
            return DateTime.valueOfGMT(str);
        }
        return null;
    }
    
    /**
     * Safely creates a request using the given parameters.
     * Behavior:
     * - If url is null, null will be returned
     * - If body is null, it will be ignored
     * - If method is neither GET nor POST, GET will be assumed
     * - If compressed is null, no compression will be used
     * - If timeout is null, the default will be used 
     */
    public static HttpRequest createRequest(String url, String body, String method, Boolean compressed, Integer timeout) {
        if (url == null) {
            return null;
        }
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        
        if (body != null) {
            req.setBody(body);
        }
        
        // NOTE: Method is case sensitive.
        if ('GET'.equals(method) || 'POST'.equals(method)) {
            req.setMethod(method);
        }
        else {
            req.setMethod('GET');
        }
        
        if (compressed != null) {
            req.setCompressed(compressed);
        }
        else {
            req.setCompressed(false);
        }
        
        // HttpRequest allows a timeout value between 1ms and 60000ms.
        if (timeout != null && timeout > 0 && timeout <= 60000) {
            req.setTimeout(timeout);
        }
        else {
            // Set default 10s timeout
            req.setTimeout(10000);
        }
        
        return req;
    }
    
    /**
     * Sets up two-way ssl for an HTTP request
     */
     public static HttpRequest setSSL(HttpRequest req, ApexPages.Message[] messages) {
        String SECRET = LicenseAuthorityAPI.getPkcsSecret(messages);
        req.setHeader('partnerId', UserInfo.getOrganizationId()+'_PROSERV');
        String certName = 'ddcprofsvcsp12';
        StaticResource certResource;
        try {
            certResource = [SELECT Name,NamespacePrefix,Body FROM StaticResource WHERE Name = :certName LIMIT 1];
        }
        catch(QueryException q) {
            System.debug('Query Exception occurred when trying to get the SSL cert.   ' +q);
            messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDCTransientError, ''));
        }
        if(certResource != null) {
            Blob keystore = certResource.Body; 
            req.setClientCertificate((String)EncodingUtil.base64Encode(keystore),SECRET); // Set certificate x509 for request
        }
        // Doesn't seem to work from asynch triggered runtimes
        // new PageReference('/resource/ddcDet__ddcprofsvcsp12').getContent(); // Fetches PKCS12
        return req;
     }
    
    /**
     * This sends the given request. If an error occurs, an ApexPage.Message will be added to the
     * messages list and null will be returned.
     */
    public static HttpResponse sendRequest(HttpRequest req, ApexPages.Message[] messages) {
        if (req == null) {
            return null;
        }
        
        HTTPResponse res;
        try {
            if (Test.isRunningTest() || Limits.getCallouts() == Limits.getLimitCallouts()) {
                System.debug('Current callout consumption :: ' +Limits.getCallouts());
                res = new HTTPResponse();
            }
            else {
                Http http = new Http();
                res = http.send(req);
            }
        }
        catch (Exception e) {
            if (messages != null) {
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.DDCConnectivityError, ''));
            }
            res = null;
        }
        return res;
    }
    
    /**
     * Fetches a concrete SObjectType from the Schema safely. Can be any type (Standard object, Custom Object, Custom Setting)
     * The input must include a namespace if the client application is within a namespaced runtime.
     */
    public static Schema.SObjectType getConcreteObject(String objName) {
        
        Schema.SObjectType result;
        objName = objName.toLowerCase();
        try {
            result = GLOBAL_DESCRIBE_CACHE.get(objName);
        }
        catch(Exception e) {
            System.debug('Exception in getting type. Invalid object name in the input: ' + objName +' Backing out and return null...');
        }
        return result;
    }
    
    /**
     * Gathers a listing of all of the fields from the Schema safely.
     * The input must include a namespace if the client application is within a namespaced runtime.
     */
    public static Map<String, Schema.SObjectField> getFieldListing(String objName) {
        
        Map<String, Schema.SObjectField> result;

        Schema.SObjectType stype = Utils.getConcreteObject(objName);
        if(stype != null) {
            try {
                if(SOBJECT_DESCRIBE_CACHE.containsKey(objName.toLowerCase())) {
                    result = new Map<String, Schema.SObjectField>(SOBJECT_DESCRIBE_CACHE.get(objName.toLowerCase()).fields.getMap());
                }
                else {
                    result = new Map<String, Schema.SObjectField>(stype.getDescribe().fields.getMap());
                }
            }
            catch(Exception e) {
                System.debug('Exception in getting the fields for the object specified: ' + objName + ' requested at runtime.');
            }
        }

        /**
         *  04062014 Removing geolocation fields from resulting field listing
         */
         for (String fName : result.keySet()) {
            if (fName.contains('__c') &&
                (result.get(fName.replace('__c', '__latitude__s')) != null 
                || 
                result.get(fName.replace('__c', '__longitude__s')) != null))
            {
                System.debug('Removed a geolocation field :: ' +fName);
                result.remove(fName);
            }
         }
        
        return result;
    }
    
    /**
     * Retrieves a listing of all of the available settings from a LIST type Custom Setting dynamically at runtime
     */
    public static SObject[] getSettingsListing(String objName) {
        
        SObject[] result;
        
        Schema.SObjectType stype = Utils.getConcreteObject(objName);
        if(stype != null) {
            SObject customSetting = stype.newSObject();
            try {
                System.assertNotEquals(null, customSetting);
                result = Database.query('SELECT Id,Name FROM ' +objName);
            }
            catch(Exception e) {
                System.debug('Exception when retrieving a listing of all custom settings for setting object: ' + objName + ' ... method returns null!');
            }
        }
        
        return result;
    }
    
    /**
     * Retrieves the describe sobject result from cache if exists or does a describe and adds it to the cache for future reference during this runtime
     */
    public static Schema.DescribeSObjectResult getSObjectDescribe(String sobjName) {
        if(sobjName == null) {
            throw new ddclib1.NullPointerException('Unexpected null argument in Utils.getSObjectDescribe. Unable to continue.');
        }
        try {
            if(SOBJECT_DESCRIBE_CACHE.containsKey(sobjName.toLowerCase())) {
                return SOBJECT_DESCRIBE_CACHE.get(sobjName.toLowerCase());
            }
            else {
                Schema.DescribeSObjectResult sDescribe = GLOBAL_DESCRIBE_CACHE.get(sobjName.toLowerCase()).getDescribe();
                SOBJECT_DESCRIBE_CACHE.put(sobjName.toLowerCase() , sDescribe);
                return sDescribe;
            }
        }
        catch(Exception e) {
            // The object probably wasn't valid or wasn't available to describe.
            throw new ddclib1.IllegalStateException('An unexpected error occurred when trying to get sobject describe from cache: ' +e +e.getStackTraceString());
        }
        // Should NEVER get to this, but have to return null if something goes totally pear-shaped.
        return null;
    }
    
    public static Schema.DescribeFieldResult getFieldDescribe(String sobjName, String fieldName) {
        if(sobjName == null || fieldName == null) {
            throw new ddclib1.NullPointerException('Unexpected null arguments in Utils.getSObjectDescribe. Unable to continue.  ' + sobjName + '  ' + fieldName);
        }
        Schema.DescribeSObjectResult dsr;
        Schema.DescribeFieldResult dfr;
        try {
            if(FIELD_DESCRIBE_CACHE.containsKey(fieldName.toLowerCase())) {
                dfr = FIELD_DESCRIBE_CACHE.get(fieldName.toLowerCase());
            }
            else if(SOBJECT_DESCRIBE_CACHE.containsKey(sobjName.toLowerCase())) {
                dsr = SOBJECT_DESCRIBE_CACHE.get(sobjName.toLowerCase());
System.debug('Utils.getFieldDescribe DESCRIBE SOBJECT RESULT ::  ' +dsr);
System.debug('Utils.getFieldDescribe DESCRIBE FIELD RESULT  ' +dfr);
                dfr = dsr.fields.getMap().get(fieldName.toLowerCase()).getDescribe();
                FIELD_DESCRIBE_CACHE.put(fieldName.toLowerCase() , dfr);
            }
            else {
                dsr = GLOBAL_DESCRIBE_CACHE.get(sobjName.toLowerCase()).getDescribe();
                SOBJECT_DESCRIBE_CACHE.put(sobjName.toLowerCase() , dsr);
                dfr = dsr.fields.getMap().get(fieldName.toLowerCase()).getDescribe();
                FIELD_DESCRIBE_CACHE.put(fieldName.toLowerCase() , dfr);
            }
        }
        catch(Exception e) {
            // The object probably wasn't valid or wasn't available to describe.
            throw new ddclib1.IllegalStateException('An unexpected error occurred when trying to get field describe from cache: ' +e +e.getStackTraceString());
        }
        // Should NEVER get to this, but have to return null if something goes totally pear-shaped.
        return dfr;
    }

    /**
     * This checks if the given profileId has the given perm.
     */
    public static Boolean getProfilePerms(String perm, String profileId) {
        String queryString = 'SELECT p.Id, p.Name, p.' + perm + ' FROM Profile p WHERE p.Id = \'' + profileId + '\' LIMIT 1';
        List<Profile> resultset = null;
        
        try {
            resultset = Database.query(queryString);
        }
        catch (QueryException q) {
            System.debug('------------------> A query exception ocurred: ' + q);
        }
        
        if (resultset == null || resultset.size() == 0) {
            return false;
        }
        
        Profile p = resultset.get(0);
        Boolean permVal = (Boolean) p.get(perm);
        System.debug('The profile perm result is: ' + permVal);
        return permVal;
    }

    /**
     *  Retrieves the correct DDType Enum mapping for an Sobject
     */
     public static DDCType getTypeBinding(Schema.SObjectType stype) {

        if (stype == Lead.SobjectType) {
                
            return DDCType.CONTACT;
        }
        else if(stype == Contact.SobjectType) {
            
            return DDCType.CONTACT;
        }
        else if(stype == Account.SobjectType) {

            return DDCType.COMPANY;
        }

        // Assume company if not known
        else return DDCType.COMPANY;
     }
    
    /**
     * Sorts a list of sObjects, sorted by the value of the specified field
     * Throws: SortingException
     */
    public static void sortByField(List<SObject> objs, String fieldName) {
        if (objs.size() < 2) return;
        Schema.SObjectType stype = objs.getSObjectType();
        Schema.DescribeSObjectResult sobjDesc;
        Schema.SObjectField field;
        Schema.DescribeFieldResult fieldDesc;
        if (stype != null) {
            try {
                // Pull from field describe cache first if possible to save resources.
                if (FIELD_DESCRIBE_CACHE.containsKey(fieldName.toLowerCase())) {
                    System.debug('Retrieving from field describe cache. :: ' +fieldName);
                    fieldDesc = FIELD_DESCRIBE_CACHE.get(fieldName);
                }
                else {
                    // Otherwise start by trying to get sobject describe from cache at a minimum
                    if (SOBJECT_DESCRIBE_CACHE.containsKey(String.valueOf(stype).toLowerCase())) {
                        System.debug('Retrieving sobject describe from cache. :: ' +String.valueOf(stype).toLowerCase());
                        field = SOBJECT_DESCRIBE_CACHE.get(String.valueOf(stype).toLowerCase()).fields.getMap().get(fieldName);
                    }
                    else {
                        // If in here, starting from scratch.
                        System.debug('Building new sobject describe. :: ' +stype);
                        sobjDesc = stype.getDescribe();
                        SOBJECT_DESCRIBE_CACHE.put(String.valueOf(stype).toLowerCase(), sobjDesc);
                        field = sobjDesc.fields.getMap().get(fieldName);
                    }
                    if (field == null) throw new ddcLib1.FatalException('No such field ' + fieldName);
                    fieldDesc = field.getDescribe();
                    FIELD_DESCRIBE_CACHE.put(fieldDesc.getName().toLowerCase(), fieldDesc);
                }
            }
            // Catch any type of exception during describes
            catch (Exception e) {
                System.debug('Unable to sort fields :: ' +e);
            }
        } // Close stype condition
        if (!fieldDesc.isSortable()) throw new ddcLib1.UnsupportedObjectTypeException('Type not sortable: ' + fieldDesc.getType());
        quicksortByField(objs, 0, objs.size()-1, field, fieldDesc.gettype());
    }
    
    /* Implements quicksort on the list of sObjects given */
    private static void quicksortByField(List<sObject> a, Integer lo0, Integer hi0, Schema.SObjectField field, Schema.DisplayType type) {
        Integer lo = lo0;
        Integer hi = hi0;
        if (lo >= hi) {
            return;
        } else if (lo == hi - 1) {
            if (compareFields(a[lo], a[hi], field, type) > 0) {
                SObject o = a[lo];
                a[lo]     = a[hi];
                a[hi]     = o;
            }
            return;
        }
        SObject pivot = a[(lo + hi) / 2];
        a[(lo + hi) / 2] = a[hi];
        a[hi] = pivot;
        while (lo < hi) {
            while (compareFields(a[lo], pivot, field, type) < 1 && lo < hi) { lo++; }
            while (compareFields(pivot, a[hi], field, type) < 1 && lo < hi) { hi--; }
            if (lo < hi) {
                sObject o = a[lo];
                a[lo]     = a[hi];
                a[hi]     = o;
            }
        }
        a[hi0] = a[hi];
        a[hi]  = pivot;
        quicksortByField(a, lo0, lo-1, field, type);
        quicksortByField(a, hi+1, hi0, field, type);
    }
    
    /* Determines the type of primitive the field represents, then returns the appropriate comparison */
    private static Integer compareFields(sObject a, sObject b, Schema.sObjectField field, Schema.DisplayType type) {
        if (type == Schema.DisplayType.Email ||
                type == Schema.DisplayType.Id ||
                type == Schema.DisplayType.Phone ||
                type == Schema.DisplayType.Picklist ||
                type == Schema.DisplayType.Reference ||
                type == Schema.DisplayType.String ||
                type == Schema.DisplayType.URL) {
                // compareTo method does the same thing as the compare methods below for Numbers and Time
                    // compareTo method on Strings is case-sensitive. Use following line for case-sensitivity
                    // return String.valueOf(a.get(field)).compareTo(String.valueOf(b.get(field)));
                    return String.valueOf(a.get(field)).toLowerCase().compareTo(String.valueOf(b.get(field)).toLowerCase());
        } else if (type == Schema.DisplayType.Currency ||
                type == Schema.DisplayType.Double ||
                type == Schema.DisplayType.Integer ||
                type == Schema.DisplayType.Percent) {
            return compareNumbers(Double.valueOf(a.get(field)), Double.valueOf(b.get(field)));
        } else if (type == Schema.DisplayType.Date ||
                type == Schema.DisplayType.DateTime ||
                type == Schema.DisplayType.Time) {
            return compareTime(Datetime.valueOf(a.get(field)), Datetime.valueOf(b.get(field)));
        } else {
            throw new ddcLib1.FatalException('Type not sortable: ' + type);
        }
    }
    
    private static Integer compareNumbers(Double a, Double b) {
        if  (a < b) { return -1; }
        else if (a > b) { return  1; }
        else        { return  0; }
    }
    
    private static Integer compareTime(Datetime a, Datetime b) {
        if  (a < b) { return -1; }
        else if (a > b) { return  1; }
        else        { return  0; }
    }

}