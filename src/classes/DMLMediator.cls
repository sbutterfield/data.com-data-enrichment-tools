public class DMLMediator {
	
    private DMLMediator() {
		//default constructor
	}

    public static Boolean updateWrappers(Wrapper[] wrappers) {

        Boolean result = false;

        if (wrappers == null) {
            return result;
        }

        SObject[] toUpdate = new List<SObject>();

        for (Wrapper w : wrappers) {

            if (w.getSObject() != null) {

                toUpdate.add(w.getSObject());
            }
        }
        
        try {

            Database.update(toUpdate, true);
        }
        catch (DMLException de) {
            result = false;
            System.debug('Save failed on wrappers :: ' +wrappers+ '  stacktrace = ' +de.getMessage()+de.getStacktraceString());
        }
        result = true;
        return result;
    }
}