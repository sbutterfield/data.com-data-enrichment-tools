/**
 *  This is the decorator definition for all subsequent Wrappers that might extend an Sobject
 *  All Wrapper extensions will have available to them the abstract type definition of ApexType.Wrapper
 */
 
global abstract class WrapperDecorator implements Wrapper {

    protected Wrapper concreteWrapper;
    
    global WrapperDecorator(Wrapper concreteWrapper) {
        this.concreteWrapper = concreteWrapper;
    }
    
    global virtual SObject getSObject() {
        return concreteWrapper.getSObject();
    }
    
    global virtual DDCType getType() {
        return concreteWrapper.getType();
    }

    global virtual MatchScenario getScenario() {
        return concreteWrapper.getScenario();
    }

    global virtual DDCObject getDdcMatch() {
        return concreteWrapper.getDdcMatch();
    }

    global virtual void setDdcMatch(DDCObject value) {
        concreteWrapper.setDdcMatch(value);
    }
}