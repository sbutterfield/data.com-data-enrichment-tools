public with sharing class GetTokenURIController {
    
    private static final String PROGRAMID = 'traction_qe_01112013';
    private static final String TOKEN_API_ENDPOINT = 'https://partnerapp.qe.data.com/rest/getToken';
    
    public String getEndpoint() {
		String server = EncodingUtil.urlEncode(System.URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/23.0/' + UserInfo.getOrganizationId(), 'UTF-8');
        String session = EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
        String token = '';
        String partner = EncodingUtil.urlEncode(PROGRAMID, 'UTF-8');
        List<String> parts = new List<String> { TOKEN_API_ENDPOINT, server, session, token, partner };
        return String.format('{0}?server={1}&session={2}&token={3}&programid={4}', parts);
    }
    
}