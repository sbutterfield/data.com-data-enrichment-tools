public class MatchedContact extends MatchedObject {

	ContactWrapper cw;
	
	public ContactWrapper getContactWrapper() {
		return this.cw;
	}
	
	private void setContactWrapper(ContactWrapper cw) {
		this.cw = cw;
	}
	
	private MatchedContact() {
		// default constructor
	}
	
	public MatchedContact(Wrapper w) {
		super();
		if(w instanceof ContactWrapper) {
			setContactWrapper((ContactWrapper)w);
		}
		else throw new ddcLib1.IllegalArgumentException('Unable to attempt a match without the correct wrapper. Expected ContactWrapper, got: ' +w);
	}
	
	public override SObject[] match() {
		System.debug('Calling match within MatchedContact with abstract final implementation.');
		return new SObject[0];
	}
	
	public void match(Wrapper w) {
		if(w instanceof ContactWrapper) {
			setContactWrapper((ContactWrapper)w);
		}
		else throw new ddcLib1.IllegalArgumentException('Unable to attempt a match without the correct wrapper. Expected ContactWrapper, got: ' +w);
		
		System.debug('Calling match within MatchedContact with abstract final implementation.');
	}
	
	public override Descriptor getDescribe() {
		
		MatchedContactDescriptor thisDescriptor; 
		
		if(thisDescriptor == null) {
			thisDescriptor = new MatchedContactDescriptor();
		}
		
		return thisDescriptor.getDescribe();
	}
	
	/**
	 *	INNER CLASSES
	 *	Descritpor for MatchedContact. Instantiated when requested, otherwise save the statements.
	 */
	private class MatchedContactDescriptor extends Descriptor {
	    
	    //---------------------------------------------------------------------------------------------
	    // Properties
	    //---------------------------------------------------------------------------------------------
	    public String matchCode;
	    
	    //---------------------------------------------------------------------------------------------
	    // Constructors
	    //---------------------------------------------------------------------------------------------
	    MatchedContactDescriptor() {
			super();
	    	getDescribe();
	    }
	    
	    //---------------------------------------------------------------------------------------------
	    // Methods
	    //---------------------------------------------------------------------------------------------
		public override Descriptor getDescribe() {
			matchCode = 'test match code result';
			return this;
		}
		
		public override String getToken() {
			return null;
		}
		
		public override String getType() {
			return null;
		}
	}
	
}