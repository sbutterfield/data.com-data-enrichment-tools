public with sharing class CleanLeadController {
	
    //---------------------------------------------------------------------------------------------
    // PROPERTIES
    //---------------------------------------------------------------------------------------------

    private final Lead l;
    private Lead ddcLead;
    private Wrapper lw;

    private Map<String, Schema.sObjectField> sObjectFieldMap;
    private transient ApexPages.Message[] messages;
    private Boolean notFound;
    private Boolean inSync;
    private Boolean hiddenFields;
    private Boolean hasError;
    private Map<String, Boolean> diffs;
    private Map<String, Boolean> checkBoxes;
    private Boolean isCompany; //DRAdd
    
    public String CRMAddressLine1 { get; set; }
    public String CRMAddressLine2 { get; set; }
    public String CRMAddressLine3 { get; set; }
    
    public String DDCAddressLine1 { get; set; }
    public String DDCAddressLine2 { get; set; }
    public String DDCAddressLine3 { get; set; }
    
    public Boolean refreshParent { get; set; }
    
    public String ddcid { get; set; }
    public String selectType { get; set; }

    public CleanLeadController() {
        this.sObjectFieldMap = Schema.SObjectType.Lead.Fields.getMap();
        this.messages = new List<ApexPages.Message>();
        this.notFound = false;
        this.inSync = false;
        this.hiddenFields = false;
        this.hasError = false;
        this.diffs = new Map<String, Boolean>();
        this.checkBoxes = new Map<String, Boolean>();

        // Fetch CRM Record
        Map<String, Schema.SObjectField> fieldmap = Utils.getFieldListing('Lead');
        
        String fieldListString = '';
        
        for (String fieldName : fieldmap.keySet()) {
            fieldListString += fieldName + ',';
        }
        
        fieldListString = fieldListString.removeEnd(',');

        String id = ApexPages.currentPage().getParameters().get('id');
        String queryString =
            'SELECT ' + fieldListString + ' FROM Lead ' +
            'WHERE Id=\'' + ((id == null) ? '' : String.escapeSingleQuotes(id)) + '\' ' +
            'LIMIT 1';
        System.debug('query string = ' +queryString);
        try {
            this.l = (Lead) Database.query(queryString);
        }
        catch (Exception e) {
            System.debug('QUERY EXCEPTION: ' +e);
            this.l = new Lead();
        }

        // Construct the wrapper for this Lead
        lw = new LeadWrapperDecorator(new SObjectWrapper(l, DDCType.CONTACT));
        MatchScenario lwScenario = lw.getScenario();
        // Grab the Data.com Ids
        String contactId_field = lwScenario.thisMapping.get(ApiUtils.TAG_NAME_CONTACT_ID).mappedFieldName;
        String companyId_field = lwScenario.thisMapping.get(ApiUtils.TAG_NAME_COMPANY_ID).mappedFieldName;
        String contactId = contactId_field == null ? null : (String) lw.getSObject().get(contactId_field);
        String companyId = companyId_field == null ? null : (String) lw.getSObject().get(companyId_field);

        // Determine the selected type of data: 
        selectType = ApexPages.currentPage().getParameters().get('type') == null ? '' : ApexPages.currentPage().getParameters().get('type');
	}
}