public class FuzzyMatcher {

    public FuzzyMatcher() {
		// default constructor
	}

    public static void getDomainMatches(Wrapper[] inputs) {
        if (inputs == null) {
            return;
        }

        ApexPages.Message[] messages = new List<ApexPages.Message>();

        for (Wrapper w : inputs) {

            // If the wrapper contains sobject of Contact type, we should skip this algorithm.
            if (w.getSObject().getSObjectType() == Contact.SObjectType) {
                continue;
            }

            Boolean enabled = false;

            enabled = w.getScenario().thisSetting.fuzzyMatchEnabled;
            enabled = enabled ? w.getScenario().thisSetting.domainMatchEnabled : enabled;

            if (!enabled) {
                // If fuzzy match is not enabled
                // or if domain matching is not enabled, skip the wrapper
                continue;
            }

            if (w.getDdcMatch() != null) {

                if (w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_COMPANY_ID) != null) {

                    // Already has a company match, skip it.
                    continue;
                }
            }

            DDCCompany[] candidates = DDCApi.findSimilarCompanies(w, messages);

            if (candidates == null) {
                // No result.
                continue;
            }

            DDCObject bestMatch;
            for (DDCObject candidate : candidates) {

                if (w.getScenario().thisMapping == null || w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_COMPANY_NAME) == null) {
                    continue;
                }

                if (candidate.objMap == null || candidate.objMap.get(ApiUtils.TAG_NAME_COMPANY_NAME) == null) {
                    continue;
                }

                // Calculate company name's similarity
                String candidate_companyName = (String) candidate.objMap.get(ApiUtils.TAG_NAME_COMPANY_NAME);
                String input_companyName = (String) w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_COMPANY_NAME).mappedFieldName);
                Double cname_sim = Double.valueOf(ddcLib2.StringComparator.compare(candidate_companyName, input_companyName, 0));

                String candidate_domain = String.isBlank((String) candidate.objMap.get(ApiUtils.TAG_NAME_WEBSITE)) ? null : URIUtils.parseDomain((String) candidate.objMap.get(ApiUtils.TAG_NAME_WEBSITE));
System.debug('candidate.objMap.get(ApiUtils.TAG_NAME_WEBSITE) :: ' +candidate.objMap.get(ApiUtils.TAG_NAME_WEBSITE));
System.debug('candidate_domain :: ' +candidate_domain);
                String input_domain = 
                    String.valueOf(w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_WEBSITE).mappedFieldName)) == null ?
                    URIUtils.parseDomain((String) w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EMAIL).mappedFieldName))
                    :
                    URIUtils.parseDomain((String) w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_WEBSITE).mappedFieldName));

                // Calculate domain similarity, start with website domain first.
                Double domain_sim = Double.valueOf(ddcLib2.StringComparator.compare(candidate_domain, input_domain, 50));

                // Set up the weight target (scale to 100)
                Double weight = Math.round(Double.valueOf(w.getScenario().matchConfidenceThreshold *10));

                System.debug('(1) within getDomainMatches(), scoring follows:');
                System.debug('weight :: ' +weight);
                System.debug('cname_sim :: ' +cname_sim);
                System.debug('domain_sim :: ' +domain_sim);

                if (cname_sim >= (Math.round(weight/1.12786)) && domain_sim >= weight) {

                    bestMatch = candidate;
                    bestMatch.score = (cname_sim + domain_sim) / 2; // Avg the values for a score.
                    // break the loop when we find one that is good enough.
                    // since candidates are sorted by active no. of contacts, this will always be the one we want.
                    break;
                }

                // Failover to allow match if domain is perfect and company name is non-null
                if ((input_domain == null || cname_sim >= 20.0) && domain_sim == 100.0) {

                    bestMatch = candidate;
                    bestMatch.score = (cname_sim + domain_sim) / 2; // Avg the values for a score.
                    break;
                }
            }

            if (bestMatch == null) {
                // No match. Skip this wrapper.
                continue;
            }

            DDCObject existingMatch = w.getDdcMatch();
            if (existingMatch == null) {
                // Set to a new match
                System.debug('New domain match! :: ' +bestMatch);
                w.setDdcMatch(bestMatch);
                continue;
            }
            // Merge new match with existing.
            existingMatch.objMap.putAll(bestMatch.objMap);
            w.setDdcMatch(existingMatch);
        }
    }

    public static void getGeneralizedMatches(Wrapper[] inputs) {
        if (inputs == null) {
            return;
        }

        for (Wrapper w : inputs) {

            Boolean enabled = false;
            enabled = w.getScenario().thisSetting.fuzzyMatchEnabled;
            enabled = enabled ? w.getScenario().thisSetting.emailGeneralizationEnabled : enabled;
            
            if (!enabled) {
                // If fuzzy match is not enabled
                // Or if Email generalization not enabled, skip the wrapper
                continue;
            }

            if (w.getType() != DDCType.CONTACT) {
                // Not valid for generalization attempt
                continue;
            }

            if (w.getDdcMatch() != null) {

                if (w.getDdcMatch().objMap.get(ApiUtils.TAG_NAME_CONTACT_ID) != null) {
                    System.debug('This wrapper :: ' +w+ '\n  Already has a match!');
                    // Has a match already. Pass over, there's already a match
                    continue;
                }
            }

            List<DDCContact> qualifiedMatches = new List<DDCContact>();
            DDCContact[] similarContacts = DDCApi.findSimilarContacts(w, new ApexPages.Message[] { });

            if (similarContacts == null) {
                // No matches, bypass this wrapper without modifying the contents of the wrapper.
                continue;
            }

            for (DDCContact c : similarContacts) {

                // If there were results, we are now using generalized email!

                // Get several fields from the wrapper
                String mappedField;

                Object temp;
                try {
                    temp = w.getSObject().get('AccountId');
                }
                catch (Exception e) {
                    // not a Contact
                }

                Wrapper relAcctWrapper;
                
                if (temp != null) {
                    
                    Id relAcctId = Id.valueOf((String) temp);
                    SObject relAcct = [SELECT Id,Name,Website FROM Account WHERE Id = :relAcctId];
                    relAcctWrapper = new AccountWrapperDecorator(new SObjectWrapper(relAcct, DDCType.COMPANY));
                }

                String wCompany;
                String wWebsite;

                if (relAcctWrapper != null) {

                    wCompany = (String) relAcctWrapper.getSObject().get('Name');
                    wWebsite = (String) relAcctWrapper.getSObject().get('Website');
                }
                else {

                    mappedField = w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_COMPANY_NAME) != null ? w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_COMPANY_NAME).mappedFieldName : null;
                    wCompany = mappedField != null ? (String) w.getSObject().get(mappedField) : null;
                    mappedField = w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_WEBSITE) != null ? w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_WEBSITE).mappedFieldName : null;
                    wWebsite = mappedField != null ? (String) w.getSObject().get(mappedField) : null;
                }


                mappedField = w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EMAIL).mappedFieldName;
                String wEmail = mappedField != null ? (String) w.getSObject().get(mappedField) : null;
                
                mappedField = w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_FIRST_NAME).mappedFieldName;
                String wFirstname = mappedField != null ? (String) w.getSObject().get(mappedField) : null;
                
                mappedField = w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_LAST_NAME).mappedFieldName;
                String wLastname = mappedField != null ? (String) w.getSObject().get(mappedField) : null;

                mappedField = w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_TITLE).mappedFieldName;
                String wTitle = mappedField != null ? (String) w.getSObject().get(mappedField) : null;

                mappedField = w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_PHONE).mappedFieldName;
                String wPhone = mappedField != null ? (String) w.getSObject().get(mappedField) : null;

                Double ldFN;
                Double ldLN;
                Double simFN;
                Double simLN;
                Double simTitle;
                Double simPhone;
                Double simEmail;

                ldFN = wFirstname.getLevenshteinDistance(c.firstname);  // Returns levenshtein diff # of chars
                ldLN = wLastname.getLevenshteinDistance(c.lastname); // Returns levenshtein diff # of chars
                simFN = ddcLib2.StringComparator.compare(wFirstname, c.firstname, 0); // Returns 0-100 similarity score
                simLN = ddcLib2.StringComparator.compare(wLastname, c.lastname, 0); // Returns 0-100 similarity score

                simTitle = ddcLib2.StringComparator.compare(wTitle, c.title, 38); // Returns 0-100 similarity score

                simPhone = Utils.phoneEquals(wPhone, c.phone) ? 90 : 0; // Returns 0/1 either equals or not

                //simEmail = c.email == wEmail ? 90 : 0;
                simEmail = ddcLib2.StringComparator.compare(wEmail, c.email, 56);
                simEmail += c.email == wEmail ? 0 : -20;

                System.debug('ldFN :: ' +ldFN);
                System.debug('ldLN :: ' +ldLN);
                System.debug('simFN :: ' +simFN);
                System.debug('simLN :: ' +simLN);
                System.debug('simTitle :: ' +simTitle);
                System.debug('simPhone :: ' +simPhone);
                System.debug( 'Email Addresses: ' +  wEmail + ' <> ' + c.email );
                System.debug('simEmail :: ' +simEmail);

                /**
                    TO-DO :: Do scoring properly in a new architecture for fuzzy matching
                    TO-DO :: Split email at "@" and do comparativity on both sides. Re-weight based on which one has highest
                    average score on both the prefix and the domain side of an email address.
                    TO-DO :: Work out a way to include and vary weighting based on areacode + postal
                 */

                // Set up the weight target (scale to 100)
                Double weight = Math.round(Double.valueOf(w.getScenario().matchConfidenceThreshold *10));
                // Set up levenshtein distance calculation model
                Double ld_k = 26.0; // num symbols in the system
                Double ld_pi = 0.055; // Probability of insertion
                Double ld_pd = 0.021; // Probability of deletion
                Double ld_ps = 0.089; // Probability of substitution
                Double ld_penalty = Math.round(Math.log( (ld_pi + ld_pd + ld_ps) / ld_k));
                Double ld_name_avg =  Math.round(ldFN + ldLN / 2); // Average transformations to match fn + ln

                Double fn_max = wFirstname.length() > c.firstname.length() ? wFirstname.length() : c.firstname.length();
                Double ln_max = wLastname.length() > c.lastname.length() ? wLastname.length() : c.lastname.length();
                Double nm_max = fn_max + ln_max;
                Double nm_max_avg = (fn_max + ln_max) / 2;

                Double ld_diff = Math.round((1 - (ld_name_avg/nm_max_avg)) *100); // Calc the difference score
                Double ld_diff_pen = ((1 - ld_name_avg/nm_max_avg) / ld_penalty) * 100; // Calc a penalty score
                ld_diff = ld_diff + ld_penalty; // Add the negative penalty
                
                System.debug('ld_diff calculated ::  ' +ld_diff);
                System.debug('weighting ::  ' +weight);
                
                /**
                 *  FN_LN_TITLE_IDX Index
                 */
                Double FN_LN_TITLE_IDX = Math.round((ld_diff + simTitle + simFN + simLN) / 4);

                System.debug('FN_LN_TITLE_IDX ::  ' +FN_LN_TITLE_IDX);
                
                if (FN_LN_TITLE_IDX >= weight) {
                
                System.debug('Setting match for FN_LN_TITLE_IDX');
                    // If we get a perfect match on email, boost the score by a couple of points
                    // This should help ensure that the ddc match assigned to the wrapper is the one with
                    // a perfect email match score.
                    //FN_LN_TITLE_IDX = simEmail == 100 ? FN_LN_TITLE_IDX + 2.2 : FN_LN_TITLE_IDX;
                    c.score = FN_LN_TITLE_IDX + ( simEmail == 100.0 ? 2.2 : 0.0  );
                    System.debug( '1st branch c.Score: ' + c.score );
                    qualifiedMatches.add(c);
                }
                else if (FN_LN_TITLE_IDX >= (weight/1.0389) && simEmail >= 79) { // Drop the weight a little, only if we got sim email
                    c.score = (FN_LN_TITLE_IDX + (weight/1.0389) + simEmail) / 3;
                    System.debug('2nd branch c.score :: ' +c.score);
                    qualifiedMatches.add(c);
                }
                else if (simEmail >= 56 || simPhone > 0) {
                    
                    if (simEmail == 100.0) {
                        c.score = (simEmail - (weight - FN_LN_TITLE_IDX));
                    }
                    else {
                       c.score = ((simEmail + simPhone) / 2) - (weight - FN_LN_TITLE_IDX);
                    }
                    System.debug('3rd branch c.score :: ' +c.score);
                    qualifiedMatches.add(c);
                }
                System.debug( 'Score: ' + c.score );
            }
            
            // If any matches qualified, call set match to make a decision
            if (qualifiedMatches != null) {
                setMatch(w, qualifiedMatches);
            }
        }

        /*
        // For all matches that were found, get details for those matches.
        // This is necessary so we have a DDCContact with all the company attributes as well.
        Map<String, DDCObject> matches = new Map<String, DDCObject>();
        for (Wrapper w : inputs) {

            matches.put((String) w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EXTERNAL_KEY).mappedFieldName), w.getDdcMatch());
        }

        // Get all the details for records.
        matches = DDCApi.getDetails(matches.values());

        for (Wrapper w : inputs) {
            // Re-set the match with a fully aware DDCObject result from the API.
            w.setDdcMatch(matches.get((String) w.getSObject().get(w.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EXTERNAL_KEY).mappedFieldName)));
        }
        */
    }

    private static void setMatch(Wrapper input, DDCObject[] matches) {
        
        // Set up the weight target (scale to 100)
        Double weight = Math.round(Double.valueOf(input.getScenario().matchConfidenceThreshold *10));

        System.debug( 'Matches: ' + matches );
        if (matches.size() == 1) {
            // Only one qualified match, set it.
            matches[0].externalKey = (String) input.getSObject().get(input.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EXTERNAL_KEY).mappedFieldName);
            input.setDdcMatch(matches[0]);
        }

        DDCObject winner = null;
        Double high_score = 0;
        
        for (Integer i = 0; i < matches.size(); i++) {
            
            Boolean isDead = Boolean.valueOf(matches[i].objMap.get(ApiUtils.TAG_NAME_GRAVEYARD_STATUS));
            System.debug('is match candidate dead? :: ' +isDead);

            if ((matches[i].score > high_score)) {
                winner = matches[i];
                high_score = matches[i].score;
                System.debug('new high_score and winner ====== ' +high_score + '  ::  ' +winner);
            }
        }
        System.debug('winning match :: ' +winner);
        if (high_score >= weight) {
            
            winner.externalKey = (String) input.getSObject().get(input.getScenario().thisMapping.get(ApiUtils.TAG_NAME_EXTERNAL_KEY).mappedFieldName);
            
            if (winner instanceof DDCContact) {
                DDCContact contactWinner = (DDCContact) winner;
                winner = contactWinner.getContactCompany();
            }
            
            input.setDdcMatch(winner);
        }
    }
    
    private static void scoreMatches(Wrapper w, DDCObject[] objects) {

    }
}