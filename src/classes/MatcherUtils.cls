public class MatcherUtils {

    public static final String MESSAGE_CONNECTION_ERROR = 'Data.com can\'t make a connection to retrieve your data. Try again later. If the problem continues, contact Data.com support.';
    public static final String MESSAGE_TRANSIENT_ERROR = 'Data.com can\'t access the data you requested. Try again later. If the problem continues, contact Data.com support.';
    public static final String MESSAGE_GENERIC_ERROR = MESSAGE_TRANSIENT_ERROR;
    public static final String MESSAGE_UNRECOVERABLE_ERROR = 'Please contact Data.com support.';
    public static final String MESSAGE_TOKEN_FAIL = 'Verify that the Data.com token is set correctly. You might need help from a Salesforce administrator.';
    public static final String MESSAGE_OBJECT_NO_ACCESS = 'You don\'t have the required permission to make changes to this object. Contact your Salesforce administrator if you need to make modifications to this object.';
    public static final String MESSAGE_RECORD_NOT_FOUND = 'The Data.com database doesn\'t have the data you requested.';
    public static final String MESSAGE_SAVE_ERROR = 'Data.com can\'t verify this data because these fields may have been customized by your administrator. You need to contact your Salesforce administrator.';
    
    // This type of PARAM_ERROR is equivalent to a record not being found.
    private static final String PARAM_ERROR_NOT_FOUND  = 'ID doesn\'t exist:';
    
    // A scheme is a letter followed by letters, digits, '+', '.', or '-'. It is followed by a ':'.
    private static final String REGEX_URL_SCHEME = '^([a-zA-Z][a-zA-Z0-9\\+\\.\\-]+):';
    
    // A valid Jigsaw_Id has only digits.
    private static final String REGEX_JIGSAW_ID = '(\\d+)';
    
    private static final String REGEX_PHONE_NUMBER = '\\W*';
    private static final Integer PHONE_NUMBER_FORMAT_LENGTH = 10;
    
    // NOTE: Order matters, we encode '&' first to avoid double encoding.
    // These arrays should have the same number of elements.
    // Using the list of entities recommended by:
    // https://www.owasp.org/index.php/XSS_%28Cross_Site_Scripting%29_Prevention_Cheat_Sheet
    // Additionally, we are escaping '\u' to prevent alternative encodings from breaking out of the
    // data context.
    private static final String[] HTML_DECODED = new String[]{ '&',     '<',    '>',    '"',      '\'',    '/',     '\\u'         };
    private static final String[] HTML_ENCODED = new String[]{ '&amp;', '&lt;', '&gt;', '&quot;', '&#39;', '&#47;', '&#92;&#117;' };

    /*Check equality of two objects*/
    public static Boolean equals(Object a, Object b) {
        if ((a == null && b != null) || (b == null && a != null)) return false;
        if (a == null && b == null) return true;
        if (a instanceof String && b instanceof String){
            return ((String)a).equals((String)b);
        }
        return a == b;
    }
    
    public static Boolean isEmpty(String s) {
    	if (s == null || s.length() == 0) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    /*Crop the String to defned size*/
    public static String cutString(String str, Integer size) {
        if (str != null && str.length() > size) {
            return str.substring(0,size);
        }
        return str;
    }
    
    /*Parse Datatime from string*/
    public static Datetime parseDate(String str) {
        if (str != null && str.length() > 0) {
            String[] tokens = str.split(' ');
            if (tokens.size()>1) {
                String dateS = tokens[0] + ' ' + tokens[1];
                return DateTime.valueOf(dateS);
            }
        }
        return null;
    }
    
    /*Parse Datatime in ISO format from string*/
    public static Datetime parseISODate(String str) {
        if (str != null && str.length() > 0) {
            str = str.replaceAll('T',' ').replaceAll('Z','');
            return DateTime.valueOfGMT(str);
        }
        return null;
    }
    
    /**
     * This checks if the given profileId has the given perm.
     */
    public static boolean getProfilePerms(String perm, String profileId) {
        String queryString = 'SELECT p.Id, p.Name, p.' + perm + ' FROM Profile p WHERE p.Id = \'' + profileId + '\' LIMIT 1';
        List<Profile> resultset = null;
        
        try {
            resultset = Database.query(queryString);
        }
        catch (QueryException q) {
            System.debug('------------------> A query exception ocurred: ' + q);
        }
        
        if (resultset == null || resultset.size() == 0) {
            return false;
        }
        
        Profile p = resultset.get(0);
        boolean permVal = (boolean) p.get(perm);
        System.debug('The profile perm result is: ' + permVal);
        return permVal;
    }

    /**
     * Safely creates a request using the given parameters.
     * Behavior:
     * - If url is null, null will be returned
     * - If body is null, it will be ignored
     * - If method is neither GET nor POST, GET will be assumed
     * - If compressed is null, no compression will be used
     * - If timeout is null, the default will be used 
     */
    public static HttpRequest createRequest(String url, String body, String method, Boolean compressed, Integer timeout) {
        if (url == null) {
            return null;
        }
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        
        if (body != null) {
            req.setBody(body);
        }
        
        // NOTE: Method is case sensitive.
        if ('GET'.equals(method) || 'POST'.equals(method)) {
        	req.setMethod(method);
        }
        else {
            req.setMethod('GET');
        }
        
        if (compressed != null) {
            req.setCompressed(compressed);
        }
        
        // HttpRequest allows a timeout value between 1ms and 60000ms.
        if (timeout != null && timeout > 0 && timeout <= 60000) {
            req.setTimeout(timeout);
        }
        
        return req;
    }
    
    /**
     * This sends the given request. If an error occurs, an ApexPage.Message will be added to the
     * messages list and null will be returned.
     */
    public static HttpResponse sendRequest(HttpRequest req, ApexPages.Message[] messages) {
        return sendRequest(req, messages, false);
    }
    
    /**
     * Pass true in the simulateError parameter to simulate an HTTP error.
     */
    private static HttpResponse sendRequest(HttpRequest req, ApexPages.Message[] messages, boolean simulateError) {
    	if (req == null) {
    		return null;
    	}
    	
        HTTPResponse res;
        try {
            if (Test.isRunningTest()) {
                if (simulateError) {
                    throw new ddcLib1.TestException('SIMULATED_ERROR');
                }
                res = new HTTPResponse();
            }
            else {
                Http http = new Http();
                res = http.send(req);
            }
        }
        catch (Exception e) {
            if (messages != null) {
            	// NOTE: The exception sometimes shows the URL which could expose the token!
            	// It is better not to include it in the message detail.
                messages.add(new ApexPages.Message(ApexPages.Severity.ERROR, MatcherUtils.MESSAGE_CONNECTION_ERROR, ''));
            }
            res = null;
        }
        return res;
    }
    
    /**
     * Given a list of ApexPages.Messages, this determines if at least the min severity level is
     * present. Note that there is a similar native implementation present:
     * ApexPages.hasMessages(ApexPages.Severity) 
     * We could use that but since it only checks for the presence of specific severity types, we
     * would need to make several calls to it to achieve the same effect as this method.
     * ApexPages.Severity Enum: { FATAL, ERROR, WARNING, INFO, CONFIRM }
     */
    public static boolean messageListHasSeverity(List<ApexPages.Message> messages, ApexPages.Severity min) {
        if (messages == null || min == null) {
            return false;
        }
        
        boolean hasMessage = false;
        for (ApexPages.Message message : messages) {
            ApexPages.Severity severity = message.getSeverity();
            if (severity.ordinal() <= min.ordinal()) {
                hasMessage = true;
                break;
            }
        }
        return hasMessage;
    }
    
    /**
     * Utility which normalizes the given website to a common form for equality tests.
     * The general strategy is reduce the given value down to just a host since that's normally all
     * we care about when comparing website values.
     * @return lowercase string containing the website host or the original value if the website is
     * not a valid URL string.
     */
    public static String normalizeWebsite(String website) {
        if (website == null) {
            return website;
        }
        
        URL url;
        try {
            // If the given website does not begin with a scheme, prepend http:// to the beginning.
            // This allows us to use the single-parameter System.URL constructor.
            Pattern p = Pattern.compile(REGEX_URL_SCHEME);
            Matcher m = p.matcher(website);
            String urlString;
            if (m.find()) {
                urlString = website;
            }
            else {
                urlString = 'http://' + website;
            }
            
            url = new URL(urlString);
        }
        catch (Exception e) {
            url = null;
        }
        
        if (url == null) {
            return website;
        }
        
        String host = url.getHost();
        if (host == null || host.length() == 0) {
            return website;
        }
        
        return host.toLowerCase('en_US');
    }
    
    /**
     * This does an HTML entity encoding on the given string. See HTML_DECODED and HTML_ENCODED for
     * the complete list of replacements.
     * NOTE: Do not use this method if you don't have to! It is not a full entity encoder
     * implementation.
     */
    public static String HTMLENCODE(String input) {
        if (input == null || input.length() == 0) {
            return input;
        }
        
        Integer size = HTML_DECODED.size();
        for (Integer i = 0; i < size; i++) {
            input = input.replace(HTML_DECODED[i], HTML_ENCODED[i]);
        }
        
        return input;
    }
    
    /**
     * Utility for test methods which creates an XML string for the given tagName and MatcherField.
     */
    public static String fieldToXml(String tagName, MatcherField field) {
        if (tagName == null || field == null || field.value == null) {
            return '';
        }
        
        String fieldValueString = HTMLENCODE(String.valueOf(field.value));
        Boolean diff = (field.diff == null) ? false : field.diff;
        return String.format('<{0} diff="{2}">{1}</{0}>',
            new List<String> { tagName, fieldValueString, String.valueOf(diff) });
    }
    
    /**
     * Utility for test methods which creates an XML string for the given tagName and Object.
     */
    public static String objectToXml(String tagName, Object value) {
        if (tagName == null || value == null) {
            return '';
        }
        
        return String.format('<{0}>{1}</{0}>', new List<String> { tagName, HTMLENCODE(String.valueOf(value)) });
    }
    
    /**
     * Utility for testing the equality of phone numbers.
     */
    public static Boolean phoneEquals(String phone1, String phone2) {
        String normalizedPhone1 = (phone1 == null) ? null : phone1.replaceAll(REGEX_PHONE_NUMBER, '');
        String normalizedPhone2 = (phone2 == null) ? null : phone2.replaceAll(REGEX_PHONE_NUMBER, '');
        if (equals(normalizedPhone1, normalizedPhone2)) {
            return true;
        }
        
        // For certain locales, Salesforce will format 10-digit phone numbers using the (XXX) XXX-XXXX
        // format. This is triggered when there is no leading '+' character and the country code is
        // '1'. Extension characters are appeneded to the end and are not counted.
        // See the formatPhone function at: https://na1.salesforce.com/static/022509/js/functions.js
        
        Integer length1 = (normalizedPhone1 == null) ? 0 : normalizedPhone1.length();
        Integer length2 = (normalizedPhone2 == null) ? 0 : normalizedPhone2.length();
        Boolean isccna1 = (length1 > PHONE_NUMBER_FORMAT_LENGTH && normalizedPhone1.startsWith('1'));
        Boolean isccna2 = (length2 > PHONE_NUMBER_FORMAT_LENGTH && normalizedPhone2.startsWith('1'));
        if (length2 > length1 && isccna2) {
            return normalizedPhone2.substring(1).equals(normalizedPhone1);
        }
        else if (length1 > length2 && isccna1) {
            return normalizedPhone1.substring(1).equals(normalizedPhone2);
        }
        else {
            return false;
        }
    }

    /* Checks for fls in bulk by passing the field name and attempted action on the appropriate sobject */
    public static boolean getFLS(String fname, String action, Map<String, Schema.SObjectField> sobjFieldMap) {
        // Filter out bad parameters.
        if (sobjFieldMap == null) {
            return false;
        }
        
        Schema.SObjectField field = sobjFieldMap.get(fname);
        if (field == null) {
            return false;
        }
        
        Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
        if (fieldDescribe == null) {
            return false;
        }
        
        boolean flsResponse;
        if ('update'.equals(action)) {
            flsResponse = fieldDescribe.isUpdateable();
        }
        else if ('create'.equals(action)) {
            flsResponse = fieldDescribe.isCreateable();
        }
        else if ('access'.equals(action)) {
            flsResponse = fieldDescribe.isAccessible();
        }
        else if ('writeRequireMasterRead'.equals(action)){
            flsResponse = fieldDescribe.isWriteRequiresMasterRead();
        }
        else {
            flsResponse = false;
        }
        
        System.debug('The describe result from fls is: ' + flsResponse);
        return flsResponse;
    }
    
    //---------------------------------------------------------------------------------------------
    // Tests
    //---------------------------------------------------------------------------------------------
    
    private static testmethod void testCutString() {
        System.assertEquals(null, MatcherUtils.cutString(null, 2));
        System.assertEquals('', MatcherUtils.cutString('', 2));
        System.assertEquals('12', MatcherUtils.cutString('12345', 2));
        System.assertEquals('12345', MatcherUtils.cutString('12345', 20));
    }
    
    private static testmethod void testEquals() {
        System.assertEquals(true, MatcherUtils.equals('12345', '12345'));
        System.assertEquals(false, MatcherUtils.equals('12345', '123452'));
        System.assertEquals(false, MatcherUtils.equals('12345', 12345));
        System.assertEquals(true, MatcherUtils.equals(12345, 12345));
        System.assertEquals(false, MatcherUtils.equals(12345, null));
        System.assertEquals(false, MatcherUtils.equals(null, '1'));
        System.assertEquals(true, MatcherUtils.equals(null, null));
    }
    
    private static testmethod void testIsEmpty() {
    	System.assertEquals(true, MatcherUtils.isEmpty(null));
    	System.assertEquals(true, MatcherUtils.isEmpty(''));
    	System.assertEquals(false, MatcherUtils.isEmpty('Not Empty'));
    }
    
    private static testmethod void testParseDate() {
        System.assertEquals(null, MatcherUtils.parseDate(null));
        System.assertEquals(null, MatcherUtils.parseDate(''));
        System.assertEquals(datetime.newInstance(2004, 4, 26, 23, 24, 40), MatcherUtils.parseDate('2004-04-26 23:24:40 PDT'));
    }
    
    private static testmethod void testParseISODate() {
        System.assertEquals(null, MatcherUtils.parseISODate(null));
        System.assertEquals(null, MatcherUtils.parseISODate(''));
        System.assertEquals(datetime.newInstanceGMT(2004, 4, 26, 23, 24, 40), MatcherUtils.parseISODate('2004-04-26T23:24:40Z'));
        System.assertEquals(datetime.newInstanceGMT(2010, 6, 28, 17, 25, 58), MatcherUtils.parseISODate('2010-06-28T17:25:58Z'));
    }
    
    private static testmethod void testGetFLS() {
        System.assertEquals(false, getFLS('bogus', 'bogus', null));
        
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias='passtest', email='passtest@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid=p.Id,
            timezonesidkey='America/Los_Angeles', username='passtest@testorg.com');
        System.runAs(u) {
            Map<String, Schema.sObjectField> sobjFieldMap = Schema.SObjectType.Account.fields.getMap();
            System.assertEquals(false, getFLS('bogus', 'bogus',  sobjFieldMap));
            System.assertEquals(false, getFLS('Phone', 'bogus',  sobjFieldMap));
            System.assertEquals(true,  getFLS('Phone', 'update', sobjFieldMap));
            System.assertEquals(true,  getFLS('Phone', 'create', sobjFieldMap));
            System.assertEquals(true,  getFLS('Phone', 'access', sobjFieldMap));
            System.assertEquals(false, getFLS('Phone', 'writeRequireMasterRead', sobjFieldMap));
        }
        
        // TODO: Enable if/when Salesforce fixes the bug with implicit caching (case #05320459).
        // If we run both of these tests, the second one fails because the field describes for the
        // previous user are implicitly cached and used below.  
        //p = [select id from profile where name='Read Only'];
        //u = new User(alias='limtest', email='limtest@testorg.com',
        //    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        //    localesidkey='en_US', profileid=p.Id,
        //    timezonesidkey='America/Los_Angeles', username='limtest@testorg.com');
        //System.runAs(u) {
        //    Map<String, Schema.sObjectField> sobjFieldMap = Schema.SObjectType.Account.fields.getMap();
        //    System.assertEquals(false, getFLS('bogus', 'bogus',  sobjFieldMap));
        //    System.assertEquals(false, getFLS('Phone', 'bogus',  sobjFieldMap));
        //   System.assertEquals(false, getFLS('Phone', 'update', sobjFieldMap));
        //    System.assertEquals(false, getFLS('Phone', 'create', sobjFieldMap));
        //    System.assertEquals(true,  getFLS('Phone', 'access', sobjFieldMap));
        //    System.assertEquals(false, getFLS('Phone', 'writeRequireMasterRead', sobjFieldMap));
        //}
    }
    
    private static testmethod void testHTMLENCODE() {
        String encodeOrig = 'Somebody\'s "Company" & Friends<br/>\\u003cbr\\u003e';
        String encodeExpected = 'Somebody&#39;s &quot;Company&quot; &amp; Friends&lt;br&#47;&gt;&#92;&#117;003cbr&#92;&#117;003e';
        System.assertEquals(null, HTMLENCODE(null));
        System.assertEquals('', HTMLENCODE(''));
        System.assertEquals(encodeExpected, HTMLENCODE(encodeOrig));
    }
    
    private static testmethod void testCreateRequest() {
        // Null URL
        HttpRequest req = createRequest(null, null, null, null, null);
        System.assertEquals(null, req);
        
        // NOTE: There is no method to retrieve the timeout.
        
        // Null Parameters
        String expectedEndpoint = 'http://www.salesforce.com';
        String expectedMethod = 'GET';
        Boolean expectedCompressed = false;
        req = createRequest(expectedEndpoint, null, null, expectedCompressed, null);
        System.assertNotEquals(null, req);
        System.assertEquals(expectedMethod, req.getMethod());
        System.assertEquals(expectedCompressed, req.getCompressed());
        
        // Valid
        expectedEndpoint = 'http://www.salesforce.com';
        expectedMethod = 'POST';
        expectedCompressed = true;
        req = createRequest(expectedEndpoint, '<matches/>', expectedMethod, expectedCompressed, 20000);
        System.assertNotEquals(null, req);
        System.assertEquals(expectedMethod, req.getMethod());
        System.assertEquals(expectedCompressed, req.getCompressed());
    }
    
    private static testmethod void testSendRequest() {
        ApexPages.Message[] messages = new List<ApexPages.Message>();
        Integer expectedMessages = 0;
        
        // Null
        System.assertEquals(null, sendRequest(null, messages));
        System.assertEquals(expectedMessages, messages.size());
        
        // Valid
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('http://www.jigsaw.com/');
        System.assertNotEquals(null, sendRequest(req, messages));
        System.assertEquals(expectedMessages, messages.size());
        
        // Simulate error
        expectedMessages++;
        System.assertEquals(null, sendRequest(req, messages, true));
        System.assertEquals(expectedMessages, messages.size());
    }
    
    private static testmethod void testGetProfilePerms() {
        // Read Only profile should not have access to customize application.
        Profile p = [select id from profile where name='Read Only']; 
        System.assertEquals(false, getProfilePerms('PermissionsCustomizeApplication', p.Id));
        
        // System Administrator profile should have access to customize application.
        p = [select id from profile where name='System Administrator']; 
        System.assertEquals(true, getProfilePerms('PermissionsCustomizeApplication', p.Id));
        
        // Invalid profile should not have access to anything.
        p = new Profile();
        System.assertEquals(false, getProfilePerms('PermissionsCustomizeApplication', p.Id));
    }
    
    private static testmethod void testMessageListHasSeverity() {
        List<ApexPages.Message> messages = null;
        System.assertEquals(false, messageListHasSeverity(messages, null));
        
        messages = new List<ApexPages.Message>();
        System.assertEquals(false, messageListHasSeverity(messages, null));
        
        messages = new List<ApexPages.Message>{new ApexPages.Message(ApexPages.Severity.INFO, 'Not an error.')};
        System.assertEquals(false, messageListHasSeverity(messages, ApexPages.Severity.ERROR));
        
        messages = new List<ApexPages.Message>{new ApexPages.Message(ApexPages.Severity.FATAL, 'Fatal error!')};
        System.assertEquals(true, messageListHasSeverity(messages, ApexPages.Severity.ERROR));
        
        messages = new List<ApexPages.Message>{new ApexPages.Message(ApexPages.Severity.ERROR, 'Regular error!')};
        System.assertEquals(true, messageListHasSeverity(messages, ApexPages.Severity.ERROR));
    }
    
    private static testmethod void testNormalizeWebsite() {
        System.assertEquals(null, normalizeWebsite(null));
        System.assertEquals('', normalizeWebsite(''));
        
        String expectedWebsite = 'not a website!';
        System.assertEquals(expectedWebsite, normalizeWebsite('Not a website!'));
        
        expectedWebsite = 'jigsaw.com';
        System.assertEquals(expectedWebsite, normalizeWebsite('JiGSaW.com'));
        
        expectedWebsite = 'www.jigsaw.com';
        System.assertEquals(expectedWebsite, normalizeWebsite('www.jigsaw.com'));
        System.assertEquals(expectedWebsite, normalizeWebsite('http://www.jigsaw.com'));
        System.assertEquals(expectedWebsite, normalizeWebsite('http://www.jigsaw.com:80/rest/searchCompany.xml?name=www.jigsaw.com#anchor'));
        System.assertEquals(expectedWebsite, normalizeWebsite('https://www.jigsaw.com/rest/searchCompany.xml?token=00D50000000IslLEAS&name=http%3A%2F%2F%2Fwww.att.com%3Fparam%3Dvalue'));
        
        // Unknown scheme
        expectedWebsite = 'urn:example:animal:ferret:nose';
        System.assertEquals(expectedWebsite, normalizeWebsite('urn:example:animal:ferret:nose'));
    }
    
    private static testmethod void testFieldToXml() {
        // Null
        System.assertEquals('', fieldToXml(null, null));
        System.assertEquals('', fieldToXml('tagName', null));
        
        // Valid
        MatcherField field = new MatcherField('value', null);
        String expectedXml = '<tagName diff="false">value</tagName>';
        System.assertEquals(expectedXml, fieldToXml('tagName', field));
    }
    
    private static testmethod void testObjectToXml() {
        // Null
        System.assertEquals('', objectToXml(null, null));
        System.assertEquals('', objectToXml('tagName', null));
        
        // Valid
        String expectedXml = '<tagName>value</tagName>';
        System.assertEquals(expectedXml, objectToXml('tagName', 'value'));
    }
    
    private static testMethod void testPhoneEquals() {
        System.assertEquals(true, phoneEquals(null, null));
        System.assertEquals(true, phoneEquals('', ''));
        System.assertEquals(false, phoneEquals(null, ''));
        System.assertEquals(false, phoneEquals('', null));
        System.assertEquals(false, phoneEquals(null, '+1.415.901.7000'));
        System.assertEquals(false, phoneEquals('', '+1.415.901.7000'));
        
        System.assertEquals(true, phoneEquals('+1.415.901.7000', '+1.415.901.7000'));
        System.assertEquals(true, phoneEquals('415.901.7000', '(415) 901-7000'));
        System.assertEquals(true, phoneEquals('+1.415.901.7000', '(415) 901-7000'));
        System.assertEquals(false, phoneEquals('+1.415.901.7000', '+44.415.901.7000'));
    }

}