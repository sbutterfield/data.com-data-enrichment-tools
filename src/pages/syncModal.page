<apex:page id="thePage" controller="SncLeadController" title="Stare and Compare" showHeader="false" sidebar="false">

    <apex:variable var="jsonString" value="{!json}"/>
    
    <!-- jQuery -->
    <apex:includeScript value="{!URLFOR($Resource.jquery_ui, '/js/jquery-1.8.3.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jquery_ui, '/js/jquery.ba-postmessage.min.js')}"/>

    <!-- qTip -->
    <link rel="stylesheet" type="text/css" href="/resource/ddcDet__jQueryQtip/jquery.qtip/jquery.qtip.min.css" />
    <apex:includeScript value="/resource/ddcDet__jQueryQtip/jquery.qtip/jquery.qtip.min.js"/>
    
<!-- Stare-and-Compare panel body styling -->
<style type="text/css">
	table.sncHeader {
	    margin-left:auto;
	    margin-right:auto;
	}
	
	table.sncHeader td {
	    vertical-align:middle;
	}
	
	.columnCell {
	    width:50%;
	    vertical-align:top;
	}
	
	.links {
	    color: #015BA7;
	    font-size: 0.92em;
	    font-weight: normal;
	}
	
	.links .actionLink {
	    color: #015BA7;
	    margin: 2px;
	    text-decoration: none;
	    vertical-align: middle;
	}
	
	.dataEqual {
	    color: #CCCCCC;
	}
	
	.dataEqual a {
	    color: #CCCCCC;
	}
	
	.dataDiff {
	    font-weight:bold;
	}
</style>

<!-- Matches Column -->
<style type="text/css">

    /* Actually above the form */
    div#syncModalWrapperOverlay{
        background-color:   #000000;
        /*display:          none;*/
        height:             444px;
        left:               0px;
        opacity:            0.5;
        position:           absolute;
        top:                0px;
        width:              900px;
        z-index:            100;
    }
    div#syncModalWrapperOverlaySpinnerWrapper{
        background-color:   #ffffff;
        border-radius:      12px;
        /*display:          none;*/
        left:               400px;
        padding:            4px;
        position:           absolute;
        top:                213px;
        z-index:            101;
    }

    /* syncModalWrapper */
    div#syncModalWrapper{
        position:   relative;
    }
    div.SNCFieldColumns{
        float:      left;
        width:      70%;
    }
    div.SNCMatchesColumn{
        border-bottom:  1px solid #dedede;
        float:          right;
        height:         375px;
        overflow-y:     scroll;
        width:          29%;
    }
    div#matchesMessage{
        border:         0px !important;
        font-size:      14px;
    }
    .clearfix:after,
    .group:after{
      content:      "";
      display:      table;
      clear:        both;
    }
    div.qTipPopupContent{
        display:    none;
    }
    
    div#match_list > div {
      border:       1px solid #dedede;
    
    }
    div#match_list ul{
        border-top: 1px solid #dedede;
        margin:     0px;
        padding:    0px;
    }
    div#match_list fieldset {
      border: none;
      padding: 0px;
      margin: 0px;
      display: block;
      width: 100%;
      height: 100%;
      -webkit-padding-before: 0;
      -webkit-padding-start: 0;
      -webkit-padding-after: 0;
      -webkit-padding-end:0;
      -webkit-margin-start: 0;
      -webkit-margin-end: 0;
    }
    div#match_list div.match_container {
        width: 90%;
        height: 100%;
        float: right;
        text-align: left;
    }
    div#match_list div.match_radio {
        width: 8%;
        height: 100%;
        float: left;
        text-align: center;
    }
    div#match_list ul {
        width:              100%;
    }
    div#match_list ul li {
        background-color:   #f3f3f4;
        border-left:        1px solid #dedede;
        border-top:         1px solid #dedede;
        border-bottom:      1px solid #ffffff;
        list-style:         none;
        margin:             0;
        position:           relative;
        padding:            14px 8px;
    }
    div#match_list ul li.selected {
      color:            #333;
      left:             0px;
      background-color: #ffffff;
      border-top:       1px solid #dedede;
      border-bottom:    1px solid #ffffff;  
      border-left:      1px solid #ffffff; 
      border-right:     none;
    }
    div#match_list ul li.selected .mq_container,
    div#match_list ul li.selected .mm_summary {
      position: relative;
      left: 3px !important;
    }
    div#match_list ul li.selected.is_matched:after {
      right:            9px;
    }
    div#match_list label:hover {
        cursor:         pointer;
    }
    div#match_list ul li:hover {
        cursor:         pointer;
    }
    div#match_list ul li:hover p {

    }
    div#match_list ul li .mq_container {
      width: 50px;
      height: 6px;
      background-color: #D9DBD9;
      border: 1px solid #C7C7C7;
      -moz-border-radius: 2px;
      -webkit-border-radius: 2px;
      border-radius: 2px;
      float: left;
    }
    div#match_list ul li .mq_container div {
      -moz-border-radius: 0;
      -webkit-border-radius: 0;
      border-radius: 0;
      background-color: #AFAFAF;
      border: none;
      width: 50%;
      height: 100%;
    }
    
    div#match_list .match_grade {
      color: #AFAFAF;
      font-size: 11px;
      display: none;
      float: left;
      clear: left;
      width: 50%;
      margin-top: 4px;
    }
    
    div#match_list .mq_left{
      display: block;
      float: left;
      clear: left;
      height: 100%;
      margin-top: 3px;
    }
    div#match_list ul li .mm_summary {
      float: right;
      width: 100%;
      position: relative;
      top: -2px;
    }
    div#match_list ul li .mm_summary p {
      font-size: 0.92em;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      margin: 0;
    }
    div#match_list ul li .mm_summary p:first-of-type {
      font-weight: bold;
    }
</style>

<script type="text/javascript">
var $j = jQuery.noConflict();

var parentDomain = "{!$CurrentPage.parameters.parentDomain}";

$j(document).ready(function() {
    
    // Cancel button
    $j("input[value=Cancel]").click(function(event) {
		event.preventDefault();
  
		// Reveal the overlay
		$j('body').html('');
		//$j('div#syncModalWrapperOverlay').show();
		//$j('div#syncModalWrapperOverlaySpinnerWrapper').show();    
   
		top.location.href = parentDomain + '/' + getURLParameter('id');
		//closeModalDialog();
    });
});

function closeModalDialog() {
	console.log('closeModalDialog');
	
	/*
    var crossResult = new Object();
    crossResult.action = "close_modal_dialog";
    
    $j.postMessage(
        crossResult,
        parentDomain,
        parent
    );
    */
}

function closeModalDialogAndRefresh() {
	console.log('closeModalDialogAndRefresh');
	
	// Redirects back to the lead record.
	top.location.href = parentDomain + '/' + getURLParameter('id');
	
	/*
    var crossResult = new Object();
    crossResult.action = "close_modal_dialog_refresh";
    
    $j.postMessage(
        crossResult,
        parentDomain,
        parent
    );
    */
}
</script>

<!-- Note: Must be inline for access to global VisualForce variables. -->
<script type="text/javascript">
function setAllCheckboxStates(state) {
    var form = document.getElementById('{!$Component.stareAndCompareForm}');
    var len = form.elements.length;
    for (var i = 0; i < len; i++) {
        var el = form.elements[i];
        if (el.type != 'checkbox') {
            continue;
        }
        
        el.checked = state;
    }
}

function cleanSelectAll() {
    setAllCheckboxStates(true);
}

function cleanClearAll() {
    setAllCheckboxStates(false);
}
</script>

<!-- Matches Column -->
<script type="text/javascript">
	
	// Access the URL var.
	function getURLParameter(name) {
	    return decodeURI(
	        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
	    );
	}

    $j(document).ready(function(){
    
        // Hide the overlay once the page has loaded.
        setTimeout(
            hide_overlay
            ,500
        );
    
        // A placeholder array of objects to populate the matches list.
        //var SNCMatches = $j(JSON.parse('{!jsonString}'));
        //console.log(SNCMatches);

        // A placeholder array of objects to populate the matches list.
        var SNCMatches = {!jsonString};

        // A field mapping array to connect field names to SFDC field Ids.
        /* We call out to the server instead to update the SNCBODY
        var SNCFieldMappings = new Array();
        SNCFieldMappings['Company'] = 'j_id58_16';
        SNCFieldMappings['Title'] = 'j_id58_24';
        */
        
        // Indicate how many matches were found.
        var matchesMessage;
        if(SNCMatches.length == 1){
            matchesMessage = '1 match was found';
        }else{
            matchesMessage = SNCMatches.length + ' matches were found';
        }
        $j('div#matchesMessage').html(matchesMessage);
        
        // Pull the script block template into a variable.
        var matchTemplate = $j('#matchTemplate').html();
        
        // Loop through each match object and add it to the page.
        for(var i=0; i<SNCMatches.length; i++){
            
            // Customize the template
            var matchRow = matchTemplate;
            matchRow = matchRow.replace('{itemCount}',(i+1));
            matchRow = matchRow.replace('{FullName}',SNCMatches[i].FullName);
            matchRow = matchRow.replace('{Company}',SNCMatches[i].Company);
            matchRow = matchRow.replace('{ddcId}',SNCMatches[i].ddcId);
            matchRow = matchRow.replace('{Title}',SNCMatches[i].Title);
            matchRow = matchRow.replace('{Website}',SNCMatches[i].Website);
            matchRow = matchRow.replace('{matchPercentage}',(SNCMatches[i].matchPercentage / 2)); // Notice halving the percentage to support the CSS width.
            matchRow = matchRow.replace('{matchCode}',SNCMatches[i].matchCode);
            matchRow = matchRow.replace('{matchDetail}',SNCMatches[i].matchDetail);
            
            console.log('ddcId selector:  ' +$j('[value$="ddcId"]').val());
            console.log('ddcId of i: ' +SNCMatches[i].ddcId);
            
            // If the ddcId isn't defined then set it to the first one.
            if($j('[id$="ddcId"]').val() == ''){
            	$j('[id$="ddcId"]').val(SNCMatches[i].ddcId);
            }
            
            // Auto-highlight the current contact.
            if($j('[id$="ddcId"]').val() == SNCMatches[i].ddcId){
                matchRow = matchRow.replace('{selected}','selected');
                matchRow = matchRow.replace('{checked}','checked="checked"');
            }else{
                matchRow = matchRow.replace('{selected}','');
                matchRow = matchRow.replace('{checked}','');
            }
            
            // Auto-generate a unique element ID.
            var input_id = 'SNCMatch' + i;
            matchRow = matchRow.replace(/{input_id}/g,input_id); // replace all occurrences
            
            // Output the new row
            $j('div#match_list ul').append(matchRow);
        }
        
        // Select a match to compare.
        $j('div#match_list input[type=radio]').change(function(){
        
            // Get the selected item's SFDC Id.
            var selectedcontactId = $j(this).attr('value');
        
            // Clear the previous selection.
            $j('div#match_list li').removeClass('selected');
            
            // Highlight the current item.
            $j(this).closest('li').addClass('selected');
            
            // Match the contactId to the whole object.
            var SNCMatchIndex;
            for(var i=0; i<SNCMatches.length; i++){
            
                // Make the match, set the index, and break out of the loop.
                if(selectedcontactId == SNCMatches[i].ddcId){
                    SNCMatchIndex = i;
                    break;
                }
            }
            
            // Reveal the overlay
            $j('div#syncModalWrapperOverlay').fadeIn(1500);
            $j('div#syncModalWrapperOverlaySpinnerWrapper').fadeIn(2500);
            
            // Update and submit the form.
            $j('[id$="ddcId"]').val(SNCMatches[SNCMatchIndex].ddcId);
            //$j('[id$="stareAndCompareForm"]').submit();
            document.location.href = '/apex/syncModal?id=' + getURLParameter('id') + '&contactId=' + SNCMatches[SNCMatchIndex].ddcId;
            
            /*
            setTimeout(
                hide_overlay
                ,5000
            );
            */
            
            // Update the form inline or perform an AJAX call to load all data for a record.
            /* We call out to the server instead.
            $j('[id$="' + SNCFieldMappings['Company'] + '"]').html(SNCMatches[SNCMatchIndex].Company);
            $j('[id$="' + SNCFieldMappings['Title'] + '"]').html(SNCMatches[SNCMatchIndex].Title);
            */
        });
        
        // Enable qTip popups
        $j('div.qTipPopup').each(function(){
            $j(this).qtip({
                content:{
                    text: $j('#' + $j(this).attr('data-content'))
                },position:{
                    my: 'left center',
                    at: 'right center',
                    target: $j(this)
                },style:{
                    classes: 'qtip-rounded qtip-shadow qtip-yellow'
                }
            });
        });
    });
    
    // The overlay is shown both when the modal loads and when a contact is selected.
    function hide_overlay(){
        $j('div#syncModalWrapperOverlaySpinnerWrapper').hide();
        $j('div#syncModalWrapperOverlay').fadeOut(500);
    }
</script>

<apex:outputPanel rendered="{!refreshParent}">
    <script type="text/javascript">
        closeModalDialogAndRefresh();
    </script>
</apex:outputPanel>

<apex:pageMessages showDetail="true"/>

<div id="syncModalWrapperOverlaySpinnerWrapper"><img border="0" id="syncModalWrapperOverlaySpinner" src="{!$Resource.spinner}" /></div>
<div class="group" id="syncModalWrapperOverlay"></div>

<apex:form id="stareAndCompareForm">
    <apex:inputHidden value="{!ddcid}" id="ddcId"/>

	<apex:pageBlock mode="maindetail">
    <apex:outputPanel layout="block" style="text-align:center; padding: 5px 0;">
        <apex:commandButton action="{!doUpdate}" value="{!IF(InSync, 'Records are in sync', 'Update')}" rendered="{!NOT(NotFound || HasError)}"/>
        <apex:commandButton action="{!cancel}" value="Cancel" rendered="{!NOT(InSync) || NotFound || HasError}"/>
    </apex:outputPanel>
    
    <apex:outputPanel layout="block">
        
        <!-- Appends the match selection list as a third column -->
       <div class="group" id="syncModalWrapper">
        
            <!-- Header -->
            <div class="SNCFieldColumns">
                <apex:panelGrid columns="3" columnClasses="columnCell,columnCell" style="width:100%; height:100px;">
                    <apex:outputPanel >
                        <table class="sncHeader">
                            <tr>
                                <td><apex:image title="Salesforce" styleClass="pageTitleIcon" alt="Salesforce" url="/s.gif" style="background-position:0 -950px;"/></td>
                                <td><h1 class="headerTitle">Salesforce</h1></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><apex:outputField value="{!CRM.LastModifiedDate}"/></td>
                            </tr>
                        </table>
                    </apex:outputPanel>
                    <apex:outputPanel >
                        <table class="sncHeader">
                            <tr>
                                <td><apex:image title="Data.com" styleClass="pageTitleIcon" alt="Data.com" url="/img/icon/datadotcom32.png"/></td>
                                <td><h1 class="headerTitle">Data.com</h1></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><apex:outputField value="{!DDC.LastModifiedDate}"/></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><apex:outputPanel layout="block" styleClass="links" rendered="{!NOT(NotFound || InSync || HasError)}"><a id="cleanSelectAllLink" class="actionLink" href="javascript:void(0);" onclick="cleanSelectAll();">Select All</a>|<a id="cleanClearAllLink" class="actionLink" href="javascript:void(0);" onclick="cleanClearAll();">Clear All</a></apex:outputPanel></td>
                            </tr>
                        </table>
                    </apex:outputPanel>
                </apex:panelGrid>
            </div>
            
            <!-- Matches Column -->
            <div class="SNCMatchesColumn">
                    
                <div id="match_list">
                
                    <div id="matchesMessage"></div>
                
                    <ul>
                        <script id="matchTemplate" type="text/html">
                            <li class="{selected}">
                                <div class="clearfix">
                                
                                    <fieldset>                  
                                        <div class="match_radio">
                                            <input type="radio" name="match" id="{input_id}" value="{ddcId}" {checked} />
                                        </div>
                            
                                        <label for="{input_id}">
                                            <div class="match_container">
                                                <div class="mm_summary">
                                                    <p>#{itemCount} {FullName}</p>
                                                    <p>{Title}</p>
                                                    <p>{Company}</p>
                                                    <p>{Website}</p>
                                                </div>
                                        
                                                <div class="mq_left">
                                                    <div class="mq_container" style="">
                                                        <div class="qTipPopup" data-content="qTipPopupContent_{input_id}" style="width:{matchPercentage}px;"></div>
                                                    </div>
                                                    <div class="qTipPopupContent" id="qTipPopupContent_{input_id}">
                                                        <strong>Match Confidence:</strong> {matchCode}
                                                        <br><strong>Match Detail:</strong> {matchDetail}
                                                    </div>
                                                </div>                  
                                            </div>
                                        </label>
                                    </fieldset>
                                </div>
                            </li>
                        </script>
                    
                    </ul>
                
                </div>
            </div>
            
            <!-- Body -->
            <apex:outputPanel layout="block">
                
                <!-- SNC Columns -->
                <div class="SNCFieldColumns">
                    <apex:dynamicComponent componentValue="{!SNCBody}" />
                </div>
            </apex:outputPanel>
        </div>
        
    </apex:outputPanel>
    
    <apex:outputPanel layout="block" style="text-align:center; padding: 5px 0;">
        <apex:commandButton action="{!doUpdate}" value="{!IF(InSync, 'Records are in sync', 'Update')}" rendered="{!NOT(NotFound || HasError)}"/>
        <apex:commandButton action="{!cancel}" value="Cancel" rendered="{!NOT(InSync) || NotFound || HasError}"/>
    </apex:outputPanel>
</apex:pageBlock>
</apex:form>

</apex:page>